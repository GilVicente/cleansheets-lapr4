/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch;

import com.google.common.io.Files;
import java.io.File;
import javax.swing.table.DefaultTableModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bie
 */
public class WorkBookCacheTest {
    
    public WorkBookCacheTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setPreview method, of class WorkBookCache.
     */
    @Test
    public void testSetGetPreview() {
        System.out.println("setgetPreview");
        DefaultTableModel preview = new DefaultTableModel(1, 1);
        WorkBookCache instance = new WorkBookCache(new File("test"), preview);
        instance.setPreview(preview);
        assertEquals(1, preview.getColumnCount());
    }

    /**
     * Test of getFile method, of class WorkBookCache.
     */
    @Test
    public void testSetGetFile() {
        System.out.println("setgetFile");
        WorkBookCache instance = new WorkBookCache(new File("test"), new DefaultTableModel());
        File expResult = new File("test");
        File result = instance.getFile();
        assertEquals(expResult.getAbsolutePath(), result.getAbsolutePath());
    }
    
}
