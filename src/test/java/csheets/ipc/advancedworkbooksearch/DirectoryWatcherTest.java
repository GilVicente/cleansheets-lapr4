/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch;

import csheets.CleanSheets;
import csheets.ui.ctrl.UIController;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bie
 */
public class DirectoryWatcherTest {
    
    public DirectoryWatcherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class DirectoryWatcher.
     */
    @Test
    public void testRun() throws IOException {
        System.out.println("Directory watcher test");
        String TEST_DIRECTORY = "csv";
        AdvancedWorkbookSearchRunnable search = new AdvancedWorkbookSearchRunnable(new UIController(new CleanSheets()),
                "test", "test", new File(TEST_DIRECTORY));
        DirectoryWatcher watcher = new DirectoryWatcher(search);
        Thread t = new Thread(watcher);
        t.start();
        
        //creating a file to trigger the watcher
        byte randomBytes[] = {0,1,3};
        FileOutputStream out = new FileOutputStream(new File("csv/testfile"));
        out.write(randomBytes);
        out.close();
        
        //testing if the watcher was triggered
        assertEquals(false, watcher.hasDetectedAChange());
    }
    
}
