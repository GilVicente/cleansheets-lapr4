/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch;

import java.io.File;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bie
 */
public class WorkbookCacheRegistryTest {
    
    public WorkbookCacheRegistryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findCachedPreview method, of class WorkbookCacheRegistry.
     */
    @Test
    public void testFindCachedPreview() {
        System.out.println("findCachedPreview");
        WorkbookCacheRegistry reg = new WorkbookCacheRegistry();
        File file = new File("testFile");
        WorkbookCacheRegistry.addCachedPreview(file, new DefaultTableModel(1, 1));
        
        WorkBookCache expResult = new WorkBookCache(file, new DefaultTableModel(1,1));
        WorkBookCache result = WorkbookCacheRegistry.findCachedPreview(file);
        assertEquals(expResult.getFile().getPath(), result.getFile().getPath());
    }
}
