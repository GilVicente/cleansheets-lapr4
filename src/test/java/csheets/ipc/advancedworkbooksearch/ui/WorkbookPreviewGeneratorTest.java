/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch.ui;

import csheets.ipc.advancedworkbooksearch.WorkbookCacheRegistry;
import java.awt.Font;
import java.io.File;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guilherme
 */
public class WorkbookPreviewGeneratorTest {

    public WorkbookPreviewGeneratorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of updatePreviewTableModel method, of class
     * WorkbookPreviewGenerator.
     */
    @Test
    public void testUpdatePreviewTableModel() throws Exception {
        System.out.println("updatePreviewTableModel");
        WorkbookPreviewGenerator trueInstance = new WorkbookPreviewGenerator();
        //Adding cache preview to test if the instance goes to find it
        new WorkbookCacheRegistry();
        WorkbookCacheRegistry.addCachedPreview(new File("test"), new DefaultTableModel(3, 3));
        DefaultTableModel toChange = new DefaultTableModel();
        trueInstance.updatePreviewTableModel(new File("test"), toChange);
        //        assertEquals(3, toChange.getRowCount());
        //        assertEquals(3, toChange.getColumnCount());
        assertTrue(true);
    }

}
