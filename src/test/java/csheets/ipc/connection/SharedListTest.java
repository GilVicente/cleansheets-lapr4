/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.connection;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class SharedListTest {
    
    public SharedListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addShared method, of class SharedList.
     */
    @Test
    public void testAddShared() {
        System.out.println("addShared");
        Shared shared = new Shared();
        List<Shared> list = new ArrayList<>();
        list.add(shared);
        
        boolean result = true;
        boolean expResult = false;
        if(list.contains(shared)) {
            expResult = true;
        }
        assertEquals(result, expResult);
    }

    /**
     * Test of geList method, of class SharedList.
     */
    @Test
    public void testGeList() {
        System.out.println("geList");
        SharedList instance = new SharedList();
        Shared shared = new Shared();
        List<Shared> expResult = new ArrayList<>();
        List<Shared> result = instance.geList();
        
        expResult.add(shared);
        result.add(shared);
        assertEquals(result, expResult);
        
    }
    
}
