/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share.ui;

import csheets.CleanSheets;
import csheets.ui.ctrl.UIController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Lacerda
 */
public class StartSharingControllerIT {
    
    public StartSharingControllerIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        CleanSheets cs = new CleanSheets();
       UIController controller= new UIController(cs);
        ActiveInstancesPanel a = new ActiveInstancesPanel(controller);
        StartSharingController instance = new StartSharingController(controller,a);
        assertNotNull(instance);
    }
    
}
