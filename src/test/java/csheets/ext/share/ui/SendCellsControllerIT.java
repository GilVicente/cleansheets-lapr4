/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share.ui;

import csheets.CleanSheets;
import csheets.ui.ctrl.UIController;
import javax.swing.JPanel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Lacerda
 */
public class SendCellsControllerIT {

    public SendCellsControllerIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {

        CleanSheets cs = new CleanSheets();
        UIController controller = new UIController(cs);
        JPanel p = new JPanel();
        SendCellsController instance = new SendCellsController(controller, p);
        assertNotNull(instance);
        
    }

}
