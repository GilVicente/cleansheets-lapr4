/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.conditionalformatting;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ext.ExtensionManager;
import csheets.ext.ToContinue;
import static csheets.ext.ToContinue.isToBeSelected;
import csheets.ext.conditionalformatting.ui.ConditionalFormattingController;
import csheets.ext.conditionalformatting.ui.ConditionalStyle;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import java.awt.Color;
import java.awt.event.ActionEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author José Santos (1140921)
 */
public class ConditionalFormattingRangesTest {

    public ConditionalFormattingRangesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of calculateRange method, of class ConditionalFormattingRanges. Test
     * on a given range if it is true or false //
     * By default cell TRUE is GREEN and FALSE is BLUE
     */
    @Test
    public void testCalculateRange() throws Exception {
        System.out.println("calculateRange");

        ConditionalFormattingRanges instance = new ConditionalFormattingRanges();
        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        isToBeSelected = true;
        ExtensionManager.getInstance();

        Cell selectedCells[][] = new Cell[2][2];

        Cell cell0 = ss.getCell(0, 0);
        cell0.setContent("12");
        Cell cell1 = ss.getCell(0, 1);
        cell1.setContent("3");
        Cell cell2 = ss.getCell(0, 1);
        cell2.setContent("10");
        Cell cell3 = ss.getCell(0, 1);
        cell3.setContent("10");
        selectedCells[0][0] = cell0;
        selectedCells[0][1] = cell1;
        selectedCells[1][0] = cell2;
        selectedCells[1][1] = cell3;
        String operator = ">";
        instance.setSelectedCells(selectedCells);
        instance.setTree("(_cell > 10)");
        instance.setOperator(operator);

        StylableCell c = (StylableCell) selectedCells[0][0].getExtension(StyleExtension.NAME);
        StylableCell c1 = (StylableCell) selectedCells[0][1].getExtension(StyleExtension.NAME);
        instance.calculateRange();
        assertTrue(c.getBackgroundColor().equals(Color.green));//cell contains 12 since 12>10 color should be green
        assertTrue(c1.getBackgroundColor().equals(Color.blue));//cell contains 12 since 3>10 color should be blue

    }

    public void testCalculateRangeOperatorLess() throws Exception {
        System.out.println("calculateRange Operator <");

        ConditionalFormattingRanges instance = new ConditionalFormattingRanges();
        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        isToBeSelected = true;
        ExtensionManager.getInstance();

        Cell selectedCells[][] = new Cell[2][2];

        Cell cell0 = ss.getCell(0, 0);
        cell0.setContent("12");
        Cell cell1 = ss.getCell(0, 1);
        cell1.setContent("3");
        Cell cell2 = ss.getCell(0, 1);
        cell2.setContent("10");
        Cell cell3 = ss.getCell(0, 1);
        cell3.setContent("10");
        selectedCells[0][0] = cell0;
        selectedCells[0][1] = cell1;
        selectedCells[1][0] = cell2;
        selectedCells[1][1] = cell3;
        String operator = "<";
        instance.setSelectedCells(selectedCells);
        instance.setTree("(_cell < 10)");
        instance.setOperator(operator);

        StylableCell c = (StylableCell) selectedCells[0][0].getExtension(StyleExtension.NAME);
        StylableCell c1 = (StylableCell) selectedCells[0][1].getExtension(StyleExtension.NAME);
        instance.calculateRange();
        assertFalse(c.getBackgroundColor().equals(Color.green));//cell contains 12 since 12<10 color should be blue
        assertFalse(c1.getBackgroundColor().equals(Color.blue));//cell contains 3 since 3<12 color should be green

    }

    public void testCalculateRangeOperatorEqual() throws Exception {
        System.out.println("calculateRange Operator =");

        ConditionalFormattingRanges instance = new ConditionalFormattingRanges();
        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        isToBeSelected = true;
        ExtensionManager.getInstance();

        Cell selectedCells[][] = new Cell[2][2];

        Cell cell0 = ss.getCell(0, 0);
        cell0.setContent("12");
        Cell cell1 = ss.getCell(0, 1);
        cell1.setContent("3");
        Cell cell2 = ss.getCell(1, 0);
        cell2.setContent("10");
        Cell cell3 = ss.getCell(1, 1);
        cell3.setContent("10");
        selectedCells[0][0] = cell0;
        selectedCells[0][1] = cell1;
        selectedCells[1][0] = cell2;
        selectedCells[1][1] = cell3;
        String operator = "=";
        instance.setSelectedCells(selectedCells);
        instance.setTree("(_cell = 3)");
        instance.setOperator(operator);

        StylableCell c = (StylableCell) selectedCells[0][0].getExtension(StyleExtension.NAME);
        StylableCell c1 = (StylableCell) selectedCells[0][1].getExtension(StyleExtension.NAME);
        instance.calculateRange();
        assertFalse(c.getBackgroundColor().equals(Color.green));//cell contains 12 since 12=3 color should be blue
        assertTrue(c1.getBackgroundColor().equals(Color.blue));//cell contains 3 since 3=3 color should be green

    }
    public void testCalculateRangeOperatorGreaterEqual() throws Exception {
        System.out.println("calculateRange Operator >=");

        ConditionalFormattingRanges instance = new ConditionalFormattingRanges();
        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        isToBeSelected = true;
        ExtensionManager.getInstance();

        Cell selectedCells[][] = new Cell[2][2];

        Cell cell0 = ss.getCell(0, 0);
        cell0.setContent("12");
        Cell cell1 = ss.getCell(0, 1);
        cell1.setContent("3");
        Cell cell2 = ss.getCell(1, 0);
        cell2.setContent("10");
        Cell cell3 = ss.getCell(1, 1);
        cell3.setContent("10");
        selectedCells[0][0] = cell0;
        selectedCells[0][1] = cell1;
        selectedCells[1][0] = cell2;
        selectedCells[1][1] = cell3;
        String operator = "=";
        instance.setSelectedCells(selectedCells);
        instance.setTree("(_cell >= 3)");
        instance.setOperator(operator);

        StylableCell c = (StylableCell) selectedCells[0][0].getExtension(StyleExtension.NAME);
        StylableCell c1 = (StylableCell) selectedCells[0][1].getExtension(StyleExtension.NAME);
        instance.calculateRange();
        assertTrue(c.getBackgroundColor().equals(Color.green));//cell contains 12 since 12>=3 color should be blue
        assertTrue(c1.getBackgroundColor().equals(Color.blue));//cell contains 3 since 3>=3 color should be green

    }
        public void testCalculateRangeOperatorLessEqual() throws Exception {
        System.out.println("calculateRange Operator <=");

        ConditionalFormattingRanges instance = new ConditionalFormattingRanges();
        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        isToBeSelected = true;
        ExtensionManager.getInstance();

        Cell selectedCells[][] = new Cell[2][2];

        Cell cell0 = ss.getCell(0, 0);
        cell0.setContent("12");
        Cell cell1 = ss.getCell(0, 1);
        cell1.setContent("3");
        Cell cell2 = ss.getCell(0, 1);
        cell2.setContent("10");
        Cell cell3 = ss.getCell(0, 1);
        cell3.setContent("10");
        selectedCells[0][0] = cell0;
        selectedCells[0][1] = cell1;
        selectedCells[1][0] = cell2;
        selectedCells[1][1] = cell3;
        String operator = "<=";
        instance.setSelectedCells(selectedCells);
        instance.setTree("(_cell <= 10)");
        instance.setOperator(operator);

        StylableCell c = (StylableCell) selectedCells[0][0].getExtension(StyleExtension.NAME);
        StylableCell c1 = (StylableCell) selectedCells[0][1].getExtension(StyleExtension.NAME);
        instance.calculateRange();
        assertFalse(c.getBackgroundColor().equals(Color.green));//cell contains 12 since 12<=10 color shouldn't be blue
        assertFalse(c1.getBackgroundColor().equals(Color.blue));//cell contains 3 since 3<=12 color shoulddn't be green

    }
        public void testCalculateRangeOperatorNotEqual() throws Exception {
        System.out.println("calculateRange Operator !=");

        ConditionalFormattingRanges instance = new ConditionalFormattingRanges();
        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        isToBeSelected = true;
        ExtensionManager.getInstance();

        Cell selectedCells[][] = new Cell[2][2];

        Cell cell0 = ss.getCell(0, 0);
        cell0.setContent("12");
        Cell cell1 = ss.getCell(0, 1);
        cell1.setContent("3");
        Cell cell2 = ss.getCell(0, 1);
        cell2.setContent("10");
        Cell cell3 = ss.getCell(0, 1);
        cell3.setContent("10");
        selectedCells[0][0] = cell0;
        selectedCells[0][1] = cell1;
        selectedCells[1][0] = cell2;
        selectedCells[1][1] = cell3;
        String operator = "!=";
        instance.setSelectedCells(selectedCells);
        instance.setTree("(_cell != 12)");
        instance.setOperator(operator);

        StylableCell c = (StylableCell) selectedCells[0][0].getExtension(StyleExtension.NAME);
        StylableCell c1 = (StylableCell) selectedCells[0][1].getExtension(StyleExtension.NAME);
        instance.calculateRange();
        assertTrue(c.getBackgroundColor().equals(Color.blue));//cell contains 12 since 12!=12 color should be green
        assertFalse(c1.getBackgroundColor().equals(Color.blue));//cell contains 3 since 3!=12 color shouldd be blue

    }

}
