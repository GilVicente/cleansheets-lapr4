/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tixa
 */
public class FileLinkTest {
    
    public FileLinkTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   /**
	 * Test of getSheet method, of class FileLink.
	 */
	@Test
	public void testGetSheet() {
		System.out.println("getSheet");
		Workbook wb = new Workbook(1);
		Spreadsheet s = wb.getSpreadsheet(0);
		FileLink instance = new FileLink(s, null, false, "-", "");
		Spreadsheet expResult = s;
		Spreadsheet result = instance.getSheet();
		assertEquals(expResult, result);;
	}

	/**
	 * Test of getFile method, of class FileLink.
	 */
	@Test
	public void testGetFile() {
		System.out.println("getFile");
		File file = new File("src-tests/csheets/ext/textFile/teste.txt");
		FileLink instance = new FileLink(null, file, false, "-", "");
		File expResult = file;
		File result = instance.getFile();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getHeader method, of class FileLink.
	 */
	@Test
	public void testGetHeader() {
		System.out.println("getHeader");
		FileLink instance = new FileLink(null, null, false, "-", "");
		boolean expResult = false;
		boolean result = instance.getHeaderOption();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getSeparator method, of class FileLink.
	 */
	@Test
	public void testGetSeparator() {
		System.out.println("getSeparator");
		FileLink instance = new FileLink(null, null, false, ";", "");
		String expResult = ";";
		String result = instance.getSeparator();
		assertEquals(expResult, result);
	}
}
