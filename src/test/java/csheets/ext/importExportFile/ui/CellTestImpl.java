/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.CellListener;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.formula.Formula;
import csheets.core.formula.compiler.FormulaCompilationException;
import java.util.SortedSet;


/**
 * Dummy class that implements Cell, in order to create Cell instances, for test purposes.
 * @author JOSENUNO
 */
public class CellTestImpl implements Cell {
    private Address address;
    private String content;
    
    
    public CellTestImpl(Address addr, String content){
        this.address = addr;
        this.content = content;
    }
    
    
    @Override
    public Spreadsheet getSpreadsheet() {
        return null;
    }

    @Override
    public Address getAddress() {
        return this.address;
    }

    @Override
    public Value getValue() {
        return null;
    }

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public Formula getFormula() {
        return null;
    }

    @Override
    public void setContent(String content) throws FormulaCompilationException {
        this.content = content;
    }

    @Override
    public void clear() {
        
    }

    @Override
    public SortedSet<Cell> getPrecedents() {
        return null;
    }

    @Override
    public SortedSet<Cell> getDependents() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void copyFrom(Cell source) {
        
    }

    @Override
    public void moveFrom(Cell source) {
        
    }

    @Override
    public void addCellListener(CellListener listener) {
        
    }

    @Override
    public void removeCellListener(CellListener listener) {
        
    }

    @Override
    public CellListener[] getCellListeners() {
        return null;
    }

    @Override
    public int compareTo(Cell o) {
        return 0;
    }

    @Override
    public Cell getExtension(String name) {
        return null;
    }
    
}
