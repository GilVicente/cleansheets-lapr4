/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;

import csheets.CleanSheets;
import csheets.core.Address;
import csheets.core.Cell;
import csheets.ui.ctrl.UIController;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JOSENUNO
 */
public class ImportFileControllerTest {
    
    public ImportFileControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isCharacterValid method, of class ImportFileController.
     */
    @Test
    public void testIsCharacterValid() {
        System.out.println("isCharacterValid");
        String s = "8";
        ExportFileController instance = new ExportFileController(null);
        boolean result = instance.isCharacterValid(s);
        assertEquals(false, result);
        s = "as";
        result = instance.isCharacterValid(s);
        assertEquals(false, result);
        s = "C";
        result = instance.isCharacterValid(s);
        assertEquals(false, result);
        s = ";";
        result = instance.isCharacterValid(s);
        assertEquals(true, result);
    }
    
}
