/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.Value;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JOSENUNO
 */
public class ExportFileControllerTest {

    public ExportFileControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of exportFile method, of class ExportFileController.
     */
    @Test
    public void testExportFile() throws Exception {
        System.out.println("exportFile");
        File file = new File("teste.txt");
        Cell[][] selectedCells = new Cell[2][2];
        selectedCells[0][0] = new CellTestImpl(new Address(2, 1), "5");
        selectedCells[0][1] = new CellTestImpl(new Address(3, 1), "8");
        selectedCells[1][0] = new CellTestImpl(new Address(2, 2), "A");
        selectedCells[1][1] = new CellTestImpl(new Address(3, 2), "4");
        boolean header = true;
        char separator = ';';
        ExportFileController instance = new ExportFileController(null);
        instance.exportFile(file, selectedCells, header, separator);

        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);

        String s = br.readLine();
        
        s = s.replace(" ", "");
        s = s.replace("\t", "");
        String[] spl = s.split(";");
        
        assertEquals(true, spl.length == 2);

        assertEquals("Columns:2", spl[0]);
        assertEquals("3", spl[1]);
        
        br.readLine();
        s = br.readLine();
        
        s = s.replace(" ", "");
        s = s.replace("\t", "");
        spl = s.split(";");

        assertEquals(true, spl.length == 2);

        assertEquals("5", spl[0]);
        assertEquals("8", spl[1]);
        
        s = br.readLine();
        
        s = s.replace(" ", "");
        s = s.replace("\t", "");
        spl = s.split(";");

        assertEquals(true, spl.length == 2);

        assertEquals("A", spl[0]);
        assertEquals("4", spl[1]);
    }

    /**
     * Test of isCharacterValid method, of class ExportFileController.
     */
    @Test
    public void testIsCharacterValid() {
        System.out.println("isCharacterValid");
        String s = "8";
        ExportFileController instance = new ExportFileController(null);
        boolean result = instance.isCharacterValid(s);
        assertEquals(false, result);
        s = "as";
        result = instance.isCharacterValid(s);
        assertEquals(false, result);
        s = "C";
        result = instance.isCharacterValid(s);
        assertEquals(false, result);
        s = ";";
        result = instance.isCharacterValid(s);
        assertEquals(true, result);
    }

}
