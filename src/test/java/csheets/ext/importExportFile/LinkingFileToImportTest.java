/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tixa
 */
public class LinkingFileToImportTest {
    
    public LinkingFileToImportTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
	 * Test of addFile method, of class LinkingFileToImport.
	 */
	@Test
	public void testAddFile() {
		System.out.println("addFile");
		FileLink fl = new FileLink(null, null, true, "-", "");
		Thread t = new Thread();
		int fileInicialResult = LinkingFileToImport.fileImport.size();
		int threadInicialResult = LinkingFileToImport.ThreadImport.size();
		LinkingFileToImport.addFile(fl, t);
		int fileResult = LinkingFileToImport.fileImport.size();
		int threadResult = LinkingFileToImport.ThreadImport.size();
		LinkingFileToImport.fileImport.clear();
		LinkingFileToImport.ThreadImport.clear();
		assertTrue("", fileInicialResult < fileResult);
		assertTrue("", threadInicialResult < threadResult);
	}

	/**
	 * Test of removeFile method, of class LinkingFileToImport.
	 */
	@Test
	public void testRemoveFile() {
		System.out.println("removeFile");
		Workbook wb = new Workbook(2);
		Spreadsheet sheet = wb.getSpreadsheet(0);
		FileLink fl = new FileLink(sheet, null, true, ";", "");
		Thread t = new Thread();
		LinkingFileToImport.addFile(fl, t);
		int fileInicialResult = LinkingFileToImport.fileImport.size();
		int threadInicialResult = LinkingFileToImport.ThreadImport.size();
		LinkingFileToImport.removeFile(sheet);
		int fileResult = LinkingFileToImport.fileImport.size();
		int threadResult = LinkingFileToImport.ThreadImport.size();
		LinkingFileToImport.fileImport.clear();
		LinkingFileToImport.ThreadImport.clear();
		assertTrue("", fileInicialResult > fileResult);
		assertTrue("", threadInicialResult > threadResult);
	}

	/**
	 * Test of searchFile method, of class LinkingFileToImport.
	 */
	@Test
	public void testSearchFile() {
		System.out.println("searchFile");
		Workbook wb = new Workbook(1);
		Spreadsheet sheet = wb.getSpreadsheet(0);
		FileLink fl = new FileLink(sheet, null, true, ";", "");
		Thread t = new Thread();
		LinkingFileToImport.addFile(fl, t);
		boolean expResult = true;
		boolean result = LinkingFileToImport.searchFile(sheet);
		LinkingFileToImport.fileImport.clear();
		LinkingFileToImport.ThreadImport.clear();
		assertEquals(expResult, result);
	}

	/**
	 * Test of importFile method, of class LinkingFileToImport.
	 */
	@Test
	public void testImportFile() {
		System.out.println("importFile");
	}

	/**
	 * Test of getFileImport method, of class LinkingFileToImport.
	 */
	@Test
	public void testGetFileImport() {
		System.out.println("getFileImport");
		FileLink fl = new FileLink(null, null, true, "-", "");
		Thread t = new Thread();
		LinkingFileToImport.addFile(fl, t);
		List<FileLink> expResult = new ArrayList<FileLink>();
		expResult.add(fl);
		List<FileLink> result = LinkingFileToImport.getFileImport();
		assertEquals(expResult, result);
	}
}
