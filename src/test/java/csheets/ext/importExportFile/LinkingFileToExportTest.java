/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tixa
 */
public class LinkingFileToExportTest {

    Workbook wb = new Workbook(1);
    Spreadsheet s = wb.getSpreadsheet(0);
    File fich = new File("src-tests/csheets/ext/importExportFile/teste.txt");

    public LinkingFileToExportTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addFile method, of class LinkingFileToExport.
     */
    @Test
    public void testAddFile() {
        System.out.println("addFile");
        LinkingFileToExport.addFile(s, fich, false, ";", "");
        assertEquals(s, LinkingFileToExport.getFileExport().get(0).getSheet());
        assertEquals(fich, LinkingFileToExport.getFileExport().get(0).getFile());
        LinkingFileToExport.fileExport.clear();
        LinkingFileToExport.ThreadExport.clear();
    }

    /**
     * Test of removeFile method, of class LinkingFileToExport.
     */
    @Test
    public void testRemoveFile() {
        System.out.println("removeFile");
        LinkingFileToExport.searchFile(s, fich, false, ";", "");
        LinkingFileToExport.removeFile(s);
        assertEquals(0, LinkingFileToExport.getFileExport().size());
        assertEquals(0, LinkingFileToExport.getThread().size());
        LinkingFileToExport.fileExport.clear();
        LinkingFileToExport.ThreadExport.clear();
    }

    /**
     * Test of searchFile method, of class LinkingFileToExport.
     */
    @Test
    public void testSearchFile() {
        System.out.println("searchFile");
        LinkingFileToExport.searchFile(s, fich, false, ";", "");
        assertEquals(false, LinkingFileToExport.getFileExport().isEmpty());
        LinkingFileToExport.fileExport.clear();
        LinkingFileToExport.ThreadExport.clear();
    }

    /**
     * Test of isLinked method, of class LinkingFileToExport.
     */
    @Test
    public void testIsLinked() {
        System.out.println("isLinked");
        LinkingFileToExport.addFile(s, fich, false, ";", "");
        boolean val = LinkingFileToExport.isLinked(s);
        assertEquals(true, val);
        LinkingFileToExport.fileExport.clear();
        LinkingFileToExport.ThreadExport.clear();
    }

}
