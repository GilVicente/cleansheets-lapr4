/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.navigationWindow.ui;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.Extension;
import csheets.ext.ExtensionManager;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Antonio Soutinho
 */
public class NavigationControllerTest {

    public NavigationControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getSpreadsheetList method, of class NavigationController.
     */
    @Test
    public void testGetSpreadsheetList() {
        System.out.println("getSpreadsheetList");
        Workbook wb = new Workbook(2);
        List<Spreadsheet> list = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Spreadsheet spreadsheet = wb.getSpreadsheet(i);
            list.add(spreadsheet);
        }

        assertTrue("result should be", list.size() > 0);
    }

    /**
     * Test of getCellsValues method, of class NavigationController.
     */
    @Test
    public void testGetCellsValues() throws FormulaCompilationException {
        System.out.println("getCellsValues");
        List<CellImpl> listCellValue = new ArrayList<>();
        Workbook wb = new Workbook(2);
        Spreadsheet parameter = wb.getSpreadsheet(0);
        
        Cell c = parameter.getCell(0, 0);
        c.setContent("23");
        for (Spreadsheet s : wb) {
            if (parameter.equals(s)) {
                    if(c.getContent()!=null)
                    listCellValue.add((CellImpl) c);
                }
            }
        
        assertTrue("result should be", listCellValue.size() > 0);
    }

//    /**
//     * Test of getCellsFormulas method, of class NavigationController.
//     */
//    @Test
//    public void testGetCellsFormulas() throws FormulaCompilationException {
//        System.out.println("getCellsFormulas");
//        List<CellImpl> listCellValue = new ArrayList<>();
//        Workbook wb = new Workbook(2);
//        Spreadsheet parameter = wb.getSpreadsheet(0);
//        
//        Cell c = parameter.getCell(0, 0);
//        c.setContent("=FOR{A1:=1;A1<10;A2:=A2+A1;A1:=A1+1}");
//        for (Spreadsheet s : wb) {
//            if (parameter.equals(s)) {
//                    if(c.getContent()!=null && c.getContent().startsWith("="))
//                    listCellValue.add((CellImpl) c);
//                }
//            }
//        
//        assertTrue("result should be", listCellValue.size() > 0);
//    }

    /**
     * Test of getCellsFormulas method, of class NavigationController.
     */
//    @Test
//    public void testGetCellsFormulas() {
//        System.out.println("getCellsFormulas");
//        Spreadsheet s = null;
//        NavigationController instance = null;
//        List<String> expResult = null;
//        List<String> result = instance.getCellsFormulas(s);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getExtensionList method, of class NavigationController.
     */
    @Test
    public void testGetExtensionList() {
        System.out.println("getExtensionList");
        CleanSheets app = new CleanSheets();
        UIController uic = new UIController(app);
        NavigationController instance = new NavigationController(uic);
        
        List<UIExtension> expResult = new ArrayList<>();
        
        ExtensionManager em = ExtensionManager.getInstance();
        em.getExtensions();
          UIExtension[] a = uic.getExtensions();

        for (int i = 0; i < a.length; i++) {
            expResult.add(a[i]);

        }

      
        
        List<UIExtension> result = instance.getExtensionList();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getAllExtensions method, of class NavigationController.
     */
//    @Test
//    public void testGetAllExtensions() {
//        System.out.println("getAllExtensions");
//        CleanSheets app = new CleanSheets();
//        UIController uic = new UIController(app);
//        NavigationController instance = new NavigationController(uic);
//        
//        /* There's a total of 16 extensions in the program as of now (20/06), so the test wil be done by
//        running and checking if there are 16 Extension instances */
//        
//        
//    
//        ArrayList<Extension> result = instance.getAllExtensions();
//        int total=16;
//        
//        assertNotNull(result);
//        assertEquals(result.size(),total);
//        
//    }

    /**
     * Test of getInactiveExtensions method, of class NavigationController.
     */
    @Test
    public void testGetInactiveExtensions() {
        System.out.println("getInactiveExtensions");
        CleanSheets app = new CleanSheets();
        UIController uic = new UIController(app);
        NavigationController instance = new NavigationController(uic);
        
        
        List<UIExtension> listActiveExtensions = instance.getExtensionList();
        List<Extension> allExtensions = instance.getAllExtensions();
        List<UIExtension> expResult = new ArrayList<>();
             for (Extension extension : allExtensions) {

            expResult.add(extension.getUIExtension(uic));
        }
        List<UIExtension> result = instance.getInactiveExtensions(listActiveExtensions, allExtensions);
        assertNotNull(result);
      
    }
    
}
