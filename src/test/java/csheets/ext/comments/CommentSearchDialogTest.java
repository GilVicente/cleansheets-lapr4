/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.comments;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ext.comments.CommentableCell;
import csheets.ext.comments.ui.CommentSearchDialog;
import java.awt.Font;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JOSENUNO
 */
public class CommentSearchDialogTest {
    
    public CommentSearchDialogTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of fillSearchList method, of class CommentSearchDialog.
     */
    @Test
    public void testFillSearchList() {
        System.out.println("fillSearchList");
        Workbook wb = new Workbook(2);
        Spreadsheet s = wb.getSpreadsheet(0);
        // get the first cell
        Cell c = s.getCell(0, 0);
        
        CommentableCell c1 = new CommentableCell(c);
        c1.setUserComment("teste");
        c1.setUserCommentFont(new Font("arial", Font.BOLD, 14));
        CommentableCell c2 = new CommentableCell(c);
        c2.setUserComment("teste2");
        c2.setUserCommentFont(new Font("arial", Font.PLAIN, 15));
        
        ArrayList<CommentableCell> l = new ArrayList<>();
        l.add(c1);
        l.add(c2);
        
        CommentSearchDialog instance = new CommentSearchDialog();
        ArrayList<String> result = instance.fillSearchList(l);
        
        assertEquals(2, result.size());
        
        String[] a = result.get(0).split("<br>");
        
        assertEquals(5, a.length);
        
        assertEquals("teste", a[2]);
        
        assertEquals("java.awt.Font[family=Arial,name=arial,style=bold,size=14]", a[3]);
    }
    
}
