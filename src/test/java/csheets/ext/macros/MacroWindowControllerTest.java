/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros;

import csheets.CleanSheets;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.macro.Macro;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Antonio Soutinho changed by Patrícia Monteiro (1140807)
 */
public class MacroWindowControllerTest {
    
    public MacroWindowControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    

    /**
     * Test of newMacro method, of class MacroWindowController.
     */
//    @Test
//    public void testNewMacro() throws FormulaCompilationException {
//        System.out.println("newMacro");
//        UIController uic = new UIController(new CleanSheets());
//        MacroWindowController instance = new MacroWindowController(uic);
//        Workbook book = new Workbook(1);
//        Spreadsheet sp = book.getSpreadsheet(0);
//        MacroWorkbookRepo mWorkbookRepo = (MacroWorkbookRepo) sp.getExtension(MacrosExtension.NAME);
//        Macro m;
//        String name = "name";
//        String instructions = "1+2";
//        m = new Macro(mWorkbookRepo,name, sp);
//        m.addLine(instructions);
//        mWorkbookRepo.addMacro(m);
//        assertTrue("result should be", mWorkbookRepo.getMacro(name).getName().equals(m.getName()));
//    }
////
//
//    /**
//     * Test of listAvailableMacros method, of class MacroWindowController.
//     */
//    @Test
//    public void testListAvailableMacros() throws FormulaCompilationException {
//        System.out.println("listAvailableMacros");
//        UIController uic = new UIController(new CleanSheets());
//        MacroWindowController instance = new MacroWindowController(uic);
//        Workbook book = new Workbook(1);
//        Spreadsheet sp = book.getSpreadsheet(0);
//        MacroWorkbookRepo mWorkbookRepo = (MacroWorkbookRepo) sp.getExtension(MacrosExtension.NAME);
//        Macro m;
//        String name = "name";
//        String instructions = "1+2";
//        m = new Macro(name, sp);
//        m.addLine(instructions);
//        mWorkbookRepo.addMacro(m);
//        List<String> expResult = new ArrayList<String>();
//        expResult.addAll(mWorkbookRepo.listAvailableMacros());
//        assertTrue("result should be", !expResult.isEmpty());
//        
//    }

    /**
     * Test of newMacro method, of class MacroWindowController.
     */
//    @Test
//    public void testNewMacro2() throws Exception {
//        System.out.println("newMacro");       
//        
//        UIController uic = new UIController(new CleanSheets());
//        MacroWindowController instance = new MacroWindowController(uic);        
//        
//        Workbook book = new Workbook();
//        book.addSpreadsheet();
//        
//        MacroWorkbookRepo repo=new MacroWorkbookRepo(book.getSpreadsheet(0));      
//               
//        String name = "name()";
//        String instructions = "1+2";
//        instance.newMacro(name, instructions);      
//        
//        Macro m = new Macro(repo, name, book.getSpreadsheet(0));
//                       
//        repo.addMacro(m);
//        assertEquals(repo.getMacro(name).getName(), m.getName());        
//       
//    }

    /**
     * Test of runMacro method, of class MacroWindowController.
     */
//    @Test
//    public void testRunMacro() throws Exception {
//        System.out.println("runMacro");
//        
//        UIController uic = new UIController(new CleanSheets());
//        MacroWindowController instance = new MacroWindowController(uic);       
//               
//        
//        String instructions = "1+2";
//        String name = "teste()";
//        
//        Value expResult = new Value (3);
//        Value result = instance.runMacro(instructions, name);
//        assertEquals(expResult.toString(), result.toString());
//        
//    }

    
    
    
    
}
