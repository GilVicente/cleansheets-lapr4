/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkingGames;

import java.io.Serializable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1130750
 */
public class PlayerDetectionTest implements Serializable {

    public PlayerDetectionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * auxiliar object for testing of methods
     */
    private class myObject implements Serializable {

        private String at1;
        private int at2;
        private String at3;

        public myObject(String at1, int at2, String at3) {
            this.at1 = at1;
            this.at2 = at2;
            this.at3 = at3;
        }

        @Override
        public boolean equals(Object o) {
            myObject obj = (myObject) o;
            return this.getAt1().equals(obj.getAt1()) && this.getAt2() == obj.getAt2() && this.getAt3().equals(obj.getAt3());
        }

        /**
         * @return the at1
         */
        public String getAt1() {
            return at1;
        }

        /**
         * @return the at2
         */
        public int getAt2() {
            return at2;
        }

        /**
         * @return the at3
         */
        public String getAt3() {
            return at3;
        }
    }

    /**
     * Test of serialize/deserialize method, of class PlayerDetection.
     */
    @Test
    public void testSerializeDeserialize() throws Exception {
        System.out.println("serialize/deserialize");
        myObject obj = new myObject("abs", 2, "asd");
        byte array[] = PlayerDetection.serialize(obj);
        myObject newObject = (myObject) PlayerDetection.deserialize(array);
        assertEquals(obj, newObject);
    }

    /**
     * Test of encode method, of class PlayerDetection.
     */
    @Test
    public void testEncode() {
        System.out.println("encode");
        byte[] array = "abc".getBytes();
        byte type = 12;
        byte[] result = PlayerDetection.encode(array, type);
        assertEquals(type, result[0]);
    }

    /**
     * Test of decode method, of class PlayerDetection.
     */
    @Test
    public void testDecode() {
        System.out.println("decode");
        byte[] array = new byte[3];
        array[0] = 1;
        array[1] = 2;
        array[2] = 3;

        byte[] expResult = new byte[2];
        expResult[0] = 2;
        expResult[1] = 3;
        byte[] result = PlayerDetection.decode(array);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of encode/decode method, of class PlayerDetection.
     */
    @Test
    public void testEncodeDecode() {
        System.out.println("decode");
        byte[] array = new byte[3];
        array[0] = 1;
        array[1] = 2;
        array[2] = 3;
        byte type = 12;
        byte[] newArray = PlayerDetection.encode(array, type);
        newArray = PlayerDetection.decode(newArray);
        assertArrayEquals(array, newArray);
    }
}
