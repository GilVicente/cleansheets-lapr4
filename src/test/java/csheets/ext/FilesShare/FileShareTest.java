/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FilesShare;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140234
 */
public class FileShareTest {
    
    public FileShareTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class FileShare.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        FileShare instance = new FileShare("teste1.txt", 200);
        String expResult = "teste1.txt -- 200KBytes";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
