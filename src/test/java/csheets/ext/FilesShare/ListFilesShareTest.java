/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FilesShare;

import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140234
 */
public class ListFilesShareTest {

    public ListFilesShareTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of fillTheList method, of class ListFilesShare.
     */
    @Test
    public void testFillTheList() throws Exception {
        System.out.println("fillTheList");

        ListFilesShare instance = new ListFilesShare();
        List<File> list = new ArrayList<>();
        for (File f : instance.getFilesExisting()) {
            list.add(f);
        }

        List<FileShare> result = instance.fillTheList(list);
        assertEquals(false, result.isEmpty());
        assertEquals(4, result.size());
    }

    /**
     * Test of getFilesExisting method, of class ListFilesShare.
     */
    @Test
    public void testGetFilesExisting() {
        System.out.println("getFilesExisting");
        ListFilesShare instance = new ListFilesShare();
        File f = new File("./filesToShare");
        File[] expResult = f.listFiles();
        File[] result = instance.getFilesExisting();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of addFile method, of class ListFilesShare.
     */
    @Test
    public void testAddFile() {
        System.out.println("addFile");
        FileShare f = new FileShare("teste.txt", 100);
        FileShare f2 = new FileShare("teste2.txt", 100);
        FileShare f3 = new FileShare("teste3.txt", 100);
        ListFilesShare instance = new ListFilesShare();
        instance.addFile(f);
        instance.addFile(f2);
        instance.addFile(f3);

        assertEquals(false, instance.returnTheList().isEmpty());
        assertEquals(3, instance.returnTheList().size());
        assertEquals(true, instance.returnTheList().contains(f));
        assertEquals(true, instance.returnTheList().contains(f2));
        assertEquals(true, instance.returnTheList().contains(f3));

    }

    /**
     * Test of returnTheList method, of class ListFilesShare.
     */
    @Test
    public void testReturnTheList() {
        System.out.println("returnTheList");
        ListFilesShare instance = new ListFilesShare();
        FileShare f = new FileShare("teste.txt", 100);
        FileShare f2 = new FileShare("teste2.txt", 100);
        FileShare f3 = new FileShare("teste3.txt", 100);
        instance.addFile(f);
        instance.addFile(f2);
        instance.addFile(f3);

        assertEquals(false, instance.returnTheList().isEmpty());
        assertEquals(3, instance.returnTheList().size());
        assertEquals(true, instance.returnTheList().contains(f));
        assertEquals(true, instance.returnTheList().contains(f2));
        assertEquals(true, instance.returnTheList().contains(f3));
    }

//    /**
//     * Test of sendListFiles method, of class ListFilesShare.
//     */
//    @Test
//    public void testSendListFiles() {
//        System.out.println("sendListFiles");
//        InetAddress destination = null;
//        ListFilesShare instance = new ListFilesShare();
//        Thread expResult = null;
//        Thread result = instance.sendListFiles(destination);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of fillListFiles method, of class ListFilesShare.
     */
    @Test
    public void testFillListFiles() throws Exception {
        System.out.println("fillListFiles");
        String path = "./filesToShare";
        ListFilesShare instance = new ListFilesShare();
        instance.fillListFiles(path);
        
        assertEquals(false, instance.returnTheList().isEmpty());
        assertEquals(4, instance.returnTheList().size());
        
    }

}
