/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.editNotes;

import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Ferreira <1130430@isep.ipp.pt>
 */
public class ContactNoteTest {
    
    public ContactNoteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addVersion method, of class ContactNote.
     */
    @Test
    public void testAddVersion() {
        System.out.println("addVersion");
        String text = "v1";
        ContactNote instance = new ContactNote(text);
        instance.addVersion(text);
        
    }

    /**
     * Test of getNotes method, of class ContactNote.
     */
//    @Test
//    public void testGetNotes() {
//        Calendar timeStamp = Calendar.getInstance(TimeZone.getDefault());
//        System.out.println("getNotes");
//        String text="text";
//        ContactNote instance = new ContactNote(text);
//        HashMap<Calendar, String> expResult = new HashMap<>();
//        expResult.put(timeStamp, text);
//        HashMap<Calendar, String> result = instance.getNotes();
//        result.put(timeStamp, text);
//        assertEquals(expResult, result);
//        
//    }

    /**
     * Test of getLatestTimestamp method, of class ContactNote.
     */
   /* @Test
    public void testGetLatestTimestamp() {
        System.out.println("getLatestTimestamp");
        String text="text";
        ContactNote instance = new ContactNote(text);
        Calendar expResult = Calendar.getInstance();
        Calendar result = instance.getLatestTimestamp();
        assertEquals(expResult, result);
        
    }*/

    /**
     * Test of getTitle method, of class ContactNote.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        String text="text";
        ContactNote instance = new ContactNote(text);
        String expResult = "text";
        String result = instance.getTitle();
        assertEquals(expResult, result);
    }
    
}
