/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.function;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Formula;
import csheets.core.formula.Function;
import csheets.core.formula.Operator;
import csheets.core.formula.UnaryOperator;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.core.formula.lang.Language;
import csheets.ui.ctrl.UIController;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import org.antlr.runtime.tree.CommonTree;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guilherme
 */
public class WizardFunctionControllerTest {
    
    private static UIController uiController;
    
    public WizardFunctionControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        CleanSheets cleansheets = new CleanSheets();
        cleansheets.create();
        uiController = new UIController(cleansheets);
        uiController.setActiveCell(cleansheets.getWorkbooks()[0].getSpreadsheet(0).getCell(0, 0));
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCell method, of class WizardFunctionController.
     */
    @Test
    public void testGetCell() {
        System.out.println("getCell");
        WizardFunctionController instance = new WizardFunctionController(uiController);
        Cell expResult = uiController.getActiveCell();
        Cell result = instance.getCell();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFunctions method, of class WizardFunctionController.
     */
    @Test
    public void testGetFunctions() {
        System.out.println("getFunctions");
        WizardFunctionController instance = new WizardFunctionController(uiController);
        Function[] expResult = Language.getInstance().getFunctions();
        Function[] result = instance.getFunctions();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of hasFunction method, of class WizardFunctionController.
     */
    @Test
    public void testHasFunction() {
        System.out.println("hasFunction");
        String identifier = "FALSE";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        boolean expResult = true;
        boolean result = instance.hasFunction(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFunction method, of class WizardFunctionController.
     */
    @Test
    public void testGetFunction() throws Exception {
        System.out.println("getFunction");
        String identifier = "AND";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        Function expResult = Language.getInstance().getFunction(identifier);
        Function result = instance.getFunction(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFormula method, of class WizardFunctionController.
     */
    @Test
    public void testGetFormula() throws Exception {
        System.out.println("getFormula");
        String source = "AND";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        Formula expResult = FormulaCompiler.getInstance().compile(uiController.getActiveCell(), source);
        Formula result = instance.getFormula(source);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSource method, of class WizardFunctionController.
     */
    @Test
    public void testGetSource() {
        System.out.println("getSource");
        String identifier = "AND";
        String[] params = new String[1];
        params[0]="False";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        String expResult = "=AND(False)";
        String result = instance.getSource(identifier, params);
        assertEquals(expResult, result);
    }

    /**
     * Test of getBinaryOperator2 method, of class WizardFunctionController.
     */
    @Test
    public void testGetBinaryOperator2() {
        System.out.println("getBinaryOperator2");
        WizardFunctionController instance = new WizardFunctionController(uiController);
        BinaryOperator[] expResult = Language.getInstance().getBinaryOperators();
        BinaryOperator[] result = instance.getBinaryOperator2();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getSource2 method, of class WizardFunctionController.
     */
    @Test
    public void testGetSource2() {
        System.out.println("getSource2");
        String identifier = "AND";
        JTextField[] params = new JTextField[1];
        params[0] = new JTextField("False");
        WizardFunctionController instance =new WizardFunctionController(uiController);
        String expResult = "=AND(False)";
        String result = instance.getSource2(identifier, params);
        assertEquals(expResult, result);
    }

    /**
     * Test of getStructure method, of class WizardFunctionController.
     */
    @Test
    public void testGetStructure() {
        System.out.println("getStructure");
        String identifier = "AND";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        String expResult = "=AND(BOOLEAN)";
        String result = instance.getStructure(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getHelp method, of class WizardFunctionController.
     */
    @Test
    public void testGetHelp() throws Exception {
        System.out.println("getHelp");
        String identifier = "AND";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        String expResult = "=AND(BOOLEAN)\nBoolean expression(BOOLEAN):A boolean expression to include";
        String result = instance.getHelp(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberParameters method, of class WizardFunctionController.
     */
    @Test
    public void testGetNumberParameters() {
        System.out.println("getNumberParameters");
        String identifier = "AND";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        int expResult = -1;
        int result = instance.getNumberParameters(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasBinaryOperator method, of class WizardFunctionController.
     */
    @Test
    public void testHasBinaryOperator() {
        System.out.println("hasBinaryOperator");
        String identifier = "SUM";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        boolean expResult = false;
        boolean result = instance.hasBinaryOperator(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasUnaryOperator method, of class WizardFunctionController.
     */
    @Test
    public void testHasUnaryOperator() {
        System.out.println("hasUnaryOperator");
        String identifier = "&";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        boolean expResult = false;
        boolean result = instance.hasUnaryOperator(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getBinaryOperator method, of class WizardFunctionController.
     */
    @Test
    public void testGetBinaryOperator() throws Exception {
        System.out.println("getBinaryOperator");
        String identifier = "+";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        BinaryOperator expResult = Language.getInstance().getBinaryOperator(identifier);
        BinaryOperator result = instance.getBinaryOperator(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUnaryOperator method, of class WizardFunctionController.
     */
    @Test
    public void testGetUnaryOperator() throws Exception {
        System.out.println("getUnaryOperator");
        String identifier = "%";
        WizardFunctionController instance = new WizardFunctionController(uiController);
        UnaryOperator expResult = Language.getInstance().getUnaryOperator(identifier);
        UnaryOperator result = instance.getUnaryOperator(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getBinaryOperators method, of class WizardFunctionController.
     */
    @Test
    public void testGetBinaryOperators() {
        System.out.println("getBinaryOperators");
        WizardFunctionController instance = new WizardFunctionController(uiController);
        Operator[] expResult = Language.getInstance().getBinaryOperators();
        Operator[] result = instance.getBinaryOperators();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getUnaryOperators method, of class WizardFunctionController.
     */
    @Test
    public void testGetUnaryOperators() {
        System.out.println("getUnaryOperators");
        WizardFunctionController instance = new WizardFunctionController(uiController);
        Operator[] expResult = Language.getInstance().getUnaryOperators();
        Operator[] result = instance.getUnaryOperators();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getFunctionList method, of class WizardFunctionController.
     */
    @Test
    public void testGetFunctionList() {
        System.out.println("getFunctionList");
        WizardFunctionController instance = new WizardFunctionController(uiController);

        String[] functionList = instance.getFunctionList();

        Language l = Language.getInstance();
        int total = l.getBinaryOperators().length+l.getUnaryOperators().length + l.getFunctions().length;
        assertTrue("should be true", total == functionList.length);

        boolean existsUnaryOperator = false, existsBinaryOperator = false, existsFunctionMath = false;
        for (String s : functionList) {
            if (!existsUnaryOperator) {
                existsUnaryOperator=l.hasUnaryOperator(s);
            }
            if (!existsBinaryOperator) {
                existsBinaryOperator = l.hasBinaryOperator(s);
            }
            if(!existsFunctionMath){
                try {
                    existsFunctionMath=Math.class.getMethod(s.toLowerCase(), new Class[] { int.class })!=null;
                } catch (NoSuchMethodException | SecurityException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
 
        assertTrue("list should contain fuctions from java.lang.Math, and Unary "
                + "and binary operators", existsBinaryOperator && existsBinaryOperator && existsFunctionMath);
    }

//    /**
//     * Test of generateTree method, of class WizardFunctionController.
//     */
//    @Test
//    public void testGenerateTree() {
//        System.out.println("generateTree");
//        
//        WizardFunctionController instance = new WizardFunctionController(uiController);
//        CommonTree ctree = instance.getTreeSource("+");
//        ctree
//        JTree tree = new JTree(new DefaultMutableTreeNode(ctree.getText()));
//        instance.generateTree(ctree, tree);
//    }

//    /**
//     * Test of getTreeSource method, of class WizardFunctionController.
//     */
//    @Test
//    public void testGetTreeSource() {
//        System.out.println("getTreeSource");
//        String source = "";
//        WizardFunctionController instance = null;
//        CommonTree expResult = null;
//        CommonTree result = instance.getTreeSource(source);
//        assertEquals(expResult, result);
//    }
    
}
