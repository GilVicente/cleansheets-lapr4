/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.function.Controller;

import csheets.ext.function.WizardFunctionController;
import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Formula;
import csheets.core.formula.Function;
import csheets.core.formula.UnaryOperator;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.core.formula.lang.Language;
import csheets.ui.ctrl.UIController;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago
 */
public class WizardFunctionControllerTest {

    public WizardFunctionControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCell method, of class WizardFunctionController.
     */
    @Test
    public void testGetCell() {
        System.out.println("getCell");
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));
        Cell expResult = null;
        Cell result = instance.getCell();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFunctions method, of class WizardFunctionController.
     */
    @Test
    public void testGetFunctions() {
        System.out.println("getFunctions");
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));
        Function[] expResult = Language.getInstance().getFunctions();
        Function[] result = instance.getFunctions();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of hasFunction method, of class WizardFunctionController.
     */
    @Test
    public void testHasFunction() {
        System.out.println("hasFunction");
        String identifier = "AND";
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));
        boolean expResult = true;
        boolean result = instance.hasFunction(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFunction method, of class WizardFunctionController.
     */
    @Test
    public void testGetFunction() throws Exception {
        System.out.println("getFunction");
        String identifier = "AND";
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));
        Function expResult = Language.getInstance().getFunction(identifier);
        Function result = instance.getFunction(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFormula method, of class WizardFunctionController.
     */
    @Test
    public void testGetFormula() throws Exception {
        System.out.println("getFormula");
        String source = "AND";
        UIController ui=new UIController(new CleanSheets());
        WizardFunctionController instance = new WizardFunctionController(ui);
        Formula expResult = FormulaCompiler.getInstance().compile(ui.getActiveCell(), source);
        Formula result = instance.getFormula(source);
        assertEquals(expResult, result);
    }

    /**
     * Test of setContent method, of class WizardFunctionController.
     */
//    @Test
//    public void testSetContent() throws Exception {
//        System.out.println("setContent");
//        String source = "AND";
//        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));
//        instance.setContent(source);
//    }
    
    /**
     * Test of getStructure method, of class WizardFunctionController.
     */
    @Test
    public void testGetStructure() {
        System.out.println("getStructure");
        String identifier = "AND";
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));
        String expResult = "=AND(BOOLEAN)";
        String result = instance.getStructure(identifier);
        assertEquals(expResult, result);
    }

    /**
     * Test of getHelp method, of class WizardFunctionController.
     */
    @Test
    public void testGetHelp() throws Exception {
        System.out.println("getHelp");
        String identifier = "AND";
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));
        String expResult = "=AND(BOOLEAN)\nBoolean expression(BOOLEAN):A boolean expression to include";
        String result = instance.getHelp(identifier);
        assertEquals(expResult, result);
    }

    @Test
    public void testSourceWithBinaryOperator() {
        System.out.println("getSourceWithBinaryOperator");
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));

        String expResult = "=1+2";

        String identifier = "+";
        String params[] = {"1", "2"};
        String result = instance.getSource(identifier, params);

        assertEquals(expResult, result);
    }

    @Test
    public void testSourceWithUnaryOperator() {
        System.out.println("getSourceWithUnaryOperator");
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));

        String expResult = "=2%";

        String identifier = "%";
        String params[] = {"2"};
        String result = instance.getSource(identifier, params);
 
        assertEquals(expResult, result);
    }

    @Test
    public void testSourceWithFunction() {
        System.out.println("getSourceWithFunction");
        WizardFunctionController instance = new WizardFunctionController(new UIController(new CleanSheets()));

        String expResult = "=sum(2;4;5;6)";

        String identifier = "sum";
        String params[] = {"2", "4", "5", "6"};
        String result = instance.getSource(identifier, params);

        assertEquals(expResult, result);
    }

    @Test
    public void testGetFunctionList() {
        System.out.println("getFunctionList");
        UIController uiController = new UIController(new CleanSheets());
        WizardFunctionController instance = new WizardFunctionController(uiController);

        String[] functionList = instance.getFunctionList();

        Language l = Language.getInstance();
        int total = l.getBinaryOperators().length+l.getUnaryOperators().length + l.getFunctions().length;
        assertTrue("should be true", total == functionList.length);

        boolean existsUnaryOperator = false, existsBinaryOperator = false, existsFunctionMath = false;
        for (String s : functionList) {
            if (!existsUnaryOperator) {
                existsUnaryOperator=l.hasUnaryOperator(s);
            }
            if (!existsBinaryOperator) {
                existsBinaryOperator = l.hasBinaryOperator(s);
            }
            if(!existsFunctionMath){
                try {
                    existsFunctionMath=Math.class.getMethod(s.toLowerCase(), new Class[] { int.class })!=null;
                } catch (NoSuchMethodException | SecurityException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
 
        assertTrue("list should contain fuctions from java.lang.Math, and Unary "
                + "and binary operators", existsBinaryOperator && existsBinaryOperator && existsFunctionMath);
    }
}
