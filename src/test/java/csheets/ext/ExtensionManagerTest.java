/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext;

import csheets.CleanSheets;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gil
 */
public class ExtensionManagerTest {
    
    public ExtensionManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of extensionsToBeLoaded method, of class ExtensionManager.
     */
    @Test
    public void testExtensionsToBeLoaded() {
        System.out.println("extensionsToBeLoaded");
        List<String> selectedExtensions = new ArrayList<String>();
        selectedExtensions.add("csheets.ext.importExportFile.ImportExportFileExtension");
        ExtensionManager instance = ExtensionManager.getInstance();
        String expResult = "csheets.ext.style.StyleExtension"+"\n"+
                    "csheets.ext.comments.CommentsExtension"+"\n"+
                    "csheets.ext.importExportFile.ImportExportFileExtension";
        
        System.out.println("Teste unitário:");
        InputStream resultStream = instance.extensionsToBeLoaded(selectedExtensions);
        String result = new Scanner(resultStream,"UTF-8").useDelimiter("\\A").next();
        System.out.println(result);
        
        assertEquals(expResult, result);
    }
    
}
