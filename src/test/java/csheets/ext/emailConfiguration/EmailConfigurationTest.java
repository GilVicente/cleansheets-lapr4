/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.emailConfiguration;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author José Santos (1140921)
 */
public class EmailConfigurationTest {

    public EmailConfigurationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testIsValid() throws Exception {
        System.out.println("ValidEmail");
        String email = "xpto@gmail.com";
        EmailConfiguration emailConfig = new EmailConfiguration("Name");
        emailConfig.isValidEmailAddress(email);
        assertEquals(emailConfig.isValidEmailAddress(email), true);
    }

    @Test
    public void testInValid() throws Exception {
        System.out.println("InValidEmail");
        String email = "xptomail.com";
        EmailConfiguration emailConfig = new EmailConfiguration("Name");
        emailConfig.isValidEmailAddress(email);
        assertEquals(emailConfig.isValidEmailAddress(email), false);
    }

//    @Test
//    public void testSendEmail() throws Exception {
//        System.out.println("SendEmail");
//        EmailConfiguration emailConfig = new EmailConfiguration(System.getProperty("user.name"));
//        
//        emailConfig.setEmailTo("1140921@isep.ipp.pt");
//        emailConfig.setEmailBody("Compiled by " + System.getProperty("user.name"));
//        emailConfig.sendEmail();
//
//    }
}
