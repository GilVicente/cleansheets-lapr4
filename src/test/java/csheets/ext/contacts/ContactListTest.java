///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package csheets.ext.contacts;
//
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author toshiba-pc
// */
//public class ContactListTest {
//    
//    public ContactListTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of getContactFromList method, of class ContactList.
//     */
//    @Test
//    public void testGetContactFromList() {
//        System.out.println("getContactFromList");
//        int index = 0;
//        ContactList instance = new ContactList();
//        Picture picture = new Picture(2, 1);
//        Contact contact = new Contact("firstname", "lastname", picture);
//        instance.addContactToList(contact);
//        Contact expResult = contact;
//        Contact result = instance.getContactFromList(index);
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of getContactPicture method, of class ContactList.
//     */
//    @Test
//    public void testGetContactPicture() {
//        System.out.println("getContactPicture");
//        int index = 0;
//        ContactList instance = new ContactList();
//        Picture picture = new Picture(2,1);
//        Contact contact = new Contact("firstname", "lastname", picture);
//        instance.addContactToList(contact);
//        Picture expResult = picture;
//        Picture result = instance.getContactPicture(index);
//        assertEquals(expResult, result);
//        
//    }
//
//    /**
//     * Test of addContactToList method, of class ContactList.
//     */
//    @Test
//    public void testAddContactToList() {
//        System.out.println("addContactToList");
//        Contact contact = new Contact("firstname", "lastname");
//        ContactList instance = new ContactList();
//        boolean expResult = true;
//        boolean result = instance.addContactToList(contact);
//        assertEquals(expResult, result);
//        
//       
//    }
//
//    /**
//     * Test of removeContactFromList method, of class ContactList.
//     */
//    @Test
//    public void testRemoveContactFromList() {
//        System.out.println("removeContactFromList");
//        Picture picture = new Picture(2,1);
//        Contact contact = new Contact("firstname", "lastname", picture);
//        ContactList instance = new ContactList();
//        instance.addContactToList(contact);
//        boolean expResult = true;
//        boolean result = instance.removeContactFromList(contact);
//        assertEquals(expResult, result);
//        ;
//    }
//
//    /**
//     * Test of size method, of class ContactList.
//     */
//    @Test
//    public void testSize() {
//        System.out.println("size");
//        ContactList instance = new ContactList();
//        int expResult = 0;
//        int result = instance.size();
//        assertEquals(expResult, result);
//        
//    }
//
//    /**
//     * Test of indexOf method, of class ContactList.
//     */
//    @Test
//    public void testIndexOf() {
//        System.out.println("indexOf");
//        Picture picture = new Picture(2, 1);
//        Contact contact = new Contact("firstname", "lastname", picture);
//        ContactList instance = new ContactList();
//        instance.addContactToList(contact);
//        int expResult = 0;
//        int result = instance.indexOf(contact);
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of contains method, of class ContactList.
//     */
//    @Test
//    public void testContains() {
//        System.out.println("contains");
//        Picture picture = new Picture(2, 1);
//        Contact contact = new Contact("firstname", "lastname", picture);
//        ContactList instance = new ContactList();
//        instance.addContactToList(contact);
//        boolean expResult = true;
//        boolean result = instance.contains(contact);
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of editContact method, of class ContactList.
//     */
//    @Test
//    public void testEditContact() throws CloneNotSupportedException {
//        System.out.println("editContact");
//        Picture picture = new Picture(2, 1);
//        Contact originalContact = new Contact("firstname", "lastname", picture);
//        Contact clonedContact = originalContact.clone();
//        ContactList instance = new ContactList();
//        boolean expResult = true;
//        boolean result = instance.editContact(originalContact, clonedContact);
//        assertEquals(expResult, result);
//        
//    }
//
//    /**
//     * Test of sort method, of class ContactList.
//     */
//    @Test
//    public void testSort() {
//        System.out.println("sort");
//        ContactList instance = new ContactList();
//        instance.sort();
//        
//    }
//       
// 
//    
//}
