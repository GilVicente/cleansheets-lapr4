///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package csheets.ext.contacts;
//
//import java.security.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.TimeZone;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Tixa
// */
//public class AgendaTest {
//    
//    public AgendaTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    
//
//    /**
//     * Test of addEventToList method, of class Agenda.
//     */
//    @Test
//    public void testAddEventToList() {
//        System.out.println("addEventToList");
//       Calendar dueDate=Calendar.getInstance(TimeZone.getDefault());
//        Event agenda = new Event(dueDate, "d1");
//        Agenda instance = new Agenda();        
//        boolean expResult = true;
//        boolean result = instance.addEventToList(agenda);
//        assertEquals(expResult, result);       
//    }
//
//    /**
//     * Test of removeEventFromList method, of class Agenda.
//     */
//    @Test
//    public void testRemoveEventFromList() {
//        System.out.println("removeEventFromList");
//        Calendar dueDate=Calendar.getInstance(TimeZone.getDefault());
//        Event agenda = new Event(dueDate, "d1");
//        Agenda instance = new Agenda();
//        instance.addEventToList(agenda);
//        boolean expResult = true;
//        boolean result = instance.removeEventFromList(agenda);
//        assertEquals(expResult, result);        
//    }
//
//    /**
//     * Test of size method, of class Agenda.
//     */
//    @Test
//    public void testSize() {
//        System.out.println("size");
//        Calendar dueDate=Calendar.getInstance(TimeZone.getDefault());
//        Event agenda = new Event(dueDate, "d1");
//        Agenda instance = new Agenda();
//        instance.addEventToList(agenda);
//        int expResult = 1;
//        int result = instance.size();
//        assertEquals(expResult, result);        
//    }
//
//    /**
//     * Test of indexOf method, of class Agenda.
//     */
//    @Test
//    public void testIndexOf() {
//        System.out.println("indexOf");
//        Calendar dueDate=Calendar.getInstance(TimeZone.getDefault());
//        Event agenda = new Event(dueDate, "d1");
//        Agenda instance = new Agenda();
//        instance.addEventToList(agenda);
//        int expResult = 0;
//        int result = instance.indexOf(agenda);
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of contains method, of class Agenda.
//     */
//    @Test
//    public void testContains() {
//        System.out.println("contains");
//        Calendar dueDate=Calendar.getInstance(TimeZone.getDefault());
//        Event agenda = new Event(dueDate, "d1");
//        Agenda instance = new Agenda();
//        instance.addEventToList(agenda);
//        boolean expResult = true;
//        boolean result = instance.contains(agenda);
//        assertEquals(expResult, result);       
//    }
//
//    /**
//     * Test of editEvent method, of class Agenda.
//     */
//    @Test
//    public void testEditEvent() {
//        System.out.println("editEvent");
//        Calendar dueDate=Calendar.getInstance(TimeZone.getDefault());
//        Event agenda = new Event(dueDate, "d1");
//        Agenda instance = new Agenda();
//        instance.addEventToList(agenda);        
//        instance.removeEventFromList(agenda);
//        
//        Calendar dueDateClone=Calendar.getInstance(TimeZone.getDefault());
//        Event agendaClone = new Event(dueDateClone, "d2");
//        Agenda instanceClone = new Agenda();
//        instanceClone.addEventToList(agendaClone);      
//      
//        boolean expResult = true;
//        boolean result = instance.editEvent(agenda, agendaClone);
//        assertEquals(expResult, result);        
//    }
//
//    /**
//     * Test of getAgendaList method, of class Agenda.
//     */
//    @Test
//    public void testGetAgendaList() {
//        System.out.println("getAgendaList");
//        Calendar dueDate=Calendar.getInstance(TimeZone.getDefault());
//        Event agenda = new Event(dueDate, "d1");
//        Agenda instance = new Agenda();
//        instance.addEventToList(agenda); 
//       
//        List<Event> expResult = null;
//        List<Event> result = instance.AgendaList();
//        assertNotEquals(expResult, result);
//       
//    }
//
//    /**
//     * Test of getEventFromList method, of class Agenda.
//     */
//    @Test
//    public void testGetEventFromList() {
//        System.out.println("getEventFromList");
//        int index = 0;
//        Calendar dueDate=Calendar.getInstance(TimeZone.getDefault());
//        Event agenda = new Event(dueDate, "d1");
//        Agenda instance = new Agenda();
//        instance.addEventToList(agenda);
//        
//        Event expResult = agenda;
//        Event result = instance.getEventFromList(index);
//        assertEquals(expResult, result);
//        
//    }
//    
//}
