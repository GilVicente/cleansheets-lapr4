///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package csheets.ext.contacts;
//
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.TimeZone;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author toshiba-pc
// */
//public class EventTest {
//
//    public EventTest() {
//    }
//
//    @BeforeClass
//    public static void setUpClass() {
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() {
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of DueDate method, of class Event.
//     */
//    @Test
//    public void testDueDate() {
//        System.out.println("DueDate");
//        Calendar dueDate = Calendar.getInstance(TimeZone.getDefault());
//        Event instance = new Event(dueDate, "description");
//        Calendar expResult = Calendar.getInstance(TimeZone.getDefault());
//        Calendar result = instance.DueDate();
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of defineDueDate method, of class Event.
//     */
//    @Test
//    public void testDefineDueDate() {
//        System.out.println("defineDueDate");
//        Calendar dueDate = Calendar.getInstance(TimeZone.getDefault());
//        Event instance = new Event(dueDate, "description");
//        instance.defineDueDate(dueDate);
//
//    }
//
//    /**
//     * Test of Description method, of class Event.
//     */
//    @Test
//    public void testDescription() {
//        System.out.println("Description");
//        Calendar dueDate = Calendar.getInstance(TimeZone.getDefault());
//        Event instance = new Event(dueDate, "description");
//        String expResult = "description";
//        String result = instance.Description();
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of defineDescription method, of class Event.
//     */
//    @Test
//    public void testDefineDescription() {
//        System.out.println("defineDescription");
//        String description = "description";
//        Calendar dueDate = Calendar.getInstance(TimeZone.getDefault());
//        Event instance = new Event(dueDate, "description");
//        instance.defineDescription(description);
//    }
//
//
//    /**
//     * Test of clone method, of class Event.
//     */
//    @Test
//    public void testClone() throws Exception {
//        System.out.println("clone");
//        Calendar dueDate = Calendar.getInstance(TimeZone.getDefault());
//        Event instance = new Event(dueDate, "description");
//        Event expResult = instance.clone();
//        Event result = instance.clone();
//        assertTrue(result.DueDate().equals(instance.DueDate()));
//        assertTrue(result.Description().equals(instance.Description()));
//    }
//
// 
//
//}
