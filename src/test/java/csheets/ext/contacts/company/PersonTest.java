/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.company;

import csheets.ext.contacts.MailNumber.Company;
import csheets.ext.contacts.MailNumber.Person;
import csheets.ext.contacts.MailNumber.Profession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guilherme
 */
public class PersonTest {
    
    public PersonTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Company method, of class Person.
     */
    @Test
    public void testCompany() {
        System.out.println("Company");
        Company company = new Company("Name","Picture");
        Profession profession = new Profession("Doc");
        Person instance = new Person("Firstname","Lastname","Picture",company,profession);
        Company expResult = company;
        Company result = instance.Company();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of defineCompany method, of class Person.
     */
    @Test
    public void testDefineCompany() {
        System.out.println("defineCompany");
        Company company = new Company("Name","Pic");
        Person instance = new Person();
        instance.defineCompany(company);
    }

    /**
     * Test of toString method, of class Person.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Person instance = new Person();
        String expResult = instance.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of Profession method, of class Person.
     */
    @Test
    public void testProfession() {
        System.out.println("Profession");
        Company company = new Company("Name","Picture");
        Profession profession = new Profession("Doc");
        Person instance = new Person("Firstname","Lastname","Picture",company,profession);
        Profession expResult = profession;
        Profession result = instance.Profession();
        assertEquals(expResult, result);
    }

    /**
     * Test of defineProfession method, of class Person.
     */
    @Test
    public void testDefineProfession() {
        System.out.println("defineProfession");
        Profession profession = new Profession("Doc");
        Person instance = new Person();
        instance.defineProfession(profession);
    }
    
}
