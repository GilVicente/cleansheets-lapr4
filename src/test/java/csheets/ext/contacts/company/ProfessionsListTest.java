/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.company;

import csheets.ext.contacts.MailNumber.Profession;
import csheets.ext.contacts.MailNumber.ProfessionsList;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guilherme
 */
public class ProfessionsListTest {
    
    public ProfessionsListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addProfessionToList method, of class ProfessionsList.
     */
    @Test
    public void testAddProfessionToList1() {
        System.out.println("addProfessionToList");
        Profession profession = new Profession("Doc");
        ProfessionsList instance = new ProfessionsList();
        boolean expResult = true;
        boolean result = instance.addProfessionToList(profession);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addProfessionToList method, of class ProfessionsList.
     */
    @Test
    public void testAddProfessionToList2() {
        System.out.println("addProfessionToList");
        Profession profession = new Profession("Doc");
        ProfessionsList instance = new ProfessionsList();
        instance.addProfessionToList(profession);
        boolean expResult = false;
        boolean result = instance.addProfessionToList(profession);
        assertEquals(expResult, result);
    }


    /**
     * Test of size method, of class ProfessionsList.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        Profession prof1=new Profession("DOC");
        Profession prof2=new Profession("Engineer");
        ProfessionsList instance = new ProfessionsList();
        instance.addProfessionToList(prof2);
        instance.addProfessionToList(prof1);
        int expResult = 2;
        int result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of getProfessionFromList method, of class ProfessionsList.
     */
    @Test
    public void testGetProfessionFromList() {
        System.out.println("getProfessionFromList");
        int index = 0;
        Profession prof1=new Profession("DOC");
        Profession prof2=new Profession("Engineer");
        ProfessionsList instance = new ProfessionsList();
        instance.addProfessionToList(prof2);
        instance.addProfessionToList(prof1);
        Profession expResult = prof2;
        Profession result = instance.getProfessionFromList(index);
        assertEquals(expResult, result);
    }

    /**
     * Test of professionList method, of class ProfessionsList.
     */
    @Test
    public void testProfessionList() {
        System.out.println("professionList");
        ProfessionsList instance = new ProfessionsList();
        List<Profession> expResult = new ArrayList();
        List<Profession> result = instance.professionList();
        assertEquals(expResult, result);
    }
    
}
