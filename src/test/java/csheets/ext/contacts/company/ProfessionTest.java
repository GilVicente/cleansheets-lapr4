/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.company;

import csheets.ext.contacts.MailNumber.Profession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guilherme
 */
public class ProfessionTest {
    
    public ProfessionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Id method, of class Profession.
     */
    @Test
    public void testId() {
        System.out.println("Id");
        Profession instance = new Profession("Doc");
        Long expResult = new Long(0);
        Long result = instance.Id();
        assertEquals(expResult, result);
    }

    /**
     * Test of Name method, of class Profession.
     */
    @Test
    public void testName() {
        System.out.println("Name");
        Profession instance = new Profession("Doc");
        String expResult = "Doc";
        String result = instance.Name();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class Profession.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Engineer";
        Profession instance = new Profession();
        instance.setName(name);
    }

    /**
     * Test of toString method, of class Profession.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Profession instance = new Profession();
        String expResult = instance.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
