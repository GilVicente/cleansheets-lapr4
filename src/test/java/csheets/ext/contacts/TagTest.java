/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brafa
 */
public class TagTest {

    public TagTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of associateContact method, of class Tag.
     */
    @Test
    public void testAssociateContact() {
        System.out.println("associateContact");
        Contact c = new Contact("luis", "teixeira");
        Contact c2 = new Contact("ana", "teixeira");
        Contact c3 = new Contact("amanhã", "sexta");

        Tag instance = new Tag("asd");

        assertEquals(true, instance.associateContact(c));
        assertEquals(true, instance.associateContact(c2));
        assertEquals(true, instance.associateContact(c3));
        assertEquals(false, instance.associateContact(c));
        assertEquals(3, instance.returnFrequency());
        assertEquals(true, instance.tagContacts().contains(c));
        assertEquals(true, instance.tagContacts().contains(c2));
        assertEquals(true, instance.tagContacts().contains(c3));

    }

    /**
     * Test of tagDescription method, of class Tag.
     */
    @Test
    public void testTagDescription() {
        System.out.println("tagDescription");
        Tag instance = new Tag("asd");
        String expResult = "asd";
        String result = instance.tagDescription();
        assertEquals(expResult, result);

    }

    /**
     * Test of returnFrequency method, of class Tag.
     */
    @Test
    public void testReturnFrequency() {
        System.out.println("returnFrequency");
        Tag instance = new Tag("asd");
        Contact c = new Contact("luis", "teixeira");
        Contact c2 = new Contact("ana", "teixeira");
        Contact c3 = new Contact("amanhã", "sexta");
        instance.associateContact(c);
        instance.associateContact(c2);
        instance.associateContact(c3);
        int expResult = 3;
        int result = instance.returnFrequency();
        assertEquals(expResult, result);

    }

    /**
     * Test of tagContacts method, of class Tag.
     */
    @Test
    public void testTagContacts() {
        System.out.println("tagContacts");
        Tag instance = new Tag("asd");
        Contact c = new Contact("luis", "teixeira");
        Contact c2 = new Contact("ana", "teixeira");
        Contact c3 = new Contact("amanhã", "sexta");
        instance.associateContact(c);
        instance.associateContact(c2);
        instance.associateContact(c3);
        List<Contact> result = instance.tagContacts();
        
        assertEquals(result.size(), instance.returnFrequency());
        assertEquals(result.contains(c), instance.tagContacts().contains(c));
        assertEquals(result.contains(c2), instance.tagContacts().contains(c2));
        assertEquals(result.contains(c3), instance.tagContacts().contains(c3));

    }

}
