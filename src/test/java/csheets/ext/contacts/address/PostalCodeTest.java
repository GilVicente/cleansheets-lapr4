/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tixa
 */
public class PostalCodeTest {
    
    public PostalCodeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validate method, of class PostalCode.
     */
    @Test
    public void testValidate() throws Exception {
        System.out.println("validate");
        int locationNumber = 4430;
        int zoneNumber = 036;
        boolean expResult = true;
        boolean result = PostalCode.validate(locationNumber, zoneNumber);
        assertEquals(expResult, result);
        
    }
    
    
    
    /**
     * Test of validate method, of class PostalCode.
     */
    @Test
    public void testValidate1() throws Exception {
        System.out.println("validate");
        int locationNumber = 44300;
        int zoneNumber = 036;
        boolean expResult = false;
        boolean result = PostalCode.validate(locationNumber, zoneNumber);
        assertEquals(expResult, result);
        
    }
}
