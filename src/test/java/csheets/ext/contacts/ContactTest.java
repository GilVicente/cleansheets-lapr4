///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package csheets.ext.contacts;
//
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Tixa
// */
//public class ContactTest {
//    
//    public ContactTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of Firstname method, of class Contact.
//     */
//    @Test
//    public void testFirstname() {
//        System.out.println("Firstname");
//        Contact instance = new Contact("Patricia", "Monteiro");
//        String expResult = "Patricia";
//        String result = instance.Firstname();
//        assertEquals(expResult, result);       
//    }
//
//    /**
//     * Test of defineFirstname method, of class Contact.
//     */
//    @Test
//    public void testDefineFirstname() {
//        System.out.println("defineFirstname");
//        String firstname = "Patricia";
//        Contact instance = new Contact("Patricia", "Monteiro");
//        instance.defineFirstname(firstname);        
//    }
//
//    /**
//     * Test of Lastname method, of class Contact.
//     */
//    @Test
//    public void testLastname() {
//        System.out.println("Lastname");
//        Contact instance = new Contact("Patricia", "Monteiro");
//        String expResult = "Monteiro";
//        String result = instance.Lastname();
//        assertEquals(expResult, result);       
//    }
//
//    /**
//     * Test of defineLastname method, of class Contact.
//     */
//    @Test
//    public void testDefineLastname() {
//        System.out.println("defineLastname");
//        String lastname = "Monteiro";
//        Contact instance =  new Contact("Patricia", "Monteiro");
//        instance.defineLastname(lastname);       
//    }
//
//    /**
//     * Test of agendaList method, of class Contact.
//     */
//    @Test
//    public void testAgendaList() {
//        System.out.println("agendaList");
//        Contact instance = new Contact("Patricia", "Monteiro");
//        Agenda agendaList=new Agenda();
//        instance.defineAgendaList(agendaList);
//        Agenda expResult = agendaList;
//        Agenda result = instance.agendaList();
//        assertEquals(expResult, result);        
//    }
//
//    /**
//     * Test of defineAgendaList method, of class Contact.
//     */
//    @Test
//    public void testDefineAgendaList() {
//        System.out.println("defineAgendaList");
//        Contact instance = new Contact("Patricia", "Monteiro");
//        Agenda agendaList = new Agenda();        
//        instance.defineAgendaList(agendaList);      
//    }
//
//    /**
//     * Test of Picture method, of class Contact.
//     */
//    @Test
//    public void testPicture() {
//        System.out.println("Picture");
//        Picture p1= new Picture(2,1);
//        Contact instance = new Contact("Patricia", "Monteiro",p1);
//        Picture expResult = p1;
//        Picture result = instance.Picture();
//        assertEquals(expResult, result);     
//    }
//
//    /**
//     * Test of definePicture method, of class Contact.
//     */
//    @Test
//    public void testDefinePicture() {
//        System.out.println("definePicture");
//        Picture p1= new Picture(2,1);       
//        Contact instance = new Contact("Patricia", "Monteiro",p1);
//        instance.definePicture(p1);        
//    }
//
//    
//
//    /**
//     * Test of clone method, of class Contact.
//     */
//    @Test
//    public void testClone1() throws Exception {
//        System.out.println("clone");
//        Picture p1= new Picture(2,1);       
//        Contact instance = new Contact("Patricia", "Monteiro",p1);
//        Contact expResult = instance;
//        Contact result = instance.clone();
//        assertTrue(result.Firstname().equals(instance.Firstname()));                       
//    }
//    
//    /**
//     * Test of clone method, of class Contact.
//     */
//    @Test
//    public void testClone2() throws Exception {
//        System.out.println("clone");
//        Picture p1= new Picture(2,1);       
//        Contact instance = new Contact("Patricia", "Monteiro",p1);
//        Contact expResult = instance;
//        Contact result = instance.clone();
//        assertTrue(result.Lastname().equals(instance.Lastname()));                       
//    }
//    
//     /**
//     * Test of clone method, of class Contact.
//     */
//    @Test
//    public void testClone3() throws Exception {
//        System.out.println("clone");
//        Picture p1= new Picture(2,1);       
//        Contact instance = new Contact("Patricia", "Monteiro",p1);
//        Contact expResult = instance;
//        Contact result = instance.clone();
//        assertTrue(result.Picture().equals(instance.Picture()));                       
//    }
//    
//}
