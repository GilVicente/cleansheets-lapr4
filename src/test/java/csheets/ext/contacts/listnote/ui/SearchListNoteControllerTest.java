/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote.ui;

import csheets.ext.contacts.listnote.ListNote;
import csheets.ext.contacts.listnote.ListNoteElement;
import csheets.ext.contacts.listnote.ListNoteElementRegistry;
import csheets.ext.contacts.persistence.PersistenceContext;
import csheets.persistence.jpa.JpaListNoteRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class SearchListNoteControllerTest {

    public SearchListNoteControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of searchListNoteByString method, of class SearchListNoteController.
     */
    @Test
    public void testSearchListNoteByString() {
        System.out.println("searchListNoteByString");
        String regex = "a*";

        ListNote listNote = new ListNote();
        listNote.setTitle("aaaa");
        ListNoteElementRegistry lnre = new ListNoteElementRegistry();
        listNote.setElements(lnre);
        listNote.getElements().addListNoteElement("aaaa");

        JpaListNoteRepository repository = (JpaListNoteRepository) PersistenceContext.repositories().listNotes();
        List<ListNote> jpaList = repository.all();

        SearchListNoteController instance = new SearchListNoteController(null);

        List<ListNote> newList = new ArrayList<>();

        newList = instance.searchListNoteByString(regex);

        assertEquals(jpaList, newList);

    }

//    /**
//     * Test of searchListNoteByTime method, of class SearchListNoteController.
//     */
    @Test
    public void testSearchListNoteByTime() {
        System.out.println("searchListNoteByTime");

        Calendar calendar = null;
        Calendar calendar2 = null;

        String inferiorLimit = "20/06/2016";
        String superiorLimit = "25/06/2016";

        //Inferior Date Limit
        SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
        Date dateObj = null;
        try {
            dateObj = curFormater.parse(inferiorLimit);
        } catch (ParseException ex) {
            Logger.getLogger(SearchListNoteDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
        calendar = Calendar.getInstance();
        calendar.setTime(dateObj);

        //Superior Date Limit
        SimpleDateFormat curFormater2 = new SimpleDateFormat("dd/MM/yyyy");
        Date dateObj2 = null;
        try {
            dateObj = curFormater.parse(superiorLimit);
        } catch (ParseException ex) {
            Logger.getLogger(SearchListNoteDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
        calendar2 = Calendar.getInstance();
        calendar2.setTime(dateObj);

        ListNote listNote = new ListNote();
        listNote.setTitle("aaaa");
        ListNoteElementRegistry lnre = new ListNoteElementRegistry();
        listNote.setElements(lnre);
        listNote.getElements().addListNoteElement("aaaa");

        JpaListNoteRepository repository = (JpaListNoteRepository) PersistenceContext.repositories().listNotes();
        List<ListNote> jpaList = repository.all();

        SearchListNoteController instance = new SearchListNoteController(null);

        List<ListNote> newList = new ArrayList<>();

        newList = instance.searchListNoteByTime(calendar, calendar2);

        assertEquals(jpaList, newList);

    }

}
