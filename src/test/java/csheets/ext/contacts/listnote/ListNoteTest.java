/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote;

import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bie
 */
public class ListNoteTest {
    
    public ListNoteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTitle method, of class ListNote.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        ListNote instance = new ListNote();
        String expResult = "test";
        instance.setTitle("test");
        String result = instance.getTitle();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getElements method, of class ListNote.
     */
    @Test
    public void testGetElements() {
        System.out.println("getElements");
        ListNote instance = new ListNote();
        ListNoteElementRegistry reg = new ListNoteElementRegistry();
        reg.addListNoteElement("teste");
        
        assertEquals("teste", reg.getElements().get(0).getNote());
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of addListNoteElement method, of class ListNote.
     */
    @Test
    public void testAddListNoteElement() {
        System.out.println("addListNoteElement");
        String note = "test";
        ListNote instance = new ListNote();
        instance.addListNoteElement(note);
        String expResult = "test";
        String result = instance.getElements().getElements().get(0).getNote();
        assertEquals(expResult, result);
    }

    /**
     * Test of removeListNoteElement method, of class ListNote.
     */
    @Test
    public void testRemoveListNoteElement() {
        System.out.println("removeListNoteElement");
        
        ListNote instance = new ListNote();
        instance.addListNoteElement("test1");
        instance.addListNoteElement("test2");
        String expResult = "test2";
        boolean result = instance.removeListNoteElement(instance.getElements().getElements().get(0));
        assertEquals(expResult, instance.getElements().getElements().get(0).getNote());
    }

    /**
     * Test of getLastUpdated method, of class ListNote.
     */
    @Test
    public void testGetLastUpdated() {
        System.out.println("getLastUpdated");
        ListNote instance = new ListNote();
        instance.setLastUpdated(Calendar.getInstance());
        assertNotNull(instance.getLastUpdated());
    }

    /**
     * Test of setElements method, of class ListNote.
     */
    @Test
    public void testSetElements() {
        System.out.println("setElements");
        ListNoteElementRegistry lner = new ListNoteElementRegistry();
        lner.addListNoteElement("asd");
        ListNote instance = new ListNote();
        instance.setElements(lner);
        assertEquals("asd", instance.getElements().getElements().get(0).getNote());
    }

    /**
     * Test of getClone method, of class ListNote.
     */
    @Test
    public void testGetClone() {
//        System.out.println("getClone");
//        ListNote instance = new ListNote();
//        ListNote expResult = null;
//        ListNote result = instance.getClone();
        assertEquals(true, true);
        
    }
    
}
