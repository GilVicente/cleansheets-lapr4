/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bie
 */
public class ListNoteElementTest {
    
    public ListNoteElementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isChecked method, of class ListNoteElement.
     */
    @Test
    public void testIsChecked() {
        System.out.println("isChecked");
        ListNoteElement instance = new ListNoteElement();
        boolean expResult = false;
        boolean result = instance.isChecked();
        assertEquals(expResult, result);
    }

    /**
     * Test of check method, of class ListNoteElement.
     */
    @Test
    public void testCheck() {
        System.out.println("check");
        ListNoteElement instance = new ListNoteElement();
        boolean expResult = true;
        boolean result = instance.check();
        assertEquals(expResult, instance.isChecked());
    }

    /**
     * Test of uncheck method, of class ListNoteElement.
     */
    @Test
    public void testUncheck() {
        System.out.println("uncheck");
        ListNoteElement instance = new ListNoteElement();
        instance.check();
        instance.uncheck();
        boolean expResult = false;
        boolean result = instance.isChecked();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ListNoteElement.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = null;
        ListNoteElement instance = new ListNoteElement("asd");
        ListNoteElement instance2 = new ListNoteElement("asd");
        boolean result = instance.equals(instance2);
        assertEquals(true, result);
        
    }
    
}
