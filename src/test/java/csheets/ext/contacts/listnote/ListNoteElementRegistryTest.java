/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bie
 */
public class ListNoteElementRegistryTest {
    
    public ListNoteElementRegistryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getElements method, of class ListNoteElementRegistry.
     */
    @Test
    public void testGetElements() {
        System.out.println("getElements");
        ListNoteElementRegistry instance = new ListNoteElementRegistry();
        instance.addListNoteElement("1");
        List<ListNoteElement> l = new ArrayList<>();
        l.add(new ListNoteElement("1"));
        assertEquals(instance.getElements(), l);
    }

    /**
     * Test of addListNoteElement method, of class ListNoteElementRegistry.
     */
    @Test
    public void testAddListNoteElement() {
        System.out.println("addListNoteElement");
        String note = "";
        ListNoteElementRegistry instance = new ListNoteElementRegistry();
        instance.addListNoteElement("test");
        assertEquals("test", instance.getElements().get(0).getNote());
    }

    /**
     * Test of removeListNoteElement method, of class ListNoteElementRegistry.
     */

    /**
     * Test of removeListNoteElement method, of class ListNoteElementRegistry.
     */
    @Test
    public void testRemoveListNoteElement_String() {
        System.out.println("removeListNoteElement");
        String noteToRemove = "asd";
        ListNoteElementRegistry instance = new ListNoteElementRegistry();
        instance.addListNoteElement("asd");
        instance.addListNoteElement("test");
        instance.removeListNoteElement("asd");
        assertTrue(!instance.getElements().contains(new ListNoteElement("asd")));
    }
    
}
