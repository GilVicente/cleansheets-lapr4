/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.outbox;

import csheets.core.Workbook;
import csheets.ext.emailConfiguration.EmailConfiguration;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Class used to update the email outbox
 * @author hugoc
 */
public class OutboxUpdateCenterTest {
    
    public OutboxUpdateCenterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of updateListByWorkbook method, of class OutboxUpdateCenter.
     */
    @Test
    public void testupdateListByWorkbook() {
        System.out.println("updateList");
        JList jl = new JList();
        OutboxUpdateCenter instance = new OutboxUpdateCenter();
        EmailConfiguration e= new EmailConfiguration("name");
        Workbook wb = new Workbook();
        wb.addOutbox(e);
        instance.updateListByWorkbook(jl, wb);
        boolean expResult = true;
        boolean result = (jl.getModel().getElementAt(0).equals(e));
        assertEquals(expResult, result);
    }
    
}
