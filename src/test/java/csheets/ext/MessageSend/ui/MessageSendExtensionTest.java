/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.MessageSend.ui;

import csheets.CleanSheets;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago
 */
public class MessageSendExtensionTest {
    
    public MessageSendExtensionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUIExtension method, of class MessageSendExtension.
     */
    @Test
    public void testGetUIExtension() {
        System.out.println("getUIExtension");
        CleanSheets app=new CleanSheets();
        UIController uiController = new UIController(app);
        MessageSendExtension instance = new MessageSendExtension();
        UIExtension expResult = new MessageUIExtension(instance, uiController);
        UIExtension result = instance.getUIExtension(uiController);
        
    }
    
}
