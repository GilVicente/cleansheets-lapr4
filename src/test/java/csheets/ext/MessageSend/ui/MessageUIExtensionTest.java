/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.MessageSend.ui;

import csheets.CleanSheets;
import csheets.ui.ctrl.UIController;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago
 */
public class MessageUIExtensionTest {
    
    public MessageUIExtensionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIcon method, of class MessageUIExtension.
     */
    @Test
    public void testGetIcon() {
        System.out.println("getIcon");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        MessageSendExtension extension = new MessageSendExtension();
        MessageUIExtension instance = new MessageUIExtension(extension, uiController);
        Icon expResult = null;
        Icon result = instance.getIcon();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMenu method, of class MessageUIExtension.
     */
    @Test
    public void testGetMenu() {
        System.out.println("getMenu");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        MessageSendExtension extension = new MessageSendExtension();
        MessageUIExtension instance = new MessageUIExtension(extension, uiController);
        JMenu expResult = instance.getMenu();
        JMenu result = instance.getMenu();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSideBar method, of class MessageUIExtension.
     */
    @Test
    public void testGetSideBar() {
        System.out.println("getSideBar");
        CleanSheets app = new CleanSheets();
        UIController uiController = new UIController(app);
        MessageSendExtension extension = new MessageSendExtension();
        MessageUIExtension instance = new MessageUIExtension(extension, uiController);
        JComponent expResult = instance.getSideBar();
        JComponent result = instance.getSideBar();
        assertEquals(expResult, result);
    }
    
}
