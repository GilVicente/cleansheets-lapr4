/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.window.secure.comunication;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author José Santos (1140921)
 */
public class SecureCommunicationTest {

    public SecureCommunicationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of encrypt method, of class SecureCommunication.
     */
    @Test
    public void testEncrypt() throws Exception {
        System.out.println("encrypt");
        String message = "This should be encrypted";
        byte[] data = message.getBytes();
        SecretKey key = SecureCommunication.generateKey();
        System.out.println("-->"+key.toString());
        String result = SecureCommunication.encrypt(data, key);
        byte[] encrypted = result.getBytes();
        String expResult = SecureCommunication.decrypt(encrypted,key);
        assertEquals(expResult, message);
    }

}
