/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.persistence.inmemory;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.TemporaryVariable;
import csheets.core.Value;
import csheets.core.Workbook;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gil
 */
public class TemporaryVariableRepositoryTest {
    
    public TemporaryVariableRepositoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class TemporaryVariableRepository.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Value expResult = new Value(19);
        String var = "_var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        // Create a cell
        Cell c1 = ss.getCell(0, 1);
        // Creates tempVarRepositorie
        TemporaryVariableRepository tempVarRep = new TemporaryVariableRepository();
        // Adds var to rep 
        tempVarRep.add(ss, c1, var, expResult);
        // Gets that same var
        TemporaryVariable tempVar = tempVarRep.getVariable(ss, c1, var);
        
        //If tempVar.getValue() equals my value variable then the variable was correctly added
        Value result = tempVar.getValue();
        
        assertEquals(expResult, result);
    }

}
