/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.TemporaryVariable;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.persistence.inmemory.TemporaryVariableRepository;
import csheets.core.formula.util.ExpressionVisitor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gil
 */
public class TempVariableRefTest {
    
    public TempVariableRefTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of evaluate method, of class TempVariableRef.
     */
    @Test
    public void testEvaluate() {
        System.out.println("evaluate");
        Value value = new Value(19);
        String var = "_var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        // Create a cell
        Cell c1 = ss.getCell(0, 1);
        
        TemporaryVariable tempVar = new TemporaryVariable(ss, c1, var, value);
        
        TempVariableRef instance = new TempVariableRef(ss, c1, var);
        
        Value result = instance.evaluate();
        System.out.println(result.toString());
        
        
    }

    /**
     * Test of getTempVariable method, of class TempVariableRef.
     */
    @Test
    public void testGetTempVariable() {
        System.out.println("getTempVariable");
        Value value = new Value(19);
        String var = "_var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        // Create a cell
        Cell c1 = ss.getCell(0, 1);
        
        // Creates tempVarRepositorie
        TemporaryVariableRepository tempVarRep = new TemporaryVariableRepository();
        // Adds var to rep 
        tempVarRep.add(ss, c1, var, value);
        // Gets that same var
        TemporaryVariable expResult = tempVarRep.getVariable(ss, c1, var);
        
        // Creates TempVariableRef and gets the TemporaryVariable
        TempVariableRef instance = new TempVariableRef(ss, c1, var);
        TemporaryVariable result = instance.getTempVariable();
        
        assertEquals(expResult, result);
    }
    
}
