/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.ExcelExpressionCompiler;
import csheets.core.formula.compiler.FormulaCompilationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gil
 */
public class AssignTest {

    public AssignTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of applyTo method, of class Assign.
     */
    @Test
    public void testApplyTo() throws Exception {
        System.out.println("applyTo");
        Assign instance = new Assign();

        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        // Create two cells
        Cell c1 = ss.getCell(0, 1);
        Cell c2 = ss.getCell(0, 2);

        ExcelExpressionCompiler compiler = new ExcelExpressionCompiler();

        // Create two expression's
        Expression leftOperand = new CellReference(c1);
        Expression rightOperand = compiler.compile(c2, "=10+9");

        // Verifies result
        Value result = instance.applyTo(leftOperand, rightOperand);
        Value expResult = new Value("19");
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of getIdentifier method, of class Assign.
     */
    @Test
    public void testGetIdentifier() {
        System.out.println("getIdentifier");
        Assign instance = new Assign();
        String expResult = ":=";
        String result = instance.getIdentifier();
        assertEquals(expResult, result);
    }

    @Test
    public void testIdentifier() throws IllegalValueTypeException, FormulaCompilationException {
        System.out.println("Identifier");
        Assign instance = new Assign();

        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        // Create two cells
        Cell a1 = ss.getCell(0, 1);
        Cell a2 = ss.getCell(0, 2);
        Cell c2 = ss.getCell(2, 1);
        ExcelExpressionCompiler compiler = new ExcelExpressionCompiler();

        // Create two expression's
        Expression leftOperand = new CellReference(a1);
        Expression rightOperand = compiler.compile(a2, "={C2:=10+9}");
        Value result = instance.applyTo(leftOperand, rightOperand);
        Value expResult = new Value("19");
        assertEquals(expResult.toString(), result.toString());

    }

    @Test
    public void testattribution() throws IllegalValueTypeException, FormulaCompilationException {
        System.out.println("Identifier");
        Assign instance = new Assign();

        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        // Create two cells
        Cell a1 = ss.getCell(0, 1);
        Cell a2 = ss.getCell(0, 2);
        Cell c2 = ss.getCell(2, 1);
        ExcelExpressionCompiler compiler = new ExcelExpressionCompiler();

        // Create two expression's
        Expression leftOperand = new CellReference(a1);
        Expression rightOperand = compiler.compile(a2, "={C2:=10+9}");
        Value expResult = new Value("19");
        instance.applyTo(leftOperand, rightOperand);

        String result = c2.getValue().toString();
        assertEquals(expResult.toString(), result);
    }

    /**
     * Test of getOperandValueType method, of class Assign.
     */
    @Test
    public void testGetOperandValueType() {
        System.out.println("getOperandValueType");
        Assign instance = new Assign();
        Value.Type expResult = Value.Type.UNDEFINED;
        Value.Type result = instance.getOperandValueType();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Assign.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Assign instance = new Assign();
        String expResult = ":=";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testBlockInstruction() throws Exception {
        System.out.println("BlockInstruction");
        Assign instance = new Assign();

        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        // Create two cells
        Cell c1 = ss.getCell(0, 0);
        Cell c2 = ss.getCell(0, 1);
        c1.setContent("2");
        c2.setContent("3");
        ExcelExpressionCompiler compiler = new ExcelExpressionCompiler();

        // Create two expression's
        Expression leftOperand = new CellReference(c1);
        Expression rightOperand = compiler.compile(c2, "={1+2;sum(A1;A2);3+4}");

        // Verifies result
        Value result = instance.applyTo(leftOperand, rightOperand);
        Value expResult = new Value("7");
        assertEquals(expResult.toString(), result.toString());
    }

    @Test
    public void testBlockInstruction1() throws Exception {
        System.out.println("BlockInstruction");
        Assign instance = new Assign();

        // Create a workbook with12 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        // Create two cells
        Cell a1 = ss.getCell(0, 0);
        Cell a2 = ss.getCell(0, 1);
        a1.setContent("1");
        a2.setContent("2");
        ExcelExpressionCompiler compiler = new ExcelExpressionCompiler();

        // Create two expression's
        Expression leftOperand = new CellReference(a1);

        Expression rightOperand = compiler.compile(a2, "=FOR{A1:=1;A1<10;A2:=A2+A1;A1:=A1+1}");

        int expResult = 2;
        for (int i = 1; i < 10; i++) {
            expResult = expResult + i;
        }
//        System.out.println("->" + j);

        // Verifies result
        Value result = instance.applyTo(leftOperand, rightOperand);
        assertEquals(String.valueOf(expResult), a2.getContent().toString());
    }

}
