/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.GlobalVariable;
import csheets.core.Spreadsheet;
import csheets.core.TemporaryVariable;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.persistence.inmemory.GlobalVariableRepository;
import csheets.core.formula.persistence.inmemory.TemporaryVariableRepository;
import csheets.core.formula.util.ExpressionVisitor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Lacerda
 */
public class GlobalVariableRefTest {

    public GlobalVariableRefTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of evaluate method, of class TempVariableRef.
     */
    @Test
    public void testEvaluate() {
        System.out.println("evaluate");
        Value value = new Value(19);
        String var = "@var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
     

        GlobalVariable globalVar = new GlobalVariable(ss, var, value);

        GlobalVariableRef instance = new  GlobalVariableRef(ss, var);

        Value result = instance.evaluate();
        System.out.println(result.toString());

    }

    /**
     * Test of getGlobalVariable method, of class GlobalVariableRef.
     */
    @Test
    public void testgetGlobalVariable() {
        System.out.println("getGlobalVariable");
        Value value = new Value(19);
        String var = "_var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);

        // Creates tempVarRepositorie
        GlobalVariableRepository globalVarRep = new GlobalVariableRepository();
        // Adds var to rep 
        globalVarRep.add(ss, var, value);
        // Gets that same var
        GlobalVariable expResult = globalVarRep.getVariable(ss, var);

        // Creates TempVariableRef and gets the TemporaryVariable
        GlobalVariableRef instance = new GlobalVariableRef(ss, var);
        GlobalVariable result = instance.getGlobalVariable();

        assertEquals(expResult, result);
    
}
}