/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.FunctionCall;
import csheets.core.formula.FunctionParameter;
import csheets.core.formula.Literal;
import csheets.core.formula.compiler.ExcelExpressionCompiler;
import csheets.core.formula.compiler.ExcelExpressionCompilerTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JOSENUNO
 */
public class EvalTest {
    
    public EvalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of applyTo method, of class Eval.
     */
    @Test
    public void testApplyTo() throws Exception {
        System.out.println("applyTo");
        ExcelExpressionCompiler excel = new ExcelExpressionCompiler();
        int expResult = 5;
        int result = excel.compile(null, "=2+3").evaluate().toNumber().intValue();
        assertEquals(expResult, result);
        String str = excel.compile(null, "=\"B\"&\"1\"").evaluate().toString();
        assertEquals("B1", str);
    }
    
}
