/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.util;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francisco
 */
public class CurrencyRateTest {

    public CurrencyRateTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetDollar() {
        System.out.println("setDollar");
        BigDecimal d = new BigDecimal("1.5");
        CurrencyRate instance = new CurrencyRate();
        instance.setDollar(d);
        BigDecimal d2 = new BigDecimal("1.5");
        assertEquals(d, d2);

    }

    /**
     * Test of setPound method, of class CurrencyRate.
     */
    @Test
    public void testSetPound() {
        System.out.println("setPound");
        BigDecimal p = new BigDecimal("3");
        CurrencyRate instance = new CurrencyRate();
        instance.setPound(p);
        BigDecimal p2 = new BigDecimal("3");
        assertEquals(p, p2);
    }

    /**
     * Test of setEuro method, of class CurrencyRate.
     */
    @Test
    public void testSetEuro() {
        System.out.println("setEuro");
        BigDecimal e = new BigDecimal("0.5");
        CurrencyRate instance = new CurrencyRate();
        instance.setEuro(e);
        BigDecimal e2 = new BigDecimal("0.5");
        assertEquals(e, e2);
    }

    /**
     * Test of getDollar method, of class CurrencyRate.
     */
    @Test
    public void testGetDollar() {
        System.out.println("getDollar");
        BigDecimal d = new BigDecimal("2.1");
        CurrencyRate instance = new CurrencyRate();
        instance.setDollar(d);
        assertEquals(d, instance.getDollar());
    }

    /**
     * Test of getEuro method, of class CurrencyRate.
     */
    @Test
    public void testGetEuro() {
        System.out.println("getEuro");
        BigDecimal d = new BigDecimal("1.3");
        CurrencyRate instance = new CurrencyRate();
        instance.setEuro(d);
        assertEquals(d, instance.getEuro());
    }
    
    /**
     * Test of getPound method, of class CurrencyRate.
     */
    @Test
    public void testGetPound() {
        System.out.println("getPound");
        BigDecimal d = new BigDecimal("5");
        CurrencyRate instance = new CurrencyRate();
        instance.setPound(d);
        assertEquals(d, instance.getPound());
        
    }

}
