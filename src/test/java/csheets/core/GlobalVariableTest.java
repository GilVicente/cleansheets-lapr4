/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago Lacerda
 */
public class GlobalVariableTest {
    
    public GlobalVariableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class GlobalVariable.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Value value = new Value(19);
        String expResult = "@var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);

        
        GlobalVariable instance = new  GlobalVariable(ss, expResult, value);
        
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getValue method, of class  GlobalVariable.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        Value expResult = new Value(19);
        String name = "@var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);

     
         GlobalVariable instance = new  GlobalVariable(ss, name, expResult);
        
        Value result = instance.getValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSheet method, of class  GlobalVariable.
     */
    @Test
    public void testGetSheet() {
        System.out.println("getSheet");
        Value value = new Value(19);
        String name = "@var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet expResult = wb.getSpreadsheet(0);
      
        GlobalVariable instance = new  GlobalVariable(expResult, name, value);
        
        Spreadsheet result = instance.getSheet();
        assertEquals(expResult, result);
    }

  

    /**
     * Test of setValue method, of class  GlobalVariable.
     */
    @Test
    public void testSetValue() {
        System.out.println("setValue");
        Value value = new Value(19);
        String name = "_var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
        
        GlobalVariable instance = new  GlobalVariable(ss, name, value);
        
        //new value
        Value val = new Value(20);
        instance.setValue(val);
        Value result = instance.getValue();
        
        Value expResult = new Value(20);
        assertEquals(result, expResult);
    }

    /**
     * Test of toString method, of class  GlobalVariable.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Value value = new Value(19);
        String name = "_var";
        // Create a workbook with1 sheets
        Workbook wb = new Workbook(1);
        // Create a spreadsheet
        Spreadsheet ss = wb.getSpreadsheet(0);
       
        GlobalVariable instance = new  GlobalVariable(ss, name, value);
        
        String expResult = "_var";
        String result = instance.toString();
        
        assertEquals(expResult, result);
    }
    
}
