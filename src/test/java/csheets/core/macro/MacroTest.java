/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.macro;

import csheets.core.IllegalValueTypeException;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.util.ExpressionVisitor;
import csheets.ext.macros.MacroWorkbookRepo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Antonio Soutinho changed by Patrícia Monteiro (1140807)
 */
public class MacroTest {
    
    public MacroTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    
    /**
     * Test of getName method, of class Macro.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Workbook workbook = new Workbook();
        workbook.addSpreadsheet();
        MacroWorkbookRepo repo=new MacroWorkbookRepo(workbook.getSpreadsheet(0));
        Macro instance = new Macro(repo, "teste()", workbook.getSpreadsheet(0));
        String expResult = "teste";
        String result = instance.getName();
        assertEquals(expResult, result);
       
    }

        

    /**
     * Test of run method, of class Macro.
     */
    @Test
    public void testAddLineAndRun() throws Exception {
        System.out.println("run");
        
        Workbook workbook = new Workbook();
        workbook.addSpreadsheet();
        MacroWorkbookRepo repo=new MacroWorkbookRepo(workbook.getSpreadsheet(0));
        Macro instance = new Macro(repo, "teste()", workbook.getSpreadsheet(0));
        
        String line1 = "_a:=2";
	String line2 = "A1:=_a";
        instance.addLine(line1);
        instance.addLine(line2);       
        
        Value expResult = new Value (2);
        Value result = instance.run();
        assertEquals(expResult.toString(), result.toString());
       
    }  
    
   
    

    /**
     * Test of parameters method, of class Macro.
     */
    @Test
    public void testParameters() {
        System.out.println("parameters");
        
        Workbook workbook = new Workbook();
        workbook.addSpreadsheet();
        MacroWorkbookRepo repo=new MacroWorkbookRepo(workbook.getSpreadsheet(0));
        Macro instance = new Macro(repo, "teste(2;3)", workbook.getSpreadsheet(0));
        
        String[] expResult = {"2","3"};
        String[] result = instance.parameters();
        assertArrayEquals(expResult, result);
        
    }

    

  



    
   

    
    
   

}
