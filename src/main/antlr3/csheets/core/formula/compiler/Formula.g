grammar Formula;

options {
	language=Java;
	output=AST;
}	
    
   
@parser::header {
package csheets.core.formula.compiler;
}
 
@lexer::header {
package csheets.core.formula.compiler;
}

// Alter code generation so catch-clauses get replace with 
// this action.
@rulecatch {
	catch (RecognitionException e) {
		reportError(e);
		throw e; 
	}
}

@members {
	protected void mismatch(IntStream input, int ttype, BitSet follow)
		throws RecognitionException 
	{
    	throw new MismatchedTokenException(ttype, input);
	}

	public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow)
		throws RecognitionException 
	{
		throw e; 
	}
	
	@Override
  	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
    	throw new MismatchedTokenException(ttype, input);
 	}
}

expression
	: EQ! (comparison|current_cell) EOF!
        | CONVERT! currency_expression EOF!
	;


currency_expression
	: currency_info LBRA^ concatenation_currency RBRA^
        ;




concatenation_currency
	: arithmetic_low_currency   (( PLUS^ | MINUS^ ) arithmetic_low_currency)*
        ;

arithmetic_low_currency
	: arithmetic_highest_currency  (( MULTI^ | DIV^ ) arithmetic_highest_currency)*
        ;

arithmetic_highest_currency
	: ( MINUS^ )? atom_currency
        ;

atom_currency
	: currency_value
        ;





currency_value
        : NUMBER currency_type
        ;

currency_type
        : EURO | DOLLAR | POUND
        ;

EURO : '€';
DOLLAR : '$';
POUND : '£';

currency_info
        : EUROINF | DOLLARINF | POUNDINF
        ;

EUROINF : 'euro' | 'EURO';
DOLLARINF : 'dollar' | 'DOLLAR';
POUNDINF : 'pound' | 'POUND';




current_cell 
	: TOKEN_CELL^ ( EQ | NEQ | GT | LT | LTEQ | GTEQ ) NUMBER;
	
TOKEN_CELL :'_cell';


eval
	: EVAL LPAR concatenation RPAR
	;



block_instructions
        :(FOR^)? LBRA! comparison (SEMI! comparison)* RBRA!  
        ;

attribution
        : reference ASSIGN^ comparison
        ;

comparison
	: concatenation
		( ( EQ^ | NEQ^ | GT^ | LT^ | LTEQ^ | GTEQ^ ) concatenation )? | attribution | block_instructions
	;

concatenation
	: arithmetic_lowest
		( AMP^ arithmetic_lowest )*
	;

arithmetic_lowest
	:	arithmetic_low
		( ( PLUS^ | MINUS^ ) arithmetic_low )*
	;

arithmetic_low
	:	arithmetic_medium
		( ( MULTI^ | DIV^ ) arithmetic_medium )*
	;

arithmetic_medium
	:	arithmetic_high
		( POWER^ arithmetic_high )?
	;

arithmetic_high
	:	arithmetic_highest ( PERCENT^ )?
	;

arithmetic_highest
	:	( MINUS^ )? atom
	;



atom
	:	function_call
	|	reference
	|	literal
	|	LPAR! comparison RPAR!
	;

function_call
	:	FUNCTION^ LPAR! 
		( comparison ( SEMI! comparison )* )?
		RPAR!
	;

reference
	:	CELL_REF
		( ( COLON^ ) CELL_REF )?
	|	TEMPVAR
	|	GLOVAR
	;

literal
	:	NUMBER
	|	STRING
	;
TEMPVAR
	:
		UNDERSCORE LETTER+
	;	

GLOVAR
        :       AT LETTER+
        ;
        


fragment LETTER: ('a'..'z'|'A'..'Z') ;

  
FOR     : 'FOR' ;

EVAL : 'EVAL' ;

RUNSCRIPT : 'RUNSCRIPT';

FUNCTION : 
	  ( LETTER )+ 
	;
	 
 
CELL_REF
	:
		( ABS )? LETTER ( LETTER )?
		( ABS )? ( DIGIT )+
	;



/* String literals, i.e. anything inside the delimiters */

STRING	:	QUOT 
		(options {greedy=false;}:.)*
		QUOT  { setText(getText().substring(1, getText().length()-1)); }
	;  	

QUOT: '"' 
	;

/* Numeric literals */
NUMBER: ( DIGIT )+ ( COMMA ( DIGIT )+ )? ;

fragment 
DIGIT : '0'..'9' ;


/* Comparison operators */
EQ      : '=' ;
NEQ     : '<>' ;
LTEQ	: '<=' ;
GTEQ	: '>=' ;
GT      : '>' ;
LT      : '<' ;

/* Text operators */
AMP	: '&' ;

/* Arithmetic operators */
PLUS	: '+' ;
MINUS	: '-' ;
MULTI	: '*' ;
DIV     : '/' ;
POWER	: '^' ;
PERCENT : '%' ;

/* Reference operators */
fragment ABS : '$' ;
fragment EXCL:  '!'  ;
COLON	: ':' ;
ASSIGN  : ':''=';
CONVERT : '#';
UNDERSCORE : '_';
AT      : '@' ;	
 
/* Miscellaneous operators */
COMMA	: ','   ;
SEMI	: ';'   ;
LPAR	: '('   ;
RPAR	: ')'   ; 
LBRA    : '{'   ;
RBRA    : '}'   ;

/* White-space (ignored) */
WS: ( ' '
	| '\r' '\n'
	| '\n'
	| '\t'
	) {$channel=HIDDEN;}
	;
