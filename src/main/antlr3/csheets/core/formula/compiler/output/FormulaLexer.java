// $ANTLR 3.5.1 C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g 2016-06-22 00:38:10

package csheets.core.formula.compiler;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FormulaLexer extends Lexer {
	public static final int EOF=-1;
	public static final int ABS=4;
	public static final int AMP=5;
	public static final int ASSIGN=6;
	public static final int AT=7;
	public static final int CELL_REF=8;
	public static final int COLON=9;
	public static final int COMMA=10;
	public static final int CONVERT=11;
	public static final int DIGIT=12;
	public static final int DIV=13;
	public static final int DOLLAR=14;
	public static final int DOLLARINF=15;
	public static final int EQ=16;
	public static final int EURO=17;
	public static final int EUROINF=18;
	public static final int EVAL=19;
	public static final int EXCL=20;
	public static final int FOR=21;
	public static final int FUNCTION=22;
	public static final int GLOVAR=23;
	public static final int GT=24;
	public static final int GTEQ=25;
	public static final int LBRA=26;
	public static final int LETTER=27;
	public static final int LPAR=28;
	public static final int LT=29;
	public static final int LTEQ=30;
	public static final int MINUS=31;
	public static final int MULTI=32;
	public static final int NEQ=33;
	public static final int NUMBER=34;
	public static final int PERCENT=35;
	public static final int PLUS=36;
	public static final int POUND=37;
	public static final int POUNDINF=38;
	public static final int POWER=39;
	public static final int QUOT=40;
	public static final int RBRA=41;
	public static final int RPAR=42;
	public static final int SEMI=43;
	public static final int STRING=44;
	public static final int TEMPVAR=45;
	public static final int TOKEN_CELL=46;
	public static final int UNDERSCORE=47;
	public static final int WS=48;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public FormulaLexer() {} 
	public FormulaLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public FormulaLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g"; }

	// $ANTLR start "EURO"
	public final void mEURO() throws RecognitionException {
		try {
			int _type = EURO;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:86:6: ( '�' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:86:8: '�'
			{
			match('\u20AC'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EURO"

	// $ANTLR start "DOLLAR"
	public final void mDOLLAR() throws RecognitionException {
		try {
			int _type = DOLLAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:87:8: ( '$' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:87:10: '$'
			{
			match('$'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOLLAR"

	// $ANTLR start "POUND"
	public final void mPOUND() throws RecognitionException {
		try {
			int _type = POUND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:88:7: ( '�' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:88:9: '�'
			{
			match('\u00A3'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POUND"

	// $ANTLR start "EUROINF"
	public final void mEUROINF() throws RecognitionException {
		try {
			int _type = EUROINF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:94:9: ( 'euro' | 'EURO' )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='e') ) {
				alt1=1;
			}
			else if ( (LA1_0=='E') ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:94:11: 'euro'
					{
					match("euro"); 

					}
					break;
				case 2 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:94:20: 'EURO'
					{
					match("EURO"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EUROINF"

	// $ANTLR start "DOLLARINF"
	public final void mDOLLARINF() throws RecognitionException {
		try {
			int _type = DOLLARINF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:95:11: ( 'dollar' | 'DOLLAR' )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0=='d') ) {
				alt2=1;
			}
			else if ( (LA2_0=='D') ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:95:13: 'dollar'
					{
					match("dollar"); 

					}
					break;
				case 2 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:95:24: 'DOLLAR'
					{
					match("DOLLAR"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOLLARINF"

	// $ANTLR start "POUNDINF"
	public final void mPOUNDINF() throws RecognitionException {
		try {
			int _type = POUNDINF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:96:10: ( 'pound' | 'POUND' )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='p') ) {
				alt3=1;
			}
			else if ( (LA3_0=='P') ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:96:12: 'pound'
					{
					match("pound"); 

					}
					break;
				case 2 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:96:22: 'POUND'
					{
					match("POUND"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POUNDINF"

	// $ANTLR start "TOKEN_CELL"
	public final void mTOKEN_CELL() throws RecognitionException {
		try {
			int _type = TOKEN_CELL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:104:12: ( '_cell' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:104:13: '_cell'
			{
			match("_cell"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TOKEN_CELL"

	// $ANTLR start "TEMPVAR"
	public final void mTEMPVAR() throws RecognitionException {
		try {
			int _type = TEMPVAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:179:2: ( UNDERSCORE ( LETTER )+ )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:180:3: UNDERSCORE ( LETTER )+
			{
			mUNDERSCORE(); 

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:180:14: ( LETTER )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= 'A' && LA4_0 <= 'Z')||(LA4_0 >= 'a' && LA4_0 <= 'z')) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
					{
					if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TEMPVAR"

	// $ANTLR start "GLOVAR"
	public final void mGLOVAR() throws RecognitionException {
		try {
			int _type = GLOVAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:184:9: ( AT ( LETTER )+ )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:184:17: AT ( LETTER )+
			{
			mAT(); 

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:184:20: ( LETTER )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( ((LA5_0 >= 'A' && LA5_0 <= 'Z')||(LA5_0 >= 'a' && LA5_0 <= 'z')) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
					{
					if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GLOVAR"

	// $ANTLR start "LETTER"
	public final void mLETTER() throws RecognitionException {
		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:189:16: ( ( 'a' .. 'z' | 'A' .. 'Z' ) )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LETTER"

	// $ANTLR start "FOR"
	public final void mFOR() throws RecognitionException {
		try {
			int _type = FOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:192:9: ( 'FOR' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:192:11: 'FOR'
			{
			match("FOR"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FOR"

	// $ANTLR start "EVAL"
	public final void mEVAL() throws RecognitionException {
		try {
			int _type = EVAL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:194:6: ( 'EVAL' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:194:8: 'EVAL'
			{
			match("EVAL"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EVAL"

	// $ANTLR start "FUNCTION"
	public final void mFUNCTION() throws RecognitionException {
		try {
			int _type = FUNCTION;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:196:10: ( ( LETTER )+ )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:197:4: ( LETTER )+
			{
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:197:4: ( LETTER )+
			int cnt6=0;
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( ((LA6_0 >= 'A' && LA6_0 <= 'Z')||(LA6_0 >= 'a' && LA6_0 <= 'z')) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
					{
					if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt6 >= 1 ) break loop6;
					EarlyExitException eee = new EarlyExitException(6, input);
					throw eee;
				}
				cnt6++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUNCTION"

	// $ANTLR start "CELL_REF"
	public final void mCELL_REF() throws RecognitionException {
		try {
			int _type = CELL_REF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:202:2: ( ( ABS )? LETTER ( LETTER )? ( ABS )? ( DIGIT )+ )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:203:3: ( ABS )? LETTER ( LETTER )? ( ABS )? ( DIGIT )+
			{
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:203:3: ( ABS )?
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0=='$') ) {
				alt7=1;
			}
			switch (alt7) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
					{
					if ( input.LA(1)=='$' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			mLETTER(); 

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:203:19: ( LETTER )?
			int alt8=2;
			int LA8_0 = input.LA(1);
			if ( ((LA8_0 >= 'A' && LA8_0 <= 'Z')||(LA8_0 >= 'a' && LA8_0 <= 'z')) ) {
				alt8=1;
			}
			switch (alt8) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
					{
					if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:204:3: ( ABS )?
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0=='$') ) {
				alt9=1;
			}
			switch (alt9) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
					{
					if ( input.LA(1)=='$' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:204:12: ( DIGIT )+
			int cnt10=0;
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( ((LA10_0 >= '0' && LA10_0 <= '9')) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt10 >= 1 ) break loop10;
					EarlyExitException eee = new EarlyExitException(10, input);
					throw eee;
				}
				cnt10++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CELL_REF"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:211:8: ( QUOT ( options {greedy=false; } : . )* QUOT )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:211:10: QUOT ( options {greedy=false; } : . )* QUOT
			{
			mQUOT(); 

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:212:3: ( options {greedy=false; } : . )*
			loop11:
			while (true) {
				int alt11=2;
				int LA11_0 = input.LA(1);
				if ( (LA11_0=='\"') ) {
					alt11=2;
				}
				else if ( ((LA11_0 >= '\u0000' && LA11_0 <= '!')||(LA11_0 >= '#' && LA11_0 <= '\uFFFF')) ) {
					alt11=1;
				}

				switch (alt11) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:212:28: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop11;
				}
			}

			mQUOT(); 

			 setText(getText().substring(1, getText().length()-1)); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "QUOT"
	public final void mQUOT() throws RecognitionException {
		try {
			int _type = QUOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:216:5: ( '\"' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:216:7: '\"'
			{
			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "QUOT"

	// $ANTLR start "NUMBER"
	public final void mNUMBER() throws RecognitionException {
		try {
			int _type = NUMBER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:220:7: ( ( DIGIT )+ ( COMMA ( DIGIT )+ )? )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:220:9: ( DIGIT )+ ( COMMA ( DIGIT )+ )?
			{
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:220:9: ( DIGIT )+
			int cnt12=0;
			loop12:
			while (true) {
				int alt12=2;
				int LA12_0 = input.LA(1);
				if ( ((LA12_0 >= '0' && LA12_0 <= '9')) ) {
					alt12=1;
				}

				switch (alt12) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt12 >= 1 ) break loop12;
					EarlyExitException eee = new EarlyExitException(12, input);
					throw eee;
				}
				cnt12++;
			}

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:220:20: ( COMMA ( DIGIT )+ )?
			int alt14=2;
			int LA14_0 = input.LA(1);
			if ( (LA14_0==',') ) {
				alt14=1;
			}
			switch (alt14) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:220:22: COMMA ( DIGIT )+
					{
					mCOMMA(); 

					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:220:28: ( DIGIT )+
					int cnt13=0;
					loop13:
					while (true) {
						int alt13=2;
						int LA13_0 = input.LA(1);
						if ( ((LA13_0 >= '0' && LA13_0 <= '9')) ) {
							alt13=1;
						}

						switch (alt13) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt13 >= 1 ) break loop13;
							EarlyExitException eee = new EarlyExitException(13, input);
							throw eee;
						}
						cnt13++;
					}

					}
					break;

			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NUMBER"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:224:7: ( '0' .. '9' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:227:9: ( '=' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:227:11: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "NEQ"
	public final void mNEQ() throws RecognitionException {
		try {
			int _type = NEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:228:9: ( '<>' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:228:11: '<>'
			{
			match("<>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEQ"

	// $ANTLR start "LTEQ"
	public final void mLTEQ() throws RecognitionException {
		try {
			int _type = LTEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:229:6: ( '<=' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:229:8: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LTEQ"

	// $ANTLR start "GTEQ"
	public final void mGTEQ() throws RecognitionException {
		try {
			int _type = GTEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:230:6: ( '>=' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:230:8: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GTEQ"

	// $ANTLR start "GT"
	public final void mGT() throws RecognitionException {
		try {
			int _type = GT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:231:9: ( '>' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:231:11: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GT"

	// $ANTLR start "LT"
	public final void mLT() throws RecognitionException {
		try {
			int _type = LT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:232:9: ( '<' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:232:11: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LT"

	// $ANTLR start "AMP"
	public final void mAMP() throws RecognitionException {
		try {
			int _type = AMP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:235:5: ( '&' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:235:7: '&'
			{
			match('&'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AMP"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:238:6: ( '+' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:238:8: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:239:7: ( '-' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:239:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MULTI"
	public final void mMULTI() throws RecognitionException {
		try {
			int _type = MULTI;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:240:7: ( '*' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:240:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULTI"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:241:9: ( '/' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:241:11: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "POWER"
	public final void mPOWER() throws RecognitionException {
		try {
			int _type = POWER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:242:7: ( '^' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:242:9: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "POWER"

	// $ANTLR start "PERCENT"
	public final void mPERCENT() throws RecognitionException {
		try {
			int _type = PERCENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:243:9: ( '%' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:243:11: '%'
			{
			match('%'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PERCENT"

	// $ANTLR start "ABS"
	public final void mABS() throws RecognitionException {
		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:246:14: ( '$' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:246:16: '$'
			{
			match('$'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ABS"

	// $ANTLR start "EXCL"
	public final void mEXCL() throws RecognitionException {
		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:247:14: ( '!' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:247:17: '!'
			{
			match('!'); 
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXCL"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:248:7: ( ':' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:248:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "ASSIGN"
	public final void mASSIGN() throws RecognitionException {
		try {
			int _type = ASSIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:249:9: ( ':' '=' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:249:11: ':' '='
			{
			match(':'); 
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASSIGN"

	// $ANTLR start "CONVERT"
	public final void mCONVERT() throws RecognitionException {
		try {
			int _type = CONVERT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:250:9: ( '#' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:250:11: '#'
			{
			match('#'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONVERT"

	// $ANTLR start "UNDERSCORE"
	public final void mUNDERSCORE() throws RecognitionException {
		try {
			int _type = UNDERSCORE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:251:12: ( '_' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:251:14: '_'
			{
			match('_'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UNDERSCORE"

	// $ANTLR start "AT"
	public final void mAT() throws RecognitionException {
		try {
			int _type = AT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:252:9: ( '@' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:252:11: '@'
			{
			match('@'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AT"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:255:7: ( ',' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:255:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "SEMI"
	public final void mSEMI() throws RecognitionException {
		try {
			int _type = SEMI;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:256:6: ( ';' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:256:8: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMI"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			int _type = LPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:257:6: ( '(' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:257:8: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAR"

	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			int _type = RPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:258:6: ( ')' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:258:8: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAR"

	// $ANTLR start "LBRA"
	public final void mLBRA() throws RecognitionException {
		try {
			int _type = LBRA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:259:9: ( '{' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:259:11: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LBRA"

	// $ANTLR start "RBRA"
	public final void mRBRA() throws RecognitionException {
		try {
			int _type = RBRA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:260:9: ( '}' )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:260:11: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RBRA"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:263:3: ( ( ' ' | '\\r' '\\n' | '\\n' | '\\t' ) )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:263:5: ( ' ' | '\\r' '\\n' | '\\n' | '\\t' )
			{
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:263:5: ( ' ' | '\\r' '\\n' | '\\n' | '\\t' )
			int alt15=4;
			switch ( input.LA(1) ) {
			case ' ':
				{
				alt15=1;
				}
				break;
			case '\r':
				{
				alt15=2;
				}
				break;
			case '\n':
				{
				alt15=3;
				}
				break;
			case '\t':
				{
				alt15=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 15, 0, input);
				throw nvae;
			}
			switch (alt15) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:263:7: ' '
					{
					match(' '); 
					}
					break;
				case 2 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:264:4: '\\r' '\\n'
					{
					match('\r'); 
					match('\n'); 
					}
					break;
				case 3 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:265:4: '\\n'
					{
					match('\n'); 
					}
					break;
				case 4 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:266:4: '\\t'
					{
					match('\t'); 
					}
					break;

			}

			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:8: ( EURO | DOLLAR | POUND | EUROINF | DOLLARINF | POUNDINF | TOKEN_CELL | TEMPVAR | GLOVAR | FOR | EVAL | FUNCTION | CELL_REF | STRING | QUOT | NUMBER | EQ | NEQ | LTEQ | GTEQ | GT | LT | AMP | PLUS | MINUS | MULTI | DIV | POWER | PERCENT | COLON | ASSIGN | CONVERT | UNDERSCORE | AT | COMMA | SEMI | LPAR | RPAR | LBRA | RBRA | WS )
		int alt16=41;
		alt16 = dfa16.predict(input);
		switch (alt16) {
			case 1 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:10: EURO
				{
				mEURO(); 

				}
				break;
			case 2 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:15: DOLLAR
				{
				mDOLLAR(); 

				}
				break;
			case 3 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:22: POUND
				{
				mPOUND(); 

				}
				break;
			case 4 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:28: EUROINF
				{
				mEUROINF(); 

				}
				break;
			case 5 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:36: DOLLARINF
				{
				mDOLLARINF(); 

				}
				break;
			case 6 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:46: POUNDINF
				{
				mPOUNDINF(); 

				}
				break;
			case 7 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:55: TOKEN_CELL
				{
				mTOKEN_CELL(); 

				}
				break;
			case 8 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:66: TEMPVAR
				{
				mTEMPVAR(); 

				}
				break;
			case 9 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:74: GLOVAR
				{
				mGLOVAR(); 

				}
				break;
			case 10 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:81: FOR
				{
				mFOR(); 

				}
				break;
			case 11 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:85: EVAL
				{
				mEVAL(); 

				}
				break;
			case 12 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:90: FUNCTION
				{
				mFUNCTION(); 

				}
				break;
			case 13 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:99: CELL_REF
				{
				mCELL_REF(); 

				}
				break;
			case 14 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:108: STRING
				{
				mSTRING(); 

				}
				break;
			case 15 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:115: QUOT
				{
				mQUOT(); 

				}
				break;
			case 16 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:120: NUMBER
				{
				mNUMBER(); 

				}
				break;
			case 17 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:127: EQ
				{
				mEQ(); 

				}
				break;
			case 18 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:130: NEQ
				{
				mNEQ(); 

				}
				break;
			case 19 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:134: LTEQ
				{
				mLTEQ(); 

				}
				break;
			case 20 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:139: GTEQ
				{
				mGTEQ(); 

				}
				break;
			case 21 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:144: GT
				{
				mGT(); 

				}
				break;
			case 22 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:147: LT
				{
				mLT(); 

				}
				break;
			case 23 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:150: AMP
				{
				mAMP(); 

				}
				break;
			case 24 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:154: PLUS
				{
				mPLUS(); 

				}
				break;
			case 25 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:159: MINUS
				{
				mMINUS(); 

				}
				break;
			case 26 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:165: MULTI
				{
				mMULTI(); 

				}
				break;
			case 27 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:171: DIV
				{
				mDIV(); 

				}
				break;
			case 28 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:175: POWER
				{
				mPOWER(); 

				}
				break;
			case 29 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:181: PERCENT
				{
				mPERCENT(); 

				}
				break;
			case 30 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:189: COLON
				{
				mCOLON(); 

				}
				break;
			case 31 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:195: ASSIGN
				{
				mASSIGN(); 

				}
				break;
			case 32 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:202: CONVERT
				{
				mCONVERT(); 

				}
				break;
			case 33 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:210: UNDERSCORE
				{
				mUNDERSCORE(); 

				}
				break;
			case 34 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:221: AT
				{
				mAT(); 

				}
				break;
			case 35 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:224: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 36 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:230: SEMI
				{
				mSEMI(); 

				}
				break;
			case 37 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:235: LPAR
				{
				mLPAR(); 

				}
				break;
			case 38 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:240: RPAR
				{
				mRPAR(); 

				}
				break;
			case 39 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:245: LBRA
				{
				mLBRA(); 

				}
				break;
			case 40 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:250: RBRA
				{
				mRBRA(); 

				}
				break;
			case 41 :
				// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:1:255: WS
				{
				mWS(); 

				}
				break;

		}
	}


	protected DFA16 dfa16 = new DFA16(this);
	static final String DFA16_eotS =
		"\2\uffff\1\43\1\uffff\6\46\1\57\1\61\2\46\1\64\2\uffff\1\70\1\72\7\uffff"+
		"\1\74\12\uffff\1\46\1\uffff\7\46\1\60\4\uffff\1\46\11\uffff\7\46\1\60"+
		"\1\116\2\117\1\120\4\46\1\60\3\uffff\2\46\2\130\1\131\2\132\3\uffff";
	static final String DFA16_eofS =
		"\133\uffff";
	static final String DFA16_minS =
		"\1\11\1\uffff\1\101\1\uffff\6\44\2\101\2\44\1\0\2\uffff\2\75\7\uffff\1"+
		"\75\12\uffff\1\44\1\uffff\7\44\1\145\4\uffff\1\44\11\uffff\1\157\1\117"+
		"\1\114\1\154\1\114\1\156\1\116\1\154\4\101\1\141\1\101\1\144\1\104\1\154"+
		"\3\uffff\1\162\1\122\5\101\3\uffff";
	static final String DFA16_maxS =
		"\1\u20ac\1\uffff\1\172\1\uffff\12\172\1\uffff\2\uffff\1\76\1\75\7\uffff"+
		"\1\75\12\uffff\1\162\1\uffff\1\71\1\122\1\101\1\154\1\114\1\165\1\125"+
		"\1\145\4\uffff\1\122\11\uffff\1\157\1\117\1\114\1\154\1\114\1\156\1\116"+
		"\1\154\4\172\1\141\1\101\1\144\1\104\1\154\3\uffff\1\162\1\122\5\172\3"+
		"\uffff";
	static final String DFA16_acceptS =
		"\1\uffff\1\1\1\uffff\1\3\13\uffff\1\20\1\21\2\uffff\1\27\1\30\1\31\1\32"+
		"\1\33\1\34\1\35\1\uffff\1\40\1\43\1\44\1\45\1\46\1\47\1\50\1\51\1\2\1"+
		"\15\1\uffff\1\14\10\uffff\1\41\1\10\1\42\1\11\1\uffff\1\17\1\16\1\22\1"+
		"\23\1\26\1\24\1\25\1\37\1\36\21\uffff\1\12\1\4\1\13\7\uffff\1\6\1\7\1"+
		"\5";
	static final String DFA16_specialS =
		"\16\uffff\1\0\114\uffff}>";
	static final String[] DFA16_transitionS = {
			"\2\42\2\uffff\1\42\22\uffff\1\42\1\uffff\1\16\1\33\1\2\1\31\1\23\1\uffff"+
			"\1\36\1\37\1\26\1\24\1\34\1\25\1\uffff\1\27\12\17\1\32\1\35\1\21\1\20"+
			"\1\22\1\uffff\1\13\3\15\1\7\1\5\1\14\11\15\1\11\12\15\3\uffff\1\30\1"+
			"\12\1\uffff\3\15\1\6\1\4\12\15\1\10\12\15\1\40\1\uffff\1\41\45\uffff"+
			"\1\3\u2008\uffff\1\1",
			"",
			"\32\44\6\uffff\32\44",
			"",
			"\1\44\13\uffff\12\44\7\uffff\32\47\6\uffff\24\47\1\45\5\47",
			"\1\44\13\uffff\12\44\7\uffff\24\47\1\50\1\51\4\47\6\uffff\32\47",
			"\1\44\13\uffff\12\44\7\uffff\32\47\6\uffff\16\47\1\52\13\47",
			"\1\44\13\uffff\12\44\7\uffff\16\47\1\53\13\47\6\uffff\32\47",
			"\1\44\13\uffff\12\44\7\uffff\32\47\6\uffff\16\47\1\54\13\47",
			"\1\44\13\uffff\12\44\7\uffff\16\47\1\55\13\47\6\uffff\32\47",
			"\32\60\6\uffff\2\60\1\56\27\60",
			"\32\62\6\uffff\32\62",
			"\1\44\13\uffff\12\44\7\uffff\16\47\1\63\13\47\6\uffff\32\47",
			"\1\44\13\uffff\12\44\7\uffff\32\47\6\uffff\32\47",
			"\0\65",
			"",
			"",
			"\1\67\1\66",
			"\1\71",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\73",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\44\13\uffff\12\44\70\uffff\1\75",
			"",
			"\1\44\13\uffff\12\44",
			"\1\44\13\uffff\12\44\30\uffff\1\76",
			"\1\44\13\uffff\12\44\7\uffff\1\77",
			"\1\44\13\uffff\12\44\62\uffff\1\100",
			"\1\44\13\uffff\12\44\22\uffff\1\101",
			"\1\44\13\uffff\12\44\73\uffff\1\102",
			"\1\44\13\uffff\12\44\33\uffff\1\103",
			"\1\104",
			"",
			"",
			"",
			"",
			"\1\44\13\uffff\12\44\30\uffff\1\105",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\106",
			"\1\107",
			"\1\110",
			"\1\111",
			"\1\112",
			"\1\113",
			"\1\114",
			"\1\115",
			"\32\46\6\uffff\32\46",
			"\32\46\6\uffff\32\46",
			"\32\46\6\uffff\32\46",
			"\32\46\6\uffff\32\46",
			"\1\121",
			"\1\122",
			"\1\123",
			"\1\124",
			"\1\125",
			"",
			"",
			"",
			"\1\126",
			"\1\127",
			"\32\46\6\uffff\32\46",
			"\32\46\6\uffff\32\46",
			"\32\60\6\uffff\32\60",
			"\32\46\6\uffff\32\46",
			"\32\46\6\uffff\32\46",
			"",
			"",
			""
	};

	static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
	static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
	static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
	static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
	static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
	static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
	static final short[][] DFA16_transition;

	static {
		int numStates = DFA16_transitionS.length;
		DFA16_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
		}
	}

	protected class DFA16 extends DFA {

		public DFA16(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 16;
			this.eot = DFA16_eot;
			this.eof = DFA16_eof;
			this.min = DFA16_min;
			this.max = DFA16_max;
			this.accept = DFA16_accept;
			this.special = DFA16_special;
			this.transition = DFA16_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( EURO | DOLLAR | POUND | EUROINF | DOLLARINF | POUNDINF | TOKEN_CELL | TEMPVAR | GLOVAR | FOR | EVAL | FUNCTION | CELL_REF | STRING | QUOT | NUMBER | EQ | NEQ | LTEQ | GTEQ | GT | LT | AMP | PLUS | MINUS | MULTI | DIV | POWER | PERCENT | COLON | ASSIGN | CONVERT | UNDERSCORE | AT | COMMA | SEMI | LPAR | RPAR | LBRA | RBRA | WS );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA16_14 = input.LA(1);
						s = -1;
						if ( ((LA16_14 >= '\u0000' && LA16_14 <= '\uFFFF')) ) {s = 53;}
						else s = 52;
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 16, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
