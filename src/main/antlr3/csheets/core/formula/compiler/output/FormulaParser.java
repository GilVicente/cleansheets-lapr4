// $ANTLR 3.5.1 C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g 2016-06-22 00:38:10

package csheets.core.formula.compiler;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class FormulaParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ABS", "AMP", "ASSIGN", "AT", 
		"CELL_REF", "COLON", "COMMA", "CONVERT", "DIGIT", "DIV", "DOLLAR", "DOLLARINF", 
		"EQ", "EURO", "EUROINF", "EVAL", "EXCL", "FOR", "FUNCTION", "GLOVAR", 
		"GT", "GTEQ", "LBRA", "LETTER", "LPAR", "LT", "LTEQ", "MINUS", "MULTI", 
		"NEQ", "NUMBER", "PERCENT", "PLUS", "POUND", "POUNDINF", "POWER", "QUOT", 
		"RBRA", "RPAR", "SEMI", "STRING", "TEMPVAR", "TOKEN_CELL", "UNDERSCORE", 
		"WS"
	};
	public static final int EOF=-1;
	public static final int ABS=4;
	public static final int AMP=5;
	public static final int ASSIGN=6;
	public static final int AT=7;
	public static final int CELL_REF=8;
	public static final int COLON=9;
	public static final int COMMA=10;
	public static final int CONVERT=11;
	public static final int DIGIT=12;
	public static final int DIV=13;
	public static final int DOLLAR=14;
	public static final int DOLLARINF=15;
	public static final int EQ=16;
	public static final int EURO=17;
	public static final int EUROINF=18;
	public static final int EVAL=19;
	public static final int EXCL=20;
	public static final int FOR=21;
	public static final int FUNCTION=22;
	public static final int GLOVAR=23;
	public static final int GT=24;
	public static final int GTEQ=25;
	public static final int LBRA=26;
	public static final int LETTER=27;
	public static final int LPAR=28;
	public static final int LT=29;
	public static final int LTEQ=30;
	public static final int MINUS=31;
	public static final int MULTI=32;
	public static final int NEQ=33;
	public static final int NUMBER=34;
	public static final int PERCENT=35;
	public static final int PLUS=36;
	public static final int POUND=37;
	public static final int POUNDINF=38;
	public static final int POWER=39;
	public static final int QUOT=40;
	public static final int RBRA=41;
	public static final int RPAR=42;
	public static final int SEMI=43;
	public static final int STRING=44;
	public static final int TEMPVAR=45;
	public static final int TOKEN_CELL=46;
	public static final int UNDERSCORE=47;
	public static final int WS=48;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FormulaParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FormulaParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return FormulaParser.tokenNames; }
	@Override public String getGrammarFileName() { return "C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g"; }


		protected void mismatch(IntStream input, int ttype, BitSet follow)
			throws RecognitionException 
		{
	    	throw new MismatchedTokenException(ttype, input);
		}

		public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow)
			throws RecognitionException 
		{
			throw e; 
		}
		
		@Override
	  	protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
	    	throw new MismatchedTokenException(ttype, input);
	 	}


	public static class expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expression"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:45:1: expression : ( EQ ! ( comparison | block_instructions | current_cell ) EOF !| CONVERT ! currency_expression EOF !);
	public final FormulaParser.expression_return expression() throws RecognitionException {
		FormulaParser.expression_return retval = new FormulaParser.expression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token EQ1=null;
		Token EOF5=null;
		Token CONVERT6=null;
		Token EOF8=null;
		ParserRuleReturnScope comparison2 =null;
		ParserRuleReturnScope block_instructions3 =null;
		ParserRuleReturnScope current_cell4 =null;
		ParserRuleReturnScope currency_expression7 =null;

		Object EQ1_tree=null;
		Object EOF5_tree=null;
		Object CONVERT6_tree=null;
		Object EOF8_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:46:2: ( EQ ! ( comparison | block_instructions | current_cell ) EOF !| CONVERT ! currency_expression EOF !)
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==EQ) ) {
				alt2=1;
			}
			else if ( (LA2_0==CONVERT) ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:46:4: EQ ! ( comparison | block_instructions | current_cell ) EOF !
					{
					root_0 = (Object)adaptor.nil();


					EQ1=(Token)match(input,EQ,FOLLOW_EQ_in_expression70); 
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:46:8: ( comparison | block_instructions | current_cell )
					int alt1=3;
					switch ( input.LA(1) ) {
					case CELL_REF:
					case FUNCTION:
					case GLOVAR:
					case LPAR:
					case MINUS:
					case NUMBER:
					case STRING:
					case TEMPVAR:
						{
						alt1=1;
						}
						break;
					case FOR:
					case LBRA:
						{
						alt1=2;
						}
						break;
					case TOKEN_CELL:
						{
						alt1=3;
						}
						break;
					default:
						NoViableAltException nvae =
							new NoViableAltException("", 1, 0, input);
						throw nvae;
					}
					switch (alt1) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:46:9: comparison
							{
							pushFollow(FOLLOW_comparison_in_expression74);
							comparison2=comparison();
							state._fsp--;

							adaptor.addChild(root_0, comparison2.getTree());

							}
							break;
						case 2 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:46:20: block_instructions
							{
							pushFollow(FOLLOW_block_instructions_in_expression76);
							block_instructions3=block_instructions();
							state._fsp--;

							adaptor.addChild(root_0, block_instructions3.getTree());

							}
							break;
						case 3 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:46:39: current_cell
							{
							pushFollow(FOLLOW_current_cell_in_expression78);
							current_cell4=current_cell();
							state._fsp--;

							adaptor.addChild(root_0, current_cell4.getTree());

							}
							break;

					}

					EOF5=(Token)match(input,EOF,FOLLOW_EOF_in_expression81); 
					}
					break;
				case 2 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:47:11: CONVERT ! currency_expression EOF !
					{
					root_0 = (Object)adaptor.nil();


					CONVERT6=(Token)match(input,CONVERT,FOLLOW_CONVERT_in_expression94); 
					pushFollow(FOLLOW_currency_expression_in_expression97);
					currency_expression7=currency_expression();
					state._fsp--;

					adaptor.addChild(root_0, currency_expression7.getTree());

					EOF8=(Token)match(input,EOF,FOLLOW_EOF_in_expression99); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expression"


	public static class currency_expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "currency_expression"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:51:1: currency_expression : currency_info LBRA ^ concatenation_currency RBRA ^;
	public final FormulaParser.currency_expression_return currency_expression() throws RecognitionException {
		FormulaParser.currency_expression_return retval = new FormulaParser.currency_expression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LBRA10=null;
		Token RBRA12=null;
		ParserRuleReturnScope currency_info9 =null;
		ParserRuleReturnScope concatenation_currency11 =null;

		Object LBRA10_tree=null;
		Object RBRA12_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:52:2: ( currency_info LBRA ^ concatenation_currency RBRA ^)
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:52:4: currency_info LBRA ^ concatenation_currency RBRA ^
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_currency_info_in_currency_expression112);
			currency_info9=currency_info();
			state._fsp--;

			adaptor.addChild(root_0, currency_info9.getTree());

			LBRA10=(Token)match(input,LBRA,FOLLOW_LBRA_in_currency_expression114); 
			LBRA10_tree = (Object)adaptor.create(LBRA10);
			root_0 = (Object)adaptor.becomeRoot(LBRA10_tree, root_0);

			pushFollow(FOLLOW_concatenation_currency_in_currency_expression117);
			concatenation_currency11=concatenation_currency();
			state._fsp--;

			adaptor.addChild(root_0, concatenation_currency11.getTree());

			RBRA12=(Token)match(input,RBRA,FOLLOW_RBRA_in_currency_expression119); 
			RBRA12_tree = (Object)adaptor.create(RBRA12);
			root_0 = (Object)adaptor.becomeRoot(RBRA12_tree, root_0);

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "currency_expression"


	public static class concatenation_currency_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "concatenation_currency"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:58:1: concatenation_currency : arithmetic_low_currency ( ( PLUS ^| MINUS ^) arithmetic_low_currency )* ;
	public final FormulaParser.concatenation_currency_return concatenation_currency() throws RecognitionException {
		FormulaParser.concatenation_currency_return retval = new FormulaParser.concatenation_currency_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token PLUS14=null;
		Token MINUS15=null;
		ParserRuleReturnScope arithmetic_low_currency13 =null;
		ParserRuleReturnScope arithmetic_low_currency16 =null;

		Object PLUS14_tree=null;
		Object MINUS15_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:59:2: ( arithmetic_low_currency ( ( PLUS ^| MINUS ^) arithmetic_low_currency )* )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:59:4: arithmetic_low_currency ( ( PLUS ^| MINUS ^) arithmetic_low_currency )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_arithmetic_low_currency_in_concatenation_currency141);
			arithmetic_low_currency13=arithmetic_low_currency();
			state._fsp--;

			adaptor.addChild(root_0, arithmetic_low_currency13.getTree());

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:59:30: ( ( PLUS ^| MINUS ^) arithmetic_low_currency )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0==MINUS||LA4_0==PLUS) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:59:31: ( PLUS ^| MINUS ^) arithmetic_low_currency
					{
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:59:31: ( PLUS ^| MINUS ^)
					int alt3=2;
					int LA3_0 = input.LA(1);
					if ( (LA3_0==PLUS) ) {
						alt3=1;
					}
					else if ( (LA3_0==MINUS) ) {
						alt3=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 3, 0, input);
						throw nvae;
					}

					switch (alt3) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:59:33: PLUS ^
							{
							PLUS14=(Token)match(input,PLUS,FOLLOW_PLUS_in_concatenation_currency148); 
							PLUS14_tree = (Object)adaptor.create(PLUS14);
							root_0 = (Object)adaptor.becomeRoot(PLUS14_tree, root_0);

							}
							break;
						case 2 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:59:41: MINUS ^
							{
							MINUS15=(Token)match(input,MINUS,FOLLOW_MINUS_in_concatenation_currency153); 
							MINUS15_tree = (Object)adaptor.create(MINUS15);
							root_0 = (Object)adaptor.becomeRoot(MINUS15_tree, root_0);

							}
							break;

					}

					pushFollow(FOLLOW_arithmetic_low_currency_in_concatenation_currency158);
					arithmetic_low_currency16=arithmetic_low_currency();
					state._fsp--;

					adaptor.addChild(root_0, arithmetic_low_currency16.getTree());

					}
					break;

				default :
					break loop4;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "concatenation_currency"


	public static class arithmetic_low_currency_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arithmetic_low_currency"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:62:1: arithmetic_low_currency : arithmetic_highest_currency ( ( MULTI ^| DIV ^) arithmetic_highest_currency )* ;
	public final FormulaParser.arithmetic_low_currency_return arithmetic_low_currency() throws RecognitionException {
		FormulaParser.arithmetic_low_currency_return retval = new FormulaParser.arithmetic_low_currency_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token MULTI18=null;
		Token DIV19=null;
		ParserRuleReturnScope arithmetic_highest_currency17 =null;
		ParserRuleReturnScope arithmetic_highest_currency20 =null;

		Object MULTI18_tree=null;
		Object DIV19_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:63:2: ( arithmetic_highest_currency ( ( MULTI ^| DIV ^) arithmetic_highest_currency )* )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:63:4: arithmetic_highest_currency ( ( MULTI ^| DIV ^) arithmetic_highest_currency )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_arithmetic_highest_currency_in_arithmetic_low_currency178);
			arithmetic_highest_currency17=arithmetic_highest_currency();
			state._fsp--;

			adaptor.addChild(root_0, arithmetic_highest_currency17.getTree());

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:63:33: ( ( MULTI ^| DIV ^) arithmetic_highest_currency )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==DIV||LA6_0==MULTI) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:63:34: ( MULTI ^| DIV ^) arithmetic_highest_currency
					{
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:63:34: ( MULTI ^| DIV ^)
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0==MULTI) ) {
						alt5=1;
					}
					else if ( (LA5_0==DIV) ) {
						alt5=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 5, 0, input);
						throw nvae;
					}

					switch (alt5) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:63:36: MULTI ^
							{
							MULTI18=(Token)match(input,MULTI,FOLLOW_MULTI_in_arithmetic_low_currency184); 
							MULTI18_tree = (Object)adaptor.create(MULTI18);
							root_0 = (Object)adaptor.becomeRoot(MULTI18_tree, root_0);

							}
							break;
						case 2 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:63:45: DIV ^
							{
							DIV19=(Token)match(input,DIV,FOLLOW_DIV_in_arithmetic_low_currency189); 
							DIV19_tree = (Object)adaptor.create(DIV19);
							root_0 = (Object)adaptor.becomeRoot(DIV19_tree, root_0);

							}
							break;

					}

					pushFollow(FOLLOW_arithmetic_highest_currency_in_arithmetic_low_currency194);
					arithmetic_highest_currency20=arithmetic_highest_currency();
					state._fsp--;

					adaptor.addChild(root_0, arithmetic_highest_currency20.getTree());

					}
					break;

				default :
					break loop6;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arithmetic_low_currency"


	public static class arithmetic_highest_currency_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arithmetic_highest_currency"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:66:1: arithmetic_highest_currency : ( MINUS ^)? atom_currency ;
	public final FormulaParser.arithmetic_highest_currency_return arithmetic_highest_currency() throws RecognitionException {
		FormulaParser.arithmetic_highest_currency_return retval = new FormulaParser.arithmetic_highest_currency_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token MINUS21=null;
		ParserRuleReturnScope atom_currency22 =null;

		Object MINUS21_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:67:2: ( ( MINUS ^)? atom_currency )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:67:4: ( MINUS ^)? atom_currency
			{
			root_0 = (Object)adaptor.nil();


			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:67:4: ( MINUS ^)?
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==MINUS) ) {
				alt7=1;
			}
			switch (alt7) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:67:6: MINUS ^
					{
					MINUS21=(Token)match(input,MINUS,FOLLOW_MINUS_in_arithmetic_highest_currency216); 
					MINUS21_tree = (Object)adaptor.create(MINUS21);
					root_0 = (Object)adaptor.becomeRoot(MINUS21_tree, root_0);

					}
					break;

			}

			pushFollow(FOLLOW_atom_currency_in_arithmetic_highest_currency222);
			atom_currency22=atom_currency();
			state._fsp--;

			adaptor.addChild(root_0, atom_currency22.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arithmetic_highest_currency"


	public static class atom_currency_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "atom_currency"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:70:1: atom_currency : currency_value ;
	public final FormulaParser.atom_currency_return atom_currency() throws RecognitionException {
		FormulaParser.atom_currency_return retval = new FormulaParser.atom_currency_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope currency_value23 =null;


		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:71:2: ( currency_value )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:71:4: currency_value
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_currency_value_in_atom_currency240);
			currency_value23=currency_value();
			state._fsp--;

			adaptor.addChild(root_0, currency_value23.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "atom_currency"


	public static class currency_value_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "currency_value"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:78:1: currency_value : NUMBER currency_type ;
	public final FormulaParser.currency_value_return currency_value() throws RecognitionException {
		FormulaParser.currency_value_return retval = new FormulaParser.currency_value_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NUMBER24=null;
		ParserRuleReturnScope currency_type25 =null;

		Object NUMBER24_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:79:9: ( NUMBER currency_type )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:79:11: NUMBER currency_type
			{
			root_0 = (Object)adaptor.nil();


			NUMBER24=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_currency_value269); 
			NUMBER24_tree = (Object)adaptor.create(NUMBER24);
			adaptor.addChild(root_0, NUMBER24_tree);

			pushFollow(FOLLOW_currency_type_in_currency_value271);
			currency_type25=currency_type();
			state._fsp--;

			adaptor.addChild(root_0, currency_type25.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "currency_value"


	public static class currency_type_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "currency_type"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:82:1: currency_type : ( EURO | DOLLAR | POUND );
	public final FormulaParser.currency_type_return currency_type() throws RecognitionException {
		FormulaParser.currency_type_return retval = new FormulaParser.currency_type_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set26=null;

		Object set26_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:83:9: ( EURO | DOLLAR | POUND )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
			{
			root_0 = (Object)adaptor.nil();


			set26=input.LT(1);
			if ( input.LA(1)==DOLLAR||input.LA(1)==EURO||input.LA(1)==POUND ) {
				input.consume();
				adaptor.addChild(root_0, (Object)adaptor.create(set26));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "currency_type"


	public static class currency_info_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "currency_info"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:90:1: currency_info : ( EUROINF | DOLLARINF | POUNDINF );
	public final FormulaParser.currency_info_return currency_info() throws RecognitionException {
		FormulaParser.currency_info_return retval = new FormulaParser.currency_info_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set27=null;

		Object set27_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:91:9: ( EUROINF | DOLLARINF | POUNDINF )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
			{
			root_0 = (Object)adaptor.nil();


			set27=input.LT(1);
			if ( input.LA(1)==DOLLARINF||input.LA(1)==EUROINF||input.LA(1)==POUNDINF ) {
				input.consume();
				adaptor.addChild(root_0, (Object)adaptor.create(set27));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "currency_info"


	public static class current_cell_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "current_cell"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:101:1: current_cell : TOKEN_CELL ^ ( EQ | NEQ | GT | LT | LTEQ | GTEQ ) NUMBER ;
	public final FormulaParser.current_cell_return current_cell() throws RecognitionException {
		FormulaParser.current_cell_return retval = new FormulaParser.current_cell_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token TOKEN_CELL28=null;
		Token set29=null;
		Token NUMBER30=null;

		Object TOKEN_CELL28_tree=null;
		Object set29_tree=null;
		Object NUMBER30_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:102:2: ( TOKEN_CELL ^ ( EQ | NEQ | GT | LT | LTEQ | GTEQ ) NUMBER )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:102:4: TOKEN_CELL ^ ( EQ | NEQ | GT | LT | LTEQ | GTEQ ) NUMBER
			{
			root_0 = (Object)adaptor.nil();


			TOKEN_CELL28=(Token)match(input,TOKEN_CELL,FOLLOW_TOKEN_CELL_in_current_cell415); 
			TOKEN_CELL28_tree = (Object)adaptor.create(TOKEN_CELL28);
			root_0 = (Object)adaptor.becomeRoot(TOKEN_CELL28_tree, root_0);

			set29=input.LT(1);
			if ( input.LA(1)==EQ||(input.LA(1) >= GT && input.LA(1) <= GTEQ)||(input.LA(1) >= LT && input.LA(1) <= LTEQ)||input.LA(1)==NEQ ) {
				input.consume();
				adaptor.addChild(root_0, (Object)adaptor.create(set29));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			NUMBER30=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_current_cell444); 
			NUMBER30_tree = (Object)adaptor.create(NUMBER30);
			adaptor.addChild(root_0, NUMBER30_tree);

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "current_cell"


	public static class eval_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "eval"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:107:1: eval : EVAL LPAR concatenation RPAR ;
	public final FormulaParser.eval_return eval() throws RecognitionException {
		FormulaParser.eval_return retval = new FormulaParser.eval_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token EVAL31=null;
		Token LPAR32=null;
		Token RPAR34=null;
		ParserRuleReturnScope concatenation33 =null;

		Object EVAL31_tree=null;
		Object LPAR32_tree=null;
		Object RPAR34_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:108:2: ( EVAL LPAR concatenation RPAR )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:108:4: EVAL LPAR concatenation RPAR
			{
			root_0 = (Object)adaptor.nil();


			EVAL31=(Token)match(input,EVAL,FOLLOW_EVAL_in_eval462); 
			EVAL31_tree = (Object)adaptor.create(EVAL31);
			adaptor.addChild(root_0, EVAL31_tree);

			LPAR32=(Token)match(input,LPAR,FOLLOW_LPAR_in_eval464); 
			LPAR32_tree = (Object)adaptor.create(LPAR32);
			adaptor.addChild(root_0, LPAR32_tree);

			pushFollow(FOLLOW_concatenation_in_eval466);
			concatenation33=concatenation();
			state._fsp--;

			adaptor.addChild(root_0, concatenation33.getTree());

			RPAR34=(Token)match(input,RPAR,FOLLOW_RPAR_in_eval468); 
			RPAR34_tree = (Object)adaptor.create(RPAR34);
			adaptor.addChild(root_0, RPAR34_tree);

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "eval"


	public static class block_instructions_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "block_instructions"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:111:1: block_instructions : ( FOR ^)? LBRA ! comparison ( SEMI ! comparison )* RBRA !;
	public final FormulaParser.block_instructions_return block_instructions() throws RecognitionException {
		FormulaParser.block_instructions_return retval = new FormulaParser.block_instructions_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token FOR35=null;
		Token LBRA36=null;
		Token SEMI38=null;
		Token RBRA40=null;
		ParserRuleReturnScope comparison37 =null;
		ParserRuleReturnScope comparison39 =null;

		Object FOR35_tree=null;
		Object LBRA36_tree=null;
		Object SEMI38_tree=null;
		Object RBRA40_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:112:9: ( ( FOR ^)? LBRA ! comparison ( SEMI ! comparison )* RBRA !)
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:112:10: ( FOR ^)? LBRA ! comparison ( SEMI ! comparison )* RBRA !
			{
			root_0 = (Object)adaptor.nil();


			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:112:10: ( FOR ^)?
			int alt8=2;
			int LA8_0 = input.LA(1);
			if ( (LA8_0==FOR) ) {
				alt8=1;
			}
			switch (alt8) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:112:11: FOR ^
					{
					FOR35=(Token)match(input,FOR,FOLLOW_FOR_in_block_instructions486); 
					FOR35_tree = (Object)adaptor.create(FOR35);
					root_0 = (Object)adaptor.becomeRoot(FOR35_tree, root_0);

					}
					break;

			}

			LBRA36=(Token)match(input,LBRA,FOLLOW_LBRA_in_block_instructions491); 
			pushFollow(FOLLOW_comparison_in_block_instructions494);
			comparison37=comparison();
			state._fsp--;

			adaptor.addChild(root_0, comparison37.getTree());

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:112:35: ( SEMI ! comparison )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==SEMI) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:112:36: SEMI ! comparison
					{
					SEMI38=(Token)match(input,SEMI,FOLLOW_SEMI_in_block_instructions497); 
					pushFollow(FOLLOW_comparison_in_block_instructions500);
					comparison39=comparison();
					state._fsp--;

					adaptor.addChild(root_0, comparison39.getTree());

					}
					break;

				default :
					break loop9;
				}
			}

			RBRA40=(Token)match(input,RBRA,FOLLOW_RBRA_in_block_instructions504); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "block_instructions"


	public static class attribution_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "attribution"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:115:1: attribution : reference ASSIGN ^ comparison ;
	public final FormulaParser.attribution_return attribution() throws RecognitionException {
		FormulaParser.attribution_return retval = new FormulaParser.attribution_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ASSIGN42=null;
		ParserRuleReturnScope reference41 =null;
		ParserRuleReturnScope comparison43 =null;

		Object ASSIGN42_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:116:9: ( reference ASSIGN ^ comparison )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:116:11: reference ASSIGN ^ comparison
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_reference_in_attribution532);
			reference41=reference();
			state._fsp--;

			adaptor.addChild(root_0, reference41.getTree());

			ASSIGN42=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_attribution534); 
			ASSIGN42_tree = (Object)adaptor.create(ASSIGN42);
			root_0 = (Object)adaptor.becomeRoot(ASSIGN42_tree, root_0);

			pushFollow(FOLLOW_comparison_in_attribution537);
			comparison43=comparison();
			state._fsp--;

			adaptor.addChild(root_0, comparison43.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "attribution"


	public static class comparison_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "comparison"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:119:1: comparison : ( concatenation ( ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation )? | attribution );
	public final FormulaParser.comparison_return comparison() throws RecognitionException {
		FormulaParser.comparison_return retval = new FormulaParser.comparison_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token EQ45=null;
		Token NEQ46=null;
		Token GT47=null;
		Token LT48=null;
		Token LTEQ49=null;
		Token GTEQ50=null;
		ParserRuleReturnScope concatenation44 =null;
		ParserRuleReturnScope concatenation51 =null;
		ParserRuleReturnScope attribution52 =null;

		Object EQ45_tree=null;
		Object NEQ46_tree=null;
		Object GT47_tree=null;
		Object LT48_tree=null;
		Object LTEQ49_tree=null;
		Object GTEQ50_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:120:2: ( concatenation ( ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation )? | attribution )
			int alt12=2;
			switch ( input.LA(1) ) {
			case FUNCTION:
			case LPAR:
			case MINUS:
			case NUMBER:
			case STRING:
				{
				alt12=1;
				}
				break;
			case CELL_REF:
				{
				switch ( input.LA(2) ) {
				case COLON:
					{
					int LA12_5 = input.LA(3);
					if ( (LA12_5==CELL_REF) ) {
						int LA12_7 = input.LA(4);
						if ( (LA12_7==EOF||LA12_7==AMP||LA12_7==DIV||LA12_7==EQ||(LA12_7 >= GT && LA12_7 <= GTEQ)||(LA12_7 >= LT && LA12_7 <= NEQ)||(LA12_7 >= PERCENT && LA12_7 <= PLUS)||LA12_7==POWER||(LA12_7 >= RBRA && LA12_7 <= SEMI)) ) {
							alt12=1;
						}
						else if ( (LA12_7==ASSIGN) ) {
							alt12=2;
						}

						else {
							int nvaeMark = input.mark();
							try {
								for (int nvaeConsume = 0; nvaeConsume < 4 - 1; nvaeConsume++) {
									input.consume();
								}
								NoViableAltException nvae =
									new NoViableAltException("", 12, 7, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 12, 5, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

					}
					break;
				case EOF:
				case AMP:
				case DIV:
				case EQ:
				case GT:
				case GTEQ:
				case LT:
				case LTEQ:
				case MINUS:
				case MULTI:
				case NEQ:
				case PERCENT:
				case PLUS:
				case POWER:
				case RBRA:
				case RPAR:
				case SEMI:
					{
					alt12=1;
					}
					break;
				case ASSIGN:
					{
					alt12=2;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
				}
				break;
			case TEMPVAR:
				{
				int LA12_3 = input.LA(2);
				if ( (LA12_3==EOF||LA12_3==AMP||LA12_3==DIV||LA12_3==EQ||(LA12_3 >= GT && LA12_3 <= GTEQ)||(LA12_3 >= LT && LA12_3 <= NEQ)||(LA12_3 >= PERCENT && LA12_3 <= PLUS)||LA12_3==POWER||(LA12_3 >= RBRA && LA12_3 <= SEMI)) ) {
					alt12=1;
				}
				else if ( (LA12_3==ASSIGN) ) {
					alt12=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case GLOVAR:
				{
				int LA12_4 = input.LA(2);
				if ( (LA12_4==EOF||LA12_4==AMP||LA12_4==DIV||LA12_4==EQ||(LA12_4 >= GT && LA12_4 <= GTEQ)||(LA12_4 >= LT && LA12_4 <= NEQ)||(LA12_4 >= PERCENT && LA12_4 <= PLUS)||LA12_4==POWER||(LA12_4 >= RBRA && LA12_4 <= SEMI)) ) {
					alt12=1;
				}
				else if ( (LA12_4==ASSIGN) ) {
					alt12=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 12, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 12, 0, input);
				throw nvae;
			}
			switch (alt12) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:120:4: concatenation ( ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation )?
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_concatenation_in_comparison555);
					concatenation44=concatenation();
					state._fsp--;

					adaptor.addChild(root_0, concatenation44.getTree());

					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:3: ( ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation )?
					int alt11=2;
					int LA11_0 = input.LA(1);
					if ( (LA11_0==EQ||(LA11_0 >= GT && LA11_0 <= GTEQ)||(LA11_0 >= LT && LA11_0 <= LTEQ)||LA11_0==NEQ) ) {
						alt11=1;
					}
					switch (alt11) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:5: ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^) concatenation
							{
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:5: ( EQ ^| NEQ ^| GT ^| LT ^| LTEQ ^| GTEQ ^)
							int alt10=6;
							switch ( input.LA(1) ) {
							case EQ:
								{
								alt10=1;
								}
								break;
							case NEQ:
								{
								alt10=2;
								}
								break;
							case GT:
								{
								alt10=3;
								}
								break;
							case LT:
								{
								alt10=4;
								}
								break;
							case LTEQ:
								{
								alt10=5;
								}
								break;
							case GTEQ:
								{
								alt10=6;
								}
								break;
							default:
								NoViableAltException nvae =
									new NoViableAltException("", 10, 0, input);
								throw nvae;
							}
							switch (alt10) {
								case 1 :
									// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:7: EQ ^
									{
									EQ45=(Token)match(input,EQ,FOLLOW_EQ_in_comparison563); 
									EQ45_tree = (Object)adaptor.create(EQ45);
									root_0 = (Object)adaptor.becomeRoot(EQ45_tree, root_0);

									}
									break;
								case 2 :
									// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:13: NEQ ^
									{
									NEQ46=(Token)match(input,NEQ,FOLLOW_NEQ_in_comparison568); 
									NEQ46_tree = (Object)adaptor.create(NEQ46);
									root_0 = (Object)adaptor.becomeRoot(NEQ46_tree, root_0);

									}
									break;
								case 3 :
									// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:20: GT ^
									{
									GT47=(Token)match(input,GT,FOLLOW_GT_in_comparison573); 
									GT47_tree = (Object)adaptor.create(GT47);
									root_0 = (Object)adaptor.becomeRoot(GT47_tree, root_0);

									}
									break;
								case 4 :
									// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:26: LT ^
									{
									LT48=(Token)match(input,LT,FOLLOW_LT_in_comparison578); 
									LT48_tree = (Object)adaptor.create(LT48);
									root_0 = (Object)adaptor.becomeRoot(LT48_tree, root_0);

									}
									break;
								case 5 :
									// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:32: LTEQ ^
									{
									LTEQ49=(Token)match(input,LTEQ,FOLLOW_LTEQ_in_comparison583); 
									LTEQ49_tree = (Object)adaptor.create(LTEQ49);
									root_0 = (Object)adaptor.becomeRoot(LTEQ49_tree, root_0);

									}
									break;
								case 6 :
									// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:40: GTEQ ^
									{
									GTEQ50=(Token)match(input,GTEQ,FOLLOW_GTEQ_in_comparison588); 
									GTEQ50_tree = (Object)adaptor.create(GTEQ50);
									root_0 = (Object)adaptor.becomeRoot(GTEQ50_tree, root_0);

									}
									break;

							}

							pushFollow(FOLLOW_concatenation_in_comparison593);
							concatenation51=concatenation();
							state._fsp--;

							adaptor.addChild(root_0, concatenation51.getTree());

							}
							break;

					}

					}
					break;
				case 2 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:121:67: attribution
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_attribution_in_comparison600);
					attribution52=attribution();
					state._fsp--;

					adaptor.addChild(root_0, attribution52.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "comparison"


	public static class concatenation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "concatenation"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:124:1: concatenation : arithmetic_lowest ( AMP ^ arithmetic_lowest )* ;
	public final FormulaParser.concatenation_return concatenation() throws RecognitionException {
		FormulaParser.concatenation_return retval = new FormulaParser.concatenation_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token AMP54=null;
		ParserRuleReturnScope arithmetic_lowest53 =null;
		ParserRuleReturnScope arithmetic_lowest55 =null;

		Object AMP54_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:125:2: ( arithmetic_lowest ( AMP ^ arithmetic_lowest )* )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:125:4: arithmetic_lowest ( AMP ^ arithmetic_lowest )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_arithmetic_lowest_in_concatenation611);
			arithmetic_lowest53=arithmetic_lowest();
			state._fsp--;

			adaptor.addChild(root_0, arithmetic_lowest53.getTree());

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:126:3: ( AMP ^ arithmetic_lowest )*
			loop13:
			while (true) {
				int alt13=2;
				int LA13_0 = input.LA(1);
				if ( (LA13_0==AMP) ) {
					alt13=1;
				}

				switch (alt13) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:126:5: AMP ^ arithmetic_lowest
					{
					AMP54=(Token)match(input,AMP,FOLLOW_AMP_in_concatenation617); 
					AMP54_tree = (Object)adaptor.create(AMP54);
					root_0 = (Object)adaptor.becomeRoot(AMP54_tree, root_0);

					pushFollow(FOLLOW_arithmetic_lowest_in_concatenation620);
					arithmetic_lowest55=arithmetic_lowest();
					state._fsp--;

					adaptor.addChild(root_0, arithmetic_lowest55.getTree());

					}
					break;

				default :
					break loop13;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "concatenation"


	public static class arithmetic_lowest_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arithmetic_lowest"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:129:1: arithmetic_lowest : arithmetic_low ( ( PLUS ^| MINUS ^) arithmetic_low )* ;
	public final FormulaParser.arithmetic_lowest_return arithmetic_lowest() throws RecognitionException {
		FormulaParser.arithmetic_lowest_return retval = new FormulaParser.arithmetic_lowest_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token PLUS57=null;
		Token MINUS58=null;
		ParserRuleReturnScope arithmetic_low56 =null;
		ParserRuleReturnScope arithmetic_low59 =null;

		Object PLUS57_tree=null;
		Object MINUS58_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:130:2: ( arithmetic_low ( ( PLUS ^| MINUS ^) arithmetic_low )* )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:130:4: arithmetic_low ( ( PLUS ^| MINUS ^) arithmetic_low )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_arithmetic_low_in_arithmetic_lowest634);
			arithmetic_low56=arithmetic_low();
			state._fsp--;

			adaptor.addChild(root_0, arithmetic_low56.getTree());

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:131:3: ( ( PLUS ^| MINUS ^) arithmetic_low )*
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==MINUS||LA15_0==PLUS) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:131:5: ( PLUS ^| MINUS ^) arithmetic_low
					{
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:131:5: ( PLUS ^| MINUS ^)
					int alt14=2;
					int LA14_0 = input.LA(1);
					if ( (LA14_0==PLUS) ) {
						alt14=1;
					}
					else if ( (LA14_0==MINUS) ) {
						alt14=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 14, 0, input);
						throw nvae;
					}

					switch (alt14) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:131:7: PLUS ^
							{
							PLUS57=(Token)match(input,PLUS,FOLLOW_PLUS_in_arithmetic_lowest642); 
							PLUS57_tree = (Object)adaptor.create(PLUS57);
							root_0 = (Object)adaptor.becomeRoot(PLUS57_tree, root_0);

							}
							break;
						case 2 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:131:15: MINUS ^
							{
							MINUS58=(Token)match(input,MINUS,FOLLOW_MINUS_in_arithmetic_lowest647); 
							MINUS58_tree = (Object)adaptor.create(MINUS58);
							root_0 = (Object)adaptor.becomeRoot(MINUS58_tree, root_0);

							}
							break;

					}

					pushFollow(FOLLOW_arithmetic_low_in_arithmetic_lowest652);
					arithmetic_low59=arithmetic_low();
					state._fsp--;

					adaptor.addChild(root_0, arithmetic_low59.getTree());

					}
					break;

				default :
					break loop15;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arithmetic_lowest"


	public static class arithmetic_low_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arithmetic_low"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:134:1: arithmetic_low : arithmetic_medium ( ( MULTI ^| DIV ^) arithmetic_medium )* ;
	public final FormulaParser.arithmetic_low_return arithmetic_low() throws RecognitionException {
		FormulaParser.arithmetic_low_return retval = new FormulaParser.arithmetic_low_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token MULTI61=null;
		Token DIV62=null;
		ParserRuleReturnScope arithmetic_medium60 =null;
		ParserRuleReturnScope arithmetic_medium63 =null;

		Object MULTI61_tree=null;
		Object DIV62_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:135:2: ( arithmetic_medium ( ( MULTI ^| DIV ^) arithmetic_medium )* )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:135:4: arithmetic_medium ( ( MULTI ^| DIV ^) arithmetic_medium )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_arithmetic_medium_in_arithmetic_low666);
			arithmetic_medium60=arithmetic_medium();
			state._fsp--;

			adaptor.addChild(root_0, arithmetic_medium60.getTree());

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:136:3: ( ( MULTI ^| DIV ^) arithmetic_medium )*
			loop17:
			while (true) {
				int alt17=2;
				int LA17_0 = input.LA(1);
				if ( (LA17_0==DIV||LA17_0==MULTI) ) {
					alt17=1;
				}

				switch (alt17) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:136:5: ( MULTI ^| DIV ^) arithmetic_medium
					{
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:136:5: ( MULTI ^| DIV ^)
					int alt16=2;
					int LA16_0 = input.LA(1);
					if ( (LA16_0==MULTI) ) {
						alt16=1;
					}
					else if ( (LA16_0==DIV) ) {
						alt16=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 16, 0, input);
						throw nvae;
					}

					switch (alt16) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:136:7: MULTI ^
							{
							MULTI61=(Token)match(input,MULTI,FOLLOW_MULTI_in_arithmetic_low674); 
							MULTI61_tree = (Object)adaptor.create(MULTI61);
							root_0 = (Object)adaptor.becomeRoot(MULTI61_tree, root_0);

							}
							break;
						case 2 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:136:16: DIV ^
							{
							DIV62=(Token)match(input,DIV,FOLLOW_DIV_in_arithmetic_low679); 
							DIV62_tree = (Object)adaptor.create(DIV62);
							root_0 = (Object)adaptor.becomeRoot(DIV62_tree, root_0);

							}
							break;

					}

					pushFollow(FOLLOW_arithmetic_medium_in_arithmetic_low684);
					arithmetic_medium63=arithmetic_medium();
					state._fsp--;

					adaptor.addChild(root_0, arithmetic_medium63.getTree());

					}
					break;

				default :
					break loop17;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arithmetic_low"


	public static class arithmetic_medium_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arithmetic_medium"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:139:1: arithmetic_medium : arithmetic_high ( POWER ^ arithmetic_high )? ;
	public final FormulaParser.arithmetic_medium_return arithmetic_medium() throws RecognitionException {
		FormulaParser.arithmetic_medium_return retval = new FormulaParser.arithmetic_medium_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token POWER65=null;
		ParserRuleReturnScope arithmetic_high64 =null;
		ParserRuleReturnScope arithmetic_high66 =null;

		Object POWER65_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:140:2: ( arithmetic_high ( POWER ^ arithmetic_high )? )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:140:4: arithmetic_high ( POWER ^ arithmetic_high )?
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_arithmetic_high_in_arithmetic_medium698);
			arithmetic_high64=arithmetic_high();
			state._fsp--;

			adaptor.addChild(root_0, arithmetic_high64.getTree());

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:141:3: ( POWER ^ arithmetic_high )?
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0==POWER) ) {
				alt18=1;
			}
			switch (alt18) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:141:5: POWER ^ arithmetic_high
					{
					POWER65=(Token)match(input,POWER,FOLLOW_POWER_in_arithmetic_medium704); 
					POWER65_tree = (Object)adaptor.create(POWER65);
					root_0 = (Object)adaptor.becomeRoot(POWER65_tree, root_0);

					pushFollow(FOLLOW_arithmetic_high_in_arithmetic_medium707);
					arithmetic_high66=arithmetic_high();
					state._fsp--;

					adaptor.addChild(root_0, arithmetic_high66.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arithmetic_medium"


	public static class arithmetic_high_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arithmetic_high"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:144:1: arithmetic_high : arithmetic_highest ( PERCENT ^)? ;
	public final FormulaParser.arithmetic_high_return arithmetic_high() throws RecognitionException {
		FormulaParser.arithmetic_high_return retval = new FormulaParser.arithmetic_high_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token PERCENT68=null;
		ParserRuleReturnScope arithmetic_highest67 =null;

		Object PERCENT68_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:145:2: ( arithmetic_highest ( PERCENT ^)? )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:145:4: arithmetic_highest ( PERCENT ^)?
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_arithmetic_highest_in_arithmetic_high721);
			arithmetic_highest67=arithmetic_highest();
			state._fsp--;

			adaptor.addChild(root_0, arithmetic_highest67.getTree());

			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:145:23: ( PERCENT ^)?
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==PERCENT) ) {
				alt19=1;
			}
			switch (alt19) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:145:25: PERCENT ^
					{
					PERCENT68=(Token)match(input,PERCENT,FOLLOW_PERCENT_in_arithmetic_high725); 
					PERCENT68_tree = (Object)adaptor.create(PERCENT68);
					root_0 = (Object)adaptor.becomeRoot(PERCENT68_tree, root_0);

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arithmetic_high"


	public static class arithmetic_highest_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arithmetic_highest"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:148:1: arithmetic_highest : ( MINUS ^)? atom ;
	public final FormulaParser.arithmetic_highest_return arithmetic_highest() throws RecognitionException {
		FormulaParser.arithmetic_highest_return retval = new FormulaParser.arithmetic_highest_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token MINUS69=null;
		ParserRuleReturnScope atom70 =null;

		Object MINUS69_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:149:2: ( ( MINUS ^)? atom )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:149:4: ( MINUS ^)? atom
			{
			root_0 = (Object)adaptor.nil();


			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:149:4: ( MINUS ^)?
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==MINUS) ) {
				alt20=1;
			}
			switch (alt20) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:149:6: MINUS ^
					{
					MINUS69=(Token)match(input,MINUS,FOLLOW_MINUS_in_arithmetic_highest742); 
					MINUS69_tree = (Object)adaptor.create(MINUS69);
					root_0 = (Object)adaptor.becomeRoot(MINUS69_tree, root_0);

					}
					break;

			}

			pushFollow(FOLLOW_atom_in_arithmetic_highest748);
			atom70=atom();
			state._fsp--;

			adaptor.addChild(root_0, atom70.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arithmetic_highest"


	public static class atom_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "atom"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:154:1: atom : ( function_call | reference | literal | LPAR ! comparison RPAR !);
	public final FormulaParser.atom_return atom() throws RecognitionException {
		FormulaParser.atom_return retval = new FormulaParser.atom_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token LPAR74=null;
		Token RPAR76=null;
		ParserRuleReturnScope function_call71 =null;
		ParserRuleReturnScope reference72 =null;
		ParserRuleReturnScope literal73 =null;
		ParserRuleReturnScope comparison75 =null;

		Object LPAR74_tree=null;
		Object RPAR76_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:155:2: ( function_call | reference | literal | LPAR ! comparison RPAR !)
			int alt21=4;
			switch ( input.LA(1) ) {
			case FUNCTION:
				{
				alt21=1;
				}
				break;
			case CELL_REF:
			case GLOVAR:
			case TEMPVAR:
				{
				alt21=2;
				}
				break;
			case NUMBER:
			case STRING:
				{
				alt21=3;
				}
				break;
			case LPAR:
				{
				alt21=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}
			switch (alt21) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:155:4: function_call
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_function_call_in_atom761);
					function_call71=function_call();
					state._fsp--;

					adaptor.addChild(root_0, function_call71.getTree());

					}
					break;
				case 2 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:156:4: reference
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_reference_in_atom766);
					reference72=reference();
					state._fsp--;

					adaptor.addChild(root_0, reference72.getTree());

					}
					break;
				case 3 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:157:4: literal
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_literal_in_atom771);
					literal73=literal();
					state._fsp--;

					adaptor.addChild(root_0, literal73.getTree());

					}
					break;
				case 4 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:158:4: LPAR ! comparison RPAR !
					{
					root_0 = (Object)adaptor.nil();


					LPAR74=(Token)match(input,LPAR,FOLLOW_LPAR_in_atom776); 
					pushFollow(FOLLOW_comparison_in_atom779);
					comparison75=comparison();
					state._fsp--;

					adaptor.addChild(root_0, comparison75.getTree());

					RPAR76=(Token)match(input,RPAR,FOLLOW_RPAR_in_atom781); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "atom"


	public static class function_call_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "function_call"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:161:1: function_call : FUNCTION ^ LPAR ! ( comparison ( SEMI ! comparison )* )? RPAR !;
	public final FormulaParser.function_call_return function_call() throws RecognitionException {
		FormulaParser.function_call_return retval = new FormulaParser.function_call_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token FUNCTION77=null;
		Token LPAR78=null;
		Token SEMI80=null;
		Token RPAR82=null;
		ParserRuleReturnScope comparison79 =null;
		ParserRuleReturnScope comparison81 =null;

		Object FUNCTION77_tree=null;
		Object LPAR78_tree=null;
		Object SEMI80_tree=null;
		Object RPAR82_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:162:2: ( FUNCTION ^ LPAR ! ( comparison ( SEMI ! comparison )* )? RPAR !)
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:162:4: FUNCTION ^ LPAR ! ( comparison ( SEMI ! comparison )* )? RPAR !
			{
			root_0 = (Object)adaptor.nil();


			FUNCTION77=(Token)match(input,FUNCTION,FOLLOW_FUNCTION_in_function_call793); 
			FUNCTION77_tree = (Object)adaptor.create(FUNCTION77);
			root_0 = (Object)adaptor.becomeRoot(FUNCTION77_tree, root_0);

			LPAR78=(Token)match(input,LPAR,FOLLOW_LPAR_in_function_call796); 
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:163:3: ( comparison ( SEMI ! comparison )* )?
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==CELL_REF||(LA23_0 >= FUNCTION && LA23_0 <= GLOVAR)||LA23_0==LPAR||LA23_0==MINUS||LA23_0==NUMBER||(LA23_0 >= STRING && LA23_0 <= TEMPVAR)) ) {
				alt23=1;
			}
			switch (alt23) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:163:5: comparison ( SEMI ! comparison )*
					{
					pushFollow(FOLLOW_comparison_in_function_call804);
					comparison79=comparison();
					state._fsp--;

					adaptor.addChild(root_0, comparison79.getTree());

					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:163:16: ( SEMI ! comparison )*
					loop22:
					while (true) {
						int alt22=2;
						int LA22_0 = input.LA(1);
						if ( (LA22_0==SEMI) ) {
							alt22=1;
						}

						switch (alt22) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:163:18: SEMI ! comparison
							{
							SEMI80=(Token)match(input,SEMI,FOLLOW_SEMI_in_function_call808); 
							pushFollow(FOLLOW_comparison_in_function_call811);
							comparison81=comparison();
							state._fsp--;

							adaptor.addChild(root_0, comparison81.getTree());

							}
							break;

						default :
							break loop22;
						}
					}

					}
					break;

			}

			RPAR82=(Token)match(input,RPAR,FOLLOW_RPAR_in_function_call821); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "function_call"


	public static class reference_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "reference"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:167:1: reference : ( CELL_REF ( ( COLON ^) CELL_REF )? | TEMPVAR | GLOVAR );
	public final FormulaParser.reference_return reference() throws RecognitionException {
		FormulaParser.reference_return retval = new FormulaParser.reference_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token CELL_REF83=null;
		Token COLON84=null;
		Token CELL_REF85=null;
		Token TEMPVAR86=null;
		Token GLOVAR87=null;

		Object CELL_REF83_tree=null;
		Object COLON84_tree=null;
		Object CELL_REF85_tree=null;
		Object TEMPVAR86_tree=null;
		Object GLOVAR87_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:168:2: ( CELL_REF ( ( COLON ^) CELL_REF )? | TEMPVAR | GLOVAR )
			int alt25=3;
			switch ( input.LA(1) ) {
			case CELL_REF:
				{
				alt25=1;
				}
				break;
			case TEMPVAR:
				{
				alt25=2;
				}
				break;
			case GLOVAR:
				{
				alt25=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 25, 0, input);
				throw nvae;
			}
			switch (alt25) {
				case 1 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:168:4: CELL_REF ( ( COLON ^) CELL_REF )?
					{
					root_0 = (Object)adaptor.nil();


					CELL_REF83=(Token)match(input,CELL_REF,FOLLOW_CELL_REF_in_reference833); 
					CELL_REF83_tree = (Object)adaptor.create(CELL_REF83);
					adaptor.addChild(root_0, CELL_REF83_tree);

					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:169:3: ( ( COLON ^) CELL_REF )?
					int alt24=2;
					int LA24_0 = input.LA(1);
					if ( (LA24_0==COLON) ) {
						alt24=1;
					}
					switch (alt24) {
						case 1 :
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:169:5: ( COLON ^) CELL_REF
							{
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:169:5: ( COLON ^)
							// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:169:7: COLON ^
							{
							COLON84=(Token)match(input,COLON,FOLLOW_COLON_in_reference841); 
							COLON84_tree = (Object)adaptor.create(COLON84);
							root_0 = (Object)adaptor.becomeRoot(COLON84_tree, root_0);

							}

							CELL_REF85=(Token)match(input,CELL_REF,FOLLOW_CELL_REF_in_reference846); 
							CELL_REF85_tree = (Object)adaptor.create(CELL_REF85);
							adaptor.addChild(root_0, CELL_REF85_tree);

							}
							break;

					}

					}
					break;
				case 2 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:170:4: TEMPVAR
					{
					root_0 = (Object)adaptor.nil();


					TEMPVAR86=(Token)match(input,TEMPVAR,FOLLOW_TEMPVAR_in_reference854); 
					TEMPVAR86_tree = (Object)adaptor.create(TEMPVAR86);
					adaptor.addChild(root_0, TEMPVAR86_tree);

					}
					break;
				case 3 :
					// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:171:4: GLOVAR
					{
					root_0 = (Object)adaptor.nil();


					GLOVAR87=(Token)match(input,GLOVAR,FOLLOW_GLOVAR_in_reference859); 
					GLOVAR87_tree = (Object)adaptor.create(GLOVAR87);
					adaptor.addChild(root_0, GLOVAR87_tree);

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "reference"


	public static class literal_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "literal"
	// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:174:1: literal : ( NUMBER | STRING );
	public final FormulaParser.literal_return literal() throws RecognitionException {
		FormulaParser.literal_return retval = new FormulaParser.literal_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set88=null;

		Object set88_tree=null;

		try {
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:175:2: ( NUMBER | STRING )
			// C:\\Users\\JOSENUNO\\Documents\\NetBeansProjects\\lapr4-2016-2db\\src\\main\\antlr3\\csheets\\core\\formula\\compiler\\Formula.g:
			{
			root_0 = (Object)adaptor.nil();


			set88=input.LT(1);
			if ( input.LA(1)==NUMBER||input.LA(1)==STRING ) {
				input.consume();
				adaptor.addChild(root_0, (Object)adaptor.create(set88));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

			catch (RecognitionException e) {
				reportError(e);
				throw e; 
			}

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "literal"

	// Delegated rules



	public static final BitSet FOLLOW_EQ_in_expression70 = new BitSet(new long[]{0x0000700494E00100L});
	public static final BitSet FOLLOW_comparison_in_expression74 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_block_instructions_in_expression76 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_current_cell_in_expression78 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_expression81 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CONVERT_in_expression94 = new BitSet(new long[]{0x0000004000048000L});
	public static final BitSet FOLLOW_currency_expression_in_expression97 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_expression99 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_currency_info_in_currency_expression112 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_LBRA_in_currency_expression114 = new BitSet(new long[]{0x0000000480000000L});
	public static final BitSet FOLLOW_concatenation_currency_in_currency_expression117 = new BitSet(new long[]{0x0000020000000000L});
	public static final BitSet FOLLOW_RBRA_in_currency_expression119 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arithmetic_low_currency_in_concatenation_currency141 = new BitSet(new long[]{0x0000001080000002L});
	public static final BitSet FOLLOW_PLUS_in_concatenation_currency148 = new BitSet(new long[]{0x0000000480000000L});
	public static final BitSet FOLLOW_MINUS_in_concatenation_currency153 = new BitSet(new long[]{0x0000000480000000L});
	public static final BitSet FOLLOW_arithmetic_low_currency_in_concatenation_currency158 = new BitSet(new long[]{0x0000001080000002L});
	public static final BitSet FOLLOW_arithmetic_highest_currency_in_arithmetic_low_currency178 = new BitSet(new long[]{0x0000000100002002L});
	public static final BitSet FOLLOW_MULTI_in_arithmetic_low_currency184 = new BitSet(new long[]{0x0000000480000000L});
	public static final BitSet FOLLOW_DIV_in_arithmetic_low_currency189 = new BitSet(new long[]{0x0000000480000000L});
	public static final BitSet FOLLOW_arithmetic_highest_currency_in_arithmetic_low_currency194 = new BitSet(new long[]{0x0000000100002002L});
	public static final BitSet FOLLOW_MINUS_in_arithmetic_highest_currency216 = new BitSet(new long[]{0x0000000400000000L});
	public static final BitSet FOLLOW_atom_currency_in_arithmetic_highest_currency222 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_currency_value_in_atom_currency240 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NUMBER_in_currency_value269 = new BitSet(new long[]{0x0000002000024000L});
	public static final BitSet FOLLOW_currency_type_in_currency_value271 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TOKEN_CELL_in_current_cell415 = new BitSet(new long[]{0x0000000263010000L});
	public static final BitSet FOLLOW_set_in_current_cell418 = new BitSet(new long[]{0x0000000400000000L});
	public static final BitSet FOLLOW_NUMBER_in_current_cell444 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EVAL_in_eval462 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_LPAR_in_eval464 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_concatenation_in_eval466 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_RPAR_in_eval468 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FOR_in_block_instructions486 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_LBRA_in_block_instructions491 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_comparison_in_block_instructions494 = new BitSet(new long[]{0x00000A0000000000L});
	public static final BitSet FOLLOW_SEMI_in_block_instructions497 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_comparison_in_block_instructions500 = new BitSet(new long[]{0x00000A0000000000L});
	public static final BitSet FOLLOW_RBRA_in_block_instructions504 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_reference_in_attribution532 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASSIGN_in_attribution534 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_comparison_in_attribution537 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_concatenation_in_comparison555 = new BitSet(new long[]{0x0000000263010002L});
	public static final BitSet FOLLOW_EQ_in_comparison563 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_NEQ_in_comparison568 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_GT_in_comparison573 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_LT_in_comparison578 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_LTEQ_in_comparison583 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_GTEQ_in_comparison588 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_concatenation_in_comparison593 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_attribution_in_comparison600 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arithmetic_lowest_in_concatenation611 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_AMP_in_concatenation617 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_arithmetic_lowest_in_concatenation620 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_arithmetic_low_in_arithmetic_lowest634 = new BitSet(new long[]{0x0000001080000002L});
	public static final BitSet FOLLOW_PLUS_in_arithmetic_lowest642 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_MINUS_in_arithmetic_lowest647 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_arithmetic_low_in_arithmetic_lowest652 = new BitSet(new long[]{0x0000001080000002L});
	public static final BitSet FOLLOW_arithmetic_medium_in_arithmetic_low666 = new BitSet(new long[]{0x0000000100002002L});
	public static final BitSet FOLLOW_MULTI_in_arithmetic_low674 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_DIV_in_arithmetic_low679 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_arithmetic_medium_in_arithmetic_low684 = new BitSet(new long[]{0x0000000100002002L});
	public static final BitSet FOLLOW_arithmetic_high_in_arithmetic_medium698 = new BitSet(new long[]{0x0000008000000002L});
	public static final BitSet FOLLOW_POWER_in_arithmetic_medium704 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_arithmetic_high_in_arithmetic_medium707 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arithmetic_highest_in_arithmetic_high721 = new BitSet(new long[]{0x0000000800000002L});
	public static final BitSet FOLLOW_PERCENT_in_arithmetic_high725 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MINUS_in_arithmetic_highest742 = new BitSet(new long[]{0x0000300410C00100L});
	public static final BitSet FOLLOW_atom_in_arithmetic_highest748 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_function_call_in_atom761 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_reference_in_atom766 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_literal_in_atom771 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_atom776 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_comparison_in_atom779 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_RPAR_in_atom781 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FUNCTION_in_function_call793 = new BitSet(new long[]{0x0000000010000000L});
	public static final BitSet FOLLOW_LPAR_in_function_call796 = new BitSet(new long[]{0x0000340490C00100L});
	public static final BitSet FOLLOW_comparison_in_function_call804 = new BitSet(new long[]{0x00000C0000000000L});
	public static final BitSet FOLLOW_SEMI_in_function_call808 = new BitSet(new long[]{0x0000300490C00100L});
	public static final BitSet FOLLOW_comparison_in_function_call811 = new BitSet(new long[]{0x00000C0000000000L});
	public static final BitSet FOLLOW_RPAR_in_function_call821 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CELL_REF_in_reference833 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_COLON_in_reference841 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_CELL_REF_in_reference846 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TEMPVAR_in_reference854 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GLOVAR_in_reference859 = new BitSet(new long[]{0x0000000000000002L});
}
