/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch;

import java.io.File;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 * Created on 20/6/2016
 * 
 * Class to save an array of workbook cached previews.
 * 
 * In my opinion, the parameters of this class should not be static. 
 * But I found out that the way the search was implemented would make it
 * very difficult to implement this class otherwise.
 * 
 * @author Miguel Freitas
 */
public class WorkbookCacheRegistry {
    
    /**
     * Array that contains the previews
     */
    private static ArrayList<WorkBookCache> list;
    
    public WorkbookCacheRegistry(){
        this.list = new ArrayList<>();
    }
    
    public static ArrayList<WorkBookCache> getCacheList(){
        return WorkbookCacheRegistry.list;
    }
    
    /**
     * Finds a cached preview of a given file and returns it. If
     * it does not exist, returns null.
     * @param file
     * @return WorkBookCache
     */
    public static WorkBookCache findCachedPreview(File file){
        for (WorkBookCache workBookCache : list) {
            if (workBookCache.getFile().getName().equals(file.getName())) {
                System.out.println("Returned a cache preview!");
                return workBookCache;
            }
        }
        return null;
    }
    
    /**
     * Adds a cache preview to the array
     * @param file
     * @param preview 
     */
    public static void addCachedPreview(File file, DefaultTableModel preview){
        WorkBookCache cache = new WorkBookCache(file, preview);
        WorkbookCacheRegistry.list.add(cache);
        System.out.println("Added a cache preview!");
    }
    
    public void removeCachedPreview(){
        //Vale a pena fazer isto? 
    }
}
