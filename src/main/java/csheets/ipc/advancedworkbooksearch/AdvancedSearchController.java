package csheets.ipc.advancedworkbooksearch;

import java.io.IOException;

/**
 * Controller to aid in the advanced workbook search
 * @author Miguel Freitas
 */
public class AdvancedSearchController {
    
    /**
     * Search that implements runnable
     */
    private AdvancedWorkbookSearchRunnable runSearch;
    
    /**
     * decision for active search or not
     */
    boolean active;
    
    /**
     * very simple implementation of the oracle WatchService API
     */
    private DirectoryWatcher watcher;    
    
    public AdvancedSearchController(AdvancedWorkbookSearchRunnable awsr, boolean active) throws IOException{
        WorkbookCacheRegistry a = new WorkbookCacheRegistry();
        this.runSearch = awsr;
        this.active = active;
        if (active) {
            runActiveSearch();
        }else{
            runSearch();
        }
        
    }

    /**
     * Runs an active search
     * @throws IOException 
     */
    private void runActiveSearch() throws IOException {
        System.out.println("Active search started");
        watcher = new DirectoryWatcher(runSearch);
        Thread t = new Thread(watcher);
        t.start();
    }

    /**
     * Runs a normal search
     */
    private void runSearch() {
        Thread t = new Thread(runSearch);
        t.start();
    }
    
}
