/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch.ui;

import csheets.ipc.advancedworkbooksearch.AdvancedSearchController;
import csheets.ipc.advancedworkbooksearch.AdvancedWorkbookSearchExtension;
import csheets.ipc.advancedworkbooksearch.AdvancedWorkbookSearchRunnable;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Guilherme
 */
public class AdvancedWorkbookSearchPanel extends JPanel {

    /**
     * JButton to aid in the advanced workbook search
     */
    private JButton buttonAdvancedWorkbookSearch;

    /**
     * UIController
     */
    private UIController uiController;
    
    private AdvancedSearchController controller;

    /**
     * AdvancedWorkbookSearchPanel Constructor
     *
     * @param uiController - the user interface controller
     */
    public AdvancedWorkbookSearchPanel(UIController uiController) {
        super(new BorderLayout());
        setName(AdvancedWorkbookSearchExtension.NAME);
        this.uiController = uiController;
        JPanel p = new JPanel();
        p.add(buttonAdvancedWorkbookSearch(), BorderLayout.CENTER);
        p.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        add(p);
    }

    /**
     * Method that returns the JButton that starts the advanced search
     *
     * @return the JButton to start the advanced workbook search
     */
    public JButton buttonAdvancedWorkbookSearch() {
        buttonAdvancedWorkbookSearch = new JButton("Advanced Search");
        buttonAdvancedWorkbookSearch.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String extension = JOptionPane.showInputDialog("What is the extension for which you are searching files?");
                String pattern = JOptionPane.showInputDialog("What is the pattern for which you are searching files?");
                String activeSearch = JOptionPane.showInputDialog("Do you want an active search? (y/n)");
                boolean active;
                if (activeSearch.equalsIgnoreCase("y")) {
                    active = true;
                } else {
                    active = false;
                }

                JFileChooser chooser = new JFileChooser();

                int userOption;
                JFrame frame = new JFrame("Choose your directory");
                JPanel gui = new JPanel(new BorderLayout(3, 3));

                //To select only folders, not files
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                userOption = chooser.showOpenDialog(new JFrame());

                if (userOption != JFileChooser.CANCEL_OPTION) {
                    try {
                        controller = new AdvancedSearchController(new AdvancedWorkbookSearchRunnable(uiController, pattern, extension, chooser.getSelectedFile()), active);
                    } catch (IOException ex) {
                        Logger.getLogger(AdvancedWorkbookSearchPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    frame.dispose();
                }
            }
        }
        );
        return buttonAdvancedWorkbookSearch;
    }

}
