/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch.ui;

import csheets.ipc.advancedworkbooksearch.AdvancedWorkbookSearchRunnable;
import csheets.ui.ctrl.UIController;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Guilherme
 */
public class SearchMainFrame implements Observer, ListSelectionListener, MouseListener {
    
    /**
	 * The user interface controller
	 */
	protected UIController uiController;
	/**
	 * The table model for the search results
	 */
	private DefaultTableModel seachResultsTableModel = null;
	/**
	 * The search results table itself
	 */
	private JTable searchResultsTable = null;
	/**
	 * The table model for the preview
	 */
	private DefaultTableModel previewTableModel = null;
	/**
	 * The workbook preview table
	 */
	private JTable previewTable = null;

	/**
	 * The current file selected on the results table
	 */
	private File currentFileSelected = null;
        
        /**
         * Current JFrame
         */
        private JFrame window;

        /**
         * SearchMainFrame Constructor
         * @param uiController - the user interface controller
         */
	public SearchMainFrame(UIController uiController) {
		this.uiController = uiController;
		createWindow();
	}

	/**
	 * Creates the main search window where the results and preview will be
	 * displayed
	 */
	void createWindow() {

		//Create and set up the window.
		JFrame frame = new JFrame("Advanced Workbook Search Results");
                window = frame;
		frame.setPreferredSize(new Dimension(950, 235));
		frame.setMinimumSize(new Dimension(400, 200));

		//Create the table
		final String[] searchResultsColumnNames = {"File name", "Directory"};
		final Object[][] rowData = {};
		seachResultsTableModel = new DefaultTableModel(rowData, searchResultsColumnNames);

		//Disables cell editing in the table
		searchResultsTable = new JTable(seachResultsTableModel) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		;
		};
		//Table configurations
		searchResultsTable.addMouseListener(this);
		searchResultsTable.getSelectionModel().addListSelectionListener(this);
		searchResultsTable.getTableHeader().
			setFont(new Font("SansSerif", Font.BOLD, 12));
		searchResultsTable.setCellEditor(null);
		searchResultsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		searchResultsTable.getColumnModel().getColumn(0).setPreferredWidth(120);
		searchResultsTable.getColumnModel().getColumn(0).setMinWidth(100);
		searchResultsTable.getColumnModel().getColumn(1).setPreferredWidth(330);
		searchResultsTable.getColumnModel().getColumn(1).setMinWidth(250);

		JScrollPane searchResultsScrollPane = new JScrollPane(searchResultsTable);

		//Create the table
		final String[] previewTableColumnNames = {"A", "B", "C", "D", "E", "F"};
		final Object[][] previewTableRowData = {};
		previewTableModel = new DefaultTableModel(previewTableRowData, previewTableColumnNames);
		//Disables cell editing in the table
		JTable previewTable = new JTable(previewTableModel) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		;
		};
		//Table configurations
		previewTable.getTableHeader().
			setFont(new Font("SansSerif", Font.BOLD, 12));
		previewTable.setCellEditor(null);
		previewTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		JScrollPane previewTableScrollPane = new JScrollPane(previewTable);
		previewTableScrollPane.setPreferredSize(new Dimension(453, 180));
		//previewTableScrollPane.setMinimumSize(new Dimension(450, 180));

		JPanel container = new JPanel();
		container.setLayout(new GridLayout(1, 2));
		container.add(searchResultsScrollPane);
		container.add(previewTableScrollPane);

		//Display the window.
		frame.add(container);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/**
	 * Adds a single result to the bottom of the displayed results list.
	 *
	 * @param file the file to be displayed as a new result
	 */
	public void appendResult(File file) {
		//Adds a row to the table
		Object[] row = {file.getName(), file.getAbsolutePath()};
		seachResultsTableModel.addRow(row);
	}

	/**
	 * Shows a preview of the workbook
	 *
	 * @param file the workbook file to be previewed
	 */
	public void showPreview(File file) throws IOException, ClassNotFoundException {
		WorkbookPreviewGenerator previewer = new WorkbookPreviewGenerator();
		previewer.updatePreviewTableModel(file, previewTableModel);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof AdvancedWorkbookSearchRunnable) {
			appendResult((File) arg);
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		currentFileSelected = new File(searchResultsTable.
			getValueAt(searchResultsTable.getSelectedRow(), 1).
			toString());
            try {
                showPreview(currentFileSelected);
            } catch (IOException ex) {
                Logger.getLogger(SearchMainFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SearchMainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		//System.out.println(currentFileSelected);
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		//No action required for the moment
		//System.out.println("Mouse pressed (# of clicks: " + e.getClickCount() + ")");
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		//No action required for the moment
		//System.out.println("Mouse released (# of clicks: "+ e.getClickCount() + ")");
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		//No action required for the moment
		//System.out.println("Mouse entered");
	}

	@Override
	public void mouseExited(MouseEvent e) {
		//No action required for the moment
		//System.out.println("Mouse exited");
	}
        
        public void dispose(){
            this.window.dispose();
        }
}
