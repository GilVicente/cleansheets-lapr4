/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch.ui;

import csheets.CleanSheets;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ipc.advancedworkbooksearch.WorkbookCacheRegistry;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Guilherme
 * @author Miguel Freitas
 */
public class WorkbookPreviewGenerator {

    /**
     * Updates a given default table model according to the file supplied for
     * preview
     *
     * @param fileToPreview the file that will be displayed
     * @param previewTableModel the table model to be updated
     * @throws java.io.IOException - thrown java IOException
     * @throws java.lang.ClassNotFoundException - thrown ClassNotFoundException
     */
    public boolean gotCache = false;
    
    public void updatePreviewTableModel(File fileToPreview, DefaultTableModel previewTableModel) throws IOException, ClassNotFoundException {

        if (WorkbookCacheRegistry.findCachedPreview(fileToPreview) != null) {
            previewTableModel = WorkbookCacheRegistry.findCachedPreview(fileToPreview).getPreview();
            System.out.println("Generator found a cache preview");
            gotCache = true;
        } else {

            CleanSheets csheetsInstance = new CleanSheets();

            Workbook loadedWorkbook = csheetsInstance.getWorkbook(fileToPreview);

            if (loadedWorkbook == null) { //if workbook isn't open yet

                try {

                    csheetsInstance.load(fileToPreview);
                } catch (IOException ex) {
                    Logger.getLogger(SearchMainFrame.class.getName()).
                            log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(SearchMainFrame.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            }
            loadedWorkbook = csheetsInstance.getWorkbook(fileToPreview);
            Spreadsheet spreadsheet = loadedWorkbook.getSpreadsheet(0);

            while (previewTableModel.getRowCount() > 0) {
                previewTableModel.removeRow(0);
            }
            int c = 0;
            for (int row = 0; row < spreadsheet.getRowCount() + 1; row++) {
                Pattern p = Pattern.compile(",+");
                String currentCellContent = spreadsheet.getCell(0, row).getContent();
                if (currentCellContent.equals("") || p.matcher(currentCellContent).matches()) {
                    System.out.println("Cell at :" + spreadsheet.getCell(0, row) + " is empty");
                } else if (previewTableModel.getRowCount() < 10) {
                    Object[] myrow = {};
                    previewTableModel.addRow(myrow);
                    String[] toUse = currentCellContent.split(",");
                    for (int i = 0; i < toUse.length; i++) {
                        if (i < 6) {
                            previewTableModel.setValueAt(toUse[i], c, i);
                        }
                        if (toUse[i].equals("")) {
                            previewTableModel.setValueAt("Empty cell", c, i);
                        }
                        if (toUse.length < 6) {
                            for (int j = toUse.length - 1; j < 6; j++) {
                                previewTableModel.setValueAt("Empty cell", c, j);
                            }
                        }
                    }
                    c++;
                }
            }
            WorkbookCacheRegistry.addCachedPreview(fileToPreview, previewTableModel);
        }
    }

}
