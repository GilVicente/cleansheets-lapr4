/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JComponent;

/**
 *
 * @author Guilherme
 */
public class AdvancedWorkbookSearchUI  extends UIExtension{
    /** Sidebar menu Component*/
    private JComponent sideBar;

    /**
     * 
     * @param extension - the extension for which components are provided
     * @param uiController - the user interface controller 
     */
    public AdvancedWorkbookSearchUI(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    /**
     * Method to return the sideBar menu
     * @return the sideBar menu
     */
    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new AdvancedWorkbookSearchPanel(uiController);
        }
        return sideBar;
    }
    
}
