/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch;

import csheets.ext.Extension;
import csheets.ipc.advancedworkbooksearch.ui.AdvancedWorkbookSearchUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Guilherme
 */
public class AdvancedWorkbookSearchExtension extends Extension{
    
    /** The name of the extension */
	public static final String NAME = "Advanced Workbook Search";

	/**
	 * Creates a new SeeExtensionsExtension
	 */
	public AdvancedWorkbookSearchExtension() {
		super(NAME);
	}
	
	/**
	 * Returns the user interface extension of this extension 
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
        @Override
	public UIExtension getUIExtension(UIController uiController) {
		return new AdvancedWorkbookSearchUI(this, uiController);
	}
    
}
