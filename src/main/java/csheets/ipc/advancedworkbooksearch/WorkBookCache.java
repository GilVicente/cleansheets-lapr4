/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch;

import java.io.File;
import java.io.IOException;
import javax.swing.table.DefaultTableModel;

/**
 * Created on 20/6/2016
 *
 * The purpose of this class is to store a preview of a workbook in memory, so
 * cleansheets only has to produce one preview per file.
 *
 * @author Miguel Freitas
 */
public class WorkBookCache {

    /**
     * Workbook to save
     */
    File workbook;
    /**
     * Preview to save
     */
    DefaultTableModel preview;

    /**
     * Constructor of the cached preview
     *
     * @param file Workbook file that you want to save the respective preview.
     * @param preview DefaulTableModel that will save the preview for the
     * workbook.
     */
    public WorkBookCache(File file, DefaultTableModel preview) {
        this.workbook = file;
        preview = new DefaultTableModel();
    }

    public void setWorkbook(File workbook) {
        this.workbook = workbook;
    }

    public void setPreview(DefaultTableModel preview) {
        this.preview = preview;
    }

    public File getFile() {
        return this.workbook;
    }

    public DefaultTableModel getPreview() {
        return this.preview;
    }

}
