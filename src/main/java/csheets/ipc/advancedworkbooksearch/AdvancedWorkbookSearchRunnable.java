/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch;

import csheets.ipc.advancedworkbooksearch.ui.SearchMainFrame;
import csheets.ui.ctrl.UIController;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author Guilherme
 */
public class AdvancedWorkbookSearchRunnable extends Observable implements Runnable {

    /**
     * The user interface controller
     */
    protected UIController uiController;
    private String pattern;
    private String extension;
    private File parentDirectory;
    private SearchMainFrame window;

    /**
     * AdvancedWorkbookSearchRunnable constructor
     *
     * @param uiController - the user interface controller
     * @param pattern - the pattern to filter the search with
     * @param extension - the extension to look for. Shouldn't start with "."
     */
    public AdvancedWorkbookSearchRunnable(UIController uiController, String pattern, String extension, File parentDirectory) {
        this.uiController = uiController;
        this.pattern = pattern;
        this.extension = extension;
        this.parentDirectory = parentDirectory;
    }

    /**
     * Runs the SearchMainFrame and the file Search
     */
    @Override
    public void run() {
        SearchMainFrame searchResultsWindow = new SearchMainFrame(uiController);
        window = searchResultsWindow;
        addObserver(searchResultsWindow);
        findFilesByExtension(parentDirectory, pattern, extension);
    }
    
    

    /**
     * Does an in-depth search through the directory tree starting from a chosen
     * parent directory in order to find files with a given extension.
     *
     * @param parentDirectory the directory where the search will start
     * @param pattern the pattern to filter the search
     * @param extension the extension to look for. Should not start by "."
     */
    public void findFilesByExtension(File parentDirectory, String pattern, String extension) {
        File[] files = parentDirectory.listFiles();
        ArrayList<File> results = new ArrayList();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    String name = file.getName();
                    if (file.getName().startsWith(pattern) && file.toString().toLowerCase().endsWith("." + extension)) {
                        setChanged();
                        notifyObservers(file);
                        results.add(file);
                    }
                } else if (file.isDirectory()) {
                    findFilesByExtension(file, pattern, extension);
                }
            }
        }
    }
    
    
    /**
     * Returns the path of the selected directory
     * @return 
     */
    public Path getPath(){
        return Paths.get(parentDirectory.getAbsolutePath());
    }
    
    /**
     * Disposes of the current search results
     */
    public void dispose(){
        this.window.dispose();
    }

}
