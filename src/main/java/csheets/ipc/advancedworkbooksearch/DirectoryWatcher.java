/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.advancedworkbooksearch;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

/**
 * This class is a simple implementation of the Oracle WatchService API. I use
 * this to listen for changes on a given directory and its files.
 *
 * @author Miguel Freitas
 */
public class DirectoryWatcher implements Runnable {

    /**
     * WatchService client to register on the directory
     */
    private final WatchService ws;

    /**
     * EventKey to poll the currently detected events on the dir
     */
    private final WatchKey eventKey;

    /**
     * The directory to watch
     */
    private final Path dir;

    /**
     * The search that will update its results whenever an event occurs
     */
    private final AdvancedWorkbookSearchRunnable awsr;

    /**
     * Saving the previous thread so I can interrupt when a new search appears
     */
    private Thread previousSearch;

    private boolean hasDetectedAChange;

    public DirectoryWatcher(AdvancedWorkbookSearchRunnable awsr) throws IOException {
        this.awsr = awsr;
        ws = FileSystems.getDefault().newWatchService();
        dir = this.awsr.getPath();
        eventKey = dir.register(ws, StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE,
                StandardWatchEventKinds.ENTRY_MODIFY);

        previousSearch = new Thread(awsr);
        previousSearch.start();
    }

    @Override
    public void run() {
        while (true) {
            for (WatchEvent<?> Event : eventKey.pollEvents()) {

                System.out.println("The folder listener captured an event.");

                hasDetectedAChange = true;
                awsr.dispose();

                Thread t = new Thread(awsr);
                previousSearch = t;
                t.start();
            }
        }
    }

    public boolean hasDetectedAChange() {
        return this.hasDetectedAChange;
    }

}
