/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.connection;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Class that associates names to IPs
 * @author Francisco
 */
public class ConnectionList {

    private InetAddress ip;
    private String connectionName;
    private HashMap<InetAddress, String> connectionsHash;

    public ConnectionList() {
        
        this.connectionsHash = new HashMap<>();
    }
    
    public HashMap<InetAddress, String> getHashMap() {
        return this.connectionsHash;
    }
    
    public void addEntry(InetAddress adr, String name) {
        
        this.connectionsHash.put(adr, name);
    }
    
    
    
}
