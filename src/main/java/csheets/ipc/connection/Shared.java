/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.connection;

import csheets.core.Cell;
import java.net.InetAddress;

/**
 *
 * @author Francisco
 */
public class Shared {
    
    public InetAddress ip;
    public String ipName;
    
    public Cell[][] cellsToShare;
    
    
    public Shared (){
        
    }
    public Shared (InetAddress ip, String ipName, Cell[][] cellsToShare) {
        this.ip = ip;
        this.ipName = ipName;
        this.cellsToShare = cellsToShare;
    }
    
    public void setIp (InetAddress ip) {
        this.ip = ip;
    }
    
    public void setIpName(String ipName){
        this.ipName = ipName;
    }
    
    public void setCellsToShare(Cell[][] cellToShare) {
        this.cellsToShare = cellToShare;
    }
    
    public InetAddress getIp() {
        return this.ip;
    }
    
    
    public String getIpName(){
        return this.ipName;
    }
    
    public Cell[][] getCellsToShare() {
        return this.cellsToShare;
    }
}
