/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.connection;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.runner.Request;

/**
 *
 * @author Filipe
 */
public class Broadcast {

    public static int PORT = -1;

    public static boolean listUpdated = false;

    private static ArrayList<InetAddress> addresses = new ArrayList<>();

    private Request request;

    private Respond respond;

    public void run() {
        this.respond = new Respond();
        this.respond.start();
    }

    public void runReceive() {
        this.request = new Request();
        this.request.start();
    }

    public void restart() {
        this.respond.interrupt();
        this.respond.start();
        this.request.interrupt();
        this.request.start();
    }

    public static ArrayList<InetAddress> getActiveInstancesList() {
        return addresses;
    }

    class Request extends Thread {

        @Override
        public void run() {
            while (true) {
                DatagramSocket clientSocket = null;
                try {
                    clientSocket = new DatagramSocket();

                    byte[] addr = new byte[]{(byte) 255, (byte) 255, (byte) 255, (byte) 255};
                    clientSocket.setBroadcast(true);
                    InetAddress IPAddress = InetAddress.getByAddress(addr);
                    byte[] sendData = "request".getBytes();

                    DatagramPacket sendPacket = new DatagramPacket(sendData,
                            sendData.length, IPAddress, Broadcast.PORT);
                    addresses.clear();

                    clientSocket.send(sendPacket);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } finally {
                    clientSocket.close();
                }

                try {
                    Thread.sleep(5000);

                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                    return;
                }
            }
        }
    }

    public class Respond extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    DatagramSocket serverSocket = new DatagramSocket(Broadcast.PORT);
                    byte[] receiveData = new byte[512];
                    byte[] sendData = new byte[512];

                    while (true) {

                        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                        serverSocket.receive(receivePacket);
                        System.out.println("passa aqui");
                        String sentence = new String(receivePacket.getData());
                        System.out.println("RECEIVED: " + sentence);

                        InetAddress IPAddress = receivePacket.getAddress();

                        int count = sentence.length();
                        boolean b = "request".equalsIgnoreCase(sentence);

                        if (sentence.startsWith("request")) {
                            sendData = "respond".getBytes();
                            DatagramPacket sendPacket
                                    = new DatagramPacket(sendData, sendData.length, IPAddress, Broadcast.PORT);
                            serverSocket.send(sendPacket);
                        } else if (sentence.startsWith("respond")) {
                            InetAddress adr = receivePacket.getAddress();
                            System.out.println(String.format("Address: %s", adr));
                            if (!addresses.contains(adr)) {
                                addresses.add(adr);

                            }

                            for (InetAddress i : addresses) {
                                System.out.println(i);

                            }
                        }

                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
