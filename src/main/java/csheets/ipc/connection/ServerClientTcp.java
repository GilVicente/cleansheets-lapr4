/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.connection;

import csheets.ext.chatMessage.MessagesReceived;
import csheets.window.secure.comunication.Crypto;
import csheets.window.secure.comunication.SecureComunicationController;
import java.io.DataInputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.util.zip.DataFormatException;
import javax.swing.JOptionPane;

public class ServerClientTcp {

    private static String ip = "localhost";
    private static String message = "Testar mensagem Grande";
    private static int var;
    private static final int CHAT = 1;
    private static final int MESSAGE_SECURE = 2;

    /**
     * @return the ip
     */
    public static String getIp() {
        return ip;
    }

    /**
     * @param aIp the ip to set
     */
    public static void setIp(String aIp) {
        ip = aIp;
    }

    /**
     * @return the message
     */
    public static String getMessage() {
        return message;
    }

    /**
     * @param aMessage the message to set
     */
    public static void setMessage(String aMessage) {
        message = aMessage;
    }

    /**
     * @param aVar the var to set
     */
    public static void setVar(int aVar) {
        var = aVar;
    }

    Server srv = new Server();

    Client cli = new Client();

    public Server getServer() {
        return srv;
    }

    public Client getClient() {
        return cli;
    }

    private class Server implements Runnable {

        @Override
        public void run() {
            try {
                if (var == CHAT) {
                    System.out.println("Escuta");
                    ServerSocket serverSocket = new ServerSocket(8181);

                    Socket clientSocket = serverSocket.accept();
//                SecureComunicationController c = new SecureComunicationController(clientSocket);

                    byte[] data = new byte[80];
                    DataInputStream is = new DataInputStream(clientSocket.getInputStream());
                    Crypto c = new Crypto();
                    is.read(data);

                    byte[] xDecompress = SecureComunicationController.decompress(data);
                    String x = new String(xDecompress);
                    String message = c.decrypt(x, "Lapr4Encryption");
                    CommunicationIncommingList.getInstance().addMessage(message);
                    String msgEncrypt = new String(x);
                    CommunicationOutcommingList.getInstance().addMessage(msgEncrypt);
                    JOptionPane.showMessageDialog(null, message);
                    MessagesReceived.addNewMessage(message);
                    System.out.println("Recebeu mensagem : " + message);
                    serverSocket.close();
                } else if (var == MESSAGE_SECURE) {
                    System.out.println("Entrou ");
                    ServerSocket serverSocket = new ServerSocket(3079);

                    Socket clientSocket = serverSocket.accept();
//                SecureComunicationController c = new SecureComunicationController(clientSocket);

                    byte[] data = new byte[80];
                    DataInputStream is = new DataInputStream(clientSocket.getInputStream());
                    Crypto c = new Crypto();
                    is.read(data);

                    byte[] xDecompress = SecureComunicationController.decompress(data);
                    String x = new String(xDecompress);
                    String message = c.decrypt(x, "Lapr4Encryption");
                    CommunicationIncommingList.getInstance().addMessage(message);
                    String msgEncrypt = new String(x);
                    if (SecureComunicationController.isSecure()) {
                        CommunicationOutcommingList.getInstance().addMessage(msgEncrypt);
                    } else {
                        CommunicationOutcommingList.getInstance().addMessage(message);

                    }
                    System.out.println("Recebeu mensagem : " + message);
                    serverSocket.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (DataFormatException ex) {
                ex.printStackTrace();
            }

        }
    }

    private class Client implements Runnable {

        @Override
        public void run() {
            try {
                if (var == CHAT) {
                    final String largeFile = "Lapr4Encryption";
                    byte[] message = largeFile.getBytes();
                    Thread.sleep(10);
                    System.out.println("Envia Chat");
                    Socket socket = new Socket(getIp(), 8181);
                    OutputStream socketOutputStream = socket.getOutputStream();
                    long startTime = System.currentTimeMillis();
                    Crypto c = new Crypto();
                    byte[] x = c.encrypt(ServerClientTcp.getMessage(), largeFile).getBytes();
                    byte[] output = SecureComunicationController.compress(x);
                    socketOutputStream.write(output, 0, output.length);
                    socketOutputStream.close();
                    socket.close();
                    long endTime = System.currentTimeMillis();
                    System.out.println(output.length + " bytes written in " + (endTime - startTime) + " ms.");
                } else if (var == MESSAGE_SECURE) {
                    final String largeFile = "Lapr4Encryption";
                    byte[] message = largeFile.getBytes();
                    Thread.sleep(10);
                    System.out.println("Envia");
                    Socket socket = new Socket("localhost", 3079);//sends to localhost the message 
                    OutputStream socketOutputStream = socket.getOutputStream();
                    long startTime = System.currentTimeMillis();
                    Crypto c = new Crypto();
                    byte[] x = c.encrypt(ServerClientTcp.getMessage(), largeFile).getBytes();
                    byte[] output = SecureComunicationController.compress(x);
                    socketOutputStream.write(output, 0, output.length);
                    socketOutputStream.close();
                    socket.close();
                    long endTime = System.currentTimeMillis();
                    System.out.println(output.length + " bytes written in " + (endTime - startTime) + " ms.");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
