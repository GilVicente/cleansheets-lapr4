/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.connection;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import csheets.core.Cell;
import csheets.core.Workbook;
import csheets.ext.FilesShare.FileShare;
import csheets.ext.FilesShare.ListFilesShare;
import csheets.ext.share.ui.ActiveInstancesPanel;
import csheets.ext.share.ui.ShareMenu;
import csheets.ui.ctrl.UIController;
import csheets.util.Files;
import csheets.window.secure.comunication.SecureCommunication;
import csheets.window.secure.comunication.SecureComunicationController;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.ControllerEventListener;
import javax.swing.JOptionPane;

/**
 * A class that manages TCP Connections, in order to send or receive data
 * between nodes at the local area network.
 *
 * @author JOSENUNO
 */
public class TCPConnection {

    public static boolean realTimeSharing = false;
//    SearchInAnotherInstanceController searchController;
    private static final int SEND_MESSAGE = 1;
    private static final int CHAT_MESSAGE = 2;
    private static final int SHARE_FILES = 3;
    private static String ip;

    public static int type = 0;

    public static InetAddress Ip;

    private String message;
    private SecureComunicationController c;

    private ListFilesShare listFilesShare;
    
    public void run() {
        Receive r = new Receive();
        r.start();
    }

    /**
     * Sends Data to a certain IP Address at the local area network via TCP.
     *
     * @param ip Address that will receive the data.
     * @param data Data that will be sent.
     */
    public static void sharingSendData(InetAddress ip, Object data) {
        while (ActiveInstancesPanel.sending);
        ActiveInstancesPanel.sending = true;
        try {
            Socket skt = new Socket(ip, Broadcast.PORT);
            ObjectOutputStream out = new ObjectOutputStream(skt.getOutputStream());
            System.out.print("Sending string: '" + data + "'\n");
            // out.writeObject(new String("teste"));
            // out.write(new String("teste"));
            out.writeObject(data);
            //  testeQ t = new testeQ(23);
            //  out.writeObject(t);
            out.flush();
            //  out.close();
            skt.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            ActiveInstancesPanel.sending = false;
        }
    }

    public static void sendTCPRequest(InetAddress ip) throws IOException {
        try {
            Socket skt = new Socket(ip, Broadcast.PORT);
            ObjectOutputStream out = new ObjectOutputStream(skt.getOutputStream());
            System.out.println("Instance request...");
            String request = "request";
            out.writeObject(request);
            out.flush();
            skt.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    



    public class receiveTCPResponse extends Thread {
//        ArrayList<CleansheetInstance> cleansheetInstances = new ArrayList<>();
//        CleanSheets cleansheetInstance = new CleanSheets();

        List<Workbook> workbooks = new ArrayList<>();
        DatagramSocket socket = null;

        @Override
        public void run() {
            if (type == CHAT_MESSAGE) {
                try {
                    socket = new DatagramSocket(Broadcast.PORT, InetAddress.getByName("0.0.0.0"));
                } catch (UnknownHostException ex) {
                    Logger.getLogger(TCPConnection.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SocketException ex) {
                    Logger.getLogger(TCPConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    socket.setBroadcast(true);
                } catch (SocketException ex) {
                    Logger.getLogger(TCPConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            while (true) {
                try {
                    if (type == 0) {
                        ServerSocket instanceSocket = new ServerSocket(Broadcast.PORT);
                        Socket skt = instanceSocket.accept();
                        ObjectInputStream in = new ObjectInputStream(skt.getInputStream());
                        System.out.println("Send data: ");
                        String request = (String) in.readObject();
                        if (request.equalsIgnoreCase("request")) {
//                       workbooks = searchController.searchOpenedWorkbooks();

                        }
                        skt.close();;
                        instanceSocket.close();
                        in.close();

                    } else if (type == CHAT_MESSAGE) {
                        ServerSocket instanceSocket = new ServerSocket(Broadcast.PORT);
                        Socket socket = instanceSocket.accept();
//                        c = new SecureComunicationController(socket);

                        //c.readByte();




                        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                        String request = (String) in.readObject();
                        JOptionPane.showMessageDialog(null, "MENSAGEM NOVA! : \n" + request);
                    } else if (type == SHARE_FILES) {
                        ServerSocket instanceSocket = new ServerSocket(Broadcast.PORT);
                        Socket socket = instanceSocket.accept();
//                        c = new SecureComunicationController(socket);

                        //c.readByte();

//                        c.readByte();


                        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                        listFilesShare = (ListFilesShare) in.readObject();
                        JOptionPane.showMessageDialog(null, "RECEBEU LISTA ");

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public ListFilesShare getListFilesShare(){
        return this.listFilesShare;
    }
    
    
    public class Receive extends Thread {

        @Override
        public void run() {

            if (type == 0) {
                while (true) {
                    try {
                        ServerSocket serv = new ServerSocket(Broadcast.PORT);
                        Socket skt = serv.accept();
                        ObjectInputStream in = new ObjectInputStream((skt.getInputStream()));
                        System.out.print("Received Data: '");

                        Cell[][] cel = (Cell[][]) in.readObject();

                        for (Cell[] c : cel) {
                            for (Cell cell : c) {
                                Cell cellToReplace = ShareMenu.action.getSpreadSheetTable().getSpreadsheet()
                                        .getCell(cell.getAddress().getColumn(), cell.getAddress().getRow());

                                cellToReplace.copyFrom(cell);
                                ShareMenu.action.repaintSS();
                            }
                        }

                        skt.close();
                        serv.close();
                        in.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            if (type == SHARE_FILES) {

            }
        }
    }
}
