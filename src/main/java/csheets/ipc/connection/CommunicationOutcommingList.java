/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ipc.connection;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author José Santos (1140921)
 */
public class CommunicationOutcommingList {

    /**
     * List of all the messages.
     */
    private List<String> incommingList;

    /**
     * Completed Constructor.
     */
    public CommunicationOutcommingList() {
        this.incommingList = new ArrayList<>();
    }

    /**
     * Adds a new message to the list.
     *
     * @param string Message to be added.
     */
    public void addMessage(String string) {
        this.incommingList.add(string);
    }

    /**
     * Returns all the incomming communications.
     *
     * @return The list with the information of all the incomming
     * communications.
     */
    public String list() {
        List<String> temp = new ArrayList<>(incommingList);
        String[] list = new String[temp.size()];
        String x = null;
        int i = 0;
        for (String s : temp) {
            x = s + "";
            list[i] = s;
            i++;
        }
        return x;
    }

    // https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
    private static class LazyHolder {

        /**
         * The instance.
         */
        private static final CommunicationOutcommingList INSTANCE = new CommunicationOutcommingList();

        /**
         * Private constructor.
         */
        private LazyHolder() {
            //Just to make this instantiation impossible.
        }
    }

    /**
     * Returns the instance.
     *
     * @return The instance of the MessageList.
     */
    public static CommunicationOutcommingList getInstance() {
        return LazyHolder.INSTANCE;
    }

}
