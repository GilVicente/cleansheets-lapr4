/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.window.secure.comunitcation.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.SideBarAction;
import csheets.ui.ext.UIExtension;
import csheets.window.secure.comunication.SecureComunicationController;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;

/**
 *
 * @author José Santos (1140921)
 */
public class UIExtensionSecureComunication extends UIExtension {

    /**
     * A panel in which the contacts are displayed
     */
    private JComponent sideBar;

    private JCheckBoxMenuItem menuSideBar;
    
    private SecureComunicationController secureComunicationController;

    public UIExtensionSecureComunication(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    /**
     * Returns a side bar that gives access to extension-specific functionality.
     *
     * @return a component, or null if the extension does not provide one
     */
    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new SecureComunicationPanel(secureComunicationController);
        }

        return sideBar;
    }

    public JCheckBoxMenuItem getCheckBoxMenuItemSidebar() {
        if (menuSideBar == null) {
            menuSideBar = new JCheckBoxMenuItem(new SideBarAction(this, sideBar));
        }
        return menuSideBar;
    }

}
