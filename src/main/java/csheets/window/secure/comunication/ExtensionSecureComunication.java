/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.window.secure.comunication;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import csheets.window.secure.comunitcation.ui.UIExtensionSecureComunication;

/**
 *
 * @author José Santos (1140921)
 */
public class ExtensionSecureComunication extends Extension {

    /**
     * name of the extension
     */
    public static final String NAME = "Secure Comunication";

    /**
     * Creates a new Example extension.
     */
    public ExtensionSecureComunication() {
        super(NAME);
    }

    /**
     * Returns the user interface extension of this extension .
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIExtensionSecureComunication(this, uiController);
    }
}
