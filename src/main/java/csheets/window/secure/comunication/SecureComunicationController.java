/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.window.secure.comunication;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 *
 * @author José Santos (1140921)
 */
public class SecureComunicationController {

    /**
     * inform if the connection is secure(true) or unsecure(false)
     */
    private static boolean secure;

    /**
     * The data input stream from the socket, where data is received.
     */
    private DataInputStream dataInputStream;

    /**
     * The data output stream to the socket, where data is sent.
     */
    private DataOutputStream dataOutputStream;

    /**
     * The key necessary to encrypt and decrypt the data.
     */
    private static String password = "Lapr4Encryption";

    private final Crypto crypto;

    /**
     * Completed constructor.
     *
     * @param socket The socket.
     * @throws java.io.IOException java.io.IOException
     */
    public SecureComunicationController() throws IOException, NoSuchAlgorithmException {

        this.crypto = new Crypto();

    }

    /**
     * Allows the decryption of the communication.
     *
     * @param data The received data to be decrypted.
     * @return The decrypted data.
     */
    public String decrypt(byte[] data) throws GeneralSecurityException, UnsupportedEncodingException, IOException, DataFormatException {

        byte[] xDecompress = decompress(data);
//                is.readFully(data);
//                InputStream s = c.decrypt(clientSocket.getInputStream(), "Lapr4Encryption");
        String x = new String(xDecompress);
        return crypto.decrypt(x, password);

    }

    /**
     * Allows the encryption and compression of the communication .
     *
     * @param data The data to be encrypted to be sent.
     * @return The encrypted data.
     */
    public byte[] encrypt(String data) throws GeneralSecurityException, UnsupportedEncodingException, IOException {
        byte[] messageEncrypted = crypto.encrypt(data, password).getBytes();

        return compress(messageEncrypted);
    }

    /**
     * @return the secure
     */
    public static boolean isSecure() {
        return secure;
    }

    /**
     * @param aSecure the secure to set
     */
    public static void setSecure(boolean aSecure) {
        secure = aSecure;
    }

    public static byte[] compress(byte[] data) throws IOException {

        Deflater deflater = new Deflater();

        deflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

        deflater.finish();

        byte[] buffer = new byte[1024];

        while (!deflater.finished()) {

            int count = deflater.deflate(buffer); // returns the generated code... index  

            outputStream.write(buffer, 0, count);

        }

        outputStream.close();

        byte[] output = outputStream.toByteArray();

        return output;

    }

    public static byte[] decompress(byte[] data) throws IOException, DataFormatException {

        Inflater inflater = new Inflater();

        inflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

        byte[] buffer = new byte[1024];

        while (!inflater.finished()) {

            int count = inflater.inflate(buffer);

            outputStream.write(buffer, 0, count);

        }

        outputStream.close();

        byte[] output = outputStream.toByteArray();

        return output;

    }
}
