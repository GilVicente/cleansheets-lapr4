/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.window.secure.comunication;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author José Santos (1140921)
 */
public class SecureCommunication {

    public static final String CIPHER_ALGORITHM = "AES";
    public static final String KEY_ALGORITHM = "AES";
    public static final String PASSWORD_HASH_ALGORITHM = "SHA-256";

    /**
     * Generates a symmetric key (AES)
     *
     * @return Symmetric Key
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static SecretKey generateKey() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(128);
        SecretKey key = keyGen.generateKey();
        return key;
    }

    /**
     * Constructs a symmetric key from the given string
     *
     * @param keyString
     * @return Symmetric Key
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static SecretKey getKey(String keyString) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        byte[] encodedKey = Base64.decodeBase64(keyString);
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        return key;
    }

    /**
     * Encrypts the text with the symmetric key
     *
     * @param rawText Text to be encrypted
     * @param key Symmetric key
     * @return Encrypted text (Base64 encoded)
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException
     */
    public static String encrypt(byte[] data, SecretKey key) throws GeneralSecurityException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return Base64.encodeBase64String(cipher.doFinal(data));
    }

    /**
     * Decrypts the text with the symmetric key
     *
     * @param data Text to be decrypted
     * @param key
     * @return Decrypted text (Base64 encoded)
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException
     */
    public static String decrypt(byte[] data, SecretKey key) throws GeneralSecurityException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key);
        return new String(cipher.doFinal(Base64.decodeBase64(data)), "UTF-8");
    }



}
