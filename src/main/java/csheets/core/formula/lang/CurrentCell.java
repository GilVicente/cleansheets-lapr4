/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.FormulaCompilationException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author José Santos (1140921)
 */
public class CurrentCell implements BinaryOperator {

    @Override
    public Value applyTo(Expression leftOperand, Expression rightOperand) throws IllegalValueTypeException {

        Cell targetCell = ((CellReference) leftOperand).getCell();
        Value rightValue = rightOperand.evaluate();
        String rightValueStr = String.valueOf(rightValue);

        try {
            targetCell.setContent(rightValueStr);
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(Assign.class.getName()).
                    log(Level.SEVERE, null, ex);
        }

        return rightValue;
    }

    @Override
    public String getIdentifier() {
        return "_cell";
    }

    @Override
    public Value.Type getOperandValueType() {
        return Value.Type.UNDEFINED;
    }

    @Override
    public String toString() {
        return getIdentifier();
    }

}
