package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.persistence.inmemory.TemporaryVariableRepository;
import csheets.core.formula.persistence.inmemory.GlobalVariableRepository;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Assign implements BinaryOperator {

    @Override
    public Value applyTo(Expression leftOperand, Expression rightOperand) throws IllegalValueTypeException {

        if (leftOperand instanceof TempVariableRef) {
            final TempVariableRef var = (TempVariableRef) leftOperand;
            final Value rhsVal = rightOperand.evaluate();
            TemporaryVariableRepository.update(var.getTempVariable(), rhsVal);

            return rhsVal;
        } else {

            if (leftOperand instanceof GlobalVariableRef) {
                final GlobalVariableRef var = (GlobalVariableRef) leftOperand;
                final Value rhsVal = rightOperand.evaluate();
                GlobalVariableRepository.update(var.getGlobalVariable(), rhsVal);

                return rhsVal;
            } else if (!(leftOperand instanceof CellReference)) {
                return new Value(new IllegalArgumentException("#OPERAND!"));
            }

            final CellReference targetCellRef = (CellReference) leftOperand;
            final Cell targetCell = targetCellRef.getCell();

            final Value rhsVal = rightOperand.evaluate();
            final String rhsValStr = String.valueOf(rhsVal);

            try {
                targetCell.setContent(rhsValStr);
            } catch (FormulaCompilationException ex) {
                Logger.getLogger(Assign.class.getName()).
                        log(Level.SEVERE, null, ex);
            }

            return rhsVal;
        }

//        Cell targetCell = ((CellReference) leftOperand).getCell();
//        Value rightValue = rightOperand.evaluate();
//        String rightValueStr = String.valueOf(rightValue);
//
//        try {
//            targetCell.setContent(rightValueStr);
//        } catch (FormulaCompilationException ex) {
//            Logger.getLogger(Assign.class.getName()).
//                    log(Level.SEVERE, null, ex);
//        }
//
//        return rightValue;
    }

    @Override
    public String getIdentifier() {
        return ":=";
    }

    @Override
    public Value.Type getOperandValueType() {
        return Value.Type.UNDEFINED;
    }

    @Override
    public String toString() {
        return getIdentifier();
    }
}
