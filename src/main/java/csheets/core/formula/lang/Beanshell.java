/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import bsh.EvalError;
import bsh.Interpreter;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import csheets.ext.BeanShellIntegration.Run_Script;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author dmaia
 */
public class Beanshell implements Function {

    /**
     * The only (but repeatable) parameter: a numeric term
     */
    public static final FunctionParameter[] parameters = new FunctionParameter[]{
        new FunctionParameter(Value.Type.NUMERIC, "Term", false,
        "")
    };

    /**
     * Creates a new instance of the AVERAGE function.
     */
    public Beanshell() {
    }

    public String getIdentifier() {
        return "RUN";
    }

    @Override
    public Value applyTo(Expression[] args) throws IllegalValueTypeException {
        int ret = 0;
       
        if (args[1].evaluate().toNumber().intValue() == 0) { //false sincrona
            File file = new File(args[0].evaluate().toText());
            ret = (int) Run_Script.run(file.getAbsolutePath());
        } else {
            File file = new File(args[0].evaluate().toText());
            ret = (int) Run_Script.run(file.getAbsolutePath());
            JOptionPane.showMessageDialog(null, "Assyncronous Run, Result:"+ret);
            return new Value();
        }

        return new Value(ret);

    }

    @Override
    public FunctionParameter[] getParameters() {
        return parameters;
    }

    @Override
    public boolean isVarArg() {
        return true;
    }

}
