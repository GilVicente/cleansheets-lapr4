/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Currency;
import csheets.core.formula.Expression;
import java.math.BigDecimal;

/**
 *
 * @author Francisco
 */
public class Pound implements Currency {

	public Pound() {
	}

        @Override
	public String getIdentifier() {
            
		return "£";
	}

        @Override
	public Value.Type getOperandValueType() {
		return Value.Type.NUMERIC;
	}

        @Override
	public String toString() {
		return getIdentifier();
	}

	@Override
	public Value applyTo(Expression currencyTo, Expression valueOf) throws IllegalValueTypeException {
		BigDecimal value = new BigDecimal(valueOf.evaluate().toDouble());
		String currencyToString = currencyTo.evaluate().toString();
		if ("euro".equalsIgnoreCase(currencyToString)) {
			currencyRate.setCoin(currencyToString);
		} else if ("pound".equalsIgnoreCase(currencyToString)) {
			currencyRate.setCoin(currencyToString);
		} else if ("dollar".equalsIgnoreCase(currencyToString)) {
			currencyRate.setCoin(currencyToString);
		}
		return new Value(currencyRate.getPound().multiply(value));
	}
}

