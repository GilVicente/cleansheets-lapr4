/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.TemporaryVariable;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.persistence.inmemory.TemporaryVariableRepository;
import csheets.core.formula.util.ExpressionVisitor;

/**
 * 
 * @author Gil
 */
public class TempVariableRef implements Expression {

	private TemporaryVariable variable;

	public TempVariableRef(TemporaryVariable variable) {
		this.variable = variable;
	}

	/**
	 * Constructor for temporary variable references
         * Checks if there's already this reference for the sheet, cell and var starting with _
	 */
	public TempVariableRef(Spreadsheet sheet,Cell cell, String var) {

		TemporaryVariable temp = TemporaryVariableRepository.getVariable(sheet, cell, var); //search faz return da variable

		/**
		 * Test to check if the temporary variable created (temp) already
		 * existed in the repository
		 */
		if (temp != null) {
			this.variable = temp;
		} else {
			TemporaryVariableRepository.add(sheet, cell, var, new Value());
			this.variable = TemporaryVariableRepository.getVariable(sheet, cell, var);
		}
	}

	@Override
	public Value evaluate() {
		return variable.getValue();
	}

	public TemporaryVariable getTempVariable() {
		return variable;
	}
        
        @Override
	public Object accept(ExpressionVisitor visitor) {
		return visitor.visitTempVariableRef(this);
	}

}
