/*
 * Copyright (c) 2005 Einar Pehrson <einar@pehrson.nu>.
 *
 * This file is part of
 * CleanSheets - a spreadsheet application for the Java platform.
 *
 * CleanSheets is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CleanSheets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CleanSheets; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package csheets.core.formula.lang;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Formula;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import csheets.core.formula.Literal;
import csheets.core.formula.compiler.ExcelExpressionCompiler;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.ext.share.ui.ShareMenu;
import csheets.ui.sheet.SpreadsheetTable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * A function that will process the parameters and return its result.
 *
 * @author JOSENUNO
 */
public class Eval implements Function {

    /**
     * The function's parameters
     */
    public static final FunctionParameter[] parameters = new FunctionParameter[]{
        new FunctionParameter(Value.Type.UNDEFINED, "Expression", false,
        "An expression whose value is checked for type compliance")
    };

    /**
     * Creates a new instance of the COUNT function.
     */
    public Eval() {
    }

    public String getIdentifier() {
        return "EVAL";
    }

    public Value applyTo(Expression[] argument) throws IllegalValueTypeException {
        Value value = argument[0].evaluate();

        /*SpreadsheetTable t = ShareMenu.action.getSpreadSheetTable();
        Cell c = t.getSelectedCell();*/
        
        Expression formula;
        try {
            ExcelExpressionCompiler cmp = new ExcelExpressionCompiler();
            formula = cmp.compile(null, "=" + value.toString());
        } catch (FormulaCompilationException ex) {
            Logger.getLogger(Eval.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        return formula.evaluate();
    }

    public FunctionParameter[] getParameters() {
        return parameters;
    }

    @Override
    public boolean isVarArg() {
        return false;
    }
}
