/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Cell;
import csheets.core.GlobalVariable;
import csheets.core.Spreadsheet;

import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.persistence.inmemory.GlobalVariableRepository;
import csheets.core.formula.util.ExpressionVisitor;

/**
 *
 * @author Tiago Lacerda
 */
public class GlobalVariableRef implements Expression {

    private GlobalVariable variable;

    public GlobalVariableRef(GlobalVariable variable) {
        this.variable = variable;
    }

    /**
     * Constructor for temporary variable references Checks if there's already
     * this reference for the sheet, cell and var starting with _
     */
    public GlobalVariableRef(Spreadsheet sheet, String var) {

        GlobalVariable temp = GlobalVariableRepository.getVariable(sheet, var); //search faz return da variable

        /**
         * Test to check if the temporary variable created (temp) already
         * existed in the repository
         */
        if (temp != null) {
            this.variable = temp;
        } else {
           GlobalVariableRepository.add(sheet, var, new Value());
            this.variable = GlobalVariableRepository.getVariable(sheet, var);
        }
    }

    @Override
    public Value evaluate() {
        return variable.getValue();
    }

    public GlobalVariable getGlobalVariable() {
        return variable;
    }

    @Override
    public Object accept(ExpressionVisitor visitor) {
        return visitor.visitGlobalVariableRef(this);
    }

}