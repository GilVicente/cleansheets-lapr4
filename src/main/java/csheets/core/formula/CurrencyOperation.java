/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.util.ExpressionVisitor;

/**
 *
 * @author Francisco
 */
public class CurrencyOperation extends Operation<Currency> {
    
    
    /**
	 * The unique version identifier used for serialization
	 */
	private static final long serialVersionUID = 2326739272985753461L;

	/**
	 * The currency which the user wants the operation to be converted to
	 */
	private Expression currencyTo;

	/**
	 * The value of the value
	 */
	private Expression value;

	public CurrencyOperation(Expression currencyTo, Currency operator, Expression value) {
		super(operator);
		this.currencyTo = currencyTo;
		this.value = value;
	}

	@Override
	public Value evaluate() throws IllegalValueTypeException {
		return operator.applyTo(currencyTo, value);
	}

	@Override
	public Object accept(ExpressionVisitor visitor) {
		return visitor.visitCurrencyOperation(this);
	}

	public Expression getCurrencyTo() {
		return currencyTo;
	}

	public Expression getValue() {
		return value;
	}
    
    
}
