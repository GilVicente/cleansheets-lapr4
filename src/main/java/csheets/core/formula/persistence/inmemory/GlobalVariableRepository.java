/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.persistence.inmemory;

import csheets.core.Spreadsheet;
import csheets.core.GlobalVariable;
import csheets.core.Value;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tiago Lacerda
 */
public class GlobalVariableRepository {
    
    /**
	 * List were the temporary variables are stored for each cell
	 */
	private static List<GlobalVariable> list = new ArrayList<GlobalVariable>();

	/**
	 * Adds variable to list
         * 
	 */
	public static void add(Spreadsheet sheet, String var, Value value) {

		GlobalVariable tempVar = new GlobalVariable(sheet,var, value);
		list.add(tempVar);

	}

	/**
         * Updates variable
         * 
	 */
	public static void update(GlobalVariable globalVar, Value value) {

		if (list.contains(globalVar)) {
			int index = list.indexOf(globalVar);
			list.get(index).setValue(value);
		}

	}

	/**
	 *
	 * @return list
	 */
	public List<GlobalVariable> getAll() {
		return list;
	}

	/**
         * Given a sheet, cell and var name this method finds the matching variable
         * 
	 */
	public static GlobalVariable getVariable(Spreadsheet sheet, String var) {

		for (GlobalVariable storedVar : list) {
			if (storedVar.getName().equals(var) && storedVar.getSheet().equals(sheet)
                            )
                        {
				return storedVar;
			}
		}
		return null;
	}
    
}