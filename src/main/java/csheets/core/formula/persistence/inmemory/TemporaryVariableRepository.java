/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.persistence.inmemory;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.TemporaryVariable;
import csheets.core.Value;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gil
 */
public class TemporaryVariableRepository {

	/**
	 * List were the temporary variables are stored for each cell
	 */
	private static List<TemporaryVariable> list = new ArrayList<TemporaryVariable>();

	/**
	 * Adds variable to list
         * 
	 */
	public static void add(Spreadsheet sheet, Cell cell,String var, Value value) {

		TemporaryVariable tempVar = new TemporaryVariable(sheet, cell,var, value);
		list.add(tempVar);

	}

	/**
         * Updates variable
         * 
	 */
	public static void update(TemporaryVariable tempVar, Value value) {

		if (list.contains(tempVar)) {
			int index = list.indexOf(tempVar);
			list.get(index).setValue(value);
		}

	}

	/**
	 *
	 * @return list
	 */
	public List<TemporaryVariable> getAll() {
		return list;
	}

	/**
         * Given a sheet, cell and var name this method finds the matching variable
         * 
	 */
	public static TemporaryVariable getVariable(Spreadsheet sheet, Cell cell, String var) {

		for (TemporaryVariable storedVar : list) {
			if (storedVar.getName().equals(var) && storedVar.getSheet().equals(sheet)
                            && storedVar.getCell().equals(cell))
                        {
				return storedVar;
			}
		}
		return null;
	}

}
