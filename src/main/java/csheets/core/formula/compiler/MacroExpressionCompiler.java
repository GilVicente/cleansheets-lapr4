/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.compiler;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperation;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionCall;
import csheets.core.formula.Literal;
import csheets.core.formula.Reference;
import csheets.core.formula.UnaryOperation;
import csheets.core.formula.lang.CellReference;
import csheets.core.formula.lang.Language;
import csheets.core.formula.lang.RangeReference;
import csheets.core.formula.lang.ReferenceOperation;
import csheets.core.formula.lang.UnknownElementException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 *
 * @author 1140234
 */
public class MacroExpressionCompiler implements ExpressionCompiler {

    /**
     * The character that signals that a cell's content is a formula ('=')
     */
    public static final char FORMULA_STARTER = '%';

    /**
     * Creates the Excel expression compiler.
     */
    public MacroExpressionCompiler() {
    }

    public char getStarter() {
        return FORMULA_STARTER;
    }

    public Expression compile(Cell cell, String source) throws FormulaCompilationException {
        // Creates the lexer and parser
        ANTLRStringStream input = new ANTLRStringStream(source);

        // create the buffer of tokens between the lexer and parser 
        FormulaLexer lexer = new FormulaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        FormulaParser parser = new FormulaParser(tokens);

        CommonTree tree = null;

        try {
            // Attempts to match an expression
            tree = (CommonTree) parser.expression().getTree();
        } catch (RecognitionException e) {
            //String message="Fatal recognition exception " + e.getClass().getName()+ " : " + e;
            String message = parser.getErrorMessage(e, parser.tokenNames);
            throw new FormulaCompilationException("At (" + e.line + ";" + e.charPositionInLine + "): " + message);
        } catch (Exception e) {
            String message = "Other exception : " + e.getMessage();
            throw new FormulaCompilationException(message);
        }

        try {
            // Converts the expression and returns it
            return convert(cell, tree);
        } catch (IllegalValueTypeException ex) {
            Logger.getLogger(ExcelExpressionCompiler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Converts the given ANTLR AST to an expression.
     *
     * @param cell cell
     * @param node the abstract syntax tree node to convert
     * @return the result of the conversion
     * @throws csheets.core.formula.compiler.FormulaCompilationException
     * exception
     */
    protected Expression convert(Cell cell, Tree node) throws FormulaCompilationException, IllegalValueTypeException {
        //System.out.println("Converting node '" + node.getText() + "' of tree '" + node.toStringTree() + "' with " + node.getChildCount() + " children.");

        //if root of tree is null means that tree represents a block of instructions
        if (node.getText() == null) {
            return convertBlockOfInstructions(cell, node);
        } else if (node.getText().equals("FOR")) {
            return convertForBlockOfInstructions(cell, node);
        }

        if (node.getChildCount() == 0) {
            try {
                switch (node.getType()) {
                    case FormulaLexer.NUMBER:
                        return new Literal(Value.parseNumericValue(node.getText()));
                    case FormulaLexer.STRING:
                        return new Literal(Value.parseValue(node.getText(), Value.Type.BOOLEAN, Value.Type.DATE));
                    case FormulaLexer.CELL_REF:
                        return new CellReference(cell.getSpreadsheet(), node.getText());
//					case FormulaParserTokenTypes.NAME:
						/* return cell.getSpreadsheet().getWorkbook().
                     getRange(node.getText()) (Reference)*/
                }
            } catch (ParseException e) {
                throw new FormulaCompilationException(e);
            }
        }

        // Convert function call
        Function function = null;
        try {
            function = Language.getInstance().getFunction(node.getText());
        } catch (UnknownElementException e) {
        }

        if (function != null) {
            List<Expression> args = new ArrayList<Expression>();
            Tree child = node.getChild(0);
            if (child != null) {
                for (int nChild = 0; nChild < node.getChildCount(); ++nChild) {
                    child = node.getChild(nChild);
                    args.add(convert(cell, child));
                }
            }
            Expression[] argArray = args.toArray(new Expression[args.size()]);
            return new FunctionCall(function, argArray);
        }

        if (node.getChildCount() == 1) // Convert unary operation
        {
            return new UnaryOperation(
                    Language.getInstance().getUnaryOperator(node.getText()),
                    convert(cell, node.getChild(0))
            );
        } else if (node.getChildCount() == 2) {
            // Convert binary operation
            BinaryOperator operator = Language.getInstance().getBinaryOperator(node.getText());
            if (operator instanceof RangeReference) {
                return new ReferenceOperation(
                        (Reference) convert(cell, node.getChild(0)),
                        (RangeReference) operator,
                        (Reference) convert(cell, node.getChild(1))
                );
            } else {
                return new BinaryOperation(
                        convert(cell, node.getChild(0)),
                        operator,
                        convert(cell, node.getChild(1))
                );
            }
        } else // Shouldn't happen
        {
            throw new FormulaCompilationException();
        }
    }

    public Expression convertForBlockOfInstructions(Cell cell, Tree node) throws FormulaCompilationException, IllegalValueTypeException {
        List<Expression> exp = new ArrayList<>();
        for (int i = 0; i < node.getChildCount(); i++) {
            if (node.getChild(i).getChildCount() > 0) {
                Expression expression = convert(cell, node.getChild(i));
                exp.add(expression);
            }
        }
        for (exp.get(0).evaluate(); exp.get(1).evaluate().toBoolean();) {
            for (int i = 2; i < exp.size(); i++) {
                exp.get(i).evaluate();
            }
        }
        Expression emptyReturn = new Literal(new Value());
        return emptyReturn;
    }

    public Expression convertBlockOfInstructions(Cell cell, Tree node) throws FormulaCompilationException, IllegalValueTypeException {
        Expression expression = null;
        for (int i = 0; i < node.getChildCount(); i++) {
            if (node.getChild(i).getChildCount() > 0) {
                expression = convert(cell, node.getChild(i));
                expression.evaluate();
            }
        }
        return expression;
    }
}
