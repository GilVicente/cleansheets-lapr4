/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.compiler;

import csheets.core.Cell;
import csheets.core.Value;
import csheets.core.formula.BinaryOperation;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.CurrencyOperation;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionCall;
import csheets.core.formula.Literal;
import csheets.core.formula.Reference;
import csheets.core.formula.lang.CellReference;
import csheets.core.formula.lang.Language;
import csheets.core.formula.lang.RangeReference;
import csheets.core.formula.lang.ReferenceOperation;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

/**
 *
 * @author Francisco
 */
public class CurrencyExpressionCompiler implements ExpressionCompiler {

    /**
     * The character that signals that a cell's content is a formula ('#')
     */
    public static final char FORMULA_STARTER = '#';

    /**
     * Creates the Excel expression compiler.
     */
    public CurrencyExpressionCompiler() {
    }

    public char getStarter() {
        return FORMULA_STARTER;
    }

    public Expression compile(Cell cell, String source) throws FormulaCompilationException {
        // Creates the lexer and parser
        ANTLRStringStream input = new ANTLRStringStream(source);

        // create the buffer of tokens between the lexer and parser
        FormulaLexer lexer = new FormulaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        FormulaParser parser = new FormulaParser(tokens);

        CommonTree tree = null;

        try {
            // Attempts to match an expression
            tree = (CommonTree) parser.expression().getTree();
        }/* catch (MismatchedTokenException e){
         //not production-quality code, just forming a useful message
         String expected = e.expecting == -1 ? "<EOF>" : parser.tokenNames[e.expecting];
         String found = e.getUnexpectedType() == -1 ? "<EOF>" : parser.tokenNames[e.getUnexpectedType()];

         String message="At ("+e.line+";"+e.charPositionInLine+"): "+"Fatal mismatched token exception: expected " + expected + " but was " + found;   
         throw new FormulaCompilationException(message);
         } catch (NoViableAltException e) {
         //String message="Fatal recognition exception " + e.getClass().getName()+ " : " + e;
         String message=parser.getErrorMessage(e, parser.tokenNames);
         String message2="At ("+e.line+";"+e.charPositionInLine+"): "+message;
         throw new FormulaCompilationException(message2);
         } */ catch (RecognitionException e) {
            //String message="Fatal recognition exception " + e.getClass().getName()+ " : " + e;
            String message = parser.getErrorMessage(e, parser.tokenNames);
            throw new FormulaCompilationException("At (" + e.line + ";" + e.charPositionInLine + "): " + message);
        } catch (Exception e) {
            String message = "Other exception : " + e.getMessage();
            throw new FormulaCompilationException(message);
        }
        // Converts the expression and returns it
        return convert(cell, tree);
    }

    /**
     * Converts the given ANTLR AST to an expression.
     *
     * @param node the abstract syntax tree node to convert
     * @return the result of the conversion
     */
    protected Expression convert(Cell cell, Tree node) throws FormulaCompilationException {

        if (node.getChildCount() == 0) {
            try {
                switch (node.getType()) {
                    case FormulaLexer.NUMBER:
                        return new Literal(Value.parseNumericValue(node.getText()));
                    case FormulaLexer.STRING:
                        return new Literal(Value.parseValue(node.getText(), Value.Type.BOOLEAN, Value.Type.DATE));
                    case FormulaLexer.CELL_REF:
                        return new CellReference(cell.getSpreadsheet(), node.getText());

                }
            } catch (ParseException e) {
                throw new FormulaCompilationException(e);
            }
        }

        //Convert function call
        Function function = null;
        try {
            function = Language.getInstance().getFunction(node.getText());
        } catch (Exception e) {
        }
        if (function != null) {
            List<Expression> args = new ArrayList<Expression>();
            Tree child = node.getChild(0);
            if (child != null) {
                for (int nChild = 0; nChild < node.getChildCount(); ++nChild) {
                    child = node.getChild(nChild);
                    args.add(convert(cell, child));
                }
            }
            Expression[] argArray = args.toArray(new Expression[args.size()]);
            return new FunctionCall(function, argArray);
        } else {

            if (node.getChildCount() == 1) // Convert 
            {
                return new CurrencyOperation(convert(cell, node.getChild(1)), Language.getInstance().getCurrencyOperator(node.getText()), convert(cell, node.getChild(0))
                );
            } else if (node.getChildCount() == 2) {
                // Convert 
                BinaryOperator operator = Language.getInstance().getBinaryOperator(node.getText());
                if (operator instanceof RangeReference) {
                    return new ReferenceOperation(
                            (Reference) convert(cell, node.getChild(0)), (RangeReference) operator, (Reference) convert(cell, node.getChild(1)));
                } else {
                    return new BinaryOperation(convert(cell, node.getChild(0)), operator, convert(cell, node.getChild(1)));
                }
            } else if (node.getChild(0).getText().equals("{")) {
                return convertSequence(cell, node);
            } else // Shouldn't happen
            {
                throw new FormulaCompilationException();
            }
        }
    }

    public Expression convertSequence(Cell cell, Tree node) throws FormulaCompilationException {
        List<Expression> lex = new ArrayList();
        for (int i = 1; i < node.getChildCount() - 1; ++i) {
            Tree child = node.getChild(i);
            if (child != null) {
                lex.add(convert(cell, child));
            }
        }

        Expression[] argArray = lex.toArray(new Expression[lex.size()]);
        return argArray[0];
    }

}
