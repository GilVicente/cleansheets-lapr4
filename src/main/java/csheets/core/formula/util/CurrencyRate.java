/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.util;

import java.math.BigDecimal;

/**
 *
 * @author Francisco
 */
public class CurrencyRate {
    
    /**
     * Name of the type of the coin (euro, dollar, pound)
     */
    private String coin;
    /**
     * type : Dollar
     */
    private BigDecimal dollar;
    /**
     * type : Euro
     */
    private BigDecimal euro;
    /**
     * type : Pound
     */
    private BigDecimal pound;
    
    public CurrencyRate(){
        
        this.coin = "EURO";
        this.dollar = new BigDecimal("1.1249");
        this.pound = new BigDecimal("0.7341");
        this.euro = new BigDecimal("1");
    }
    
    public void setCoin(String coin){
        
        this.coin = coin;
        updateCoin();
        
    }
    
    
    public void updateCoin(){
        
        switch (coin) {
            case "EURO":
                setDollar(new BigDecimal("1,1400"));
                setPound(new BigDecimal("0.7835"));
                setEuro(new BigDecimal("1"));
                break;
            case "DOLLAR":
                setDollar(new BigDecimal("1"));
                setPound(new BigDecimal ("0,6869"));
                setEuro(new BigDecimal("0,8770"));
                break;
            case "POUND":
                setDollar(new BigDecimal("1,4556"));
                setPound(new BigDecimal("1"));
                setEuro(new BigDecimal("1,27660"));
                break;
        }
    }

    public void setDollar(BigDecimal d) {
        this.dollar = d;
    }

    public void setPound(BigDecimal p) {
        this.pound = p;
    }

    public void setEuro(BigDecimal e) {
        this.euro = e;
    }
    public BigDecimal getDollar(){
        return this.dollar;
    }
    public BigDecimal getEuro(){
        return this.euro;
    }
    public BigDecimal getPound(){
        return this.pound;
    }
    
    
}
