/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.util.CurrencyRate;

/**
 *
 * @author Francisco
 */
public interface Currency extends Operator {
    
    public CurrencyRate currencyRate = new CurrencyRate();

    public Value applyTo(Expression currencyTo, Expression valueOf) throws IllegalValueTypeException;
    
}
