/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core;

/**
 *
 * @author Gil
 */
public class TemporaryVariable {

	private Spreadsheet sheet;
        
        private Cell cell;
        
	private String name;

	private Value varValue;

	/**
	 * Constructor for the class TempVariable, creates a new temporary variable
	 * Uses sheet, cell, name and varValue to specifie temp var
	 */
	public TemporaryVariable(Spreadsheet sheet, Cell cell,String name, Value varValue) {

		this.sheet = sheet;
                this.cell = cell;
		this.name = name;
		this.varValue = varValue;
	}

	/**
	 * Method to get the name of the temporary variable
	 *
	 * @return the name of the temporary variable
	 */
	public String getName() {
		return name;
	}

	/**
	 * Method to get the value of the temporary variable
	 *
	 * @return the value of the temporary variable
	 */
	public Value getValue() {
		return varValue;
	}

	/**
	 * Method to get the sheet where the temporary variable is being used
	 *
	 * @return the sheet where the temporary variable is being used
	 */
	public Spreadsheet getSheet() {
		return sheet;
	}
        /**
         * Method to get the cell where the temporary variable is being used
         * @return 
         */
        public Cell getCell(){
            return cell;
        }

	/**
	 *
	 * @param value is the new value applied to the temporary variable
	 */
	public void setValue(Value value) {
		this.varValue = value;
	}

	/**
	 *
	 * Method declaration to print
	 *
	 * @return name and value format
	 */
	@Override
	public String toString() {
		return (this.getName());
	}

}
