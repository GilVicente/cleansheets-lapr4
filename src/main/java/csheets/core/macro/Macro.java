/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.macro;

import csheets.core.IllegalValueTypeException;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Formula;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.core.formula.util.ExpressionVisitor;
import csheets.ext.macros.MacroWorkbookRepo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Class that represents a Macro and its components
 *
 * @author 1140234 and Antonio Soutinho changed by Patrícia Monteiro (1140807)
 */
public class Macro implements Serializable, Expression {

    /**
     * Unique identifier for the macro.
     */
    private final String name;

    /**
     * The sheet this macro refers to.
     */
    private final Spreadsheet sheet;

    /**
     * The lines of code written by the user.
     */
    private List<String> lines;
    /**
     * The list of formulas compiled from the written lines.
     */
    private final List<Formula> formulas;

    private String[] parameters;

    private MacroWorkbookRepo repo;

    /**
     * Creates a new macro.
     *
     * @param repo MacroWorkbook repository
     * @param name A String identifier for this macro.
     * @param sheet The Spreadsheet to associate with the macro.
     */
    public Macro(MacroWorkbookRepo repo, String name, Spreadsheet sheet) {
        //validateName(name);
        this.repo = repo;        
        String[] temp = getParameters(name);        
        this.name = name.substring(0, name.indexOf("("));
        parameters = temp;
        this.sheet = sheet;
        this.lines = new ArrayList<String>();
        this.formulas = new ArrayList<Formula>();
    }

    /**
     * Validates the name of a macro wich can only contain letters
     *
     * @param name name Macro
     */
    private void validateName(String name) {
        if (!Pattern.matches("[0-9]*[a-zA-Z]+[0-9]*[a-zA-Z]*", name)) {
            throw new IllegalArgumentException("Macro name can only contain letters and numbers and () at final");
        }
    }

    /**
     * Returns the name of the macro.
     *
     * @return Name of the macro.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the instructions of this macro
     *
     * @return String with the instructions.
     */
    public String getMacroText() {
        StringBuilder builder = new StringBuilder();
        for (String line : lines) {
            builder.append(line);
            builder.append("\n");
        }

        return builder.toString();
    }

    public String[] parameters() {
        return parameters;
    }

    /**
     * Adds a line of instructions to the macro.
     *
     * @param line An instruction, in string format.
     */
    public void addLine(String line) {
        //Stores every line, including comments, as text
        lines.add(line);
        //The cell passed as a parameter doesn's matter since it is only used to find out the active sheet
    }

    /**
     * Runs the macro. Iterates over all the operations and executes each one.
     *
     * @return The result of the last operation executed
     * @throws IllegalValueTypeException If there's an invalid value in one of
     * the operations
     */
    public Value run() throws IllegalValueTypeException {

        convertToFormulas(macroReplace());
        Value result = null;
        for (Formula f : formulas) {
            result = f.evaluate();
        }
        return result; //Returns last result only
    }

    private void convertToFormulas(List<String> lines) {
        formulas.clear();
        for (String line : lines) {
            Formula f;
            try {
                f = FormulaCompiler.getInstance().compile(sheet.getCell(128, 128), "=" + line);
                if (f.getExpression() != null) {
                    formulas.add(f);
                }
            } catch (FormulaCompilationException ex) {
                Logger.getLogger(Macro.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Should return a list with the lines instead of replacing original;
    //This makes it so the saved code is not affected.
    private List<String> macroReplace() {
        //Should create copy of this.lines;
        List<String> copyLines = this.lines.subList(0, lines.size());//linhas da macro
        
        
        for (int i = 0; i < copyLines.size(); i++) {
                       
            for (Macro elementMacro : repo.values()) {           //lista da macros 

                //identifies parameters
                if (copyLines.get(i).matches("(.*)" + elementMacro.name + "(.*)")) {

                    //ir buscar parametros   
                    String[] res = getParameters(copyLines.get(i));
                  
                       
                    copyLines.remove(i);
                    List<String> parameterInitialization = new LinkedList();
                    for (int j = 0; j < res.length; j++) {
                        String param = "_" + elementMacro.parameters()[j] + ":=" + res[j];
                        parameterInitialization.add(param);
                    }
                    addAllAtFirst(copyLines, parameterInitialization, i);
                    addAllAtFirst(copyLines, elementMacro.lines, i);
                    i = 0;
                }
            }
        }

        return copyLines;
    }

    /*
    * after changing the variable back to the beginning of the line
     */
    private void addAllAtFirst(List<String> destination, List<String> source, int currentLine) {
        for (String element : source) {
            
            if (!element.equals("_:="))
                destination.add(currentLine, element);
        }
    }

    /**
     * GetParameters - It will only get the parameters of the macro
     */
    private static String[] getParameters(String macroDeclaration) {
        String[] result = {};
        String copy = macroDeclaration;
               
        if (copy.matches(".*(.*)")) {           
            copy = copy.substring(copy.indexOf("("), copy.indexOf(")"));
        
            if (copy != null) {              
                copy = copy.replaceAll("[() ]", "");             
                result = copy.split(";");               
            }           
            return result;            
        }
     
        return result;
    }

    @Override
    public Value evaluate() throws IllegalValueTypeException {
        return this.run();
    }

    @Override
    public Object accept(ExpressionVisitor visitor) {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Macro)) {
            return false;
        }
        Macro otherM = (Macro) o;

        return this.name.equals(otherM.name);
    }

    @Override
    public int hashCode() {
        return super.hashCode() + this.name.hashCode() * 7;
    }
}
