/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core;

/**
 *
 * @author Tiago Lacerda
 */
public class GlobalVariable {

    private Spreadsheet sheet;


    private String name;

    private Value varValue;

    /**
     * Constructor for the class GlobalVariable, creates a new temporary variable
     * Uses sheet, cell, name and varValue to specifie temp var
     */
    public GlobalVariable(Spreadsheet sheet, String name, Value varValue) {

        this.sheet = sheet;
        this.name = name;
        this.varValue = varValue;
    }

    /**
     * Method to get the name of the global variable
     *
     * @return the name of the global variable
     */
    public String getName() {
        return name;
    }

    /**
     * Method to get the value of the global variable
     *
     * @return the value of the global variable
     */
    public Value getValue() {
        return varValue;
    }

    /**
     * Method to get the sheet where the global variable is being used
     *
     * @return the sheet where the global variable is being used
     */
    public Spreadsheet getSheet() {
        return sheet;
    }



    /**
     *
     * @param value is the new value applied to the global variable
     */
    public void setValue(Value value) {
        this.varValue = value;
    }

    /**
     *
     * Method declaration to print
     *
     * @return name and value format
     */
    @Override
    public String toString() {
        return (this.getName());
    }


}