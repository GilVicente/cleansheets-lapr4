/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ui;

import csheets.ext.ToContinue;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static csheets.ext.ToContinue.tContinue;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 *
 * @author Gil
 */
public class SelectExtensions {
    private static List<JCheckBox> checkBoxes = new ArrayList<JCheckBox>();
    private static BufferedReader buffer = null;
    private static JFrame frame = new JFrame("Extensions");
    private static JDialog dialog = null;
    
    public SelectExtensions(String inputStreamString,ToContinue toContinue){
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
       
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    selectExtensionsUI(inputStreamString,toContinue);
                } catch (IOException ex) {
                    Logger.getLogger(SelectExtensions.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
    private static void selectExtensionsUI(String inputStreamString, ToContinue toContinue) throws IOException {
        //Create and set up the window.
        dialog = new JDialog();
        dialog.setTitle("Select Extensions To Be Loaded");
        dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        dialog.setBackground(Color.blue);
        
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("closing...");
                System.exit(0);
            }
        });
        
        SelectExtensionsCheckBoxList cbList = new SelectExtensionsCheckBoxList();
        String info;
        
        String str[] = inputStreamString.split("\n");
        //System.out.println("SPlit");
        for(String s: str){
            //System.out.println(s);
            JCheckBox check = new JCheckBox(s);
            check.setSelected(true);
            checkBoxes.add(check);
        }
        cbList.setListData(checkBoxes.toArray());
        dialog.getContentPane().add(cbList);

        JButton okay = new JButton("Ok");
        okay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okayActionPerformed(evt,toContinue);
            }
   
        });
        dialog.add(okay, BorderLayout.SOUTH);

        //Display the window.
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        
    }

    public static void okayActionPerformed(java.awt.event.ActionEvent evt,ToContinue toContinue) {
        for (JCheckBox checkBox : checkBoxes) {
            if (checkBox.isSelected()) {
                String[] str = checkBox.getText().split("-");
                toContinue.selectedExtensions.add(str[0]);
            }
        }
        tContinue = true;
        dialog.setVisible(false); //you can't see me!
        //dialog.dispose(); //Destroy the JFrame object

    }

}
