/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.emailConfiguration;

import java.io.Serializable;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author José Santos (1140921)
 */
public class EmailConfiguration implements Serializable {

    static Properties mailServerProperties;
    static Session getMailSession;
    static MimeMessage generateMailMessage;
    private String name;
    private String email;
    private String emailTo;
    private String emailBody;
    private String subject;

//    public static void main(String args[]) throws MessagingException {
//        generateAndSendEmail();
//        System.out.println("\n\n ===> Your Java Program has just sent an Email successfully. Check your email..");
//    }
    public EmailConfiguration(String name) {
        setName(name);
    }

    /**
     * Generate and send an Email
     *
     * @param name User name
     * @param email User email
     * @throws AddressException
     * @throws MessagingException
     */
    public void sendEmail() throws AddressException, MessagingException {

        // setup Mail Server Properties..
        // System.out.println("\n 1st ===> setup Mail Server Properties..");
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        //System.out.println("Mail Server Properties have been setup successfully..");

        // get Mail Session..
        // System.out.println("\n\n 2nd ===> get Mail Session..");
        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        generateMailMessage = new MimeMessage(getMailSession);
        generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(this.getEmailTo()));
        generateMailMessage.setSubject("Greetings ..");

        generateMailMessage.setContent(getEmailBody(), "text/html");
        //System.out.println("Mail Session has been created successfully..");

        // Get Session and Send mail
        //  System.out.println("\n\n 3rd ===> Get Session and Send mail");
        Transport transport = getMailSession.getTransport("smtp");

        // Enter your correct gmail UserID and Password
        transport.connect("smtp.gmail.com", "laprIsepDB", "password12345Easy");
        transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
        transport.close();
    }

    /**
     * Validate the email
     *
     * @param email to validate
     * @return true if is valid
     */
    public boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    /**
     * @return the emailTo
     */
    public String getEmailTo() {
        return emailTo;
    }

    /**
     * @return the emailBody
     */
    public String getEmailBody() {
        return emailBody;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        if (isValidEmailAddress(email)) {
            this.email = email;
        } else {
            throw new IllegalArgumentException("Invalid Email");
        }
    }

    /**
     * @param emailTo the emailTo to set
     */
    public void setEmailTo(String emailTo) {
        if (isValidEmailAddress(emailTo)) {
            this.emailTo = emailTo;
        } else {
            throw new IllegalArgumentException("Invalid Email");
        }
    }

    /**
     * @param emailBody the subject to set
     */
    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    @Override
    public String toString(){
        return this.getSubject();
    }

}
