/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.emailConfiguration;

import javax.mail.MessagingException;

/**
 *
 * @author José Santos (1140921)
 */
public class EmailConfigurationController {

    EmailConfiguration emailConfig;

    /**
     * setup the configurations for email.
     *
     * @param name user
     */
    public void ConfigureEmailName(String name, String emailTo, String subject, String body) {
        emailConfig = new EmailConfiguration(name);
        emailConfig.setEmailTo(emailTo);
        emailConfig.setEmailBody(subject);
        emailConfig.setSubject(subject);
    }
    /**
     * Send an email to the respective configurations
     * @throws MessagingException 
     */
    public void sendEmail() throws MessagingException{
        emailConfig.sendEmail();
    }

}
