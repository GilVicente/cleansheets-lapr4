package csheets.ext.emailConfiguration.ui;

import csheets.ext.email.SendEmailAction;
import csheets.ext.simple.ui.*;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;

import csheets.ui.ctrl.UIController;

/**
 * Representes the UI extension menu of the simple extension.
 * @author Alexandre Braganca
 */
public class EmailMenu extends JMenu {

	/**
	 * Creates a new simple menu.
	 * This constructor creates and adds the menu options. 
	 * In this simple example only one menu option is created.
	 * A menu option is an action (in this case {@link csheets.ext.simple.ui.ExampleAction})
	 * @param uiController the user interface controller
	 */
	public EmailMenu(UIController uiController) {
		super("Email");
		setMnemonic(KeyEvent.VK_E);

		// Adds font actions
		add(new ConfigurationEmailAction(uiController));
                add(new SendEmailAction(uiController));
	}	
}
