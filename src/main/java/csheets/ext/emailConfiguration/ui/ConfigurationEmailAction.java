package csheets.ext.emailConfiguration.ui;

import csheets.ext.emailConfiguration.EmailConfigurationController;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.Frame;

/**
 * An action of the simple extension that exemplifies how to interact with the
 * spreadsheet.
 *
 * @author José Santos (1140921)
 */
public class ConfigurationEmailAction extends BaseAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public ConfigurationEmailAction(UIController uiController) {
        this.uiController = uiController;
    }

    protected String getName() {
        return "Email configuration";
    }

    protected void defineProperties() {
    }

    /**
     * A simple action that presents a email configuration dialog. The user
     * insert is contents (destination, subject and body)
     *
//     * @param event the event that was fired
     */
    public void actionPerformed(ActionEvent event) {

        new EmailConfigurationJDialog((Frame)null, this.uiController);

    }
}
