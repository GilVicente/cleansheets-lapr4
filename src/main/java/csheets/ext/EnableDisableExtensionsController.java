package csheets.ext;

import csheets.CleanSheets;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.awt.Component;

/**
 * Controller that allows to enable and disable extensions.
 * 
 * @author 1141277
 * @author 1140234
 * @author 1140439
 * @author 1140618
 */
public class EnableDisableExtensionsController {

    private CleanSheets app;
    private UIController controller;

    public EnableDisableExtensionsController(CleanSheets app, UIController controller) {
        this.app = app;
        this.controller = controller;
    }
    
    /**
     * Method to make the wanted Extension disabled.
     * @param uie UIExtension corresponding to the extension
     * @param flag boolean that represents the actual state of the extension(disabled or enabled)
     */
    public void setEnableOrDisableExtension(UIExtension uie, boolean flag){
        
        if (uie.getMenu() != null) {
            uie.getMenu().setEnabled(flag);
        }
        
        if (uie.getToolBar() != null) {
            for (Component comp : uie.getToolBar().getComponents()) {
                comp.setEnabled(flag);
                //comp.setVisible(!flag);
            }
        }
        
        if (uie.getSideBar() != null) {
            for (Component comp : uie.getSideBar().getComponents()) {
                //comp.setEnabled(!flag);
                comp.setVisible(flag);
            }
        }
    }
    
    /**
     * Searches an UIExtension for its components to find out if its enabled or not.
     * @param uie UIExtension to test
     * @return true or false wether the extension is enabled or not
     */
    public boolean isEnabled(UIExtension uie){
        
        if (uie.getMenu() != null) {
            if (uie.getMenu().isEnabled()) {
                return true;
            }
        }
        
        if (uie.getToolBar() != null) {
            for (Component comp : uie.getToolBar().getComponents()) {
                if (comp.isEnabled()) {
                    return true;
                }
            }
        }
        
        if (uie.getSideBar() != null) {
            for (Component comp : uie.getSideBar().getComponents()) {
                if (comp.isEnabled()) {
                    return true;
                }
            }
        }
        return false;
    }
}
