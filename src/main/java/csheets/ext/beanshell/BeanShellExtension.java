/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell;

import csheets.ext.Extension;
import csheets.ext.beanshell.ui.UIBeanShellExtension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Daniela Ferreira
 */
public class BeanShellExtension extends Extension {

    /**
     * The name of the extension
     */
    public static final String NAME = "BeanShell";

    /**
     * Creates a new BeanShell extension.
     */
    public BeanShellExtension() {
        super(NAME);
    }
    
    /**
	 * Returns the user interface extension of this extension
	 * In this extension of BeanShell we are only extending the user interface.
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
        @Override
	public UIExtension getUIExtension(UIController uiController) {
                if (this.uiExtension == null) {
			this.uiExtension = new UIBeanShellExtension(this, uiController);
		}
		return this.uiExtension;
	}

}
