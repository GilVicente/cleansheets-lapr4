/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell.ui;

import csheets.ext.beanshell.BeanShellExtension;
import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author Daniela Ferreira
 */
public class BeanShellMenu extends JMenu{

    private JMenuItem aux;
    private UIController m_uic;

    /**
     * Creates the BeanShell menu. This constructor creates and adds the menu
     * options. In this simple example only one menu option is created. 
     *
     * @param uiController the user interface controller
     */
    public BeanShellMenu(UIController uiController) {
        super(BeanShellExtension.NAME);
        add(new BeanShellAction(uiController));

    }
    
}
