/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Daniela Ferreira 
 */
public class BeanShellAction extends BaseAction{
    private BeanShellWindow bsw;
    /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public BeanShellAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "BeanShell";
    }

    @Override
    protected void defineProperties() {
    }

    /**
     * A BeanShell action that presents a confirmation dialog. If the user confirms
     * then the contents of the cell A1 of the current sheet are set to the
     * string "Changed".
     *
     * @param event the event that was fired
     */
    @Override
    public void actionPerformed(ActionEvent event) {

         bsw = new BeanShellWindow(null, uiController);
            
	}
}
