/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.beanshell.ui;

import bsh.EvalError;
import bsh.Interpreter;
import bsh.util.JConsole;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.*;

/**
 * Class for BeanShell Console
 *
 * @author Daniela Ferreira
 */
public class UIBeanShell extends JDialog {

    private UIController uic;

    /**
     * This Method was the User Interface to the BeanShell extension
     *
     * @param parent parent
     * @param uiController uiController
     */
    public UIBeanShell(Frame parent, UIController uiController) {
        super(parent, "BeanShell");
        this.uic = uiController;
        JPanel mainPanel = new JPanel(new BorderLayout());
        JConsole area = new JConsole();
        mainPanel.add(area, BorderLayout.CENTER);

        /**
         * Calling the console from the BSH Library.A new interactive
         * interpreter attached to the specified console.
         *
         */
        Interpreter interpreter = new Interpreter(area);

        /**
         * Create an interpreter for evaluation only.
         *
         */
        Interpreter i = new Interpreter();

        try {
            JOptionPane.showMessageDialog(parent, (String) i.source("beanshellScripts/testScript.bsh"),
                    "Success!", JOptionPane.INFORMATION_MESSAGE);
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(parent, e, "Not Found!", JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(parent, e, "Not Found!", JOptionPane.ERROR_MESSAGE);
        } catch (EvalError e) {
            JOptionPane.showMessageDialog(parent, e, "Evaluation Failed!", JOptionPane.ERROR_MESSAGE);
        }

        try {
            interpreter.set("controller", uiController);
        } catch (EvalError e) {
            JOptionPane.showMessageDialog(parent, e, "Not Found!", JOptionPane.ERROR_MESSAGE);
        }

        new Thread(interpreter).start();

        add(mainPanel);
        setSize(600, 400);
        setLocationRelativeTo(null);
        setVisible(true);

    }
}
