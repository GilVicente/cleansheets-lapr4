/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Cell;
import csheets.core.CellListener;
import static java.lang.Thread.sleep;

/**
 *
 * @author Tixa
 */
public class ExportThread implements Runnable {

	private int flag;
	private int interrupt = 0;
	private FileLink fl = new FileLink();
	private final CellChangeListenerExport cg = new CellChangeListenerExport();

	/**
	 * Sets the FileLink to this thread
	 *
	 * @param fl
	 */
	public ExportThread(FileLink fl) {
		this.fl = fl;
		this.fl.getSheet().addCellListener(cg);
	}

	/**
	 * run method of the thread
	 *
	 */
	@Override
	public void run() {
		flag = 1;
		for (;;) {
			if (interrupt == 1) {
				Thread.currentThread().interrupt();
				break;
			}
			ExportTXT expTxt = new ExportTXT();
			expTxt.
				exporttoTXT(fl.getSheet(), fl.getFile(), fl.getHeaderOption(), fl.
							getSeparator(), fl.getHeader());
			flag = 0;
			while (flag == 0) {
				try {
					sleep(10);
				} catch (InterruptedException e) {
					End();
				}
			}
		}
	}

	/**
	 * Class that implements the CellListener interface and alerts any changes
	 * that occurs on the sheet
	 *
	 */
	private class CellChangeListenerExport implements CellListener {

		public CellChangeListenerExport() {
		}

		@Override
		public void valueChanged(Cell cell) {
			flag = 1;
		}

		@Override
		public void contentChanged(Cell cell) {
			flag = 1;
		}

		@Override
		public void dependentsChanged(Cell cell) {
		}

		@Override
		public void cellCleared(Cell cell) {
		}

		@Override
		public void cellCopied(Cell cell, Cell source) {
		}
	}

	private void End() {
		interrupt = 1;
	}

}
