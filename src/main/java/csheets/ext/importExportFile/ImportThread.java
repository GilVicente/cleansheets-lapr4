/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Spreadsheet;
import java.io.File;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

/**
 *
 * @author Tixa
 */
public class ImportThread implements Runnable {

	private FileLink fl;
	private ImportTXT imp;
	private Path path;
	private String fileN;
	private Spreadsheet sheet;

	/**
	 * Sets the FileLink to this Thread
	 *
	 * @param fl
	 */
	public ImportThread(FileLink fl) {
		this.fl = fl;
		this.sheet = fl.getSheet();
	}

	/**
	 * run method of the thread
	 *
	 */
	@Override
	public void run() {
		imp = new ImportTXT(sheet);
		String absolutePath = fl.getFile().getAbsolutePath();
		String sPath = absolutePath.
			substring(0, absolutePath.lastIndexOf(File.separator));
		path = Paths.get(sPath);
		fileN = absolutePath.
			substring((absolutePath.lastIndexOf(File.separator) + 1));
		watchDirectory();

	}

	/**
	 * Method to watch the directory for changes and import when a change is
	 * triggered
	 */
	@SuppressWarnings("unchecked")
	public void watchDirectory() {
		try {
			FileSystem fs = path.getFileSystem();

			WatchService service = fs.newWatchService();
			imp.txtImport(fl);
			path.register(service, ENTRY_MODIFY);
			WatchKey key = null;
			for (;;) {
				sleep(100);
				key = service.take();
				WatchEvent.Kind<?> kind = null;
				for (WatchEvent<?> watchEvent : key.pollEvents()) {
					kind = watchEvent.kind();
					Path fileName = ((WatchEvent<Path>) watchEvent).context();
					if (OVERFLOW == kind) {
						continue;
					} else if (ENTRY_MODIFY == kind && fileName.toString().
						equals(fileN)) {
						imp.txtImport(fl);
					}
				}

				if (!key.reset()) {
					break;
				}
			}
		} catch (IOException ex) {
		} catch (InterruptedException ex) {
		}
	}
}
