package csheets.ext.importExportFile;

import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Tixa
 */
public class JTextFieldLimit extends PlainDocument {

	private int limit;

	private int maximo = 15;

	public JTextFieldLimit(int limit) {
		super();
		if (limit == 0) {
			this.limit = maximo;
		} else {
			this.limit = limit;
		}
	}

	@Override
	public void insertString(int offset, String str,
							 javax.swing.text.AttributeSet attr)
		throws BadLocationException {
		if (str == null) {
			return;
		}

		if ((getLength() + str.length()) <= limit) {
			super.
				insertString(offset, str, (javax.swing.text.AttributeSet) attr);
		}
	}
}
