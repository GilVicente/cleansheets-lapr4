/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.ext.Extension;
import csheets.ext.importExportFile.ui.ImportExportFileExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * 
 * @author JOSENUNO
 */
public class ImportExportFileExtension extends Extension {

    public static final String NAME = "ImportExportFile";

    public ImportExportFileExtension() {
        super(NAME);
    }

    public UIExtension getUIExtension(UIController uiController) {
        return new ImportExportFileExtensionUI(this, uiController);
    }
}
