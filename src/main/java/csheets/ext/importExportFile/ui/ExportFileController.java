/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.ext.importExportFile.ui.*;
import csheets.ext.importExportFile.ExportTXT;
import csheets.ext.importExportFile.ExportTXT;
import csheets.ext.importExportFile.FileLink;
import csheets.ext.importExportFile.FileLink;
import csheets.ext.importExportFile.ImportTXT;
import csheets.ext.importExportFile.LinkingFileToExport;
import csheets.ext.importExportFile.LinkingFileToExport;
import csheets.ext.importExportFile.LinkingFileToImport;
import csheets.ui.ctrl.UIController;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author JOSENUNO
 * @changed by Patrícia Monteiro
 */
public class ExportFileController {

    private UIController uiController;
    private String fileName;

    public ExportFileController(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * Exports a file.
     *
     * @param file Location of the File.
     * @param selectedCells Selected Cells at the active SpreadsheeTable.
     * @param header True if the user wishes to write a header on the file,
     * False otherwhise.
     * @param separator Character that will serve to separate columns.
     * @throws IOException IOException is thrown if it cannot be possible to
     * write at the specified file.
     */
    public void exportFile(File file, Cell[][] selectedCells, boolean header, char separator) throws IOException {
        FileWriter fw = new FileWriter(file);

        int y;
        int sz = selectedCells[0].length;
        if (header) {

            fw.write("Columns:\t");

            for (y = 0; y < sz; y++) {
                Cell c = selectedCells[0][y];

                fw.write(String.format("%d %s ", c.getAddress().getColumn(), y == sz - 1 ? "" : separator));
            }

            fw.write("\r\n\r\n");
        }

        int szx = selectedCells.length;
        for (int x = 0; x < szx; x++) {
            if (header) {
                fw.write("\t\t");
            }
            for (y = 0; y < sz; y++) {
                Cell c = selectedCells[x][y];
                String s = c.getContent();
                fw.write(String.format("%s %s ", s == null ? " " : s,
                        y == sz - 1 ? "" : separator));
            }
            fw.write("\r\n");
        }

        fw.close();
    }

    /**
     * Returns if a String is a valid Character.
     *
     * @param s String to be checked.
     * @return True if it is valid, False otherwise.
     */
    public boolean isCharacterValid(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            if (s.length() != 1) {
                return false;
            }

            char c = s.charAt(0);
            return (c < 'a' || c > 'z') && (c < 'A' || c > 'Z');
        }

        return false;
    }

    /**
     * **********************************************************************
     */
    /**
     *
     * @param ficheiro
     * @param separator
     * @param headerLine
     * @param header
     */
    public void exportTxt(File ficheiro, String separator, boolean headerLine, String header) {
        ExportTXT exptxt = new ExportTXT();
        Spreadsheet sheet = uiController.getActiveSpreadsheet();
        boolean result = exptxt.
                exporttoTXT(sheet, ficheiro, headerLine, separator, header);

        if (result) {
            JOptionPane.
                    showMessageDialog(null, "File created successfully.", "Export to TXT", JOptionPane.DEFAULT_OPTION);
        } else {
            JOptionPane.
                    showMessageDialog(null, "Error creating the file.", "Export to TXT", JOptionPane.ERROR_MESSAGE);
        }
    }

       

    public boolean checkSheetExportationLink(Spreadsheet sheet) {
        List<FileLink> files = LinkingFileToExport.getFileExport();
        for (FileLink fl : files) {
            if (fl.getSheet() == sheet) {
                fileName = fl.getFile().getName();
                return true;
            }
        }
        return false;
    }

    

    

    public String getFileName() {
        return fileName;
    }

    public void removeExportationLink(Spreadsheet sheet) {
        LinkingFileToExport.removeFile(sheet);
    }

     

    boolean exportTxt(Spreadsheet sheet, File f, String separator, boolean headerLine, boolean link, String headerText) {
        return LinkingFileToExport.
                searchFile(sheet, f, headerLine, separator, headerText);
    }
}
