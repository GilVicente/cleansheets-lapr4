/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;


import csheets.ext.share.ui.*;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author JOSENUNO
 */
public class ImportExportMenu extends JMenu {
    public static ImportAction action =null;

    public ImportExportMenu(UIController uiController) {
        super("Import/Export File");
        ExportAction exaction = new ExportAction(uiController);
        add(exaction);
        
        ImportAction imaction = new ImportAction(uiController);
        add(action = imaction);   
       
        
    }

}
