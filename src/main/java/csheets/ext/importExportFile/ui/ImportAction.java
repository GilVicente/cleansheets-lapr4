/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;


/**
 * 
 * @author JOSENUNO
 * @changed by Patrícia Monteiro
 */
public class ImportAction extends BaseAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public ImportAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Import File";
    }

    @Override
    protected void defineProperties() {
    }

    /**
     * Creates a panel to send a messagem
     *
     * @param event the event that was fired
     */
    @Override
    public void actionPerformed(ActionEvent event) {

        try {
            ImportTextFilePanel i = new ImportTextFilePanel(uiController);
        } catch (Exception ex) {
            System.out.println(ex.getCause());
        }
        
//        SpreadsheetTable s = this.getSpreadSheetTable();
//        if (s == null) {
//            System.out.println("No active SpreadsheetTable");
//            return;
//        }
//        Cell[][] selectedCells = s.getSelectedCells();
//
//        JFileChooser f = new JFileChooser();
//        f.setDialogTitle("Import File");
//        f.setFileFilter(new FileNameExtensionFilter(".txt File", ext));
//
//        int userSelection = f.showOpenDialog(null);
//
//        if (userSelection != JFileChooser.APPROVE_OPTION) {
//            return;
//        }
//
//        File file = f.getSelectedFile();
//
//        String answer = null;
//        
//        do {
//            answer = JOptionPane.showInputDialog(null, "Select special character",
//                    "Select the special character works as a column separator at that file",
//                    JOptionPane.OK_OPTION);
//        } while (!this.controller.isCharacterValid(answer));
//
//        char separator = answer.charAt(0);
//
//        boolean header = JOptionPane.showOptionDialog(null,
//                "Does the file contain a header?",
//                "Choose an option",
//                JOptionPane.YES_NO_OPTION,
//                JOptionPane.QUESTION_MESSAGE,
//                null,
//                new Object[]{"No", "Yes"},
//                "No") == 1;
//
//        try {
//            this.controller.importFile(file, selectedCells, header, separator);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            JOptionPane.showMessageDialog(null, "Not possible to read the selected file");
//        } catch (FormulaCompilationException ex) {
//            Logger.getLogger(ImportAction.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public SpreadsheetTable getSpreadSheetTable() {
//        return super.focusOwner;
//    }
//        
        
        
        
        
    }
}
