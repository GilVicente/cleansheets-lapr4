/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;

import csheets.ext.share.ui.*;
import csheets.ext.Extension;
import csheets.ext.comments.ui.CommentPanel;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 * 
 * @author JOSENUNO
 */
public class ImportExportFileExtensionUI extends UIExtension {
    
    public static String NAME = "ImportExportFile";
    
    private Icon icon;
    
    private ImportExportMenu menu;
    
    
    public ImportExportFileExtensionUI (Extension extension, UIController uiController){
        super(extension, uiController);
    }
    
    public Icon getIcon(){
        return null;
    }
    
    public JMenu getMenu(){
        if(menu == null)
            menu = new ImportExportMenu(uiController);
        return menu;
    }
    
    
}
