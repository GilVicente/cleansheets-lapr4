/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.importExportFile.FileLink;
import csheets.ext.importExportFile.ImportTXT;
import csheets.ext.importExportFile.LinkingFileToImport;
import csheets.ext.share.ui.*;
import csheets.ui.ctrl.UIController;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author JOSENUNO
 * @changed by Patrícia Monteiro
 */
public class ImportFileController {

    private UIController uiController;
    private String fileName;

    public ImportFileController(UIController uiController) {
        this.uiController = uiController;
    }

    public void importTxt(Spreadsheet sheet, File f, String separator,
            boolean headerLine) {
        ImportTXT imp = new ImportTXT(sheet);
        FileLink fl = new FileLink(sheet, f, headerLine, separator, "");
        try {
            imp.txtImport(fl);
        } catch (IOException ex) {
        }
    }

    public boolean checkSheetImportationLink(Spreadsheet sheet) {
        if (LinkingFileToImport.searchFile(sheet)) {
            List<FileLink> files = LinkingFileToImport.getFileImport();
            for (FileLink fl : files) {
                if (fl.getSheet() == sheet) {
                    fileName = fl.getFile().getName();
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public boolean checkFile(Spreadsheet sheet) {
        return LinkingFileToImport.searchFile(sheet);
    }

    public String getFileName() {
        return fileName;
    }

    public void removeImportationLink(Spreadsheet sheet) {
        LinkingFileToImport.removeFile(sheet);
    }

    public boolean importTxt(Spreadsheet sheet, File f, String separator,
            boolean headerLine, boolean b) {
        return LinkingFileToImport.importFile(f, sheet, headerLine, separator);
    }

    /**
     * Imports a file.
     *
     * @param file Location of the File.
     * @param selectedCells Selected Cells at the active SpreadsheeTable.
     * @param header True if the file has a header, False otherwhise.
     * @param separator Character that serves to separate columns on the file.
     * @throws IOException IOException is thrown if it cannot be possible to
     * read the specified file.
     * @throws
     * csheets.ext.importExportFile.ui.ImportFileController.InvalidCellFileException
     * InvalidCellFileException is thrown if the selected file is not valid.
     * @throws csheets.core.formula.compiler.FormulaCompilationException
     */
//    public void importFile(File file, Cell[][] selectedCells, boolean header, char separator) throws IOException, InvalidCellFileException, FormulaCompilationException {
//        FileReader fr = new FileReader(file);
//        BufferedReader br = new BufferedReader(fr);
//        
//        String s = null;
//        
//        if (header) br.readLine();
//        
//        Cell cell = ImportExportMenu.action.getSpreadSheetTable().getSelectedCells()[0][0];
//        
//        int cx = cell.getAddress().getColumn();
//        int cy = cell.getAddress().getRow();
//        
//        boolean flag = false;
//        for (int y = 0; (s = br.readLine()) != null; y++){
//            flag = true;
//            s = s.replace(" ", "");
//            s = s.replace("\t", "");
//            
//            if (s.isEmpty())
//            {
//                cy--;
//                
//                continue;
//            }
//            
//            String[] spl = s.split(String.valueOf(separator));
//            
//            int sz = spl.length;
//            for (int x = 0; x < sz; x++){
//                Cell cellToReplace = ImportExportMenu.action.getSpreadSheetTable().
//                        getSpreadsheet().getCell(x + cx, y + cy);
//                
//                cellToReplace.setContent(spl[x]);
//            }
//            
//            if (sz == 0) {
//                fr.close();
//                br.close();
//                throw new InvalidCellFileException("File has at least one invalid column");
//            }
//        }
//
//        fr.close();
//        br.close();
//        
//        if (!flag) throw new InvalidCellFileException("File does not have any valid row");
//    }
//    
    /**
     * Returns if a String is a valid Character (Has length == 1 and is not a
     * digit nor a letter).
     *
     * @param s String to be checked.
     * @return True if it is valid, False otherwise.
     */
    public boolean isCharacterValid(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            if (s.length() != 1) {
                return false;
            }

            char c = s.charAt(0);
            return (c < 'a' || c > 'z') && (c < 'A' || c > 'Z');
        }

        return false;
    }

    /**
     * Exception that is thrown when an invalid Cell File is detected while
     * reading it.
     */
    public class InvalidCellFileException extends IOException {

        public InvalidCellFileException(String message) {
            super(message);
        }
    }
}
