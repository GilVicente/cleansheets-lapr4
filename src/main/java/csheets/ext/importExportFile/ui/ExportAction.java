/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile.ui;


import csheets.core.Cell;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 *
 * @author JOSENUNO
 * @changed by Patrícia Monteiro
 */
public class ExportAction extends BaseAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public ExportAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Export File";
    }

    @Override
    protected void defineProperties() {
    }

    /**
     * Creates a panel to send a messagem
     *
     * @param event the event that was fired
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        
//          SpreadsheetTable s = this.getSpreadSheetTable();
//        if (s == null) {
//            System.out.println("No active SpreadsheetTable");
//            return;
//        }
//        Cell[][] selectedCells = s.getSelectedCells();
//
//        JFileChooser f = new JFileChooser();
//        f.setDialogTitle("Export File");
//        f.setFileFilter(new FileNameExtensionFilter(".txt File", ext));
//
//        int userSelection = f.showSaveDialog(null);
//
//        if (userSelection != JFileChooser.APPROVE_OPTION) {
//            return;
//        }
//
//        File file = f.getSelectedFile();
//        String temp = file.getName();
//        if (!temp.endsWith("." + ext)) {
//            file = new File(file + "." + ext);
//        }
//
//        String answer = null;
//        
//        do {
//            answer = JOptionPane.showInputDialog(null, "Select special character",
//                    "Select a special character that will work as a column separator", JOptionPane.OK_OPTION);
//        } while (!this.controller.isCharacterValid(answer));
//
//        char separator = answer.charAt(0);
//
//        boolean header = JOptionPane.showOptionDialog(null,
//                "Do you want to write the row's header on the file?",
//                "Choose an option",
//                JOptionPane.YES_NO_OPTION,
//                JOptionPane.QUESTION_MESSAGE,
//                null,
//                new Object[]{"No", "Yes"},
//                "No") == 1;
//
//        try {
//            this.controller.exportFile(file, selectedCells, header, separator);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            JOptionPane.showMessageDialog(null, "Not possible to write at the selected file");
//        }
           
        
        

        try {
            ExportTextFilePanel e = new ExportTextFilePanel(uiController);
        } catch (Exception ex) {
            System.out.println(ex.getCause());
        }
    }
}
