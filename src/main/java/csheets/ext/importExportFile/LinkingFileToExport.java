/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Spreadsheet;
import java.io.File;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 *
 * @author Tixa
 */
public class LinkingFileToExport {

	/**
	 * List to save the FileLink of the sheets
	 */
	public static List<FileLink> fileExport = new ArrayList<FileLink>();

	/**
	 * List to save the threads to connect to FileLink
	 */
	public static List<Thread> ThreadExport = new ArrayList<Thread>();

	/**
	 * Creates a new FileLink instance and adds this object to the respective
	 * list; creates a new ExportThread and adds this object to the Thread List
	 *
	 * @param sheet current spreadsheet
	 * @param fileName linked file
	 * @param header header option
         * @param headerOption boolean option 
	 * @param separator separator chosen
	 * @return
	 */
	public static void addFile(Spreadsheet sheet, File fileName,
							   boolean headerOption,
							   String separator, String header) {
		FileLink fl = new FileLink(sheet, fileName, headerOption, separator, header);
		fileExport.add(fl);
		ExportThread ex = new ExportThread(fl);
		Thread t1 = new Thread(ex);
		t1.start();
		ThreadExport.add(t1);
	}

	/**
	 * Removes the exportation link
	 *
	 * @param sheet current spreadsheet
	 */
	public static void removeFile(Spreadsheet sheet) {
		int index;
		Thread t;
		try {
			for (FileLink fl : fileExport) {
				if (fl.getSheet() == sheet) {
					index = fileExport.indexOf(fl);
					fileExport.remove(index);
					t = ThreadExport.get(index);

					t.interrupt();
					ThreadExport.remove(index);
				}
			}
		} catch (ConcurrentModificationException ce) {
			removeFile(sheet);
		}
	}

	/**
	 * Check if already exists an exportation link to the current sheet; if
	 * true, removes the exportation link and adds a new link; else adds a new
	 * link.
	 *
	 * @param sheet current spreadsheet
	 * @param fileName file
	 * @param header
	 * @param separator
	 * @return true
	 */
	public static boolean searchFile(Spreadsheet sheet, File fileName,
									 boolean headerOption, String separator,
									 String header) {
		boolean valida = false;
		for (FileLink f : fileExport) {
			if (f.getFile().getName().equals(fileName.getName())) {
				return false;
			} else {
				if (f.getSheet() == sheet) {
					valida = true;
				}
			}
		}
		if (valida) {
			removeFile(sheet);
		}
		addFile(sheet, fileName, headerOption, separator, header);
		return true;
	}

	/**
	 * Check if already exists an exportation link to the current sheet
	 *
	 * @param s the current spreadsheet
	 * @return true if the exportation link exists and false if the exportation
	 * link does not exists
	 */
	public static boolean isLinked(Spreadsheet s) {
		for (FileLink f : fileExport) {
			if (s == f.getSheet()) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * @return the FileLink list
	 */
	public static List<FileLink> getFileExport() {
		return fileExport;
	}

	/**
	 *
	 * @return the Thread list
	 */
	public static List<Thread> getThread() {
		return ThreadExport;
	}

}
