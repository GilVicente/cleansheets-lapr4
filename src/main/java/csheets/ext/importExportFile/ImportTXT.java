/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Spreadsheet;
import csheets.core.formula.compiler.FormulaCompilationException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ConcurrentModificationException;

/**
 *
 * @author Tixa
 */
public class ImportTXT {

	private final Spreadsheet sheet;

	public ImportTXT(Spreadsheet sheet) {
		this.sheet = sheet;
	}

	public void txtImport(FileLink fl) throws FileNotFoundException, IOException {
		InputStream is = new FileInputStream(fl.getFile());
		Reader streamReader = new InputStreamReader(is);
		BufferedReader reader = new BufferedReader(streamReader);

		Spreadsheet sheet = fl.getSheet();

		String line;
		int columns = 0;
		if (!fl.getHeaderOption()) {
			reader.readLine();
		}
		while ((line = reader.readLine()) != null) {
			String[] row = line.split(fl.getSeparator());
			for (int i = 0; i < row.length; i++) {
				try {
					synchronized (sheet.getCell(i, columns)) {
						sheet.getCell(i, columns).setContent(row[i]);
					}
				} catch (FormulaCompilationException ex) {

				} catch (ConcurrentModificationException e) {
					synchronized (sheet.getCell(i, columns)) {
						try {
							sheet.getCell(i, columns).setContent(row[i]);
						} catch (FormulaCompilationException ex) {
						}
					}
				}
			}
			columns++;
		}
		reader.close();
	}
}
