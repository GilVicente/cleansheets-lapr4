/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Spreadsheet;
import java.io.File;
import java.util.Formatter;

/**
 *
 * @author Tixa
 */
public class ExportTXT {

	public ExportTXT() {

	}

	public boolean exporttoTXT(Spreadsheet sheet, File ficheiro,
							   boolean headerLine, String separator,
							   String header) {
		try {
			Formatter fout = new Formatter(ficheiro);
			int row = 0;
			if (headerLine) {
				fout.format(header);
				fout.format("%n");
			}
			for (; row < sheet.getRowCount(); row++) {
				for (int column = 0; column < sheet.getColumnCount() + 1; column++) {
					synchronized (sheet.getCell(column, row)) {
						fout.format(sheet.getCell(column, row).getContent()
							+ separator);
					}
				}
				fout.format("%n");
			}

			fout.close();
			return true;
		} catch (Exception ex) {
		}
		return false;
	}

}
