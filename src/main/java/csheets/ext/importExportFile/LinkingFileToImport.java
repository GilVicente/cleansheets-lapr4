/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportFile;

import csheets.core.Spreadsheet;
import java.io.File;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 *
 * @author Tixa
 */
public class LinkingFileToImport {

	/**
	 * List to save the FileLink of the sheets
	 */
	public static List<FileLink> fileImport = new ArrayList<FileLink>();

	/**
	 * List to save the threads to connect to FileLink
	 */
	public static List<Thread> ThreadImport = new ArrayList<Thread>();

	/**
	 *
	 * Adds the given ob to the respective list
	 *
	 * 
         * @param fl fileLink
         * @param t thread
	 */
	public static void addFile(FileLink fl, Thread t) {
		fileImport.add(fl);
		ThreadImport.add(t);

	}

	/**
	 * Given a spreadsheet, removes the FileLink and the thread from the list
	 * and interrupts the respective thread
	 *
	 * @param ss
	 */
	public static void removeFile(Spreadsheet ss) {
		int index;
		Thread t;
		try {
			for (FileLink fl : fileImport) {
				if (fl.getSheet() == ss) {
					index = fileImport.indexOf(fl);
					fileImport.remove(index);
					t = ThreadImport.get(index);
					t.interrupt();
					ThreadImport.remove(index);
				}
			}
		} catch (ConcurrentModificationException ce) {
			removeFile(ss);
		}

	}

	/**
	 *
	 * Given a spreadsheet, it searchs a FileLink and returns true if any exists
	 *
	 * @param sheet
	 * @return true if exists a FileLink to the given sheet
	 */
	public static boolean searchFile(Spreadsheet sheet) {
		for (FileLink fl : fileImport) {
			if (fl.getSheet() == sheet) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * Creates a FileLink and a Thread to run, it adds the new objects to the
	 * lists and starts the thread
	 *
	 * @param file
	 * @param sheet
	 * @param header
	 * @param separator
	 * @return
	 */
	public static boolean importFile(File file, Spreadsheet sheet,
									 boolean header,
									 String separator) {
		FileLink fl = new FileLink(sheet, file, header, separator, "");
		ImportThread it = new ImportThread(fl);
		Thread t = new Thread(it);
		addFile(fl, t);
		t.start();
		return true;
	}

	/**
	 *
	 * @return the FileLink list
	 */
	public static List<FileLink> getFileImport() {
		return fileImport;
	}

}
