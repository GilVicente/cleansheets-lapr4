/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.function;

import csheets.core.Cell;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Formula;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import csheets.core.formula.Operator;
import csheets.core.formula.UnaryOperator;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.core.formula.compiler.FormulaLexer;
import csheets.core.formula.compiler.FormulaParser;
import csheets.core.formula.lang.Language;
import csheets.core.formula.lang.UnknownElementException;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;

/**
 *
 * @author Tiago
 */
public class WizardFunctionController {

    private final UIController uiController;

    private final Language language;

    private final FormulaCompiler compiler;

    /**
     * Creates a new Controller.
     *
     * @param uiController user interface controller
     */
    public WizardFunctionController(UIController uiController) {
        this.uiController = uiController;
        this.language = Language.getInstance();
        this.compiler = FormulaCompiler.getInstance();
    }

    /**
     * return the active cell
     *
     * @return the active cell
     */
    public Cell getCell() {
        return this.uiController.getActiveCell();
    }

    /**
     * Returns the functions that are supported
     *
     * @return the functions that are supported
     */
    public Function[] getFunctions() {
        return this.language.getFunctions();
    }

    /**
     * Returns whether there is a function with the given identifier.
     *
     * @param identifier identifier of function
     * @return whether there is a function with the given identifier
     */
    public boolean hasFunction(String identifier) {
        return this.language.hasFunction(identifier);
    }

    /**
     * Returns the function with the given identifier.
     *
     * @param identifier identifier of function
     * @return the function with the given identifier
     * @throws csheets.core.formula.lang.UnknownElementException
     */
    public Function getFunction(String identifier) throws UnknownElementException {
        return this.language.getFunction(identifier);
    }

    /**
     * Formula compile with source
     *
     * @param source source to compile
     * @return Formula formula compile
     * @throws FormulaCompilationException error in compilation of source
     */
    public Formula getFormula(String source) throws FormulaCompilationException {
        return this.compiler.compile(this.uiController.getActiveCell(), source);
    }

    /**
     * copy source for the active cell
     *
     * @param source source to copy
     * @throws FormulaCompilationException error in compilation of source
     */
    public void setContent(String source) throws FormulaCompilationException {
        uiController.getActiveCell().setContent(source);
    }

//    /**
//     * Returns the textual representation of the operator to compile
//     *
//     * @param identifier identifier of operator
//     * @param params parameters of operator
//     * @return the textual representation of the operator to compile
//     */
//    public String getSource(String identifier, String params[]) {
//        String source = "=" + (hasFunction(identifier) ? identifier + "(" : "");
//        String separator = hasFunction(identifier) ? ";" : identifier;
//
//        for (String param : params) {
//            if (!param.equals("")) {
//                source += param + separator;
//            }
//        }
//        source = source.endsWith(separator) ? source.substring(0, source.length() - separator.length()) : source;
//        source += hasFunction(identifier) ? ")" : "";
//        return source;
//    }
    /**
     * Returns the textual representation of the operator to compile
     *
     * @param identifier identifier of operator
     * @param params parameters of operator
     * @return the textual representation of the operator to compile
     */
    public String getSource(String identifier, String params[]) {
        String source = "=";

        if (hasFunction(identifier)) {
            source += identifier + "(";
            String separator = ";";
            for (String param : params) {
                if (!param.equals("")) {
                    source += param + separator;
                }
            }
            source = source.endsWith(separator) ? source.substring(0, source.length() - separator.length()) : source;
            source += ")";
        } else if (hasUnaryOperator(identifier)) {
            try {
                source += params[0] + identifier;
            } catch (ArrayIndexOutOfBoundsException ex) {
                source+=identifier;
            }
        } else if (hasBinaryOperator(identifier)) {
            try {
                source += params[0] + identifier + params[1];
            } catch (ArrayIndexOutOfBoundsException ex) {
                source+=identifier;
            }
        }
        return source;
    }
    
    /**
     * Returns the binary operators that are supported
     *
     * @return the binary operators that are supported
     */
    public BinaryOperator[] getBinaryOperator2() {
        return this.language.getBinaryOperators();
    }
    
    /**
     * Returns the textual representation of the operator to compile
     *
     * @param identifier identifier of operator
     * @param params parameters of operator
     * @return the textual representation of the operator to compile
     */
    public String getSource2(String identifier, JTextField[] params) {
        String source = "=" + (hasFunction(identifier) ? identifier + "(" : "");
        String separator = hasFunction(identifier) ? ";" : identifier;

        for (JTextField param : params) {
            if (!param.getText().equals("")) {
                source += param.getText() + separator;
            }
        }
        source = source.endsWith(separator) ? source.substring(0, source.length() - separator.length()) : source;
        source += hasFunction(identifier) ? ")" : "";
        if (hasBinaryOperator(identifier) && !source.contains(identifier)) {
            return source + identifier;
        }
        return source;
    }

    /**
     * Returns the text structure of the operator
     *
     * @param identifier identifier of operator
     * @return the text structure of the operator
     */
    public String getStructure(String identifier) {
        try {
            if (hasFunction(identifier)) {
                Function function = getFunction(identifier);
                String str = "=" + function.getIdentifier() + "(";
                for (FunctionParameter funcParam : function.getParameters()) {
                    str += ((funcParam.isOptional()) ? "{" : "") + funcParam.getValueType() + ((funcParam.isOptional()) ? "}" : "") + ";";
                }
                if (str.endsWith(";")) {
                    str = str.substring(0, str.length() - 1);
                }
                str += ")";
                return str.trim();

            } else if (hasBinaryOperator(identifier)) {
                BinaryOperator op = getBinaryOperator(identifier);
                String str = "=" + op.getOperandValueType() + op.getIdentifier() + op.getOperandValueType();
                return str.trim();
            } else if (hasUnaryOperator(identifier)) {
                UnaryOperator op = getUnaryOperator(identifier);
                String str = "=";
                if (op.isPrefix()) {
                    str += op.getIdentifier() + op.getOperandValueType();
                } else {
                    str += op.getOperandValueType() + op.getIdentifier();
                }
                return str.trim();
            }
        } catch (UnknownElementException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return "";
    }

    /**
     * Returns the textual representation of the assistance of the operator
     *
     * @param identifier identifier of operator
     * @return the textual representation of the assistance of the operator
     * @throws UnknownElementException Operator Unknown
     */
    public String getHelp(String identifier) throws UnknownElementException {
        String help = getStructure(identifier);
        if (hasFunction(identifier)) {
            Function function = getFunction(identifier);
            for (FunctionParameter funcParam : function.getParameters()) {
                help += "\n" + funcParam.getName() + "(" + funcParam.getValueType().name() + ")" + ":" + funcParam.getDescription();
            }
        }
        return help;
    }

    /**
     * Returns the number of parameters of a function/operator
     *
     * @param identifier of a function/operator
     * @return
     */
    public int getNumberParameters(String identifier) {
        try {
            if (hasFunction(identifier)) {
                Function f = getFunction(identifier);
                if (f.isVarArg()) {
                    return -1;
                } else {
                    return f.getParameters().length;
                }
            } else if (hasUnaryOperator(identifier)) {
                return 1;
            } else if (hasBinaryOperator(identifier)) {
                return 2;
            }
        } catch (UnknownElementException ex) {
            Logger.getLogger(WizardFunctionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     * Returns whether there is a binary operator with the given identifier.
     *
     * @param identifier identifier of a binary operator
     * @return whether there is a binary operator with the given identifier
     */
    public boolean hasBinaryOperator(String identifier) {
        return this.language.hasBinaryOperator(identifier);
    }

    /**
     * Returns whether there is an unary operator with the given identifier.
     *
     * @param identifier identifier of a unary operator
     * @return whether there is an unary operator with the given identifier
     */
    public boolean hasUnaryOperator(String identifier) {
        return this.language.hasUnaryOperator(identifier);
    }

    /**
     * Returns a binary operator with the given identifier.
     *
     * @param identifier identifier of the binary operator
     * @return the binary operator with the given identifier
     * @throws csheets.core.formula.lang.UnknownElementException
     */
    public BinaryOperator getBinaryOperator(String identifier) throws UnknownElementException {
        return this.language.getBinaryOperator(identifier);
    }

    /**
     * Returns an unary operator with the given identifier.
     *
     * @param identifier identifier of the unary operator
     * @return the unary operator with the given identifier
     * @throws csheets.core.formula.lang.UnknownElementException
     */
    public UnaryOperator getUnaryOperator(String identifier) throws UnknownElementException {
        return this.language.getUnaryOperator(identifier);
    }

    /**
     * @return the binary operators supported
     */
    public Operator[] getBinaryOperators() {
        return this.language.getBinaryOperators();
    }

    /**
     * @return the unary operators supported
     */
    public Operator[] getUnaryOperators() {
        return this.language.getUnaryOperators();
    }

    /**
     * @return an array of strings containing the identifiers of all functions
     * and operators supported
     */
    public String[] getFunctionList() {

        List<String> functionList = new ArrayList<>();
        for (Function f : getFunctions()) {
            functionList.add(f.getIdentifier());
        }

        for (Operator o : getBinaryOperators()) {
            functionList.add(o.getIdentifier());
        }
        for (Operator o : getUnaryOperators()) {
            functionList.add(o.getIdentifier());
        }

        Object[] objectArray = functionList.toArray();
        return Arrays.asList(objectArray).toArray(new String[objectArray.length]);
    }
    
    /**
     * Generates the parse tree GUI representation
     *
     * @param ctree parse tree
     * @param tree GUI to generate.
     */
    public void generateTree(CommonTree ctree, JTree tree) {
        DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        walkTree(ctree, root);
        model.nodeStructureChanged(root);
    }

    /**
     * Walks parse tree and adds it to GUI representation
     *
     * @param ctree Parse Tree to walk
     * @param root GUI node to add
     */
    private void walkTree(CommonTree ctree, DefaultMutableTreeNode root) {
        if (ctree == null) {
            return;
        }
        DefaultMutableTreeNode tn;
        for (int i = 0; i < ctree.getChildCount(); i++) {
            tn = new DefaultMutableTreeNode(ctree.getChild(i).getText());
            root.add(tn);
            if (ctree.getChild(i).getChildCount() > 0) {
                walkTree((CommonTree) ctree.getChild(i), tn);
            }
        }
    }
    
    /**
     * Return the tree of parser of source
     *
     * @param source source of tree
     * @return the tree of parser of source
     */
    public CommonTree getTreeSource(String source) {
        ANTLRStringStream input = new ANTLRStringStream(source);
        FormulaLexer lexer = new FormulaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FormulaParser parser = new FormulaParser(tokens);
        try {
            CommonTree ast = (CommonTree) parser.expression().getTree();
            return ast;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        return null;
    }


}
