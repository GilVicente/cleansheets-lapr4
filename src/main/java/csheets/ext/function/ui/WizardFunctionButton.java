
package csheets.ext.function.ui;

import csheets.ext.function.WizardFunctionController;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class WizardFunctionButton extends JButton implements ActionListener{
    
    private UIController uiController;
    
    public WizardFunctionButton(UIController uiController) {
        this.uiController=uiController;
        getPreferredSize();
        setText("Wizard");
        addActionListener(this);

    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        new WizardFunctionUI(new WizardFunctionController(uiController));
//        new WizardDialog(new WizardFunctionController(uiController));
    }
    
}
