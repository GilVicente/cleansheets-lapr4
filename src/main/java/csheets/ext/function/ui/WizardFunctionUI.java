/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.function.ui;

import csheets.core.Cell;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Formula;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.lang.UnknownElementException;
import csheets.ext.function.WizardFunctionController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import org.antlr.runtime.tree.CommonTree;

/**
 *
 * @author Guilherme
 */
public class WizardFunctionUI extends JDialog {
    
    /**
     * This hides useless notifications throwed by the wizard
     */
    static {
        Logger logger = Logger.getLogger(WizardFunctionUI.class.getName());
        logger.setUseParentHandlers(false);
    }
    /**
     * Controller for this functionality
     */
    private final WizardFunctionController controller;

    private JPanel paramsPanel;
    private JPanel lowerPanel;

    private JButton insertButton;
    private JButton cancelButton;
    private JButton helpButton;

    private JComboBox operationsList;
    private DefaultComboBoxModel operationsListModel;

    private JTextField operationStructure;
    private JTextField[] operationParams;
    private JTextField operationResult;

    private JCheckBox treeCheckBox;
    private JTree tree;
    private JScrollPane treeScroll;

    String source = "";

    /**
     * Create Wizard Frame
     *
     * @param controller the Wizard Controller
     */
    public WizardFunctionUI(WizardFunctionController controller) {
        this.controller = controller;
        initializeComponents();
        initOperationsList();
        actions();
        initActiveFunctionCell();
    }

    /**
     * Initialize Components of Frame
     */
    private void initializeComponents() {

        //Generic settings of the dialog window
        this.setTitle("Function Wizard");
        this.setResizable(true);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        paramsPanel = new JPanel();

        //Functions List as a Combo Box to select the function
        JLabel selectFunctionLabel = new JLabel(" Functions: ");
        JPanel operationsPanel = new JPanel(new FlowLayout());
        operationsList = new JComboBox();
        operationsList.setPreferredSize(new Dimension(180, 25));
        operationsListModel = new DefaultComboBoxModel();
        operationsList.setModel(operationsListModel);
        operationsPanel.add(selectFunctionLabel);
        operationsPanel.add(operationsList);
        helpButton = new JButton("Help");
        operationsPanel.add(helpButton);

        //Edit Box to give function parameters
        operationStructure = new JTextField();
        operationStructure.setEditable(false);
        operationResult = new JTextField();
        operationResult.setEditable(false);

        //Checkbox to enable or disable tree visualization
        treeCheckBox = new JCheckBox("Show tree");
        treeCheckBox.setSelected(false);

        // content.add(editBox);
        //Panel to wrap the buttons and button declaration
        lowerPanel = new JPanel(new BorderLayout());

        JPanel buttonPanel = new JPanel(new FlowLayout());
        insertButton = new JButton("Insert");
        cancelButton = new JButton("Cancel");
        buttonPanel.add(insertButton);
        buttonPanel.add(cancelButton);

        lowerPanel.add(treeCheckBox, BorderLayout.NORTH);
        lowerPanel.add(buttonPanel, BorderLayout.SOUTH);

        //   ((CardLayout)cards.getLayout()).show(cards,"notree");
        //Add componenets to the dialog
        this.add(operationsPanel, BorderLayout.NORTH);
        this.add(paramsPanel, BorderLayout.CENTER);
        this.add(lowerPanel, BorderLayout.SOUTH);

        pack();
        this.setVisible(true);
    }

    /**
     * Pack and Re-Center Frame
     */
    @Override
    public void pack() {
        super.pack();
        setLocationRelativeTo(null);
    }

    /**
     * Return the active identifier
     *
     * @return the active identifier
     */
    private String getActiveIdentifier() {
        String item = (String) operationsList.getSelectedItem();
        return item.substring(item.indexOf("]") + 2);
    }

    /**
     * Actions of Frame and Components
     */
    private void actions() {

        //Prompt when user clicks to exit the window
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                if (JOptionPane.showConfirmDialog(WizardFunctionUI.this, "Do you want to exit?", "Exit Function Wizard",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    dispose();
                }
            }
        });

        //Inserts the function into the actual selected cell after the formula parse and calculation.
        this.insertButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    controller.setContent(source);
                    dispose();
                } catch (FormulaCompilationException ex) {
                    Logger.getLogger(WizardFunctionUI.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(WizardFunctionUI.this, "Please Check the Function Syntax!\n" + ex.getLocalizedMessage(), "Function Wizard - Syntax", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        //close frame
        this.cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });

        //Shows the selected function description
        this.helpButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String identifier = getActiveIdentifier();
                    JOptionPane.showMessageDialog(null, controller.getHelp(identifier), "Help Wizard Function [" + identifier + "]", JOptionPane.INFORMATION_MESSAGE);
                } catch (UnknownElementException ex) {
                    Logger.getLogger(WizardFunctionUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (HeadlessException ex) {
                    Logger.getLogger(WizardFunctionUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        this.operationsList.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    String identifier = getActiveIdentifier();

                    paramsPanel.removeAll();
                    paramsPanel.setLayout(new GridLayout(0, 2));
                    paramsPanel.add(new JLabel("  Structure:"));
                    paramsPanel.add(operationStructure);

                    if (controller.hasFunction(identifier)) {
                        createFieldsFunction(controller.getFunction(identifier));
                    } else if (controller.hasBinaryOperator(identifier)) {
                        createFieldsBinaryOperator(controller.getBinaryOperator(identifier));
                    }

                    operationStructure.setText(controller.getStructure(identifier));
                    paramsPanel.add(new JLabel("  Result:"));
                    paramsPanel.add(operationResult);
                    RunTimeCompiler();
                    pack();
                } catch (Exception ex) {
                    Logger.getLogger(WizardFunctionUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        this.operationsList.setSelectedIndex(0);

        this.treeCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (treeCheckBox.isSelected()) {
                    RunTimeCompiler();
                } else {
                    lowerPanel.remove(treeScroll);
                    pack();
                }
            }
        });

        treeAction();
    }

    /**
     * Adds highlight action to tree nodes when clicked.
     */
    private void treeAction() {
        this.tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                resetHighlighter();
                DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                DefaultMutableTreeNode root = (DefaultMutableTreeNode) tree.getModel().getRoot();
                JTextField field;
                if (root.equals(selectedNode)) {
                    field = operationStructure;
                } else {
                    if (selectedNode.getParent().equals(root)) {
                        field = operationParams[root.getIndex(selectedNode)];
                    } else {
                        field = operationParams[root.getIndex((TreeNode) e.getPath().getPathComponent(1))];
                    }
                }
                try {
                    field.getHighlighter().addHighlight(0,field.getText().length(), new DefaultHighlighter.DefaultHighlightPainter(Color.orange));
                } catch (BadLocationException ex) {
                    JOptionPane.showMessageDialog(null, ex.getLocalizedMessage());
                }
            }
        });
    }
    
    

    /**
     * Tries to obtain the available functions from the configuration file
     * parsed by Language class. If done correctly the functions will appear in
     * the combo box to select.
     */
    private void initOperationsList() {
        Function[] functions = this.controller.getFunctions();
        BinaryOperator[] binaryOperator = this.controller.getBinaryOperator2();
        if (functions.length > 0 || binaryOperator.length > 0) {
            for (Function func : functions) {
                this.operationsListModel.addElement("[F] " + func.getIdentifier());
            }
            for (BinaryOperator operator : binaryOperator) {
                this.operationsListModel.addElement("[O] " + operator.getIdentifier());
            }
        } else {
            JOptionPane.showMessageDialog(this, "There are no Operations Available to insert!", "Function Wizard - No Operations", JOptionPane.INFORMATION_MESSAGE);
            dispose();
        }
    }

    /**
     * Select function of the cell , and default parameters with the values ​​of
     * the cell
     */
    private void initActiveFunctionCell() {
        Cell cell = this.controller.getCell();
        if (cell != null) {
            Formula formula = cell.getFormula();
            if (formula != null) {
                String parms[] = formula.toString().replace("(", ";").replace(")", ";").split(";");
                for (int i = 0; i < this.operationsListModel.getSize(); i++) {
                    if (((String) this.operationsListModel.getElementAt(i)).equals("[F] " + parms[0])) {
                        this.operationsList.setSelectedIndex(i);
                        for (int j = 0; j < operationParams.length; j++) {
                            if (parms.length > j + 1) {
                                operationParams[j].setText(parms[j + 1]);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Create fields to the function
     *
     * @param function Function to create fields
     */
    private void createFieldsFunction(Function function) {
        operationParams = new JTextField[function.getParameters().length];
        int i = 0;
        for (FunctionParameter funcParam : function.getParameters()) {
            paramsPanel.add(new JLabel("  " + (funcParam.isOptional() ? "{" : "") + funcParam.getName() + (funcParam.isOptional() ? "}" : "") + ": "));
            operationParams[i] = new JTextField();
            operationParams[i].getDocument().addDocumentListener(new RunTimeCompilerField());
            operationParams[i].setToolTipText(funcParam.getDescription());
            paramsPanel.add(operationParams[i]);
            i++;
        }
    }

    /**
     * Create fields to the binary operator
     *
     * @param operator binary operator to create fields
     * @throws UnknownElementException
     */
    private void createFieldsBinaryOperator(BinaryOperator operator) throws UnknownElementException {
        operationParams = new JTextField[2];
        for (int i = 0; i < 2; i++) {
            paramsPanel.add(new JLabel("  " + operator.getOperandValueType() + (i + 1) + ":"));
            operationParams[i] = new JTextField();
            operationParams[i].getDocument().addDocumentListener(new RunTimeCompilerField());
            paramsPanel.add(operationParams[i]);
        }
    }

    /**
     * Listener to the text fields parameters. whenever there is a change the
     * field, the formula is recompiled.
     */
    private class RunTimeCompilerField implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            RunTimeCompiler();//recompiled
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            RunTimeCompiler();//recompiled
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            RunTimeCompiler();//recompiled
        }
    }

    /**
     * Compile function with user-entered paramtros, in run-time
     */
    public void RunTimeCompiler() {
        try {
            source = controller.getSource2(getActiveIdentifier(), operationParams);
            CommonTree ctree = controller.getTreeSource(source);
            if (treeCheckBox.isSelected()) {
                //generates tree
                tree = new JTree(new DefaultMutableTreeNode(ctree.getText()));
                controller.generateTree(ctree, tree);
                treeAction();
            }
            updateTreeComponent();
            operationResult.setText(controller.getFormula(source).evaluate().toString());
            operationResult.setToolTipText("<html>" + source + "<br>" + ctree.toStringTree() + "</html>");
            operationResult.setForeground(Color.GREEN);
            insertButton.setEnabled(true);
        } catch (Exception ex) {
            resetHighlighter();
            Logger.getLogger(WizardFunctionUI.class.getName()).log(Level.SEVERE, null, ex);
            operationResult.setToolTipText(ex.getLocalizedMessage());
            operationResult.setText(source);
            operationResult.setForeground(Color.RED);
            if (treeCheckBox.isSelected()) {
                //Generates error tree
                tree = new JTree(new DefaultMutableTreeNode(ex.getLocalizedMessage()));
            }
            updateTreeComponent();
            insertButton.setEnabled(false);
        }
    }

    /**
     * Resets highlights on all text fields.
     */
    private void resetHighlighter() {
        operationStructure.getHighlighter().removeAllHighlights();
        for (JTextField operationParam : operationParams) {
            operationParam.getHighlighter().removeAllHighlights();
        }
    }

    /**
     * Updates JTree display.
     */
    private void updateTreeComponent() {
        if (treeCheckBox.isSelected()) {
            if (lowerPanel.getComponentCount() == 3) {
                lowerPanel.remove(treeScroll);
                expandAllNodes();
                treeScroll = new JScrollPane(tree);
                lowerPanel.add(treeScroll, BorderLayout.CENTER);
                pack();
            } else {
                expandAllNodes();
                treeScroll = new JScrollPane(tree);
                lowerPanel.add(treeScroll, BorderLayout.CENTER);
                pack();
            }
        } else {
            if (lowerPanel.getComponentCount() == 3) {
                lowerPanel.remove(treeScroll);
                pack();
            }
        }
    }

    /**
     * Expands all JTree nodes
     */
    private void expandAllNodes() {
        for (int i = 0; i < tree.getRowCount(); i++) {
            tree.expandRow(i);
        }
    }
    
}
