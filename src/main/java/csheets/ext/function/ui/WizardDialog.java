package csheets.ext.function.ui;

import csheets.core.IllegalValueTypeException;
import csheets.core.formula.Formula;
import csheets.core.formula.Function;
import csheets.core.formula.Operator;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.lang.UnknownElementException;
import csheets.ext.function.WizardFunctionController;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class WizardDialog extends javax.swing.JDialog {

    private WizardFunctionController wizardController;

    List<JTextField> parameters;

    boolean infinitePam;

    public WizardDialog(WizardFunctionController wizardController) {
        this.wizardController = wizardController;
        parameters = new ArrayList<>();
        setTitle("Wizard");
        setVisible(true);
        initComponents();
        setLocationRelativeTo(null);
        infinitePam = false;
    }

    public DefaultListModel getList() {
        DefaultListModel list = new DefaultListModel();

        Object array[] = wizardController.getFunctionList();
        for (int i = 0; i < array.length; i++) {
            list.add(i, array[i]);
        }
        return list;
    }

    public List<JTextField> getParameters() {
        return parameters;
    }

    public void setParameters(List<JTextField> pams) {
        this.parameters = pams;
    }

    public boolean check() {
        if (this.getParameters().isEmpty()) {
            return false;
        }

        for (JTextField jText : this.getParameters()) {
            if (jText.getText().length() == 0) {
                return false;
            }
        }
        return true;
    }
    
    public String[] convertParametersToStringArray(){
        String []pamsString=new String[parameters.size()];
        for(int i=0;i<parameters.size();i++){
            pamsString[i]=parameters.get(i).getText();
        }
        return pamsString;
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        japlly = new javax.swing.JButton();
        jcancel = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jhelp = new javax.swing.JButton();
        jFormula = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jResult = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        editPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        japlly.setText("Apply");
        japlly.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                japllyActionPerformed(evt);
            }
        });

        jcancel.setText("Cancel");
        jcancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcancelActionPerformed(evt);
            }
        });

        jList1.setModel(getList());
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        jhelp.setText("Help");
        jhelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jhelpActionPerformed(evt);
            }
        });

        jFormula.setEditable(false);

        jLabel1.setText("Formula");

        jResult.setEditable(false);

        jLabel2.setText("Result");

        jLabel3.setText("Edit Boxes");

        javax.swing.GroupLayout editPanelLayout = new javax.swing.GroupLayout(editPanel);
        editPanel.setLayout(editPanelLayout);
        editPanelLayout.setHorizontalGroup(
            editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        editPanelLayout.setVerticalGroup(
            editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 42, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(68, 68, 68)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(jResult)
                    .addComponent(jFormula)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(japlly)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jcancel, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jhelp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jFormula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(editPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jhelp)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(japlly)
                            .addComponent(jcancel)))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jcancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcancelActionPerformed
        dispose();
    }//GEN-LAST:event_jcancelActionPerformed

    private void jhelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jhelpActionPerformed
        String ident = (String) jList1.getSelectedValue();
        try {
            String descricao = wizardController.getHelp(ident);
            JOptionPane.showConfirmDialog(null, descricao, "Descricao", JOptionPane.OK_CANCEL_OPTION);
        } catch (UnknownElementException ex) {
            Logger.getLogger(WizardDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jhelpActionPerformed

    private void japllyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_japllyActionPerformed
        try {
            String form=wizardController.getSource(jList1.getSelectedValue().toString(), convertParametersToStringArray());
            wizardController.setContent(form);
            dispose();
        } catch (FormulaCompilationException ex) {
            JOptionPane.showMessageDialog(this,"Invalid Formula", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_japllyActionPerformed

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
        String selected = (String) jList1.getSelectedValue();
        jFormula.setText(wizardController.getStructure(selected));

        int pamNum = wizardController.getNumberParameters(selected);
        if (pamNum == -1) {
            pamNum = 1;
            infinitePam = true;
        } else {
            infinitePam = false;
        }

        editPanel.removeAll();
        GridLayout gLayout = new GridLayout(1, pamNum, 10, 10);
        editPanel.setLayout(gLayout);

        parameters.clear();
        String exp = wizardController.getSource(selected, new String[0]);

        if (wizardController.getNumberParameters(selected) == 0) {
            try {
                Formula f = wizardController.getFormula(exp);
                exp = f.evaluate().toString();
            } catch (FormulaCompilationException | IllegalValueTypeException ex) {
                Logger.getLogger(WizardDialog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        jResult.setText(exp);
        for (int i = 0; i < pamNum; i++) {
            JTextField jText = new JTextField();
            jText.setSize(10, 10);
            editPanel.add(jText);
            parameters.add(jText);
            jText.getDocument().addDocumentListener(new MyDocument());
        }
        editPanel.validate();
        editPanel.repaint();
    }//GEN-LAST:event_jList1ValueChanged

    private class MyDocument implements DocumentListener {

        public void changedUpdate(DocumentEvent e) {
            warn();
        }

        public void removeUpdate(DocumentEvent e) {
            warn();
        }

        public void insertUpdate(DocumentEvent e) {
            warn();
        }

        public void warn() {
            String identifier = (String) jList1.getSelectedValue();
            String pams[]=convertParametersToStringArray();
            
            String exp = wizardController.getSource(identifier, pams);

            Formula f;
            try {
                f = wizardController.getFormula(exp);
                jResult.setText(f.evaluate().toString());
            } catch (IllegalValueTypeException | FormulaCompilationException ex) {
                jResult.setText(ex.getMessage());
            }
            if (infinitePam) {
                if (check()) {
                    JTextField jText = new JTextField(10);
                    editPanel.add(jText);
                    parameters.add(jText);
                    jText.getDocument().addDocumentListener(new MyDocument());

                    editPanel.validate();
                    editPanel.repaint();
                }
            }
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel editPanel;
    private javax.swing.JTextField jFormula;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JList jList1;
    private javax.swing.JTextField jResult;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton japlly;
    private javax.swing.JButton jcancel;
    private javax.swing.JButton jhelp;
    // End of variables declaration//GEN-END:variables
}
