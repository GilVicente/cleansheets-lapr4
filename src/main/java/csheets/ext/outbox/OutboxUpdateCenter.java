/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.outbox;

import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ext.emailConfiguration.EmailConfiguration;
import csheets.ipc.connection.Broadcast;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author hugo1140465
 */
public class OutboxUpdateCenter extends FocusOwnerAction {

    @Override
    protected String getName() {
        return "";
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

    }
    /**
     * 
     * @return the spreadsheet of the active isntance
     */
    public Spreadsheet getSpreadsheets(){
        return super.focusOwner.getSpreadsheet();
    }
    
    /**
     * updates a Jlist based on a workbook usefull for testing
     * @param jl JList to update
     * @param wb workbook that has the emails
     */
    public void updateListByWorkbook(JList jl, Workbook wb){
        
        DefaultListModel model = new DefaultListModel();
        for (EmailConfiguration email : wb.getOutEmails()) {
            model.addElement(email);
        }
        jl.setModel(model);
    
    }
    
    
    /**
     * updates de jList
     * @param jl jlist to update
     */
    public void updateList(JList jl) {
        updateListByWorkbook(jl, super.focusOwner.getSpreadsheet().getWorkbook());
    }

}
