package csheets.ext.outbox.ui;

/*
 * Copyright (c) 2013 Alexandre Braganca, Einar Pehrson
 *
 * This file is part of
 * CleanSheets Extension for Comments
 *
 * CleanSheets Extension for Assertions is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * CleanSheets Extension for Assertions is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CleanSheets Extension for Assertions; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA  02111-1307  USA
 */
import csheets.ext.email.*;
import com.sun.glass.ui.Size;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import csheets.core.Cell;
import csheets.ext.comments.CommentableCell;
import csheets.ext.comments.CommentableCellListener;
import csheets.ext.comments.CommentsExtension;
import csheets.ext.emailConfiguration.EmailConfiguration;
import csheets.ext.outbox.OutboxUpdateCenter;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.SelectionEvent;
import csheets.ui.ctrl.SelectionListener;
import csheets.ui.ctrl.UIController;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * The panel that supports the outbox
 * @author hugo1140465
 */
@SuppressWarnings("serial")
public class OutboxPanel extends JPanel {
    /**
     * send
     */
    private SendEmailController controller;
    private JList listBox = new JList();
    private JButton update = new JButton();
    private OutboxUpdateCenter outboxUC = new OutboxUpdateCenter();
    private static Size BUTTON_SIZE = new Size(35, 30);

    /**
     * Creates a new comment panel.
     *
     * @param uiController the user interface controller
     */
    public OutboxPanel(UIController uiController) {
        // Configures panel
        super(new BorderLayout());
        setName(CommentsExtension.NAME);

        // Creates controller
        controller = new SendEmailController();

        //double click on an email
        listBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() % 2 == 0 && !e.isConsumed()) {
                    e.consume();
                    SentEmailDialog sed = new SentEmailDialog(null, (EmailConfiguration)listBox.getSelectedValue());
                    
                }
            }

            @Override
            public void mousePressed(MouseEvent me) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseReleased(MouseEvent me) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseExited(MouseEvent me) {
               // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

        listBox.setPreferredSize(new Dimension(120, 340));		// width, height
        listBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        listBox.setAlignmentX(Component.CENTER_ALIGNMENT);

        update.setAlignmentX(Component.CENTER_ALIGNMENT);
        update.setText("Update");
        update.setPreferredSize(new Dimension(120, 20));
        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateOutbox();

            }
        });

        // Lays out comment components
        JPanel emailPanel = new JPanel();
        emailPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        emailPanel.setPreferredSize(new Dimension(130, 400));
        emailPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        emailPanel.add(listBox);
        emailPanel.add(update);

        // Adds borders
        TitledBorder border = BorderFactory.createTitledBorder("Outbox");
        border.setTitleJustification(TitledBorder.CENTER);
        emailPanel.setBorder(border);

        // Adds panels
        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(emailPanel, BorderLayout.NORTH);
        add(northPanel, BorderLayout.NORTH);
    }
    /**
     * updates the sent emails list
     */
    private void updateOutbox() {
        this.outboxUC.updateList(listBox);
    }
}
