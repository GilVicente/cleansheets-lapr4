package csheets.ext.outbox;

import csheets.ext.outbox.ui.UIExtensionOutbox;
import csheets.ext.email.*;
import csheets.ext.comments.*;
import csheets.core.Cell;
import csheets.ext.Extension;
import csheets.ext.comments.ui.UIExtensionComments;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * An extension to the email outbox
 * @see Extension
 * @author hugo1140465
 */
public class OutboxExtension extends Extension {

	/** The name of the extension */
	public static final String NAME = "Email outbox";

	/**
	 * Creates a new Example extension.
	 */
	public OutboxExtension() {
		super(NAME);
	}
	

	
	/**
	 * Returns the user interface extension of this extension 
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionOutbox(this, uiController);
	}
}

