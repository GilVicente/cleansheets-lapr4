/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.conditionalformatting;

import csheets.ext.Extension;
import csheets.ext.conditionalformatting.ui.UIExtensionConditionalFormatting;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author 1141277
 */

public class ConditionalFormattingExtension extends Extension{
    
    /** Name of the extension*/
    public static final String NAME = "Conditional Formatting";
    
    public ConditionalFormattingExtension() {
        super(NAME);
    }
    
    /**
     * returns the user interface for this extension
     * @param uicontroller the user interface controller
     * @return a user interface, or null if none is found
     */
    @Override
    public UIExtension getUIExtension(UIController uicontroller){
        return new UIExtensionConditionalFormatting(this, uicontroller);
    }
    
}
