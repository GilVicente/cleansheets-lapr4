/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.conditionalformatting;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.conditionalformatting.ui.ConditionalFormattingController;
import static csheets.ext.conditionalformatting.ui.ConditionalFormattingController.falseStyle;
import csheets.ext.conditionalformatting.ui.ConditionalStyle;
import csheets.ext.share.ui.ShareMenu;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.border.Border;
import org.antlr.runtime.tree.Tree;

/**
 *
 * @author José Santos (1140921)
 */
public class ConditionalFormattingRanges extends FocusOwnerAction {

    /**
     * UiController get cell range
     */
    private static Cell selectedCells[][];
    private String operator;

    private ConditionalStyle trueStyle;

    private ConditionalStyle falseStyle;
    private Tree node;
    private String value;
    private String nodeTree;

    public ConditionalFormattingRanges(Tree node) {
        this.node = node;

    }

    public ConditionalFormattingRanges() {
    }

    /**
     * Calculate in pre define range cell if it true or false the colour apply
     * by the user
     */
    public void calculateRange() throws FormulaCompilationException, IllegalValueTypeException {
        if (this.selectedCells == null) {
            getSelectedCells();
        }

//        System.out.println("-->" + cs.getTrueStyle());
        for (int i = 0; i < selectedCells.length; i++) {
            for (int j = 0; j < selectedCells[0].length; j++) {
                if (!selectedCells[i][j].getContent().isEmpty() && selectedCells[i][j] != null) {
                    StylableCell c = (StylableCell) selectedCells[i][j].getExtension(StyleExtension.NAME);
                    // ConditionalStyleCell cond = new ConditionalStyleCell(selectedCells[i][j]);
                    String cellTemp = selectedCells[i][j].getContent();
                    StringBuilder builder = new StringBuilder();
                    builder.append("=");
                    builder.append(cellTemp);
                    builder.append(this.operator);
                    builder.append(this.value);
                    selectedCells[i][j].setContent(builder.toString());
                    selectedCells[i][j].getValue().getType();
                    if (selectedCells[i][j].getValue().toBoolean()) {
                        if (ConditionalFormattingController.trueStyle == null) {
                            c.setBackgroundColor(Color.green);
                        } else {
                            c.setBackgroundColor(ConditionalFormattingController.trueStyle.getBgColor());
                        }
                        selectedCells[i][j].setContent(cellTemp);
                    } else {
                        if (ConditionalFormattingController.falseStyle == null) {
                            c.setBackgroundColor(Color.blue);
                        } else {
                            c.setBackgroundColor(ConditionalFormattingController.falseStyle.getBgColor());
                        }
                        selectedCells[i][j].setContent(cellTemp);
                    }
                }

            }
        }
        // getSpreadSheetTable().getSelectedCells();
    }

    @Override
    protected String getName() {
        return "Cell Range";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    /**
     * The cells that are range selected in the SpreadSheet
     *
     * @return a matrix of the selected cells (Cell[][])
     */
    public Cell[][] getSelectedCells() {
        this.selectedCells = ShareMenu.action.getSpreadSheetTable().getSelectedCells();

        return this.selectedCells;
    }

    public void setOperator(String operator) throws FormulaCompilationException, IllegalValueTypeException {
        this.operator = operator;
        if (this.operator != null && !this.operator.isEmpty()) {
            Value();
        }
    }

    public void setTree(String nodeTree) {
        this.nodeTree = nodeTree;
    }

    /**
     * This method just get the value from a node Tree
     *
     * @throws FormulaCompilationException
     * @throws IllegalValueTypeException
     */
    public void Value() throws FormulaCompilationException, IllegalValueTypeException {
        String m[] = this.nodeTree.trim().split(this.operator);
        String x = m[1].replace(String.valueOf(m[1].charAt(m.length + 1)), "");
        this.value = x.trim();
        calculateRange();

    }

    public void setSelectedCells(Cell[][] selectedCells) {
        this.selectedCells = selectedCells;
    }

    /**
     * @param trueStyle the trueStyle to set
     */
    public void setTrueStyle(Font font, int hAlignment, int vAlignment, Color fgColor, Color bgColor, Border border) {
        this.trueStyle = new ConditionalStyle(font, hAlignment, vAlignment, fgColor, bgColor, border);
    }

    /**
     * @param falseStyle the falseStyle to set
     */
    public void setFalseStyle(Font font, int hAlignment, int vAlignment, Color fgColor, Color bgColor, Border border) {
        this.falseStyle = new ConditionalStyle(font, hAlignment, vAlignment, fgColor, bgColor, border);
    }

}
