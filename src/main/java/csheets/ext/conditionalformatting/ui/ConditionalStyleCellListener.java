/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.conditionalformatting.ui;

import csheets.ext.style.ConditionalStyleCell;
import java.util.EventListener;

/**
 *
 * @author bie
 */
public interface ConditionalStyleCellListener extends EventListener{
    
    public void stylableCellChanged(ConditionalStyleCell cell);
    
}
