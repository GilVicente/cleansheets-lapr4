/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.conditionalformatting.ui;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.conditionalformatting.ConditionalFormattingRanges;
import csheets.ext.style.ConditionalStyleCell;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.border.Border;

/**
 *
 * @author bie
 */
public class ConditionalFormattingController extends FocusOwnerAction {

    private UIController uiController;

    private ConditionalFormattingPanel uiPanel;

    public static ConditionalStyle trueStyle;
    public static ConditionalStyle falseStyle;

    private ConditionalFormattingRanges conditionalRange;

    @Override
    public void setEnabled(boolean newValue) {
        super.setEnabled(newValue); //To change body of generated methods, choose Tools | Templates.
    }

    public ConditionalFormattingController(UIController uiController, ConditionalFormattingPanel uiPanel) {
        this.uiController = uiController;
        this.uiPanel = uiPanel;
    }

    public ConditionalFormattingController() {

    }

    public void applyCondition(String condition, ConditionalStyleCell cell) throws FormulaCompilationException, IllegalValueTypeException {

        Cell activeCell = uiController.getActiveCell();

        String previousContent = activeCell.getContent();

        StylableCell stylableCell = (StylableCell) uiController.getActiveCell().getExtension(StyleExtension.NAME);
        activeCell.setContent(condition);
        System.out.println(condition);

        if (activeCell.getValue().toBoolean() == false) {

            if (cell.stylesAreValid()) {
                ConditionalStyle falseStyle = cell.getFalseStyle();
//                System.out.println("Cor falseStyle: " + falseStyle.getBgColor());
                stylableCell.setFont(falseStyle.getFont());
                stylableCell.setHorizontalAlignment(falseStyle.gethAlignment());
                stylableCell.setVerticalAlignment(falseStyle.getvAlignment());
                stylableCell.setBackgroundColor(falseStyle.getBgColor());
                stylableCell.setForegroundColor(falseStyle.getFgColor());
                stylableCell.setBorder(falseStyle.getBorder());
            }

        } else if (cell.stylesAreValid()) {
            ConditionalStyle trueStyle = cell.getTrueStyle();
            stylableCell.setFont(trueStyle.getFont());
            stylableCell.setHorizontalAlignment(trueStyle.gethAlignment());
            stylableCell.setVerticalAlignment(trueStyle.getvAlignment());
            stylableCell.setBackgroundColor(trueStyle.getBgColor());
            stylableCell.setForegroundColor(trueStyle.getFgColor());
            stylableCell.setBorder(trueStyle.getBorder());
        }

        //activeCell.setContent(previousContent);
    }

    /**
     * @return the trueStyle
     */
    public ConditionalStyle getTrueStyle() {
        System.out.println("Entrou Get Style");

        return trueStyle;
    }

    /**
     * @param trueStyle the trueStyle to set
     */
    public void setTrueStyle(Font font, int hAlignment, int vAlignment, Color fgColor, Color bgColor, Border border) {
        this.trueStyle = new ConditionalStyle(font, hAlignment, vAlignment, fgColor, bgColor, border);
    }

    /**
     * @return the falseStyle
     */
    public ConditionalStyle getFalseStyle() {
        return falseStyle;
    }

    /**
     * @param falseStyle the falseStyle to set
     */
    public void setFalseStyle(Font font, int hAlignment, int vAlignment, Color fgColor, Color bgColor, Border border) {
        this.falseStyle = new ConditionalStyle(font, hAlignment, vAlignment, fgColor, bgColor, border);
    }

    @Override
    protected String getName() {
        return "Cell range";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    public SpreadsheetTable getSpreadSheetTable() {
        return super.focusOwner;
    }

    public void repaintSS() {
        super.focusOwner.repaint();
    }
  

}
