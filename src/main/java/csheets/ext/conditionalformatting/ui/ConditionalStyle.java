/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.conditionalformatting.ui;

import java.awt.Color;
import java.awt.Font;
import javax.swing.border.Border;

/**
 *
 * @author bie
 */
public class ConditionalStyle {

    /**
     * The font used when rendering the cell's content
     */
    private Font font;

    /**
     * The horizontal alignment of the cell's content
     */
    private int hAlignment;

    /**
     * The vertical alignment of the cell's content
     */
    private int vAlignment;

    /**
     * The color used when rendering the cell's content
     */
    private Color fgColor;

    /**
     * The background color of the cell
     */
    private Color bgColor;

    /**
     * The border of the cell
     */
    private Border border;

    public ConditionalStyle(Font font, int hAlignment, int vAlignment, Color fgColor, Color bgColor, Border border) {
        this.font = font;
        this.hAlignment = hAlignment;
        this.vAlignment = vAlignment;
        this.fgColor = fgColor;
        this.bgColor = bgColor;
        this.border = border;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public int gethAlignment() {
        return hAlignment;
    }

    public void sethAlignment(int hAlignment) {
        this.hAlignment = hAlignment;
    }

    public int getvAlignment() {
        return vAlignment;
    }

    public void setvAlignment(int vAlignment) {
        this.vAlignment = vAlignment;
    }

    public Color getFgColor() {
        return fgColor;
    }

    public void setFgColor(Color fgColor) {
        this.fgColor = fgColor;
    }

    public Color getBgColor() {
        return bgColor;
    }

    public void setBgColor(Color bgColor) {
        this.bgColor = bgColor;
    }

    public Border getBorder() {
        return border;
    }

    public void setBorder(Border border) {
        this.border = border;
    }
    
}
