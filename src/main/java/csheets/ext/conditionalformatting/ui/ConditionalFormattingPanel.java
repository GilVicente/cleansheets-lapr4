/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.conditionalformatting.ui;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.comments.CommentsExtension;
import csheets.ext.style.ConditionalStyleCell;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ext.style.ui.StyleToolBar;
import csheets.ui.ctrl.SelectionEvent;
import csheets.ui.ctrl.SelectionListener;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 *
 * @author bie
 */
public class ConditionalFormattingPanel extends JPanel implements SelectionListener, ConditionalStyleCellListener {

    public static ConditionalFormattingController controller;

    private JTextField condition;

    private StyleToolBar toolBar;

    private ConditionalStyleCell stylableCell;

    private ConditionalStyleCell previousCell = null;

    public ConditionalFormattingPanel(UIController uiController) {
        super(new BorderLayout());
        setName(CommentsExtension.NAME);

        // Creates controller
        controller = new ConditionalFormattingController(uiController, this);

        condition = new JTextField();

        condition.setPreferredSize(new Dimension(120, 45));
        condition.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel conditionPanel = new JPanel();

        uiController.addSelectionListener(this);

        conditionPanel.setLayout(new BoxLayout(conditionPanel, BoxLayout.PAGE_AXIS));
        conditionPanel.setPreferredSize(new Dimension(130, 45));
        conditionPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        conditionPanel.add(condition);

        JPanel styleTrue = new JPanel();

        JButton saveTrueFormatting = new JButton("True");

        saveTrueFormatting.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                StylableCell sCell = (StylableCell) uiController.getActiveCell().getExtension(StyleExtension.NAME);

                System.out.println(uiController.getActiveCell().getExtension(StyleExtension.NAME));

                System.out.println("Celula true");
                System.out.println("Cor bg:" + sCell.getBackgroundColor());
                System.out.println("Font:" + sCell.getFont());
                System.out.println("H:" + sCell.getHorizontalAlignment());
                System.out.println("v:" + sCell.getVerticalAlignment());
                System.out.println("fg:" + sCell.getForegroundColor());
                System.out.println("bg:" + sCell.getBackgroundColor());
                System.out.println("border:" + sCell.getBorder());

                stylableCell.setTrueStyle(sCell.getFont(), sCell.getHorizontalAlignment(), sCell.getVerticalAlignment(), sCell.getForegroundColor(), sCell.getBackgroundColor(), sCell.getBorder());

                controller.setTrueStyle(sCell.getFont(), sCell.getHorizontalAlignment(), sCell.getVerticalAlignment(), sCell.getForegroundColor(), sCell.getBackgroundColor(), sCell.getBorder());
            }
        });

        JButton saveFalseFormatting = new JButton("False");

        saveFalseFormatting.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                StylableCell sCell = (StylableCell) uiController.getActiveCell().getExtension(StyleExtension.NAME);

                System.out.println("Celula false");
                System.out.println("Cor bg:" + sCell.getBackgroundColor());

                stylableCell.setFalseStyle(sCell.getFont(), sCell.getHorizontalAlignment(), sCell.getVerticalAlignment(), sCell.getForegroundColor(), sCell.getBackgroundColor(), sCell.getBorder());
                controller.setFalseStyle(sCell.getFont(), sCell.getHorizontalAlignment(), sCell.getVerticalAlignment(), sCell.getForegroundColor(), sCell.getBackgroundColor(), sCell.getBorder());

            }
        });

        JButton aplicarCondicao = new JButton("Aplicar");

        aplicarCondicao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    controller.applyCondition(condition.getText(), stylableCell);
                } catch (FormulaCompilationException ex) {
                    Logger.getLogger(ConditionalFormattingPanel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalValueTypeException ex) {
                    Logger.getLogger(ConditionalFormattingPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        JPanel aplicar = new JPanel();

        aplicar.setLayout(new BoxLayout(aplicar, BoxLayout.PAGE_AXIS));
        aplicar.setPreferredSize(new Dimension(130, 45));
        aplicar.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        aplicar.add(aplicarCondicao);

        styleTrue.setLayout(new BoxLayout(styleTrue, BoxLayout.PAGE_AXIS));
        styleTrue.setPreferredSize(new Dimension(130, 45));
        styleTrue.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        styleTrue.add(saveTrueFormatting);

        JPanel styleFalse = new JPanel();

        styleFalse.setLayout(new BoxLayout(styleFalse, BoxLayout.PAGE_AXIS));
        styleFalse.setPreferredSize(new Dimension(130, 45));
        styleFalse.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        styleFalse.add(saveFalseFormatting);

        // Adds borders
        TitledBorder border = BorderFactory.createTitledBorder("Condition");
        border.setTitleJustification(TitledBorder.CENTER);
        conditionPanel.setBorder(border);

        // Adds panels
        JPanel northPanel = new JPanel(new GridLayout(3, 1));

        northPanel.add(conditionPanel);
        northPanel.add(styleTrue, CENTER_ALIGNMENT);
        northPanel.add(styleFalse, CENTER_ALIGNMENT);
        northPanel.add(aplicar);

        add(northPanel, BorderLayout.NORTH);

    }

    public void selectionChanged(SelectionEvent event) {

        Cell cell = event.getCell();

        if (cell != null) {
            StylableCell activeCell = new StyleExtension().extend(cell);

            ConditionalStyleCell ssc = new ConditionalStyleCell(activeCell);

//            ssc.setTrueStyle(activeCell.getFont(), activeCell.getHorizontalAlignment(), activeCell.getVerticalAlignment(), activeCell.getForegroundColor(), activeCell.getBackgroundColor(), activeCell.getBorder());
//            ssc.setFalseStyle(activeCell.getFont(), activeCell.getHorizontalAlignment(), activeCell.getVerticalAlignment(), activeCell.getForegroundColor(), activeCell.getBackgroundColor(), activeCell.getBorder());
            // ssc.addConditionalStyleCellListener(this);
            stylableCellChanged(ssc);
        }

        // Stops listening to previous active cell
        if (event.getPreviousCell() != null) {
            //cannot remove previous
        }
    }

    @Override
    public void stylableCellChanged(ConditionalStyleCell cell) {
        this.stylableCell = cell;
    }

}
