package csheets.ext.conditionalformatting.ui;

import csheets.ext.Extension;
import csheets.ext.comments.ui.CommentPanel;
import csheets.ext.style.ui.StyleToolBar;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JToolBar;

/**
 *
 * @author 1141277
 */
public class UIExtensionConditionalFormatting extends UIExtension {

    private JComponent sideBar;
    
    private StyleToolBar toolBar;

    public UIExtensionConditionalFormatting(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public JMenu getMenu() {
        return null;
    }

    public JToolBar getToolBar() {
        return null;
    }

    @Override
    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new ConditionalFormattingPanel(uiController);
        }
        return sideBar;
    }

}
