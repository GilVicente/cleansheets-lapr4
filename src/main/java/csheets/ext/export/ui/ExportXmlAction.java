/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.export.ui;

import csheets.ext.export.ui.ExportToXmlUI;
import csheets.CleanSheets;
import csheets.ui.FileChooser;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import static javax.swing.Action.MNEMONIC_KEY;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.ImageIcon;

/**
 * action to start the export to xml progress
 * @author hugo1140465
 */
public class ExportXmlAction extends BaseAction{
    
    /** The CleanSheets application */
	protected CleanSheets app;

	/** The user interface controller */
	protected UIController uiController;

	/** The file chooser to use when prompting the user for the file to open */
	private FileChooser chooser;
        
        private ExportToXmlUI exportUI;
        
        

    public ExportXmlAction(CleanSheets app, UIController uiController, FileChooser chooser) {
        this.app = app;
        this.uiController = uiController;
        this.chooser = chooser;
    }
                       
    @Override
    protected String getName() {
        return "Export to XML";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        exportUI = new ExportToXmlUI(this.uiController);
        exportUI.setVisible(true);
    }  
    
        @Override
    protected void defineProperties() {
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
		putValue(SMALL_ICON, new ImageIcon(CleanSheets.class.getResource("res/img/save_as.gif")));
	}
    
    
}
