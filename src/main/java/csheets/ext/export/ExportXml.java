/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.export;

import csheets.core.Cell;
import csheets.ui.ctrl.UIController;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class used to export to XML
 *
 * @author hugo1140465
 */
public class ExportXml {

    private ExportXmlTags tags;
    private ExportController controller;
    private int spreadsheetCount;

    public ExportXml() {

    }

    public boolean export(ExportController controller, ExportXmlTags tags, UIController uiController) {

        this.controller = controller;
        this.tags = tags;

        spreadsheetCount = 0;
        String xmlData = createXml();
        return saveToFile(xmlData, controller.getPath());
    }

    private String createXml() {

        StringBuilder xmlData = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");

        if (controller.getActiveWorkbook() != null) {
            writeWorkbook(xmlData);
            return xmlData.toString();
        }
        if (controller.getSelectedCells() != null) {
            writeSelectedCells(xmlData);
        }
        if (controller.getActiveSpreadsheet() != null) {
            writeSpreadsheet(xmlData);
        }
        if (controller.getSpreadsheetsList() != null) {
            writeSpreadsheets(xmlData);
        }

        return xmlData.toString();
    }

    public boolean saveToFile(String xmlData, String filePath) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(xmlData);
        } catch (IOException e) {
        } finally {
            try {
                writer.close();
                return true;
            } catch (IOException e) {
                return false;
            }
        }
    }

    private void writeWorkbook(StringBuilder xmlData) {

        xmlData.append("<" + tags.getWorkbook() + ">\n");
        writeSpreadsheets(xmlData);
        xmlData.append("</" + tags.getWorkbook() + ">\n");
    }

    private void writeSpreadsheet(StringBuilder xmlData) {

        spreadsheetCount++;
        xmlData.
                append("\t\t<" + tags.getSpreadsheet() + " id=\"" + spreadsheetCount + "\">\n");

        xmlData.append("\t\t\t<" + tags.getCells() + ">\n");
        for (int i = 0; i <= controller.getActiveSpreadsheet().getRowCount(); i++) {

            for (int j = 0; j <= controller.getActiveSpreadsheet().
                    getColumnCount(); j++) {
                Cell cell = controller.getActiveSpreadsheet().getCell(j, i);

                if (!cell.getContent().isEmpty()) {
                    xmlData.append("\t\t\t\t<" + tags.getCell() + " id=\"" + cell.getAddress() + "\">\n");
                    xmlData.append("\t\t\t\t\t<" + tags.getContent() + ">");
                    xmlData.append(cell.getContent());
                    xmlData.append("</" + tags.getContent() + ">\n");
                    xmlData.append("\t\t\t\t</" + tags.getCell() + ">\n");
                }
            }
        }
        xmlData.append("\t\t\t</" + tags.getCells() + ">\n");
        xmlData.append("\t\t</" + tags.getSpreadsheet() + ">\n");
    }

    private void writeSelectedCells(StringBuilder xmlData) {

        xmlData.append("\t\t\t<" + tags.getCells() + ">\n");
        for (int j = 0; j < controller.getSelectedCells().size(); j++) {
            String cellId = controller.getSelectedCells().get(j);
            xmlData.append("\t\t\t\t<" + tags.getCell() + " id=\"" + cellId + "\">\n");
            xmlData.append("\t\t\t\t\t<" + tags.getContent() + ">");
            xmlData.append(controller.getSelectedCells().get(j));
            xmlData.append("</" + tags.getContent() + ">\n");
            xmlData.append("\t\t\t\t</" + tags.getCell() + ">\n");
        }
        xmlData.append("\t\t\t</" + tags.getCells() + ">\n");
    }
    
    private void writeSpreadsheets(StringBuilder xmlData) {

        xmlData.append("\t<" + tags.getSpreadsheets() + ">\n");
        for (int i = 0; i < controller.getSpreadsheetsList().size(); i++) {
            controller.setActiveSpreadsheet(controller.getSpreadsheetsList().get(i));
            writeSpreadsheet(xmlData);
        }
        xmlData.append("\t</" + tags.getSpreadsheets() + ">\n");
    }


}
