/*
 */
package csheets.ext.export;

/**
 * class to store the xml tags defined by the user
 * @author hug1140465
 */
public class ExportXmlTags {

    private String workbook;
    private String spreadsheets;
    private String spreadsheet;
    private String cells;
    private String cell;
    private String content;

    public ExportXmlTags() {
        this.workbook = "workbook";
        this.spreadsheets = "spreadsheets";
        this.spreadsheet = "spreadsheet";
        this.cells = "cells";
        this.cell = "cell";
        this.content = "content";
    }

    public ExportXmlTags(String workbook, String spreadsheets, String spreadsheet, String cells, String cell, String content) {
        this.workbook = workbook;
        this.spreadsheets = spreadsheets;
        this.spreadsheet = spreadsheet;
        this.cells = cells;
        this.cell = cell;
        this.content = content;
    }

    public String getWorkbook() {
        return workbook;
    }

    public String getSpreadsheets() {
        return spreadsheets;
    }

    public String getSpreadsheet() {
        return spreadsheet;
    }

    public String getContent() {
        return content;
    }

    public String getCells() {
        return cells;
    }

    public String getCell() {
        return cell;
    }

}
