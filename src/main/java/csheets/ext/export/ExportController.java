/*
 */
package csheets.ext.export;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ext.export.ExportXmlTags;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * The export to xml controlelr
 * @author hugo1140465
 */
public class ExportController extends FocusOwnerAction {

	private UIController uiController;


	private Workbook activeWorkbook;
	private Spreadsheet activeSpreadsheet;
	private List<Spreadsheet> spreadsheetsList;
	private HashMap<Address, String> selectedCells;
        private String filePath;

        
        /**
         * controller constructor
         * @param uiController ui controller
         */
	public ExportController(UIController uiController) {
		this.uiController = uiController;
	}
        
        /**
         * method to export the xml file
         * @param filePath path to export to
         * @param exportTagsInfo xml tags to use
         */
	public void export( String filePath, ExportXmlTags exportTagsInfo) {

                ExportXml export = new ExportXml();
                this.setFilePath(filePath);
		
                
		boolean result = export.export(this, exportTagsInfo, this.uiController);

		if (result) {
			JOptionPane.
				showMessageDialog(null, "File created successfully.",
								  "Export to XML" , JOptionPane.DEFAULT_OPTION);
		} else {
			JOptionPane.
				showMessageDialog(null, "Error creating the file.",
								  "Export to XML" , JOptionPane.ERROR_MESSAGE);
		}
	}

	public void addActiveWorkbook() {

            this.setActiveWorkbook(uiController.getActiveWorkbook());
		this.setSpreadsheetsList(new ArrayList<Spreadsheet>());

		int numberOfSpreadsheets = getActiveWorkbook().getSpreadsheetCount();
		for (int i = 0; i < numberOfSpreadsheets; i++) {
                    this.getSpreadsheetsList().add(getActiveWorkbook().getSpreadsheet(i));
		}
                this.setActiveSpreadsheet(null);
		this.setSelectedCells(null);
	}

        public void addActiveSpreadsheet() {

            this.setActiveSpreadsheet(uiController.getActiveSpreadsheet());
            this.setActiveWorkbook(null);
		this.setSpreadsheetsList(null);
		this.setSelectedCells(null);
	}

        
        public String getPath(){
            return this.filePath;
        }
        
        
        public void addSelectedSpreadsheets(List<Integer> spreadsheetsIndexes) {

		this.setSpreadsheetsList(new ArrayList<Spreadsheet>());
                for (int spreadsheetIndex : spreadsheetsIndexes) {
                    this.getSpreadsheetsList().add(uiController.getActiveWorkbook().
                            getSpreadsheet(spreadsheetIndex));
		}
		this.setActiveWorkbook(null);
		this.setActiveSpreadsheet(null);
		this.setSelectedCells(null);
        }
        
	public void addSelectedCells() {

		Cell[][] selectedCells = focusOwner.getSelectedCells();
		this.setSelectedCells(new HashMap<Address, String>());

		for (int i = 0; i < selectedCells.length; i++) {
			for (int j = 0; j < selectedCells[0].length; j++) {
                            Cell c = selectedCells[i][j];
                            this.getSelectedCells().put(c.getAddress(), c.
							getContent());
			}
		}

		this.setActiveWorkbook(null);
		this.setActiveSpreadsheet(null);
		this.setSpreadsheetsList(null);
	}

	@Override
	protected String getName() {
		return "";
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

    /**
     * @return the activeWorkbook
     */
    public Workbook getActiveWorkbook() {
        return activeWorkbook;
    }

    /**
     * @return the activeSpreadsheet
     */
    public Spreadsheet getActiveSpreadsheet() {
        return activeSpreadsheet;
    }

    /**
     * @return the spreadsheetsList
     */
    public List<Spreadsheet> getSpreadsheetsList() {
        return spreadsheetsList;
    }

    /**
     * @return the selectedCells
     */
    public HashMap<Address, String> getSelectedCells() {
        return selectedCells;
    }

    /**
     * @param activeWorkbook the activeWorkbook to set
     */
    public void setActiveWorkbook(Workbook activeWorkbook) {
        this.activeWorkbook = activeWorkbook;
    }

    /**
     * @param activeSpreadsheet the activeSpreadsheet to set
     */
    public void setActiveSpreadsheet(Spreadsheet activeSpreadsheet) {
        this.activeSpreadsheet = activeSpreadsheet;
    }

    /**
     * @param spreadsheetsList the spreadsheetsList to set
     */
    public void setSpreadsheetsList(List<Spreadsheet> spreadsheetsList) {
        this.spreadsheetsList = spreadsheetsList;
    }

    /**
     * @param selectedCells the selectedCells to set
     */
    public void setSelectedCells(HashMap<Address, String> selectedCells) {
        this.selectedCells = selectedCells;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
