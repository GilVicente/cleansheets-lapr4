/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share.ui;

import csheets.core.Cell;
import csheets.core.CellImpShared;
import csheets.core.CellImpl;
import csheets.core.CellListener;
import csheets.ext.share.ShareExtension;
import csheets.ext.style.StylableCell;
import csheets.ipc.connection.Broadcast;
import csheets.ipc.connection.ConnectionList;
import csheets.ipc.connection.Shared;
import csheets.ipc.connection.SharedList;
import csheets.ipc.connection.TCPConnection;
import csheets.ui.ctrl.SelectionEvent;
import csheets.ui.ctrl.SelectionListener;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTableModel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.EventListener;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.xml.bind.Marshaller.Listener;

/**
 *
 * @author Filipe
 */
public class ActiveInstancesPanel extends JPanel implements SelectionListener {

    private ConnectionList connectionList = new ConnectionList();

    private Shared shared = new Shared();

    public static SharedList listOfShares = new SharedList();

    private StartSharingController controller;

    private Cell[][] cells;

    public static boolean sending = false;

    private static JList listBox = new JList();

    private boolean automaticUpdate = false;

    public static String returnAddress() {
        return (String) listBox.getSelectedValue();
    }

    public ActiveInstancesPanel(UIController uiController) {
        super(new BorderLayout());
        setName(ShareExtension.NAME);

        controller = new StartSharingController(uiController, this);
        uiController.addSelectionListener(this);

        ApplyAction applyAction = new ApplyAction();

        listBox.setPreferredSize(new Dimension(120, 240));		// width, height
        listBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        listBox.addFocusListener(applyAction);
        listBox.setAlignmentX(Component.CENTER_ALIGNMENT);

        listBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getClickCount() % 2 == 0 && !e.isConsumed()) {
                    e.consume();

                    String ipName = JOptionPane.showInputDialog("Introduza o nome da ligação:");
                    if (ipName != null) {

                        String ip = String.valueOf(listBox.getSelectedValue()).replace("/", "").replace(".", "#");
                        String[] holder = ip.split("#");
                        listBox.setSelectedValue(ipName, true);

                        byte[] addr = new byte[]{(byte) Integer.parseInt(holder[0]),
                            (byte) Integer.parseInt(holder[1]),
                            (byte) Integer.parseInt(holder[2]),
                            (byte) Integer.parseInt(holder[3])};

                        try {
                            connectionList.addEntry(InetAddress.getByAddress(addr), ipName);
                        } catch (UnknownHostException ex) {
                            Logger.getLogger(ActiveInstancesPanel.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        Shared shared1 = new Shared();
                        try {
                            shared1.setIp(InetAddress.getByAddress(addr));
                        } catch (UnknownHostException ex) {
                            Logger.getLogger(ActiveInstancesPanel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        shared1.setIpName(ipName);
                        shared1.setCellsToShare(ShareMenu.action.getSpreadSheetTable().getSelectedCells());
                        listOfShares.addShared(shared1);

//                        Cell[][] justForTheMessage = ShareMenu.action.getSpreadSheetTable().getSelectedCells();
                        Cell[][] justForTheMessage = shared1.getCellsToShare();
                        JOptionPane.showMessageDialog(null, "Cells that are beeing shared with " + shared1.getIpName() + ":\n" + justForTheMessage[0][0] + ":" + justForTheMessage[justForTheMessage.length - 1][justForTheMessage[0].length - 1]);

                        int optionSelected;
                        optionSelected = JOptionPane.showConfirmDialog(null, new String("Do you wish to start real time cell sharing?"));

                        try {
                            TCPConnection.Ip = InetAddress.getByAddress(addr);
                        } catch (UnknownHostException ex) {
                            Logger.getLogger(ActiveInstancesPanel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (optionSelected == 0) {

                            shareCellListener listener = new shareCellListener(addr);
                            ShareMenu.action.getSpreadSheetTable().getSpreadsheet().addCellListener(listener);
                            TCPConnection.realTimeSharing = true;
                            ShareMenu.stopAction.setEnabled(true);
                        }
                        if (!TCPConnection.realTimeSharing) {
                            Cell[][] selectedCells = ShareMenu.action.getSpreadSheetTable().getSelectedCells();
                            for (Shared sharedIp : listOfShares.geList()) {

                                TCPConnection.sharingSendData(sharedIp.getIp(), selectedCells);
                            }

                        }

                    }

                }

            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

        });

        // Lays out comment components
        JPanel commentPanel = new JPanel();
        commentPanel.setLayout(new BoxLayout(commentPanel, BoxLayout.PAGE_AXIS));
        commentPanel.setPreferredSize(new Dimension(130, 336));
        commentPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        commentPanel.add(listBox);

        // Adds borders
        TitledBorder border = BorderFactory.createTitledBorder("Active network instances");
        border.setTitleJustification(TitledBorder.CENTER);
        commentPanel.setBorder(border);

        // Adds panels
        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(commentPanel, BorderLayout.NORTH);
        add(northPanel, BorderLayout.NORTH);

        InstancesListUpdate t = new InstancesListUpdate();
        t.start();

    }

    /**
     * Updates the comments field
     *
     * @param event the selection event that was fired
     */
    @Override
    public void selectionChanged(SelectionEvent event) {

    }

    protected class ApplyAction implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            //TODO
        }

        @Override
        public void focusLost(FocusEvent e) {
            //Do nothing
        }

    }

    public class InstancesListUpdate extends Thread {

        @Override
        public void run() {
            while (true) {

                //  while (!Broadcast.listUpdated){/*System.out.println("list false");*/}
                // System.out.println("ff");
                DefaultListModel model = new DefaultListModel();
                for (InetAddress i : Broadcast.getActiveInstancesList()) {
                    //hashmap

                    if (connectionList.getHashMap().containsKey(i)) {
                        model.addElement(connectionList.getHashMap().get(i));
                    } else {
                        model.addElement(i);
                    }
                }
                listBox.setModel(model);

                try {
                    Thread.sleep(5000);
                    //System.out.println("dd");
                } catch (InterruptedException ex) {
                    Logger.getLogger(ActiveInstancesPanel.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

    }

    private class shareCellListener implements CellListener {

        private byte[] addr;

        private boolean busy = false;

        private Cell[][] cells = new Cell[1][1];

        public shareCellListener(byte[] addr) {
            this.addr = addr;
        }

        ;
        
        @Override
        public void valueChanged(Cell cell) {

            if (TCPConnection.realTimeSharing) {
                if (!busy) {

                    this.busy = true;
                    boolean flag = false;
                    for (Shared s : listOfShares.geList()) {

                        Cell[][] cl = s.cellsToShare;
                        for (Cell[] cl1 : cl) {

                            for (Cell cl11 : cl1) {
                                if (cl11.getAddress().getColumn() == cell.getAddress().getColumn()
                                        && cl11.getAddress().getRow() == cell.getAddress().getRow()) {
                                    CellImpShared c = new CellImpShared(null, null);
                                    c.copyFrom(cell);

                                    cells[0][0] = c;
                                    flag = true;
                                    TCPConnection.sharingSendData(s.getIp(), cells);
                                    break;
                                }
                            }
                            if (flag) {
                                break;
                            }
                        }

                    }
                    System.out.println(cell.getExtension("Style"));

                }
            }
            busy = false;

        }

        @Override
        public void contentChanged(Cell cell) {

            if (TCPConnection.realTimeSharing) {
                if (!busy) {
                    this.busy = true;
                    boolean flag = false;
                    for (Shared s : listOfShares.geList()) {

                        Cell[][] cl = s.cellsToShare;
                        for (Cell[] cl1 : cl) {

                            for (Cell cl11 : cl1) {
                                if (cl11.getAddress().getColumn() == cell.getAddress().getColumn()
                                        && cl11.getAddress().getRow() == cell.getAddress().getRow()) {
                                    CellImpShared c = new CellImpShared(null, null);
                                    c.copyFrom(cell);

                                    cells[0][0] = c;
                                    flag = true;
                                    TCPConnection.sharingSendData(s.getIp(), cells);
                                    break;
                                }
                            }
                            if (flag) {
                                break;
                            }
                        }

                    }
                    System.out.println(cell.getExtension("Style"));

                }
            }
            busy = false;
        }

        @Override
        public void dependentsChanged(Cell cell) {
            if (TCPConnection.realTimeSharing) {
                if (!busy) {

                    this.busy = true;
                    boolean flag = false;
                    for (Shared s : listOfShares.geList()) {

                        Cell[][] cl = s.cellsToShare;
                        for (Cell[] cl1 : cl) {

                            for (Cell cl11 : cl1) {
                                if (cl11.getAddress().getColumn() == cell.getAddress().getColumn()
                                        && cl11.getAddress().getRow() == cell.getAddress().getRow()) {
                                    CellImpShared c = new CellImpShared(null, null);
                                    c.copyFrom(cell);

                                    cells[0][0] = c;
                                    flag = true;
                                    TCPConnection.sharingSendData(s.getIp(), cells);
                                    break;
                                }
                            }
                            if (flag) {
                                break;
                            }
                        }

                    }
                    System.out.println(cell.getExtension("Style"));

                }
            }
            busy = false;
        }

        @Override
        public void cellCleared(Cell cell) {
            if (TCPConnection.realTimeSharing) {
                if (!busy) {

                    this.busy = true;
                    boolean flag = false;
                    for (Shared s : listOfShares.geList()) {

                        Cell[][] cl = s.cellsToShare;
                        for (Cell[] cl1 : cl) {

                            for (Cell cl11 : cl1) {
                                if (cl11.getAddress().getColumn() == cell.getAddress().getColumn()
                                        && cl11.getAddress().getRow() == cell.getAddress().getRow()) {
                                    CellImpShared c = new CellImpShared(null, null);
                                    c.copyFrom(cell);

                                    cells[0][0] = c;
                                    flag = true;
                                    TCPConnection.sharingSendData(s.getIp(), cells);
                                    break;
                                }
                            }
                            if (flag) {
                                break;
                            }
                        }

                    }
                    System.out.println(cell.getExtension("Style"));

                }
            }
            busy = false;
        }

        @Override
        public void cellCopied(Cell cell, Cell source) {
            if (TCPConnection.realTimeSharing) {
                if (!busy) {

                    this.busy = true;
                    boolean flag = false;
                    for (Shared s : listOfShares.geList()) {

                        Cell[][] cl = s.cellsToShare;
                        for (Cell[] cl1 : cl) {

                            for (Cell cl11 : cl1) {
                                if (cl11.getAddress().getColumn() == cell.getAddress().getColumn()
                                        && cl11.getAddress().getRow() == cell.getAddress().getRow()) {
                                    CellImpShared c = new CellImpShared(null, null);
                                    c.copyFrom(cell);

                                    cells[0][0] = c;
                                    flag = true;
                                    TCPConnection.sharingSendData(s.getIp(), cells);
                                    break;
                                }
                            }
                            if (flag) {
                                break;
                            }
                        }

                    }
                    System.out.println(cell.getExtension("Style"));

                }
            }
            busy = false;
        }

    }
}
