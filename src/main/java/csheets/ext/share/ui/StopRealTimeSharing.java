/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share.ui;

import csheets.ipc.connection.TCPConnection;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;

/**
 *
 * @author hugoc
 */
class StopRealTimeSharing extends FocusOwnerAction {

    public StopRealTimeSharing(StartSharingAction action) {
    }

    @Override
    protected String getName() {
        return "Stop real time cell sharing";
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        TCPConnection.realTimeSharing=false;
        this.setEnabled(false);
    }
    

}
