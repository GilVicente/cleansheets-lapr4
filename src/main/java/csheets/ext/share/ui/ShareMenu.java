/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share.ui;

import csheets.ext.workbooknetworksearch.ui.WorkbookNetworkSearchMenu;
import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author Filipe
 */
public class ShareMenu extends JMenu{
    public static StartSharingAction action =null;
    
    public static StopRealTimeSharing stopAction = null;
    
    public ShareMenu(UIController uiController){
        super("Share");
        add(action = new StartSharingAction(uiController));
        add(new ChangePortAction(uiController,action));
        add(stopAction = new StopRealTimeSharing(action));
        add(new WorkbookNetworkSearchMenu(uiController));
        
        stopAction.setEnabled(false);
        action.setEnabled(false);
        
    }
        
    
}
