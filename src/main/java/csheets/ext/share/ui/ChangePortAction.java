/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share.ui;

import csheets.ipc.connection.Broadcast;
import csheets.ipc.connection.TCPConnection;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author Filipe
 */
public class ChangePortAction extends FocusOwnerAction {

    protected UIController uiController;

    private StartSharingAction action;

    public ChangePortAction(UIController uiController, StartSharingAction action) {
        this.uiController = uiController;
        this.action = action;
    }

    @Override
    protected String getName() {
        return "Configure port";
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        /*try {
         CleanSheets.broadcast.restart();
         } catch (InterruptedException ex) {
         Logger.getLogger(ChangePortAction.class.getName()).log(Level.SEVERE, null, ex);
         }
         //CleanSheets.tcpC.restart();*/
        Broadcast.PORT = getPort();
        Broadcast b = new Broadcast();
        b.run();
        TCPConnection tcp = new TCPConnection();
        tcp.run();
        this.action.setEnabled(true);
        this.setEnabled(false);

    }

    protected int getPort() {
        int port = -1;
        while (!validatePort(port)) {
            try {
                port = Integer.parseInt(JOptionPane.showInputDialog(null, "Insert the port", "Port Configuration"));
            } catch (NumberFormatException ex) {
                continue;
            }
        }
        return port;
    }

    protected boolean validatePort(int port) {
        return (port >= 0 && port < Short.MAX_VALUE * 2);
    }

    public SpreadsheetTable getSpreadSheetTable() {
        return super.focusOwner;
    }
}
