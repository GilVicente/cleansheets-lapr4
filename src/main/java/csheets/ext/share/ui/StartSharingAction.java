/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share.ui;

import csheets.core.Cell;
import csheets.ipc.connection.Broadcast;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.event.ActionEvent;

/**
 *
 * @author Filipe
 */
public class StartSharingAction extends FocusOwnerAction {
    

    @Override
    public void setEnabled(boolean newValue) {
        super.setEnabled(newValue); //To change body of generated methods, choose Tools | Templates.
    }
    
    protected UIController uiController;
    
    public StartSharingAction(UIController uiController){
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Start Sharing";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Broadcast r = new Broadcast();
        r.runReceive();
        
    }
    
    public SpreadsheetTable getSpreadSheetTable(){
        return super.focusOwner;
    }
    
    public void repaintSS(){
        super.focusOwner.repaint();
    }
}
