/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share.ui;

import csheets.ext.Extension;
import csheets.ext.comments.ui.CommentPanel;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 *
 * @author Filipe
 */
public class ShareExtensionUI extends UIExtension {
    
    public static String NAME = "Share";
    
    private Icon icon;
    
    private ShareMenu menu;
    
    private JComponent sideBar;
    
    
    public ShareExtensionUI (Extension extension, UIController uiController){
        super(extension, uiController);
    }
    
    public Icon getIcon(){
        return null;
    }
    
    public JMenu getMenu(){
        if(menu == null)
            menu = new ShareMenu(uiController);
        return menu;
    }
    
    public JComponent getSideBar(){
        if (sideBar == null)
			sideBar = new ActiveInstancesPanel(uiController);
		return sideBar;
    }
    
    
}
