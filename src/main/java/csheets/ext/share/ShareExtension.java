/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share;

import csheets.ext.Extension;
import csheets.ext.share.ui.ShareExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Filipe
 */
public class ShareExtension extends Extension {
    
    
    public static final String NAME = "Share";
    
    public ShareExtension(){
        super(NAME);
    }
    
    public UIExtension getUIExtension(UIController uiController) {
		return new ShareExtensionUI(this, uiController);
	}
}
