/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.share;

import csheets.core.Address;
import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author Filipe
 */
public class CellSharing implements Serializable{
    
    private Map<Address,String> cellsToShare;
    
    
    public CellSharing(Map<Address, String> cellsToShare){
        this.cellsToShare = cellsToShare;
    }
    
    public Map<Address, String> getCellsToShare(){
        return cellsToShare;
    }
    
    public void addCell(Address cellAddress, String cellContent){
        cellsToShare.put(cellAddress, cellContent);
    }
    
}
