/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FilesShare;

import csheets.ext.macros.*;
import csheets.core.Spreadsheet;
import csheets.ext.Extension;
import csheets.ext.FilesShare.ui.UIExtensionFilesShare;
import csheets.ext.SpreadsheetExtension;
import csheets.ext.macros.ui.UIExtensionMacros;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author 1140234
 */
public class FilesShareExtension extends Extension {

    /**
     * The name of the extension
     */
    public static final String NAME = "Files Share";

    /**
     * Creates a new Import/Export text Extension.
     */
    public FilesShareExtension() {
        
        super(NAME);

    }

    
    /**
     * Returns the user interface extension of this extension (an instance of
     * the class)
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        
        return new UIExtensionFilesShare(this, uiController);

    }
}
