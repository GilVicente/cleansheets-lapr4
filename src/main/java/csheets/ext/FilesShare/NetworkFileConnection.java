package csheets.ext.FilesShare;

import java.net.InetAddress;
import java.util.ArrayList;

public class NetworkFileConnection {

    /**
     * The networkFileConnection
     */
    private static NetworkFileConnection service = null;
    
    /**
     * List of InetAddresses
     */
    private ArrayList<InetAddress> listOfInstances = new ArrayList<>();
    
    /**
     * List of files to share
     */
    private ListFilesShare listFilesShare = new ListFilesShare();

    /**
     * Deafault Constructor
     */
    public NetworkFileConnection() {

    }
    
    /**
     * Return the service
     * @return 
     */
    public static NetworkFileConnection getInstance() {
        if (service == null) {
            service = new NetworkFileConnection();
        }
        return service;
    }

    /**
     * Remove all instances
     */
    public void removeAll() {
        for (int i = 0; i < listOfInstances.size(); i++) {
            listOfInstances.remove(i);
        }
    }
    
    /**
     * Return the list files shared
     * @return 
     */
    public ListFilesShare getListFilesShare() {
        return listFilesShare;
    }
    
    /**
     * set the list files shared
     * @param listFilesShare 
     */
    public void setListFilesShare(ListFilesShare listFilesShare) {
        this.listFilesShare = listFilesShare;
    }
    
    /**
     * Add a instance to the list
     * @param instanceData 
     */
    public void saveInstance(InetAddress instanceData) {
        if (!this.listOfInstances.contains(instanceData)) {

            this.getListOfInstances().add(instanceData);
        }
    }

    /**
     * Return the list of instances
     * @return 
     */
    public ArrayList<InetAddress> getListOfInstances() {
        return listOfInstances;
    }
    
    /**
     * Set the list of instances
     * @param listOfInstances 
     */
    public void setListOfInstances(ArrayList<InetAddress> listOfInstances) {
        this.listOfInstances = listOfInstances;
    }
}
