/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FilesShare.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author 1140234
 */
public class FilesShareWindowAction extends BaseAction {

    /**
     * The User Interface Controller
     */
    protected UIController uiController;

    /**
     * Creates a new FileShare action.
     *
     * @param uiController the user interface controller
     */
    public FilesShareWindowAction(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * Returns the name of the action to be used in the user interface
     *
     * @return the name of the action
     */
    @Override
    protected String getName() {
        return "File Share";
    }

    /**
     * The code fired when action is performed
     *
     * @param event the action event
     */
    @Override
    public void actionPerformed(ActionEvent event) {
       new FilesShareUI();
    }

}
