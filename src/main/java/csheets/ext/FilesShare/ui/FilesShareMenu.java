/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FilesShare.ui;

import csheets.ext.FilesShare.FilesShareExtension;
import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author 1140234
 */
public class FilesShareMenu extends JMenu {

    /**
     * Creates a new FileShare menu.
     *
     * @param uiController the user interface controller
     */
    public FilesShareMenu(UIController uiController) {
        super(FilesShareExtension.NAME);
        add(new FilesShareWindowAction(uiController));
        add(new FilesReceiveWindowAction(uiController));
    }
}
