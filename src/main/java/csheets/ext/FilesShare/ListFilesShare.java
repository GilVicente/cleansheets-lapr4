/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FilesShare;

import csheets.ext.FilesShare.ui.FilesShareUI;
import csheets.ext.share.ui.ActiveInstancesPanel;
import csheets.ipc.connection.TCPConnection;
import static csheets.ipc.connection.TCPConnection.sharingSendData;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1140234
 */
public class ListFilesShare implements Serializable {
    
    /**
     * List of files
     */
    private List<FileShare> listFiles;

    public ListFilesShare() {
        listFiles = new ArrayList<>();

    }

    /**
     * Fill the list o files
     * @param list
     * @return
     * @throws IOException 
     */
    public List<FileShare> fillTheList(List<File> list) throws IOException {
        List<FileShare> toReturn = new ArrayList<>();
        for (File aux : list) {
            copyFile(aux);
            FileShare a = new FileShare(aux.getName(), Integer.parseInt(String.valueOf(aux.length())));

            toReturn.add(a);

        }
        return toReturn;

    }
    
    
    /**
     * Get the files existing in the repository
     * @return 
     */
    public File[] getFilesExisting() {
        File dir = new File("./filesToShare");
        return (dir.listFiles());
    }

    /**
     * Add a single file
     * @param f 
     */
    public void addFile(FileShare f) {
        if (!this.listFiles.contains(f)) {
            this.listFiles.add(f);
        }
    }

    /**
     * Copy the file to the share directory
     * @param f
     * @throws IOException 
     */
    public void copyFile(File f) throws IOException {
        File des = new File("./filesToShare/" + f.getName());
        Files.copy(f.toPath(), des.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * Return the files list
     * @return 
     */
    public List<FileShare> returnTheList() {
        return this.listFiles;
    }

    /**
     * Send the list file by tcp, giving the ip destination
     * @param destination
     * @return 
     */
    public Thread sendListFiles(InetAddress destination) {
        try {
            TCPSenderFiles tCPSenderFiles = new TCPSenderFiles(destination, this);

            Thread thread_sender_tcp = new Thread(tCPSenderFiles);
            thread_sender_tcp.start();

            return thread_sender_tcp;
        } catch (IOException ex) {
            Logger.getLogger(FilesShareUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public void fillListFiles(String path) throws UnknownHostException, IOException {

        List<File> list = loadAllFiles(path);

        List<FileShare> send = fillTheList(list);
        for (FileShare fs : send) {
            listFiles.add(fs);
        }
    }
    
    
    /**
     * Load all files from the specified repository
     * @param path
     * @return 
     */
    private List<File> loadAllFiles(String path) {
        File directory = new File(path);
        List<File> resultList = new ArrayList<>();
        File[] fileArray = directory.listFiles();
        resultList.addAll(Arrays.asList(fileArray));
        for (File file : fileArray) {
            if (file.isDirectory()) {
                resultList.addAll(loadAllFiles(file.getAbsolutePath()));
            }
        }
        return resultList;
    }

}
