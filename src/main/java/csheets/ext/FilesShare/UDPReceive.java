package csheets.ext.FilesShare;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPReceive implements Runnable {

    private int broadcastPort = 4123;

    public UDPReceive() {
    }

    @Override
    public void run() {

        while (true) {
            try {
                findNewInstance();
            } catch (IOException ex) {
                System.out.println("Error trying to answer the requests of presence");
                System.out.println(ex);
            }
        }
    }

    public void findNewInstance() throws IOException {

        DatagramSocket socket = null;

        try {
            socket = new DatagramSocket(broadcastPort);
        } catch (SocketException ex) {
            System.out.println("Error trying search for new instances with broadcast address\n" + ex);
            System.exit(1);
        }

        byte[] data = new byte[300];

        String line;
        DatagramPacket packet = new DatagramPacket(data, data.length);
        socket.receive(packet);
        InetAddress destinationAddress = packet.getAddress();
        line = new String(packet.getData(), 0, packet.getLength());

        if (line.equals("#Request#")) {

            InetAddress addr = InetAddress.getLocalHost();

            String response = "Response;" + addr.getHostAddress();

            byte[] data_resposta = new byte[300];

            data_resposta = response.getBytes();

            int port = packet.getPort();

            DatagramPacket packet_resposta = new DatagramPacket(data_resposta, data_resposta.length, destinationAddress, port);

            socket.send(packet_resposta);
        }

        socket.close();
    }
}
