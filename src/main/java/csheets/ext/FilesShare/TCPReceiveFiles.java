package csheets.ext.FilesShare;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TCPReceiveFiles implements Runnable {
    
    private ServerSocket serverSocket;
    
    private Socket acceptSocket;
    
    private final int port = 40130;
    
    public TCPReceiveFiles() {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException ex) {
            System.out.println("Error assigning socket");
        }
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                acceptSocket = this.serverSocket.accept();
                receiveFileData();
                
            } catch (Exception e) {
                
                System.out.println(e);
            }
        }
    }
    
    /**
     * Receive the file data and convert to object and save
     * @throws IOException 
     */
    private void receiveFileData() throws IOException {
        ObjectInputStream inStream = new ObjectInputStream(acceptSocket.getInputStream());

        ListFilesShare fileList = new ListFilesShare();
        
        try {
            fileList = (ListFilesShare) convertFromBytes((byte[]) inStream.readObject());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TCPReceiveFiles.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        NetworkFileConnection.getInstance().setListFilesShare(fileList);
    }

    /**
     * Convert an array of bytes into an Object
     *
     * @param bytes Array of bytes
     * @return Object
     * @throws IOException Exception
     * @throws ClassNotFoundException Exception
     */
    private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
                ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }
    
}
