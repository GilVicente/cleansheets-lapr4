package csheets.ext.FilesShare;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;


public class UDPSender implements Runnable {

    private final int refreshTime = 5;


    private final int broadcastPort = 4123;


    public UDPSender() {
    }

    @Override
    public void run() {

        while (true) {
            try {
                NetworkFileConnection.getInstance().removeAll();
                searchForNewInstances();
                try {
                    Thread.sleep(refreshTime * 1000);
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            } catch (Exception e) {

                System.out.println("Erro ao tentar procurar por novos servidores!");
                System.out.println(e);
            }
        }
    }

    private void searchForNewInstances() throws SocketException, IOException {

        InetAddress destination_IP = null;
        String broadcast = "255.255.255.255";
        String keySearch = "#Request#";

        try {
            destination_IP = InetAddress.getByName(broadcast);
        } catch (UnknownHostException ex) {
            System.out.println("Error trying search for new instances with broadcast address\n" + ex);
            System.exit(1);
        }
        DatagramSocket broadcastSocket = new DatagramSocket();
        broadcastSocket.setBroadcast(true);
        byte[] send_Data = new byte[300];
        send_Data = keySearch.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(send_Data, keySearch.length(), destination_IP, broadcastPort);
        broadcastSocket.send(sendPacket);

        // Define a Timeout of 5 sec. If in the end of the 5 sec the socket do not receive
        // any response for other instances, the socket is closed.
        // resposta dos servidores procurados, termina a escuta
        broadcastSocket.setSoTimeout(5 * 1000);

        while (true) {
            byte[] received_Data = new byte[300];

            DatagramPacket received_Packet = new DatagramPacket(received_Data, received_Data.length);

            try {
                broadcastSocket.receive(received_Packet); // If the Timeout is exceeded, he throw the exception

            } catch (SocketTimeoutException e) {
                break;
            }
            InetAddress response_IP = received_Packet.getAddress();

            //We use this condition in order to refuse answers from itself!
            // if(!response_IP.equals(InetAddress.getLocalHost())){
            String response_String = new String(received_Packet.getData(), 0, received_Packet.getLength());

            System.out.println("Response - " + response_String);

            String[] split = response_String.split(";");
            
            NetworkFileConnection.getInstance().saveInstance(response_IP);

            // }
        }

        broadcastSocket.close();
    }
}
