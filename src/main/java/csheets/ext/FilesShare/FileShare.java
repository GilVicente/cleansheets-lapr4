/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FilesShare;

import java.io.Serializable;

/**
 * File with the information to share
 *
 * @author 1140234
 */
public class FileShare implements Serializable {

    /**
     * The file name
     */
    private String name;

    /**
     * The file size
     */
    private int size;

    /**
     * Default Constructor
     */
    public FileShare() {

    }

    /**
     * Constructor
     *
     * @param name - name of the file
     * @param size - size of the file
     */
    public FileShare(String name, int size) {
        this.name = name;
        this.size = size;
    }

    /**
     * return the textual representation of shared file
     *
     * @return textual representation
     */
    @Override
    public String toString() {
        return this.name + " -- " + this.size + "KBytes";
    }

}
