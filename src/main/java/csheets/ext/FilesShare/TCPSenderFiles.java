package csheets.ext.FilesShare;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

public class TCPSenderFiles implements Runnable {

    private final Socket socket;

    private final int port = 40130;

    private final String data;

    private ListFilesShare fileList;

    public TCPSenderFiles(InetAddress destinationAddress, ListFilesShare fileList) throws IOException {
        this.fileList = fileList;

        socket = new Socket(destinationAddress, port);

        InetAddress addr = InetAddress.getLocalHost();

        this.data = addr.getHostName() + ";TCPSenderFilesTest;Test2;Test3";
    }

    @Override
    public void run() {
        try {
            sendFileData();

        } catch (IOException ex) {

            System.out.println(ex);
        }
    }
    
    
    /**
     * Send the file data
     * @throws IOException 
     */
    public void sendFileData() throws IOException {
        ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());

        System.out.println("TCPFileSender - " + data);

//        output.writeObject(convertToBytes(data));
        output.writeObject(convertToBytes(fileList));
        socket.close();
    }

    /**
     * Convert an array of bytes into an Object
     *
     * @param bytes Array of bytes
     * @return Object
     * @throws IOException Exception
     * @throws ClassNotFoundException Exception
     */
    private byte[] convertToBytes(Object object) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        }
    }
}
