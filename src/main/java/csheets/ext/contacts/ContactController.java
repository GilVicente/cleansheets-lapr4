/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts;

import csheets.ext.contacts.MailNumber.Company;
import csheets.ext.contacts.MailNumber.Person;
import csheets.ext.contacts.MailNumber.Profession;
import csheets.ext.contacts.persistence.AppSettings;
import csheets.ext.contacts.persistence.ContactsJpaRepository;
import csheets.ext.contacts.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Guilherme
 */
public class ContactController {

//    public void registerContact(String firstname, String lastname, String image){
//        Contact contact = new Contact(firstname, lastname, image);
//        ContactsJpaRepository rep= new ContactsJpaRepository();
//        rep.addNewContact(contact);
//     }
    /**
     * Registration of person type contact (database)
     *
     * @param firstname - firstname
     * @param lastname - lastname
     * @param image - image
     * @param comp - company
     * @param profession - profession
     */
    public void registerPerson(String firstname, String lastname, String image, Company comp, Profession profession) {
        Person person = new Person(firstname, lastname, image, comp, profession);
        PersistenceContext.repositories().contacts().addNewContact(person);
//        ContactsJpaRepository rep = new ContactsJpaRepository();
//        rep.addNewContact(person);
    }

    /**
     * Registration of person type contact,without Company(database)
     *
     * @param firstname - firstname
     * @param lastname - lastname
     * @param image - image
     * @param profession - profession
     */
    public void registerPersonNoCompany(String firstname, String lastname, String image, Profession profession) {
        Person person = new Person(firstname, lastname, image, profession);
        PersistenceContext.repositories().contacts().addNewContact(person);
//        ContactsJpaRepository rep = new ContactsJpaRepository();
//        rep.addNewContact(person);
    }

    /**
     * Registration of Company type contact(database)
     *
     * @param firstname - firstname
     * @param image - image
     */
    public void registerCompany(String firstname, String image) {
        Company company = new Company(firstname, image);
        PersistenceContext.repositories().contacts().addNewContact(company);
//        ContactsJpaRepository rep = new ContactsJpaRepository();
//        rep.addNewContact(company);
    }

//    public void editContact(Contact contact,String firstname,String lastname,String photo){
//        contact.defineFirstname(firstname);
//        contact.defineLastname(lastname);
//        contact.definePicture(photo);
//        ContactsJpaRepository rep= new ContactsJpaRepository();
//        rep.updateEditContact(contact);
//     }
    /**
     * Editing a person type contact(database)
     *
     * @param person - person
     * @param firstname - firstname
     * @param lastname - lastname
     * @param image - image
     * @param comp - company
     * @param profession - profession
     */
    public void editPerson(Person person, String firstname, String lastname, String image, Company comp, Profession profession) {
        person.defineFirstname(firstname);
        person.defineLastname(lastname);
        person.definePicture(image);
        person.defineCompany(comp);
        person.defineProfession(profession);
        PersistenceContext.repositories().contacts().updateEditContact(person);
//        ContactsJpaRepository rep = new ContactsJpaRepository();
//        rep.updateEditContact(person);
    }

    /**
     * Edit a person type contact,no company(database)
     *
     * @param person - person
     * @param firstname - firstname
     * @param lastname - lastname
     * @param image - image
     * @param profession - profession
     */
    public void editPersonNoCompany(Person person, String firstname, String lastname, String image, Profession profession) {
        person.defineFirstname(firstname);
        person.defineLastname(lastname);
        person.definePicture(image);
        person.defineCompany(null);
        person.defineProfession(profession);
        PersistenceContext.repositories().contacts().updateEditContact(person);
//        ContactsJpaRepository rep = new ContactsJpaRepository();
//        rep.updateEditContact(person);
    }

    /**
     * Edit of Company type contact
     *
     * @param company - company
     * @param firstname - firstname
     * @param image - image
     */
    public void editCompany(Company company, String firstname, String image) {
        company.defineFirstname(firstname);
        company.definePicture(image);
        PersistenceContext.repositories().contacts().updateEditContact(company);
        ContactsJpaRepository rep = new ContactsJpaRepository();
        rep.updateEditContact(company);
    }

    /**
     * Get all the contacts stored in the database
     *
     * @return all the contacts stored in the database
     */
    public List<Contact> getContacts() {
        return PersistenceContext.repositories().contacts().allContacts();
//        ContactsJpaRepository rep = new ContactsJpaRepository();
//        return rep.allContacts();

    }

    /**
     * Get all the companies stored in the database
     *
     * @return all the companies stored in the database
     */
    public List<Company> getCompanies() {
        ContactsJpaRepository rep = new ContactsJpaRepository();
        return rep.companies();
    }

    /**
     * Get all the professions stored in the database
     *
     * @return
     */
    public List<Profession> getProfessions() {
        ContactsJpaRepository rep = new ContactsJpaRepository();
        return rep.professions();
    }

    /**
     * Returns a contact with id the same as the parameter
     *
     * @param idContact - id of Contact to be found
     * @return contact with id the same as the parameter
     */
    public Contact getContactId(long idContact) {
        ContactsJpaRepository rep = new ContactsJpaRepository();
        return (Contact) rep.findById(idContact);
    }

    /**
     * Removes contact from database
     *
     * @param contact - Contact to be removed
     */
    public void removeContact(Contact contact) {
        PersistenceContext.repositories().contacts().deleteContact(contact);
//        ContactsJpaRepository rep = new ContactsJpaRepository();
//        rep.deleteContact(contact);
    }

    /**
     * Verify if a tag exists and return the tag
     *
     * @param t
     * @return
     */
    public Tag verifyTag(String t) {

        for (Tag tag : PersistenceContext.repositories().tags().allTags()) {

            if (t.matches("^[a-zA-Z0-9]*$")) {
                if (tag.tagDescription().equals(t)) {
                    return tag;
                }
            }
        }
        return null;
    }

    /**
     * Associate an existent tag to a contact
     *
     * @param t
     * @param c
     * @return
     */
    public boolean associateTag(Tag t, Contact c) {

        return t.associateContact(c);
    }

    public boolean updateTag(Tag t) {
        return PersistenceContext.repositories().tags().updateTag(t);
    }

    /**
     * Create a new tag and associate to a contact
     *
     * @param t
     * @param c
     * @return
     */
    public boolean newTag(String t, Contact c) {
        if (t.matches("^[a-zA-Z0-9]*$")) {
            Tag tag = new Tag(t);
            tag.associateContact(c);
            if (verifyTag(t) == null) {
                return PersistenceContext.repositories().tags().newTag(tag);
            }
        }
        return false;
    }

    /**
     * Return a hashmap with the frequency of the tag utilization sorted
     * (descending)
     *
     * @return
     */
    public Map<String, Integer> frequencyOfTags() {
        Map map = new HashMap();
        for (Tag t : PersistenceContext.repositories().tags().allTags()) {
            map.put(t.tagDescription(), t.returnFrequency());
        }

//        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>();
//        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
//            @Override
//            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
//                return (o2.getValue()).compareTo(o1.getValue());
//            }
//        });
        return sortByComparator(map);
    }

    /**
     * Verify if the tags exists and return it's contacts
     *
     * @param t
     * @return
     */
    public List<Contact> tagContacts(String t) {
        Tag tag = verifyTag(t);
        if (tag != null) {
            return tag.tagContacts();
        }
        return null;
    }

    /**
     * Search all the tags that contains the substring
     * @param t
     * @return 
     */
    public List<Contact> tagContactsWithExpression(String t) {
        List<Contact> lst = new ArrayList<>();
        for (Tag tag : PersistenceContext.repositories().tags().allTags()) {

            if (tag.tagDescription().contains(t)) {

                for (Contact c : tag.tagContacts()) {
                    if (!lst.contains(c)) {

                        lst.add(c);
                    }
                }
            }
        }
        return lst;
    }

    /**
     * verify if a contact is already associated to a tag
     * @param t
     * @param c
     * @return 
     */
    public boolean containsContact(String t, Contact c) {
        return PersistenceContext.repositories().tags().containsContact(t, c);
    }
    
    /**
     * Sort the map
     * @param unsortMap
     * @return 
     */
    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) {

		// Convert Map to List
		List<Map.Entry<String, Integer>> list = 
			new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
                                           Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

}
