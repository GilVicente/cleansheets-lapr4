/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.bootstrapp;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.Tag;

import csheets.ext.contacts.address.ContactAddress;
import csheets.ext.contacts.address.PostalCode;
import csheets.ext.contacts.listnote.ListNote;
import csheets.ext.contacts.listnote.ListNoteElement;
import csheets.ext.contacts.persistence.PersistenceContext;
import java.util.List;


/**
 *
 * @author Tixa
 */
public class ContactBootstrap {

    public ContactBootstrap(){
        execute();
    }
    
    public boolean execute(){
        try {

            Contact c1 = new Contact("Patricia", "Monteiro");
            c1.setPrimaryContactAdress(new ContactAddress("Rua ISEP", "ISEP", "Porto", "Tuga", new PostalCode(300, 34)));
            Contact c2 = new Contact("Marco", "Sousa");
            Contact c3 = new Contact("Dinis", "Sousa");


            PersistenceContext.repositories().contacts().addNewContact(c1);
            PersistenceContext.repositories().contacts().addNewContact(c2);
            PersistenceContext.repositories().contacts().addNewContact(c3);
            
            //tags
            
            Tag t = new Tag("principal");
            Tag t2 = new Tag("dsa");
            Tag t3 = new Tag("qwe");
            Tag t4 = new Tag("zxc");
            
            t.associateContact(c1);
            t.associateContact(c2);
            t.associateContact(c3);
            t2.associateContact(c1);
            t3.associateContact(c2);
            t4.associateContact(c3);
            
            PersistenceContext.repositories().tags().newTag(t);
            PersistenceContext.repositories().tags().newTag(t2);
            PersistenceContext.repositories().tags().newTag(t3);
            PersistenceContext.repositories().tags().newTag(t4);
            
        } catch (Exception ex) {
            //Problems with primary key just ignore
        }
        return false;

    }
}
