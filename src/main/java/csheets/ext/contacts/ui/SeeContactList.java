/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.ContactController;
import csheets.ext.contacts.MailNumber.Company;
import csheets.ext.contacts.MailNumber.Person;
import csheets.ext.contacts.MailNumber.Profession;
import csheets.ext.contacts.MailNumber.ProfessionsList;
import csheets.ext.contacts.MailNumber.ProfessionsReader;
import csheets.ext.contacts.Tag;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Guilherme
 */
public class SeeContactList extends JDialog {

    /**
     * The contact controller responsible for the "manipulation" of contacts
     */
    private ContactController contactController;

    /**
     * JList to display the list of contacts
     */
    JList listContacts;

    /**
     * JButton to aid in the edit of the contact
     */
    JButton buttonEdit;

    /**
     * JButton to aid in the removal of the contact
     */
    JButton buttonRemove;

    /**
     * JButton to aid in the listing of a contact's agenda
     */
    JButton buttonSeeAgenda;

    /**
     * JButton to associate a tago to selected contact
     */
    JButton buttonAssociateTag;

    private DefaultListModel<Contact> modelContactList = new DefaultListModel();
    private List<Contact> contactList;
    private ProfessionsList professionsList;

    /**
     * Constructor SeeContactList
     *
     * @param contactController
     */
    public SeeContactList(ContactController contactController, ProfessionsList professionsList) {
        this.contactController = contactController;
        this.professionsList = professionsList;
        setLayout(new BorderLayout());
        add(panelContacts(), BorderLayout.NORTH);
        add(buttonEditContact(), BorderLayout.SOUTH);
        add(panelTwoButtonsPanel(), BorderLayout.CENTER);
        add(buttonAssociateTag(), BorderLayout.EAST);
        setLocationRelativeTo(null);
        setResizable(false);
        setModal(true);
        pack();
        setVisible(true);
    }

    public JPanel panelContacts() {
        JPanel p = new JPanel();
        showContacts();
        listContacts = new JList(modelContactList);
        listContacts.setCellRenderer(new ClientCellRenderer());
        JScrollPane pane = new JScrollPane(listContacts);
        p.add(pane);
        return p;
    }

    public void showContacts() {
        contactList = contactController.getContacts();
        if (contactList.size() > 0) {
            for (Contact c : contactList) {
                System.out.println(c.Id() + " - " + c.Firstname() + "  " + c.Lastname() + "\n");
                modelContactList.addElement(c);
            }
            JOptionPane.showMessageDialog(this, "Contacts fully loaded with success!");
        } else {
            JOptionPane.showMessageDialog(this, "Contacts not found", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No contacts found.");
        }
    }

    /**
     * JButton to aid the edit of a contact
     *
     * @return JButton to aid the edit of a contact
     */
    public JButton buttonEditContact() {
        buttonEdit = new JButton("Edit Contact");
        buttonEdit.setEnabled(true);
        buttonEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    professionsList = ProfessionsReader.getProfessionsListFromFile(professionsList);
                    Contact contact = (Contact) listContacts.getSelectedValue();
                    EditContact editContact = new EditContact(contactController, contact, professionsList);
                    dispose();
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "No Contact Selected");
                } catch (SAXException | ParserConfigurationException | IOException ex) {
                    Logger.getLogger(SeeContactList.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        );
        return buttonEdit;
    }

    public JPanel panelTwoButtonsPanel() {
        JPanel p = new JPanel();
        p.add(buttonRemoveContact(), BorderLayout.WEST);
        p.add(buttonSeeEvents(), BorderLayout.EAST);
        return p;
    }

    /**
     * JButton to aid the removal of a contact
     *
     * @return
     */
    public JButton buttonRemoveContact() {
        buttonRemove = new JButton("Remove Contact");
        buttonRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    Contact contact = (Contact) listContacts.getSelectedValue();
                    if (contact instanceof Company) {
                        Company company = (Company) contact;
                        contactController.removeContact(company);
                    } else if (contact instanceof Person) {
                        Person person = (Person) contact;
                        contactController.removeContact(person);
                    }

                    JOptionPane.showMessageDialog(null, "Contact removed with sucess");
                    dispose();
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "No Contact Was Selected");
                }
            }
        }
        );
        return buttonRemove;
    }

    public JButton buttonSeeEvents() {
        buttonSeeAgenda = new JButton("See Agenda");
        buttonSeeAgenda.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Contact contact = (Contact) listContacts.getSelectedValue();
                if (contact instanceof Person) {
                    SeePersonEventsList seeEventsList = new SeePersonEventsList(contact);
                } else if (contact instanceof Company) {
                    Company company = (Company) contact;
                    SeeCompanyAgenda seeCompanyAgenda = new SeeCompanyAgenda(company);
                }

            }

        });
        return buttonSeeAgenda;
    }

    public JButton buttonAssociateTag() {
        this.buttonAssociateTag = new JButton("Tag");
        this.buttonAssociateTag.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Contact contact = (Contact) listContacts.getSelectedValue();
                if (contact != null) {
                    String s = "";
                    s = (String) JOptionPane.showInputDialog(null, "Tag's description:", "Tag", JOptionPane.QUESTION_MESSAGE);

                    if (!s.isEmpty() || s != null || s.equals("null")) {

                        Tag t = contactController.verifyTag(s);

                        if (t != null) {

                            if (contactController.containsContact(s, contact) == false) {
                                System.out.println("entrou");
                                contactController.associateTag(t, contact);
                                contactController.updateTag(t);
                                JOptionPane.showMessageDialog(null, "Tag " + s + " added too contact " + contact.Firstname() + ".", "Tag existent sucess!", JOptionPane.PLAIN_MESSAGE);
                            } else {
                                JOptionPane.showMessageDialog(null, "Contact already associated to the tag.", "Tag exception", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            boolean b;
                            b = contactController.newTag(s, contact);
                            if (b == false) {
                                JOptionPane.showMessageDialog(null, "The tag name is not valid.", "Tag exception", JOptionPane.ERROR_MESSAGE);
                            } else {
                                JOptionPane.showMessageDialog(null, "Tag " + s + " added too contact " + contact.Firstname() + ".", "Tag create sucess!", JOptionPane.PLAIN_MESSAGE);
                            }
                        }
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "Select a contact first.", "Contact exception", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        return buttonAssociateTag;
    }

}
