/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui;

import csheets.ext.contacts.Contact;

import csheets.ext.contacts.ContactController;
import csheets.ext.contacts.MailNumber.Company;
import csheets.ext.contacts.MailNumber.Profession;
import csheets.ext.contacts.MailNumber.ProfessionsList;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.xml.sax.SAXException;

/**
 *
 * @author Guilherme
 */
    public final class AddContact extends JDialog {
    
    /** Label with firstname*/
    private JLabel labelFirstname;
    
    /** Label with lastname*/
    private JLabel labelLastName;
    
    /** Textfield to introduce the firstname of the contact*/
    private JTextField txFirstname;
    
    /** Textfield to introduce the second of the contact*/
    private JTextField txLastname;
    
    private JTextField txImage;
    
    /** JButton to aid in the loading of the picture*/
    private JButton btLoadPicture;
    
    /** JFileChooser so the image can be chosen from any location in the computer*/
    private JFileChooser jFileChooser;
    
    /** Picture chosen */
    private File chosenFile;
    
    /** ImageIcon to display the picture*/
    private ImageIcon image;
    
    /** JButton to aid in the registry of the contact*/
    private JButton buttonRegister;
    
    private JRadioButton buttonPerson;
    private JRadioButton buttonCompany;
    private JScrollPane companiesScrollPane;
    private JScrollPane professionsScrollPane;
    private JCheckBox connect;
    private List<Company> companies;
    private List<Profession> professions;
    private JButton buttonClose;
    
    private ButtonGroup buttonGroup;
    
    /** The contact controller responsible for the "manipulation" of contacts*/
    private ContactController contactController;
    
     private EventListenerList listenerList = new EventListenerList();

    
    private JList listCompanies;
    private JList listProfessions;
    private ProfessionsList professionsList;
    DefaultListModel<Contact> modelCompaniesList;
    DefaultListModel<String> modelProfessionsList;
    private ButtonEvent event;
   
    /**
     * Constructor AddContact
     * @param contactController 
     */
    public AddContact(ContactController contactController,ProfessionsList professionsList) throws SAXException{
        this.contactController=contactController;
        this.professionsList=professionsList;
        
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        add(panelFirstName(),c);
        c.gridy = 1;
        add(panelLastName(),c);
        c.gridy = 2;
        add(picture(),c);
        c.gridy=3;
        event = new ButtonEvent();
        buttonGroup = new ButtonGroup();
        buttonPerson=new JRadioButton("Person");
        buttonPerson.setActionCommand("Person");
        buttonPerson.addActionListener(event);
        buttonCompany=new JRadioButton("Company");
        buttonCompany.setActionCommand("Company");
        buttonCompany.addActionListener(event);
        buttonGroup.add(buttonPerson);
        buttonGroup.add(buttonCompany);
        add(buttonPerson);
        add(buttonCompany);
        add(panelCompanies(),c);
        c.gridy=4;
        add(panelProfessions(),c);
        c.gridy=5;
        add(panelRegisterClose(),c);
        setLocationRelativeTo(null);
        setResizable(false);
        setModal(true);
        pack();
        setVisible(true);  
    }
    
    /**
     * JPanel for the editing of the first name
     * @return JPanel for the editing first name
     */
    public JPanel panelFirstName() {
        JPanel n = new JPanel(new BorderLayout());
        labelFirstname = new JLabel("Firstname:", JLabel.LEFT);
        txFirstname = new JTextField();
        txFirstname.setColumns(30);
        n.add(labelFirstname, BorderLayout.NORTH);
        n.add(txFirstname, BorderLayout.CENTER);
        n.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        return n;
    }
    
    /**
     * JPanel for the editing of the last name
     * @return JPanel for the editing last name
     */
    public JPanel panelLastName() {
        JPanel n = new JPanel(new BorderLayout());
        labelLastName = new JLabel("Lastname:", JLabel.LEFT);
        txLastname = new JTextField();
        txLastname.setColumns(30);
        n.add(labelLastName, BorderLayout.NORTH);
        n.add(txLastname, BorderLayout.CENTER);
        n.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        return n;
    }
   
     /**
      * JPanel to display the picture
      * @return JPanel to display the picture
      */
     public JPanel picture() {
        JPanel s = new JPanel();
        txImage=new JTextField();
        txImage.setColumns(30);
        s.add(txImage);
        s.add(createButtonLoadPicture(), BorderLayout.EAST);
        s.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
        return s;
    }
     
     /**
      * JButton to aid in the loading of the picture
      * @return JButton to aid in the loading of the picture
      */
     public JButton createButtonLoadPicture() {
        btLoadPicture = new JButton("Load Picture");
        btLoadPicture.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                FileNameExtensionFilter filter = new FileNameExtensionFilter("jpg", "jpg");
                jFileChooser = new JFileChooser();
                jFileChooser.setFileFilter(filter);
                int returnValue = jFileChooser.showOpenDialog(null);
                if (returnValue == jFileChooser.APPROVE_OPTION) {
                    chosenFile = jFileChooser.getSelectedFile();
                    txImage.setText(chosenFile.getAbsolutePath());
                } else {
                    JOptionPane.showMessageDialog(null, "Access to file was cancelled by the user!");
                }
            }
        });

        return btLoadPicture;
     }
     
     
    
     public Company getCompanySelected(String name){
            for(Company com : companies){
                if(name.equals(com.Firstname())){
                    return com;
                }
            }
            return null;
     }
     
     public JPanel panelCompanies(){
         JPanel p= new JPanel();
         connect = new JCheckBox("Works for :", true);
         connect.setEnabled(false);
         connect.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(!connect.isSelected()){
                    listCompanies.clearSelection();
                    listCompanies.setEnabled(false);
                    } else {
                    listCompanies.clearSelection();
                    listCompanies.repaint();
                    listCompanies.setEnabled(true);
                }
            }
        });
        p.add(connect);
        modelCompaniesList = new DefaultListModel();
        showCompanies();
        listCompanies = new JList(modelCompaniesList);
        listCompanies.setFixedCellWidth(200);
        listCompanies.setVisibleRowCount(3);
        listCompanies.setEnabled(false);
        companiesScrollPane = new JScrollPane(listCompanies);
        p.add(companiesScrollPane);
        return p;
     }
     
     private void showCompanies() {
        companies = contactController.getCompanies();
        if (companies.size() > 0) {
            for (Company comp : companies) {
                modelCompaniesList.addElement(comp);
            }
            JOptionPane.showMessageDialog(this, "Companies fully loaded with success!");
        } else {
            JOptionPane.showMessageDialog(this, "Companies not found", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No companies found.");
        }
    }
     
     public JPanel panelProfessions(){
        JPanel p=new JPanel(); 
        professions = contactController.getProfessions();
        JLabel labelProfession=new JLabel("Profession: ",JLabel.LEFT);
        modelProfessionsList = new DefaultListModel<String>();
        for(int i=0;i<professionsList.size();i++){
            modelProfessionsList.addElement(professionsList.getProfessionFromList(i).toString());  
        }
        p.add(labelProfession);
        listProfessions = new JList(modelProfessionsList);
        listProfessions.setFixedCellWidth(200);
        listProfessions.setVisibleRowCount(3);
        listProfessions.setEnabled(false);
        professionsScrollPane = new JScrollPane(listProfessions);
        p.add(professionsScrollPane);
        return p;
     }
     
     public Profession getProfessionSelected(String name){
         for(Profession profession : professions){
             if(name.equals(profession.Name())){
                 return profession;
             }
         }
         return null;
     }
     
     /**
      * JPanel that will have the register button
      * @return JPanel that will have the register button
      */
     public JPanel panelRegisterClose() {
        JPanel p = new JPanel(new BorderLayout());
        JPanel subPanel = new JPanel();
        subPanel.add(createButtonRegister());
        subPanel.add(createButtonClose());
        p.add(subPanel,BorderLayout.EAST);
        p.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));
        return p;
    }
     
     /**
      * JButton that will register the contact
      * @return JButton that will register the contact
      */
     public JButton createButtonRegister() {
        buttonRegister = new JButton("Register Contact");
        buttonRegister.setEnabled(true);
        buttonRegister.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try{
                    if (buttonCompany.isSelected()) {
                        contactController.registerCompany(txFirstname.getText(),txImage.getText());
                        JOptionPane.showMessageDialog(null, "Company contact created with success!");
                    } else if(buttonPerson.isSelected() && connect.isSelected() && listCompanies.getSelectedIndex()>=0 && listProfessions.getSelectedIndex()>=0) {
                        contactController.registerPerson(txFirstname.getText(), txLastname.getText(), txImage.getText(),getCompanySelected(listCompanies.getSelectedValue().toString()),getProfessionSelected(listProfessions.getSelectedValue().toString()));
                        JOptionPane.showMessageDialog(null, "Personnal contact created with success!");
                    }else if(buttonPerson.isSelected() && connect.isSelected() && listCompanies.getSelectedIndex()==-1 && listProfessions.getSelectedIndex()>=0) {
                        contactController.registerPersonNoCompany(txFirstname.getText(), txLastname.getText(), txImage.getText(),getProfessionSelected(listProfessions.getSelectedValue().toString()));
                        JOptionPane.showMessageDialog(null, "Personnal contact created with success!");
                    }
                    dispose();
                }catch(NullPointerException ex){
                    JOptionPane.showMessageDialog(null, "Agenda generated automatically");
                    dispose();
                }
            }
        }
        );
        return buttonRegister;
    }
     

     public JButton createButtonClose() {
        buttonClose = new JButton("Close");
        buttonClose.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        return buttonClose;
    }
     
     private class ButtonEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (buttonCompany.isSelected()) {
                connect.setSelected(false);
                connect.setEnabled(false);
                txLastname.setText("(Empresa)");
                txLastname.setEditable(false);
                listCompanies.clearSelection();
                listCompanies.setEnabled(false);
                listProfessions.setEnabled(false);
                companiesScrollPane.setEnabled(false);
            } else if (buttonPerson.isSelected()) {
                connect.setSelected(false);
                txLastname.setText("");
                txLastname.setEditable(true);
                connect.setEnabled(true);
                listProfessions.setEnabled(true);
            }
        }
    }

}
