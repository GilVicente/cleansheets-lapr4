/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui;

import csheets.ext.contacts.MailNumber.Company;
import csheets.ext.contacts.MailNumber.Profession;
import csheets.ext.contacts.MailNumber.ProfessionsList;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.ContactController;
import csheets.ext.contacts.MailNumber.Person;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Guilherme
 */
public class EditContact extends JDialog {
    
    /** The contact controller responsible for the "manipulation" of contacts */
    private ContactController contactController;
    
    /** Label Firstname*/
    private JLabel labelFirstname;
    
    /** TextField to introduce the firstname of the contact*/
    private JTextField txFirstname;
    
    /** Label Lastname*/
    private JLabel labelLastname;
    
    /** TextField to introduce the lastname of the contact*/
    private JTextField txLastname;
    
    private JTextField txImage;
    
    /** JButton to aid in the edit of the contact*/
    private JButton buttonEditContact;
    private JButton btLoadPicture;
    private JButton buttonClose;
    private JFileChooser jFileChooser;
    private File chosenFile;
    private ButtonEvent event;
    private JRadioButton buttonPerson;
    private JRadioButton buttonCompany;
    private ButtonGroup buttonGroup;
    private JCheckBox connect;
    private JScrollPane companiesScrollPane;
    private JScrollPane professionsScrollPane;
    private JList listCompanies;
    private JList listProfessions;
    private List<Company> companies;
    private List<Profession> professions;
    DefaultListModel modelCompaniesList;
    DefaultListModel<String> modelProfessionsList;
    DefaultListModel modelContactList;
    private EventListenerList listenerList = new EventListenerList();
    
    /** Contact to be edited*/
    private Contact contact;
    private List<Contact> contactList;
    private Company company;
    private Person person;
    
    private ProfessionsList professionsList;
    
    /**
     * Constructor EditContact 
     * @param contactController - controller for the manipulation of the contact list
     * @param contact - contact to be edited
     */
    public EditContact(ContactController contactController,Contact contact,ProfessionsList professionsList){
        this.contactController=contactController;
        this.contact=contact;
        this.professionsList=professionsList;
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        add(panelFirstName(contact.Firstname()),c);
        c.gridy = 1;
        add(panelLastName(contact.Lastname()),c);
        c.gridy = 2;
        add(picture(contact.Picture()),c);
        c.gridy=3;
        event = new EditContact.ButtonEvent();
        buttonGroup = new ButtonGroup();
        buttonPerson=new JRadioButton("Person");
        buttonPerson.setActionCommand("Person");
        buttonPerson.addActionListener(event);
        buttonCompany=new JRadioButton("Company");
        buttonCompany.setActionCommand("Company");
        buttonCompany.addActionListener(event);
        buttonGroup.add(buttonPerson);
        buttonGroup.add(buttonCompany);
        add(buttonPerson);
        add(buttonCompany);
        add(panelCompanies(),c);
        c.gridy=4;
        add(panelProfessions(),c);
        c.gridy=5;
        add(panelEditClose(),c);
        setLocationRelativeTo(null);
        setResizable(false);
        setModal(true);
        pack();
        setVisible(true); 
    }
    
    /**
     * JPanel for the display and edit of the contact's firstname
     * @param firstname - contact's firstname
     * @return JPanel for the display and editing of the contact's firstname
     */
    public JPanel panelFirstName(String firstname) {
        JPanel n = new JPanel(new BorderLayout());
        labelFirstname = new JLabel("Firstname:", JLabel.LEFT);
        txFirstname = new JTextField(firstname,40);
        n.add(labelFirstname, BorderLayout.NORTH);
        n.add(txFirstname, BorderLayout.CENTER);
        n.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        return n;
    }
    
    /**
     * JPanel to display and edit the lastname of the contact to be edited
     * @param lastname - contact's lastname
     * @return JPanel to display and edit the lastname of the contact to be edited
     */
    public JPanel panelLastName(String lastname) {
        JPanel n = new JPanel(new BorderLayout());
        labelLastname = new JLabel("Lastname:", JLabel.LEFT);
        txLastname = new JTextField(lastname,40);
        n.add(labelLastname, BorderLayout.NORTH);
        n.add(txLastname, BorderLayout.CENTER);
        n.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        return n;
    }
    
     /**
      * JPanel to display the picture
      * @return JPanel to display the picture
      */
     public JPanel picture(String picture) {
        JPanel s = new JPanel();
        txImage=new JTextField(picture,40);
        txImage.setColumns(30);
        s.add(txImage);
        s.add(createButtonLoadPicture(), BorderLayout.EAST);
        s.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
        return s;
    }
     
     /**
      * JButton to aid in the loading of the picture
      * @return JButton to aid in the loading of the picture
      */
     public JButton createButtonLoadPicture() {
        btLoadPicture = new JButton("Load Picture");
        btLoadPicture.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                FileNameExtensionFilter filter = new FileNameExtensionFilter("jpg", "jpg");
                jFileChooser = new JFileChooser();
                jFileChooser.setFileFilter(filter);
                int returnValue = jFileChooser.showOpenDialog(null);
                if (returnValue == jFileChooser.APPROVE_OPTION) {
                    chosenFile = jFileChooser.getSelectedFile();
                    txImage.setText(chosenFile.getAbsolutePath());
                } else {
                    JOptionPane.showMessageDialog(null, "Access to file was cancelled by the user!");
                }
            }
        });

        return btLoadPicture;
     }
     
      public Company getCompanySelected(String name){
            for(Company com : companies){
                if(name.equals(com.Firstname())){
                    return com;
                }
            }
            return null;
     }
     
     public JPanel panelCompanies(){
         JPanel p= new JPanel();
         connect = new JCheckBox("Works for :", true);
         connect.setEnabled(false);
         connect.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(!connect.isSelected()){
                    listCompanies.clearSelection();
                    listCompanies.setEnabled(false);
                    } else {
                    listCompanies.clearSelection();
                    listCompanies.repaint();
                    listCompanies.setEnabled(true);
                }
            }
        });
        p.add(connect);
        modelCompaniesList = new DefaultListModel();
        showCompanies();
        listCompanies = new JList(modelCompaniesList);
        listCompanies.setFixedCellWidth(200);
        listCompanies.setVisibleRowCount(3);
        listCompanies.setEnabled(false);
        companiesScrollPane = new JScrollPane(listCompanies);
        p.add(companiesScrollPane);
        return p;
     }
     
     private void showCompanies() {
        companies = contactController.getCompanies();
        if (companies.size() > 0) {
            for (Company comp : companies) {
                modelCompaniesList.addElement(comp);
            }
            JOptionPane.showMessageDialog(this, "Companies fully loaded with success!");
        } else {
            JOptionPane.showMessageDialog(this, "Companies not found", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No companies found.");
        }
     }
     
     public JPanel panelProfessions(){
        JPanel p=new JPanel(); 
        professions = contactController.getProfessions();
        JLabel labelProfession=new JLabel("Profession: ",JLabel.LEFT);
        modelProfessionsList = new DefaultListModel<String>();
        for(int i=0;i<professionsList.size();i++){
            modelProfessionsList.addElement(professionsList.getProfessionFromList(i).toString());  
        }
        p.add(labelProfession);
        listProfessions = new JList(modelProfessionsList);
        listProfessions.setFixedCellWidth(200);
        listProfessions.setVisibleRowCount(3);
        listProfessions.setEnabled(false);
        professionsScrollPane = new JScrollPane(listProfessions);
        p.add(professionsScrollPane);
        return p;
     }
     
     public Profession getProfessionSelected(String name){
         for(Profession profession : professions){
             if(name.equals(profession.Name())){
                 return profession;
             }
         }
         return null;
     }
    
     /**
      * JPanel that will have the register button
      * @return JPanel that will have the register button
      */
     public JPanel panelEditClose() {
        JPanel p = new JPanel(new BorderLayout());
        JPanel subPanel = new JPanel();
        subPanel.add(createEditButton());
        subPanel.add(createCloseButton());
        p.add(subPanel,BorderLayout.EAST);
        p.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));
        return p;
    }
     
    /**
     * JPanel for the close button
     * @return JPanel for the close button
     */ 
    public JButton createCloseButton() {
        buttonClose = new JButton("Close");
        buttonClose.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        return buttonClose;
    }
    
    /**
     * JButton for the create edit button
     * @return JButton for the create edit button
     */
    public JButton createEditButton() {
        buttonEditContact = new JButton("Confirm Edit of Contact");
        buttonEditContact.setEnabled(true);
        buttonEditContact.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try{
                    if(contact instanceof Company){
                         company = (Company) contact;
                    }
                    else{
                         person = (Person) contact;
                    }
                    if (buttonCompany.isSelected()) {
                        contactController.editCompany(company,txFirstname.getText(),txImage.getText());
                    } else if(buttonPerson.isSelected() && connect.isSelected() && listCompanies.getSelectedIndex()>=0 && listProfessions.getSelectedIndex()>=0) {
                        contactController.editPerson(person,txFirstname.getText(), txLastname.getText(), txImage.getText(),getCompanySelected(listCompanies.getSelectedValue().toString()),getProfessionSelected(listProfessions.getSelectedValue().toString()));
                    } else if(buttonPerson.isSelected() && connect.isSelected() && listCompanies.getSelectedIndex()>=-1 && listProfessions.getSelectedIndex()>=0) {
                        contactController.editPersonNoCompany(person,txFirstname.getText(), txLastname.getText(), txImage.getText(),getProfessionSelected(listProfessions.getSelectedValue().toString()));
                    }
                    JOptionPane.showMessageDialog(null, "Contact edited with success");  
                    dispose();
                    
                }catch(NullPointerException ex){
                    JOptionPane.showMessageDialog(null, "Agenda was kept the same for the edited contact");
                    dispose();
                }
            }
        }
        );
        return buttonEditContact;
    }
    
    private class ButtonEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (buttonCompany.isSelected()) {
                connect.setSelected(false);
                connect.setEnabled(false);
                txLastname.setText("(Empresa)");
                txLastname.setEditable(false);
                listCompanies.clearSelection();
                listCompanies.setEnabled(false);
                listProfessions.setEnabled(false);
                companiesScrollPane.setEnabled(false);
            } else if (buttonPerson.isSelected()) {
                connect.setSelected(false);
                txLastname.setText("");
                txLastname.setEditable(true);
                connect.setEnabled(true);
                listProfessions.setEnabled(true);
            }
        }
    }
}
