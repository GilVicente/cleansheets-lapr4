/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui;


import csheets.ext.contacts.Contact;
import csheets.ext.contacts.Event;
import csheets.ext.contacts.EventController;
import csheets.ext.contacts.MailNumber.Person;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

/**
 *
 * @author Guilherme
 */
public class SeeCompanyAgenda extends JDialog{
    
    private EventController eventController;
    private Contact contact;
    private List<Person> companyEventList;
    DefaultListModel<Event> modelEventList = new DefaultListModel();
    DefaultListModel<Event> modelEventsToday = new DefaultListModel();
    private JList listEventsToday;
    private JOptionPane pane;
    private JList listEvents;
    
    public SeeCompanyAgenda(Contact contact){
        eventController=new EventController();
        this.contact=contact;
        setLayout(new BorderLayout());
        showCompanyEvents(this.contact);
        popUpPanel(this.contact);
        listEvents = new JList(modelEventList);
        listEvents.setCellRenderer(new EventsCellRenderer());
        JScrollPane pane = new JScrollPane(listEvents);
        add(pane, BorderLayout.NORTH);
        setLocationRelativeTo(null);
        setResizable(false);
        setModal(true);
        pack();
        setVisible(true); 
    }
    
    public void popUpPanel(Contact contact) {

        JPanel p = new JPanel();
        showEventsToday(contact);
        listEventsToday = new JList(modelEventsToday);
        p.add(listEventsToday);
        JOptionPane pane = new JOptionPane(p);

        final JDialog dialog = pane.createDialog("TODAY'S EVENTS");
        Timer timer = new Timer(5000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dialog.dispose();
            }
        });
        timer.setRepeats(false);
        timer.start();
        dialog.setModal(true);
        dialog.setVisible(true);
    }
    
    public void showCompanyEvents(Contact contact){
        companyEventList = eventController.getCompanyAgenda(contact);
        if (companyEventList.size() > 0) {
            for (Person a : companyEventList) {
                for(int i=0;i<a.agendaList().size();i++){
                modelEventList.addElement(a.agendaList().getEventFromList(i));
                }
                
            }
            JOptionPane.showMessageDialog(this, "Company's agenda fully loaded with success!");
        } else {
            JOptionPane.showMessageDialog(this, "Company does not have not events", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No events found for the company selected.");
        }
    }
    
    public void showEventsToday(Contact contact){
        companyEventList = eventController.getCompanyAgenda(contact);
        if (companyEventList.size() > 0) {
            for (Person a : companyEventList) {
                for(int i=0;i<a.agendaList().size();i++){
                   Calendar todaysDate = Calendar.getInstance();
                Calendar eventDate = a.agendaList().getEventFromList(i).DueDate();
                
                if(TimeIgnoringCompares(eventDate, todaysDate)){
                  modelEventsToday.addElement(a.agendaList().getEventFromList(i));  
                } 
                }
        }
    }
   }
    
    public boolean TimeIgnoringCompares(Calendar c1,Calendar c2){
        if(c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)){
            return true;
        }
            return false;
    }
    
}
