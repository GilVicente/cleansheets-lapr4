/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.Event;
import csheets.ext.contacts.EventController;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

/**
 *
 * @author Guilherme
 */
public class SeePersonEventsList extends JDialog {
    
     /** The contact controller responsible for the "manipulation" of contacts */
    private EventController eventController;
    private Contact contact;

    /** JList to display the list of contacts*/
    private JList listEvents;
    
    /** JButton to aid in the edit of the contact*/
    private JButton buttonEdit;
    
    /** JButton to aid in the removal of the contact*/
    private JButton buttonRemove;
    
    /** JButton to aid in the listing of a contact's agenda*/
    private JButton buttonAdd;
    
    private List<Event> contactsEventList;
    DefaultListModel<Event> modelEventList = new DefaultListModel();
    DefaultListModel<Event> modelEventsToday = new DefaultListModel();
    private JList listEventsToday;
    private JOptionPane pane;
    public SeePersonEventsList(Contact contact){
        eventController=new EventController();
        this.contact=contact;
        setLayout(new BorderLayout());
        showContactsEvents(this.contact);
        popUpPanel(this.contact);
        listEvents = new JList(modelEventList);
        listEvents.setCellRenderer(new EventsCellRenderer());
        JScrollPane pane = new JScrollPane(listEvents);
        add(pane, BorderLayout.NORTH);
        add(panelEditRemoveEvent(),BorderLayout.CENTER);
        add(buttonAddEvent(),BorderLayout.SOUTH);
        setLocationRelativeTo(null);
        setResizable(false);
        setModal(true);
        pack();
        setVisible(true); 
    }
    
    public void popUpPanel(Contact contact) {

        JPanel p = new JPanel();
        showEventsToday(contact);
        listEventsToday = new JList(modelEventsToday);
        p.add(listEventsToday);
        JOptionPane pane = new JOptionPane(p);

        final JDialog dialog = pane.createDialog("TODAY'S EVENTS");
        Timer timer = new Timer(5000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dialog.dispose();
            }
        });
        timer.setRepeats(false);
        timer.start();
        dialog.setModal(true);
        dialog.setVisible(true);
    }
    
    public void showContactsEvents(Contact contact){
        contactsEventList = eventController.getContactAgenda(contact);
        if (contactsEventList.size() > 0) {
            for (Event e : contactsEventList) {
                System.out.println(e.toString()+ "\n");
                modelEventList.addElement(e);
            }
            JOptionPane.showMessageDialog(this, "Contact's agenda fully loaded with success!");
        } else {
            JOptionPane.showMessageDialog(this, "Contact does not have not events", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No events found for the contact selected.");
        }
    }
    
    public void showEventsToday(Contact contact){
        contactsEventList = eventController.getContactAgenda(contact);
        if (contactsEventList.size() > 0) {
            for (Event e : contactsEventList) {
                Calendar todaysDate = Calendar.getInstance();
                Calendar eventDate = e.DueDate();
                
                if(TimeIgnoringCompares(eventDate, todaysDate)){
                  modelEventsToday.addElement(e);  
                }
                
            }
    }
   }
    public JPanel panelEditRemoveEvent(){
        JPanel p = new JPanel();
        p.add(buttonEditEvent(),BorderLayout.WEST);
        p.add(buttonRemoveEvent(),BorderLayout.EAST);
        return p;
    }
    
    /**
     * JButton to aid the edit of a contact
     * @return JButton to aid the edit of a contact
     */
    public JButton buttonEditEvent(){
        buttonEdit = new JButton("Edit Event");
        buttonEdit.setEnabled(true);
        buttonEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try{
                      Event event=(Event) listEvents.getSelectedValue();
                      EditEvent editEvent=new EditEvent(eventController,contact,event);
                    dispose();
                }catch(NullPointerException ex){
                    JOptionPane.showMessageDialog(null, "Invalid Data");
                } 
            }
        }
        );
        return buttonEdit;
    }
    
    /**
     * JButton to aid the removal of a contact
     * @return 
     */
    public JButton buttonRemoveEvent(){
        buttonRemove = new JButton("Remove Event");
        buttonRemove.setSize(20,20);
        buttonRemove.setEnabled(true);
        buttonRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try{
                    Event event=(Event) listEvents.getSelectedValue();
                    eventController.removeEvent(event,contact);
                    dispose();
                }catch(NullPointerException ex){
                    JOptionPane.showMessageDialog(null, "No contact was selected");
                } 
            }
        }
        );
        return buttonRemove;
    }
    
    /**
     * JButton to aid the removal of a contact
     * @return 
     */
    public JButton buttonAddEvent(){
        buttonAdd = new JButton("Add Event");
        buttonAdd.setSize(20,20);
        buttonAdd.setEnabled(true);
        buttonAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try{
                    AddEvent addEvent = new AddEvent(eventController,contact);
                    dispose();
                }catch(NullPointerException ex){
                    JOptionPane.showMessageDialog(null, "No contact was selected");
                } 
            }
        }
        );
        return buttonAdd;
    }
    
    public boolean TimeIgnoringCompares(Calendar c1,Calendar c2){
        if(c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) && c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH)){
            return true;
        }
            return false;
    }
    
  
}
