/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.ui;


import csheets.ext.contacts.Contact;
import csheets.ext.contacts.EventController;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Guilherme
 */
public class AddEvent extends JDialog {
    
    private EventController eventController;
    private Contact contact;
    private JButton buttonClose;
    private JButton buttonAdd;
    private JLabel labelDescricao;
    private JTextField txDescription;
    private JComboBox listDays;
    private JComboBox listMonths;
    private JComboBox listYears;
    private String[] months = {"January", "February", "March", "April", "May" , "June", "July", "August", "September", "October", "November", "December"};
    private String[] monthsEnumerated = {"0","1","2","3","4","5","6","7","8","9","10","11"};
    private String[] days = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    private String[] years = {"2015", "2016", "2017", "2018", "2019", "2020", "2021"};
    
    public AddEvent(EventController eventController,Contact contact){
        this.contact=contact;
        this.eventController=eventController;
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        add(panelDescricaoName(),c);
        c.gridy = 1;
        add(panelCalendar(),c);
        c.gridy=2;
        add(panelAddCloseButton());
        setLocationRelativeTo(null);
        setResizable(false);
        setModal(true);
        pack();
        setVisible(true);  
    }
    
    /**
     * JPanel for the editing of the first name
     * @return JPanel for the editing first name
     */
    public JPanel panelDescricaoName() {
        JPanel n = new JPanel(new BorderLayout());
        labelDescricao = new JLabel("Description:", JLabel.LEFT);
        txDescription = new JTextField(40);
        n.add(labelDescricao, BorderLayout.NORTH);
        n.add(txDescription, BorderLayout.CENTER);
        n.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        return n;
    }
    
    /**
     * JPanel for the editing of the last name
     * @return JPanel for the editing last name
     */
    public JPanel panelCalendar() {
        JPanel p = new JPanel(new BorderLayout());
        JLabel lbData = new JLabel("Event Date :", JLabel.LEFT);

        listDays = new JComboBox(new DefaultComboBoxModel(days));
        listMonths = new JComboBox(new DefaultComboBoxModel(months));
        listYears = new JComboBox(new DefaultComboBoxModel(years));

        p.add(lbData, BorderLayout.NORTH);
        p.add(listDays, BorderLayout.WEST);
        p.add(listMonths, BorderLayout.CENTER);
        p.add(listYears, BorderLayout.EAST);
        p.setBorder(BorderFactory.createEmptyBorder(20, 10, 0, 20));
        return p;
    }
    
    /**
     * JPanel for the close button
     * @return JPanel for the close button
     */
     public JPanel panelAddCloseButton() {
        JPanel p = new JPanel(new BorderLayout());
        JPanel subPanel = new JPanel();
        subPanel.add(buttonAdd());
        subPanel.add(buttonClose());
        p.add(subPanel, BorderLayout.CENTER);
        p.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 20));
        return p;
    }
     
    public JButton buttonClose(){
        buttonClose = new JButton("Close");
        buttonClose.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        return buttonClose;
    }
    
    public JButton buttonAdd(){
        buttonAdd = new JButton("Add");
        buttonAdd.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try{
                    Calendar c1 = GregorianCalendar.getInstance();
                    c1.set(Integer.parseInt(years[listYears.getSelectedIndex()]),Integer.parseInt(monthsEnumerated[listMonths.getSelectedIndex()]),Integer.parseInt(days[listDays.getSelectedIndex()]));

                    if(timeValidation(c1.getTime())==true){
                       JOptionPane.showMessageDialog(null, "Invalid date was introduced"); 
                    }
                    else{
                       eventController.registerEvent(txDescription.getText(), Integer.parseInt(years[listYears.getSelectedIndex()]), Integer.parseInt(monthsEnumerated[listMonths.getSelectedIndex()]), Integer.parseInt(days[listDays.getSelectedIndex()]),contact);
                       dispose(); 
                    }
                    }catch(NullPointerException ex){
                    JOptionPane.showMessageDialog(null, "Invalid data was introduced");
                }
             }
        });
        return buttonAdd;
    }
    
    public boolean timeValidation(Date c1){
        if(c1.compareTo(Calendar.getInstance().getTime())<0){
            return true;
        }
            return false;
    }
}
