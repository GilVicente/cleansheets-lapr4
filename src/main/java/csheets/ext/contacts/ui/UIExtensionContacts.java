package csheets.ext.contacts.ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import csheets.ext.contacts.ContactController;
import csheets.ext.Extension;
import csheets.ext.contacts.ContactsExtension;
import csheets.ext.contacts.MailNumber.ProfessionsList;
import csheets.ext.contacts.MailNumber.ProfessionsReader;

import csheets.ext.simple.ui.ExampleMenu;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.CellDecorator;
import csheets.ui.ext.TableDecorator;
import csheets.ui.ext.UIExtension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 *
 * @author Guilherme
 */
public class UIExtensionContacts extends UIExtension {

    /**
     * The icon to display with the extension's name
     */
    private Icon icon;

    /**
     * The menu of the extension
     */
    private ContactMenu menu;

    /**
     * A panel in which the contacts are displayed
     */
    private JComponent sideBar;

    /**
     * A list in which the contacts are displayed
     */
    JList listContacts;

    /**
     * A button to help in the selection of the adding of contact
     */
    JButton btAdd;

    /**
     * The contact controller responsible for the "manipulation" of contacts
     */
    private ContactController contactController = new ContactController();

    private ProfessionsList professionsList;

    /**
     * DefaultListModel for list presentation
     */
    DefaultListModel model;

    /**
     * Button to aid in the creation of contact
     */
    JButton createButton;

    /**
     * Button to display the listing of all contacts
     */
    JButton seeContactList;

    /**
     * List of all contacts
     */
    JList lstContacts;

    /**
     * Textfield to introduce the firstname of the contact
     */
    JTextField txFirstname;

    /**
     * Textfield to introduce the lastname of the contact
     */
    JTextField txLastname;

    /**
     * Creates a new user interface extension.
     *
     * @param extension the extension for which components are provided
     * @param uiController the user interface controller
     */
    public UIExtensionContacts(Extension extension, UIController uiController) {
        super(extension, uiController);
        professionsList = new ProfessionsList();
    }

    /**
     * Returns an icon to display with the extension's name.
     *
     * @return an icon with style
     */
    public Icon getIcon() {
        return null;
    }

    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @see ExampleMenu
     * @return a JMenu component
     */
    public JMenu getMenu() {
        return null;
    }

    /**
     * Returns a cell decorator that visualizes the data added by the extension.
     *
     * @return a cell decorator, or null if the extension does not provide one
     */
    public CellDecorator getCellDecorator() {
        return null;
    }

    /**
     * Returns a table decorator that visualizes the data added by the
     * extension.
     *
     * @return a table decorator, or null if the extension does not provide one
     */
    public TableDecorator getTableDecorator() {
        return null;
    }

    /**
     * Returns a toolbar that gives access to extension-specific functionality.
     *
     * @return a JToolBar component, or null if the extension does not provide
     * one
     */
    public JToolBar getToolBar() {
        return null;
    }

    /**
     * Returns a side bar that gives access to extension-specific functionality.
     *
     * @return a component, or null if the extension does not provide one
     */
    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new ContactPanel(uiController);
            sideBar.setName(ContactsExtension.NAME);
            JPanel p = new JPanel();
            p.add(seeContactList());
            p.add(btCreateContact());
            p.add(btSeeTags());
            sideBar.add(p);

        }

        return sideBar;
    }

    /**
     * Returns a JButton that will allow the user to list all the contacts
     *
     * @return a JButton
     */
    public JButton seeContactList() {
        seeContactList = new JButton("See Contact List");
        seeContactList.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    professionsList = ProfessionsReader.getProfessionsListFromFile(professionsList);

                    SeeContactList seeContactList = new SeeContactList(contactController, professionsList);

                } catch (NullPointerException e) {
                    JOptionPane.showMessageDialog(null, "There are no contacts registered!!!");
                } catch (SAXException | ParserConfigurationException | IOException ex) {
                    Logger.getLogger(UIExtensionContacts.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        seeContactList.setVisible(true);
        return seeContactList;
    }

    /**
     * Returns a JButton that will allow the user to create a contact
     *
     * @return a JButton
     */
    public JButton btCreateContact() {
        createButton = new JButton("Create Contact");
        createButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    professionsList = ProfessionsReader.getProfessionsListFromFile(professionsList);

                    AddContact addContactWindow = new AddContact(contactController, professionsList);

                } catch (SAXException | ParserConfigurationException | IOException | NullPointerException ex) {
                    Logger.getLogger(UIExtensionContacts.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "You must especify a name and lastname for contact!!!");
                }
            }
        });
        createButton.setVisible(true);
        return createButton;
    }

    /**
     * Returns a JButton that will allow the user to create a contact
     *
     * @return a JButton
     */
    public JButton btSeeTags() {
        createButton = new JButton("Search Tags");
        createButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new SearchTagsUI(null, contactController, professionsList);
            }
        });
        createButton.setVisible(true);
        return createButton;
    }

}
