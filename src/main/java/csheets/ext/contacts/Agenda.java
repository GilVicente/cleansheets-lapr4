/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


/**
 *
 * @author Guilherme
 */
@Entity
public class Agenda implements Serializable {
        
        /** Agenda's id*/
        @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
        
        /** Agenda's event list*/
        @OneToMany(cascade = CascadeType.MERGE)
        private List<Event> agendaList;

    /**
    * CRM Purposes Contact
    */
    public Agenda() {
         this.agendaList=new ArrayList();
    }   
    
        /**
     * 
     * returns the event that is stored in the index position of the agenda
     * @param index - index
     * @return the event that is stored index position of the agenda
     */
    public Event getEventFromList(int index) {
        return agendaList.get(index);
    }
    
    /**
     * get Contact BY Username
     *
     * @param ID - Contact ID
     * @return contact's ID
     */
    public Event getEventByID(long ID) {
        for (Event e : this.agendaList) {
            if (e.Id()==(ID)) {
                return e;
            }
        }
        return null;
    }
    
    /**
     * Returns the event's timestamp from the event stored in the index position of the agenda
     * @param index - index
     * @return the index event's timestamp
     */
    public Calendar getEventTimestamp(int index) {
        return agendaList.get(index).DueDate();
    }

    /**
     * Adds the event to the agenda
     * @param agenda - agenda
     * @return true if the event is added to the list , false if not
     */
    public boolean addEventToList(Event agenda) {
        if (!this.agendaList.contains(agenda)) {
            return this.agendaList.add(agenda);
        }
        return false;
    }

    /**
     * Removes the event from the agenda
     * @param agenda - agenda
     * @return true if event is removed from agenda
     */
    public boolean removeEventFromList(Event agenda) {
        return this.agendaList.remove(agenda);
    }

    /**
     * Return the size of the agenda
     * @return size of the agenda
     */
    public int size() {
        if (!agendaList.isEmpty()) {
            return this.agendaList.size();
        } else {
            return 0;
        }
    }

    /**
     * Return an event's index
     * @param agenda - agenda
     * @return index of an event in the agenda
     */
    public int indexOf(Event agenda) {
        return this.agendaList.indexOf(agenda);
    }

    /**
     * Sees if the agenda contains an event
     * @param agenda - agenda
     * @return true if the agenda contains the event
     */
    public boolean contains(Event agenda) {
        return this.agendaList.contains(agenda);
    }

    /**
     * @return the agendaList
     */
    public List<Event> AgendaList() {
        return agendaList;
    }

    /**
     * @return the id
     */
    public long Id() {
        return id;
    }

    /**
     * @param agendaList the agendaList to set
     */
    public void defineAgendaList(List<Event> agendaList) {
        this.agendaList = agendaList;
    }
}
