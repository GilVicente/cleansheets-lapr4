/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.address.ContactAddress;
import csheets.ext.contacts.address.PostalCode;
import csheets.ui.ctrl.UIController;
import java.io.FileNotFoundException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class CreateAddress extends javax.swing.JDialog {

    /**
     * Creates new form CreateAddress
     */
    private Contact contact;
    private ContactAddressController addressController;

    public CreateAddress(ContactAddressController addressController) {
        this.addressController = addressController;

        initComponents();

        //TODO verid«ficar se é necessário
        pack();
        setVisible(true);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("New Address");

        jLabel1.setText("New Address");

        jLabel2.setText("Street:");

        jLabel3.setText("Location:");

        jLabel4.setText("City:");

        jLabel5.setText("Country:");

        jButton1.setText("Add Primary Address");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton3.setText("Cancel");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel6.setText("Postal Code:");

        jLabel7.setText("-");

        jButton2.setText("Add Secundary Address");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField1)
                            .addComponent(jTextField2)
                            .addComponent(jTextField3)
                            .addComponent(jTextField4)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 53, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addGap(34, 34, 34))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton3)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        //TODO verificar se contacto já tem primary address
        if (jTextField1.getText().equals("") || jTextField1.getText().equals("") || jTextField2.
                getText().equals("") || jTextField2.getText().equals("") || jTextField3.
                getText().equals("") || jTextField4.getText().equals("")) {
            JOptionPane.
                    showMessageDialog(this, "You need to fill all fields", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (jTextField4.getText().equalsIgnoreCase("portugal")) {
            try {
                if (!PostalCode.
                        validate(Integer.parseInt(jTextField5.getText()), Integer.
                                parseInt(jTextField6.getText()))) {

                    JOptionPane.
                            showMessageDialog(this, "You you entered a non-valid Zip Code", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    PostalCode zip = new PostalCode(Integer.parseInt(jTextField5.
                            getText()), Integer.parseInt(jTextField6.getText()));

                    addressController.setStreet(jTextField1.getText());
                    addressController.setTown(jTextField2.getText());
                    addressController.setCity(jTextField3.getText());
                    addressController.setCountry(jTextField4.getText());
                    addressController.setPostalCode(zip);

                    addressController.savePrimaryAdress();
                    JOptionPane.
                            showMessageDialog(this, "Primary Address successfully added!", "Success", JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                }
            } catch (FileNotFoundException ex) {
                JOptionPane.
                        showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            PostalCode zip = new PostalCode(Integer.parseInt(jTextField5.
                    getText()), Integer.parseInt(jTextField6.getText()));

            addressController.setStreet(jTextField1.getText());
            addressController.setTown(jTextField2.getText());
            addressController.setCity(jTextField3.getText());
            addressController.setCountry(jTextField4.getText());
            addressController.setPostalCode(zip);

            addressController.savePrimaryAdress();

            JOptionPane.
                    showMessageDialog(this, "Primary Address successfully added!", "Success", JOptionPane.INFORMATION_MESSAGE);
            dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        String[] opcoes = {"Yes", "No"};
        int resposta = JOptionPane.
                showOptionDialog(CreateAddress.this, "Are you sure, you want to cancel?", "Add Address", 0, JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[1]);
        if (resposta == 0) {
            dispose();

        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        //TODO verificar .... secondary address
        if (jTextField1.getText().equals("") || jTextField1.getText().equals("") || jTextField2.
                getText().equals("") || jTextField2.getText().equals("") || jTextField3.
                getText().equals("") || jTextField4.getText().equals("")) {
            JOptionPane.
                    showMessageDialog(this, "You need to fill all fields", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (jTextField4.getText().equalsIgnoreCase("portugal")) {
            try {
                JOptionPane.
                        showMessageDialog(this, "Validating Zip Code...", "Validating", JOptionPane.INFORMATION_MESSAGE);
                if (!PostalCode.
                        validate(Integer.parseInt(jTextField5.getText()), Integer.
                                parseInt(jTextField6.getText()))) {

                    JOptionPane.
                            showMessageDialog(this, "You you entered a non-valid Zip Code", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    PostalCode zip = new PostalCode(Integer.parseInt(jTextField5.
                            getText()), Integer.parseInt(jTextField6.getText()));

                    addressController.setStreet(jTextField1.getText());
                    addressController.setTown(jTextField2.getText());
                    addressController.setCity(jTextField3.getText());
                    addressController.setCountry(jTextField4.getText());
                    addressController.setPostalCode(zip);

                    addressController.saveSecundaryAdress();

                    JOptionPane.
                            showMessageDialog(this, "Secundary Address successfully added!", "Success", JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                }

            } catch (FileNotFoundException ex) {
                JOptionPane.
                        showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            PostalCode zip = new PostalCode(Integer.parseInt(jTextField5.
                    getText()), Integer.parseInt(jTextField6.getText()));

            addressController.setStreet(jTextField1.getText());
            addressController.setTown(jTextField2.getText());
            addressController.setCity(jTextField3.getText());
            addressController.setCountry(jTextField4.getText());
            addressController.setPostalCode(zip);

            addressController.saveSecundaryAdress();

            JOptionPane.
                    showMessageDialog(this, "Secundary Address successfully added!", "Success", JOptionPane.INFORMATION_MESSAGE);
            dispose();
        }
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    // End of variables declaration//GEN-END:variables

}
