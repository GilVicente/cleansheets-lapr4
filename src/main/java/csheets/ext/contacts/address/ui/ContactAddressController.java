/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.PhoneNumber;
import csheets.ext.contacts.address.ContactAddress;
import csheets.ext.contacts.address.PostalCode;
import csheets.ext.contacts.persistence.PersistenceContext;
import csheets.ui.ctrl.UIController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tixa
 */
public class ContactAddressController {

    /**
     * The user interface controller
     */
    private UIController uiController;

    private ContactAddress contactAddress;

    private AddressPanel uiPanel;
    /**
     * List of contacts of the user.
     */
    private List<Contact> contactsList;

    private Contact contacto;

    private String street;
    private String city;
    private String country;
    private String town;
    private PostalCode postalCode;

    public ContactAddressController() {
    }
    
    
    

    public ContactAddressController(UIController uiController, AddressPanel addressPanel) {
        this.uiController = uiController;
        this.uiPanel = addressPanel;
        this.contactsList = PersistenceContext.repositories().contacts().allContacts();
        //createDummyContactList();
        //ContactsJpaRepository rep = new ContactsJpaRepository();
        //rep.allContacts();
    }

    /**
     * Returns the list of Contacts of the User.
     *
     * @return contactsList
     */
    public List<Contact> getContacts() {
        return this.contactsList;
    }

    public void selectContact(Contact contact) {
        this.contacto = contact;
    }

    public boolean validateContact() {
        if (contacto.getPrimaryContactAdress() != null && contacto.getSecundaryContactAdress() != null) {
            return false;
        }
        return true;
    }

    public Contact contact(String cont) {
        List<Contact> listContact = PersistenceContext.repositories().contacts().allContacts();
        for (Contact c : listContact) {
            if (c.Firstname().compareToIgnoreCase(cont) == 0) {
                return c;
            }
        }
        return null;
    }

    public void createDummyContactList() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Sara", "Costa", null));
        contacts.add(new Contact("Ken", "Follet", null));
        contacts.add(new Contact("Lars", "Kepler", null));
        this.contactsList = contacts;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setPostalCode(PostalCode postalCode) {
        this.postalCode = postalCode;
    }

    public void savePrimaryAdress() {
        this.contactAddress = new ContactAddress(street, town, city, country, postalCode);
        contacto.setPrimaryContactAdress(contactAddress);
        PersistenceContext.repositories().contacts().addNewContact(contacto);
    }

    public void editPrimaryAdress() {
        this.contactAddress = new ContactAddress(street, town, city, country, postalCode);
        contacto.setPrimaryContactAdress(contactAddress);
        PersistenceContext.repositories().contacts().addNewContact(contacto);
    }
        
    public void saveSecundaryAdress() {
        this.contactAddress = new ContactAddress(street, town, city, country, postalCode);
        contacto.setSecundaryContactAdress(contactAddress);
        PersistenceContext.repositories().contacts().addNewContact(contacto);
    }
    
    public void editSecundaryAdress(String street, String town, String city, String country, int locationCode, int zoneCode) {
        PostalCode pt=new PostalCode(locationCode, zoneCode);
        this.contactAddress=new ContactAddress(street, country, city, country, pt);
        contacto.setSecundaryContactAdress(contactAddress);
        PersistenceContext.repositories().contactAddress().editUpdateContactAddress(contactAddress);
    }

    public ContactAddress getPrimaryAddress() {
        return contacto.getPrimaryContactAdress();
    }

    public ContactAddress getSecundaryAddress() {
        return contacto.getSecundaryContactAdress();
    }
    
     public void removeAdress() {
        PersistenceContext.repositories().contactAddress().deleteContactAddress(contactAddress);
        PersistenceContext.repositories().contacts().updateEditContact(contacto);
    }
    
    

}
