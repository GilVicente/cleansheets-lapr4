/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.address.ContactAddressExtension;
import csheets.ext.contacts.ui.ClientCellRenderer;
import csheets.ui.ctrl.UIController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author Tixa
 */
public class AddressPanel extends JPanel {

    private JList listContacts;

    private ContactAddressController controller;

    private JPanel addressPanel;

    private List<Contact> contactsList;
    private DefaultListModel<Contact> modelContactList = new DefaultListModel();
    private JTree tree;

    public AddressPanel(UIController uiController) {
        super(new BorderLayout());
        setName(ContactAddressExtension.NAME);

        controller = new ContactAddressController(uiController, this);

        addressPanel = new JPanel();
        addressPanel.setLayout(new BorderLayout());
        addressPanel.setPreferredSize(new Dimension(130, 336));
        addressPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));        // width, height

        addressPanel.add(panelContacts(), BorderLayout.NORTH);
        addressPanel.add(panelButtons(), BorderLayout.SOUTH);

        add(addressPanel);
    }

    public JPanel panelContacts() {
        JPanel p = new JPanel();
        showContacts();
        listContacts = new JList(modelContactList);
        listContacts.setCellRenderer(new ClientCellRenderer());
        JScrollPane pane = new JScrollPane(listContacts);
        p.add(pane);
        return p;
    }

    public void showContacts() {
        contactsList = controller.getContacts();
        if (contactsList.size() > 0) {
            for (Contact c : contactsList) {
                modelContactList.addElement(c);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Contacts not found", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No contacts found.");
        }
    }

    private JPanel panelButtons() {
        JPanel p = new JPanel(new GridLayout(4, 1));
        //p.setLayout(new BorderLayout());
        p.add(createAddress());
        p.add(editPrimaryAddress());
        p.add(editSecundaryAddress());
        p.add(removeAddress());
        return p;

    }

    public JButton createAddress() {
        JButton btnCreateAddress = new JButton("Create Address");
        btnCreateAddress.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    Contact contact = (Contact) listContacts.getSelectedValue();
                    if (contact != null) {
                        controller.selectContact(contact);
                        new CreateAddress(controller);
                    }
                } catch (NullPointerException e) {
                    JOptionPane.showMessageDialog(null, "There are no contacts registered!!!");
                }
            }

        }
        );
        btnCreateAddress.setVisible(
                true);
        return btnCreateAddress;
    }

    public JButton editPrimaryAddress() {
        JButton btnEditAddress = new JButton("Edit Primary Address");
        btnEditAddress.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    Contact contact = (Contact) listContacts.getSelectedValue();
                    if (contact != null) {
                        controller.selectContact(contact);
                        new EditPrimaryAddress(controller);
                    }
                } catch (NullPointerException e) {
                    JOptionPane.showMessageDialog(null, "There are no contacts registered!!!");
                }
            }

        });
        btnEditAddress.setVisible(true);
        return btnEditAddress;
    }

    public JButton editSecundaryAddress() {
        JButton btnEditAddress = new JButton("Edit Secundary Address");
        btnEditAddress.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    Contact contact = (Contact) listContacts.getSelectedValue();
                    if (contact != null) {
                        controller.selectContact(contact);
                        new EditSecundaryAddress(controller);
                    }
                } catch (NullPointerException e) {
                    JOptionPane.showMessageDialog(null, "There are no contacts registered!!!");
                }
            }

        });
        btnEditAddress.setVisible(true);
        return btnEditAddress;
    }

    public JButton removeAddress() {
        JButton btnRemoveAddress = new JButton("Remove Address");
        btnRemoveAddress.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    Contact contact = (Contact) listContacts.getSelectedValue();
                    if (contact != null) {
                        //new RemoveAddress(addressControler);
                    }
                } catch (NullPointerException e) {
                    JOptionPane.showMessageDialog(null, "There are no contacts registered!!!");
                }
            }

        });
        btnRemoveAddress.setVisible(true);
        return btnRemoveAddress;
    }
}
