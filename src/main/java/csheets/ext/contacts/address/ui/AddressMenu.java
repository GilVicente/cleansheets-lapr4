/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;


class AddressMenu extends JMenu{
    public AddressMenu(UIController uiController) {
		super("Contact");
		
                setMnemonic(KeyEvent.VK_A);

		// Adds font actions
		add(new AddressAction(uiController));
	}
}
