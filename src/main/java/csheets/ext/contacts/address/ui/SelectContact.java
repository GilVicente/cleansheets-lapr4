/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address.ui;

import csheets.ext.contacts.Contact;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author Tixa
 */
class SelectContact extends JDialog {

     /**
     * JList to display the list of contacts
     */
    JList listContacts;
    /**
     * JButton to aid in the removal of the contact
     */
    JButton buttonSelect;
    /**
     * JButton to aid in the listing of a contact's agenda
     */
    JButton buttonCancel;
    private ContactAddressController addressController;
    private List<Contact> contactList;
    private DefaultListModel<Contact> modelContactList = new DefaultListModel();

    public SelectContact(ContactAddressController addressControler) {
        this.addressController = addressController;

        setLayout(new BorderLayout());
        add(panelContacts(), BorderLayout.NORTH);
        add(panelTwoButtonsPanel(), BorderLayout.CENTER);
        setLocationRelativeTo(null);
        setResizable(false);
        setModal(true);
        pack();
        setVisible(true);
    }

    public JPanel panelContacts() {
        JPanel p = new JPanel();
        showContacts();
        listContacts = new JList(modelContactList);
        //listContacts.setCellRenderer(new ClientCellRenderer());
        JScrollPane pane = new JScrollPane(listContacts);
        p.add(pane);
        return p;
    }

    public void showContacts() {
        contactList = addressController.getContacts();
        if (contactList.size() > 0) {
            for (Contact c : contactList) {
                System.out.println(c.Id() + " - " + c.Firstname() + "  " + c.Lastname() + "\n");
                modelContactList.addElement(c);
            }
            JOptionPane.showMessageDialog(this, "Contacts fully loaded with success!");
        } else {
            JOptionPane.showMessageDialog(this, "Contacts not found", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No contacts found.");
        }
    }

    public JPanel panelTwoButtonsPanel() {
        JPanel p = new JPanel();
        p.add(buttonSelectContact(), BorderLayout.WEST);
        p.add(buttonCancel(), BorderLayout.EAST);
        return p;
    }

    /**
     * JButton to aid the removal of a contact
     *
     * @return
     */
    public JButton buttonSelectContact() {
        buttonSelect = new JButton("Select Contact");
        buttonSelect.addActionListener(new ActionListener() {
                                           @Override
                                           public void actionPerformed(ActionEvent ae) {
                                               try {
                                                   Contact contact = (Contact) listContacts.getSelectedValue();
                                                   addressController.selectContact(contact);
                                                   JOptionPane.showMessageDialog(null, "Contact select with sucess");
                                                   dispose();
                                               } catch (NullPointerException ex) {
                                                   JOptionPane.showMessageDialog(null, "No Contact Was Selected");
                                               }
                                           }
                                       }
        );
        return buttonSelect;
    }

    public JButton buttonCancel() {
        buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }

        });
        return buttonCancel;
    }
}



