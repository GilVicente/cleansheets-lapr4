/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

public class AddressAction extends BaseAction {

    protected UIController uiController;

    public AddressAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Address ...";
    }

    @Override
    protected void defineProperties() {
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        // Lets user select a font
        int result = JOptionPane.showConfirmDialog(null, "You have selected the Example option. Do you want to set cell A1 to 'Changed'");

        if (result == JOptionPane.YES_OPTION) {
            // Vamos exemplificar como se acede ao modelo de dominio (o workbook)
            try {
                this.uiController.getActiveSpreadsheet().getCell(0, 0).setContent("Changed");
            } catch (Exception ex) {
                // para ja ignoramos a excepcao
            }
        }
    }

}
