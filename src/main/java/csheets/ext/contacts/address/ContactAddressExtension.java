/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address;

import csheets.ext.Extension;
import csheets.ext.contacts.address.ui.UIExtensionsAddress;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Tixa
 */
public class ContactAddressExtension extends Extension {
    
    /** The name of the extension */
	public static final String NAME = "Address";

	/**
	 * Creates a new SeeExtensionsExtension
	 */
	public ContactAddressExtension() {
            super(NAME);
	}
	
	/**
	 * Returns the user interface extension of this extension 
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
        @Override
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionsAddress(this, uiController);
	}
    
}
