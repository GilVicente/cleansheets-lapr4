/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Patrícia Monteiro <1140807@isep.ipp.pt>
 */
@Embeddable
public class PostalCode implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * location number of the postal code
     */
    @Column(name = "LOCATIONNUMBER")
    private int locationNumber;
    
    /**
     * zone number of the postal code
     */
    @Column(name = "ZONENUMBER")
    private int zoneNumber;

    /**
     * Construtor vazio
     */
    public PostalCode() {
    }

    /**
     * creates a new postal code with the default parameters
     *
     * @param locationNumber location number of the postal code
     * @param zoneNumber zone number of the postal code
     */
    public PostalCode(int locationNumber, int zoneNumber) {
        this.locationNumber = locationNumber;
        this.zoneNumber = zoneNumber;
    }

    public int getLocationNumber() {
        return this.locationNumber;
    }

    public int getZoneNumber() {
        return this.zoneNumber;
    }

    /**
     * Validate postalCode
     *
     * @param locationNumber location number
     * @param zoneNumber zone number
     * @return boolean
     */
    public static boolean validate(int locationNumber, int zoneNumber) throws FileNotFoundException {
        List<Integer> allLocationNumbers = getAllLocationNumbers();
        List<Integer> allZoneNumbers = getAllZoneNumbers();

        return (allLocationNumbers.contains(locationNumber) && allZoneNumbers.contains(zoneNumber));

    }

    /**
     * @return @throws FileNotFoundException
     */
    private static List<Integer> getAllLocationNumbers() throws FileNotFoundException {
        List<String> allEntries = lerFicheiro("todos_cp.txt");

        String[] aux;
        List<Integer> allLocationNumbers = new ArrayList();
        for (String entry : allEntries) {
            aux = entry.split(";");
            allLocationNumbers.add(Integer.parseInt(aux[14]));

        }
        return allLocationNumbers;

    }

    private static List<Integer> getAllZoneNumbers() throws FileNotFoundException {
        List<String> allEntries = lerFicheiro("todos_cp.txt");

        String[] aux;
        List<Integer> allZoneNumbers = new ArrayList();
        for (String entry : allEntries) {
            aux = entry.split(";");
            allZoneNumbers.add(Integer.parseInt(aux[15]));

        }
        return allZoneNumbers;

    }

    /**
     * @param path
     * @return
     * @throws FileNotFoundException
     */
    private static List<String> lerFicheiro(String path) throws FileNotFoundException {

        BufferedReader reader = new BufferedReader(new FileReader(path));
        String aux = null;
        List<String> entry = new ArrayList();
        try {
            while ((aux = reader.readLine()) != null) {
                aux = aux.trim();
                if (!aux.equals("")) {
                    entry.add(aux);
                }
            }

            reader.close();
            return entry;

        } catch (IOException ex) {

            return null;
        }

    }
}
