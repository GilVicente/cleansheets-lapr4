/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.address;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author Patrícia Monteiro <1140807@isep.ipp.pt>
 */
@Entity
public class ContactAddress {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    //Address
    private String street;
    private String town;
    private PostalCode postalCode;
    private String city;
    private String country;


    /**
     * Construtor vazio
     */
    public ContactAddress() {

    }

    /**
     * creates a new contact address with all parameters
     *
     * @param street     street
     * @param location   town
     * @param postalCode postal code
     * @param city       city
     * @param country    country
     */
    public ContactAddress(String street, String location, String city, String country, PostalCode postalCode) {
        this.street = street;
        this.town = location;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @return the postalCode
     */
    public PostalCode getPostalCode() {
        return postalCode;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "ContactAddress{" + "street=" + street + ", town=" + town + ", postalCode=" + postalCode + ", city=" + city + ", country=" + country + '}';
    }

    public boolean sameAs(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean is(Long id) {
        return id.equals(this.getId());
    }

    public Long id() {
        return this.getId();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @param town the town to set
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(PostalCode postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }
    
    
}
