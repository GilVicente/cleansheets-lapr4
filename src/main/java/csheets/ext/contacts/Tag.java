/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author 1140234
 */
@Entity
@Table(name = "Tag")
@Inheritance(strategy = InheritanceType.JOINED)
public class Tag {

    /**
     * The description of the tag
     */
    @Id
    private String description;

    /**
     * The users that use the tag
     */
    @OneToMany(cascade = CascadeType.MERGE)
    private List<Contact> contacts;

    /**
     * The constructor
     *
     */
    public Tag() {
    }

    /**
     * The constructor
     *
     * @param tag
     */
    public Tag(String tag) {
        this.description = tag;
        this.contacts = new ArrayList<Contact>();
    }

    /**
     * Associate a contact to the tag
     *
     * @param c
     * @return
     */
    public boolean associateContact(Contact c) {
        if (!this.contacts.contains(c)) {
            return this.contacts.add(c);
        }
        return false;
    }

    /**
     * Return the tag description.
     *
     * @return
     */
    public String tagDescription() {
        return this.description;
    }

    /**
     * Return the number of contacts that are associated to the tag
     *
     * @return
     */
    public int returnFrequency() {
        return this.contacts.size();
    }
    
    public List<Contact> tagContacts() {
        return contacts;
    }

}
