/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

import csheets.ext.contacts.address.ContactAddress;

/**
 *
 * @author Tixa
 */
public interface ContactAddressRepository {
    
    boolean addNewContactAddress(ContactAddress ac);

    boolean deleteContactAddress(ContactAddress ac);

    boolean editUpdateContactAddress(ContactAddress ac);

    
    
}
