/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.Tag;
import java.util.List;

/**
 *
 * @author 1140234
 */
public interface TagRepository {
    /**
     * Adds a new tag
     * @param t
     * @return 
     */
    boolean newTag(Tag t);
    
    /**
     * Return all tags in repository
     * @return 
     */
    List<Tag> allTags();
    
    /**
     * Update the object tag
     * @param t
     * @return 
     */
    boolean updateTag(Tag t);
    
    /**
     * verify if the tag contains the contact already
     * @param c
     * @param t
     * @return 
     */
    boolean containsContact(String t,Contact c);
}
