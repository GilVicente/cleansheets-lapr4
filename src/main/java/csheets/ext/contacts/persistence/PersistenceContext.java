/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Guilherme
 */
public class PersistenceContext {
    
    public static RepositoryFactory repositories() {
        // return new InMemoryRepositoryFactory();
        // return new JpaRepositoryFactory();

        final String factoryClassName = AppSettings.instance().getRepositoryFactory();
        try {
            return (RepositoryFactory) Class.forName(factoryClassName).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            // FIXME handle exception properly
            Logger.getLogger(PersistenceContext.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private PersistenceContext() {
    }
}
