/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

//import csheets.ext.contacts.company.Company;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.Company;
import csheets.ext.contacts.MailNumber.Person;

import csheets.persistence.JpaRepository;
import java.util.List;

/**
 *
 * @author Guilherme
 */
public class ContactsJpaRepository extends JpaRepository<Contact,Long> implements ContactsRepository{

    @Override
    protected String persistenceUnitName() {
        return "csheets_jar_1.0-SNAPSHOTPU";
    }

    @Override
    public boolean addNewContact(Contact c) {
        try {
            this.save(c);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteContact(Contact c) {
        try {
            if (c instanceof Company) {
                List<Person> listEmployees = getCompanyEmployees(c);
                if (listEmployees.size() > 0) {
                    for (Person a : listEmployees) {
                        a.defineCompany(null);
                    }
                }
            }
            this.remove(c);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean updateEditContact(Contact c) {
        try {
            this.update(c);
            this.save(c);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public List<Contact> allContacts() {
        return all();
    }
    
    public List<Company> getCompanies(){
        return companies();
    }
    
}
