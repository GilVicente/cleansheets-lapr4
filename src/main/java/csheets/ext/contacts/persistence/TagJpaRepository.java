/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.Tag;
import csheets.persistence.JpaRepository;
import java.util.List;

/**
 *
 * @author 1140234
 */
public class TagJpaRepository extends JpaRepository<Tag, Long> implements TagRepository {

    @Override
    protected String persistenceUnitName() {
        return "csheets_jar_1.0-SNAPSHOTPU";
    }

    @Override
    public boolean newTag(Tag t) {
        try {
            
            this.save(t);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }
    
    @Override
    public boolean updateTag(Tag t){
       try {
            this.save(t);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public List<Tag> allTags() {

        return all();
        
    }

    @Override
    public boolean containsContact(String tag, Contact c) {
        List<Tag> lstTags= allTags();
        for (Tag t : lstTags) {
            for(Contact c2 : t.tagContacts()){
                if(c.Id()==c.Id()){
                    return true;
                }
            }
            
        }
        return false;
    }
}
