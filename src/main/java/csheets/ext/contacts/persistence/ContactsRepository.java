/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

import csheets.ext.contacts.Contact;
import java.util.List;

/**
 *
 * @author Guilherme
 */
public interface ContactsRepository {
    
    boolean addNewContact(Contact c);

    boolean deleteContact(Contact c);

    boolean updateEditContact(Contact c);

    List<Contact> allContacts();
    
}
