/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.Event;
import java.util.List;

/**
 *
 * @author Guilherme
 */
public interface EventsRepository {
    
    boolean addEvent(Event e,Contact c);

    boolean deleteEvent(Event e,Contact c);

    boolean updateEditEvent(Event e,Contact c);

    List<Event> showAll();
}
