/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

import csheets.persistence.jpa.ListNoteRepository;
import csheets.persistence.jpa.MailsRepository;
import csheets.persistence.jpa.PhoneNumberRepository;

/**
 *
 * @author Guilherme
 */
public interface RepositoryFactory {
    
        ContactsRepository contacts();
        ContactAddressRepository contactAddress();
        EventsRepository events();
        MailsRepository mails();
        PhoneNumberRepository numbers();
        TagRepository tags();
        ListNoteRepository listNotes();

}
