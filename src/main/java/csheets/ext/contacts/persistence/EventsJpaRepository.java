/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.persistence;

import csheets.ext.contacts.Agenda;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.Event;
import csheets.persistence.JpaRepository;
import java.util.List;

/**
 *
 * @author Guilherme
 */
public class EventsJpaRepository extends JpaRepository<Event,Long> implements EventsRepository {

    @Override
    protected String persistenceUnitName() {
        return "csheets_jar_1.0-SNAPSHOTPU";
    }

    @Override
    public boolean addEvent(Event e,Contact c) {
        try {
            List<Event>listEvent = showContactAgenda(c);
            listEvent.add(e);
            this.save(e);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteEvent(Event e,Contact c) {
        try {
            Agenda agenda = eventToRemove(c, e);
            int index=agenda.indexOf(agenda.getEventByID(e.Id()));
            this.remove(agenda.AgendaList().remove(index));
           } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean updateEditEvent(Event e,Contact c) {
        try {
            List<Event>listEvent = showContactAgenda(c);
            listEvent.remove(e);
            this.update(e);
            this.save(e);
            listEvent.add(e);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public List<Event> showAll() {
        return all();
    }
    
    public List<Event> showContactAgenda(Contact contact){
        return contactAgenda(contact);
    }    
}
