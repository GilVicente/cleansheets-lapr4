/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;

import csheets.ext.contacts.Contact;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Guilherme
 */
@Entity
@Table(name = "Company")
public class Company extends Contact implements Serializable {

    /**
     * Company's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * CRM Purposes Constructor
     */
    public Company() {
        super();
    }

    /**
     * Constructor for data attribution
     *
     * @param companyName - companyName
     * @param picture     - picture
     */
    public Company(String companyName, String picture) {
        super(companyName, "Company Contact", picture);
    }

    /**
     * Constructor for data attribution(with number)
     *
     * @param companyName - companyName
     * @param picture     - picture
     * @param number      - number
     */
    public Company(String companyName, String picture, PhoneNumber number) {
        super(picture, companyName, "Company Contact", number);

    }

    /**
     * @return toString of a Company
     */
    @Override
    public String toString() {
        return this.Firstname();
    }
}
