/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;


import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.UI.MailAndNumberEditionPanel;
import csheets.ext.contacts.persistence.ContactsJpaRepository;
import csheets.ext.contacts.persistence.PersistenceContext;

import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dmaia
 */
public class MailController {

     /**
     * The user interface controller
     */
    private UIController uiController;

    private  Mail mail;

    private MailAndNumberEditionPanel uiPanel;
    /**
     * List of contacts of the user.
     */
    private List<Contact> contactsList;

    private Contact contact;

    public MailController(Contact contact) {
        this.contact=contact;
    }
    
    public MailController(UIController uiController, MailAndNumberEditionPanel mailPanel) {
        this.uiController = uiController;
        this.uiPanel = mailPanel;
        this.contactsList = PersistenceContext.repositories().contacts().allContacts();
        //createDummyContactList();
        //ContactsJpaRepository rep = new ContactsJpaRepository();
        //rep.allContacts();
    }

   
     /**
     * Returns a contact with id the same as the parameter
     *
     * @param idContact - id of Contact to be found
     * @return contact with id the same as the parameter
     */
    public Contact getContactId(long idContact) {
        ContactsJpaRepository rep = new ContactsJpaRepository();
        return (Contact) rep.findById(idContact);
    }

//     /**
//     * Removes contact from database
//     *
//     * @param mail - Contact to be removed
//     */
//    public void removeMail(Mail mail) {
//        JpaContactsRepository rep = new JpaContactsRepository();
//        rep.deleteMail(mail);
//    }
    
    
    public void createDummyContactList() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Sara", "Costa", null));
        contacts.add(new Contact("Ken", "Follet", null));
        contacts.add(new Contact("Lars", "Kepler", null));
        this.contactsList = contacts;
    }

  
    public boolean saveMail(String email, boolean principal) {
        this.mail = new Mail(email,principal);
        if (this.mail.mailIsValid()) {
            PersistenceContext.repositories().mails().addMail(mail, contact);
            PersistenceContext.repositories().contacts().updateEditContact(contact);
            return true;
        }
            return false;
            
        
        
    }
    
    
    
     public void removeMail(Mail m) {
        PersistenceContext.repositories().mails().deleteMail(m, contact);
        PersistenceContext.repositories().contacts().updateEditContact(contact);
    }

//      /**
//     * Get all the professions stored in the database
//     *
//     * @return
//     */
//    public List<Mail> getMail() {
//        JpaContactsRepository rep = new JpaContactsRepository();
//        return rep.mails();
//    }
    
    /**
     * Edit of Company type contact
     *
     * @param email
     * @param principal
     */
    public void editMail(Mail m,String email, boolean principal) {
        m.defineEmail(email);
        m.definePrincipal(principal);
        PersistenceContext.repositories().mails().updateEditMail(m, contact);
        PersistenceContext.repositories().contacts().updateEditContact(contact);
     
    }
    
    
    public List<Contact> getContacts(){
     
        return PersistenceContext.repositories().contacts().allContacts();
    }
    
    
}
