/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.UI.MailAndNumberEditionPanel;
import csheets.ext.contacts.persistence.ContactsJpaRepository;
import csheets.ext.contacts.persistence.PersistenceContext;
import csheets.ui.ctrl.UIController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dmaia
 */
public class NumberController {

   /**
     * The user interface controller
     */
    private UIController uiController;

    private  PhoneNumber number;

    private MailAndNumberEditionPanel uiPanel;
    /**
     * List of contacts of the user.
     */
    private List<Contact> contactsList;

    private Contact contact;

    public NumberController(Contact contact) {
        this.contact = contact;
    }

    public NumberController(UIController uiController, MailAndNumberEditionPanel mailPanel) {
        this.uiController = uiController;
        this.uiPanel = mailPanel;
        this.contactsList = PersistenceContext.repositories().contacts().allContacts();
        //createDummyContactList();
        //ContactsJpaRepository rep = new ContactsJpaRepository();
        //rep.allContacts();
    }

   
     /**
     * Returns a contact with id the same as the parameter
     *
     * @param idContact - id of Contact to be found
     * @return contact with id the same as the parameter
     */
    public Contact getContactId(long idContact) {
        ContactsJpaRepository rep = new ContactsJpaRepository();
        return (Contact) rep.findById(idContact);
    }

//     /**
//     * Removes contact from database
//     *
//     * @param mail - Contact to be removed
//     */
//    public void removeMail(Mail mail) {
//        JpaContactsRepository rep = new JpaContactsRepository();
//        rep.deleteMail(mail);
//    }
    
    
    public void createDummyContactList() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Sara", "Costa", null));
        contacts.add(new Contact("Ken", "Follet", null));
        contacts.add(new Contact("Lars", "Kepler", null));
        this.contactsList = contacts;
    }

  
    public boolean saveNumberCompany(String phoneNumber) {
        this.number = new PhoneNumber(phoneNumber);
        if (this.number.workNumberIsValid()) {  
         PersistenceContext.repositories().numbers().addNumber(this.number, contact);
         PersistenceContext.repositories().contacts().updateEditContact(contact);
         return true;
        }
        return false;
    }
    
     public boolean saveNumberPerson(String work,String home,String celular1,String celular2) {
        this.number = new PhoneNumber(work, home, celular1, celular2);
         if (this.number.workNumberIsValid() && this.number.homeIsValid() && this.number.celular1Valid() && this.number.celular2Valid()) {
        contact.defineNumber(number);
        PersistenceContext.repositories().numbers().addNumber(this.number, contact);
        PersistenceContext.repositories().contacts().updateEditContact(contact);
        return true;
        }
       return false; 
    }
    
    
     public void removeNumber(PhoneNumber pN) {
        PersistenceContext.repositories().numbers().deleteNumber(pN, contact);
        PersistenceContext.repositories().contacts().updateEditContact(contact);
    }
     
     /**
     * Edit of Company type contact
     *
     * @param phoneNumber
     */
    public void editNumberCompany(PhoneNumber pN,String phoneNumber) {
        this.number.defineWork(phoneNumber);
        PersistenceContext.repositories().numbers().updateEditNumber(pN, contact);
        PersistenceContext.repositories().contacts().updateEditContact(contact);
     
    }
    
     /**
     * Edit of Company type contact
     *
     * @param phoneNumber
     */
    public void editNumberPerson(PhoneNumber pN,String work,String home,String celular1,String celular2) {
        this.number.defineWork(work);
        this.number.defineHome(home);
        this.number.defineCelular1(celular1);
        this.number.defineCelular2(celular2);
        PersistenceContext.repositories().numbers().updateEditNumber(number, contact);
        PersistenceContext.repositories().contacts().updateEditContact(contact);
     
    }
    

}
