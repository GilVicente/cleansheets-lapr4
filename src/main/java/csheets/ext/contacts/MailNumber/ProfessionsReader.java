/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author Guilherme
 */
public class ProfessionsReader {

    /**
     * @param professionList - professions list
     * @return the profession list from file
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public static ProfessionsList getProfessionsListFromFile(ProfessionsList professionList) throws SAXException, ParserConfigurationException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document docProfessions = db.parse("Professions.xml");
        Element professionsRoot = docProfessions.getDocumentElement();
        NodeList professionsList = professionsRoot.getElementsByTagName("Profession");
        int size = professionsList.getLength();
        for (int i = 0; i < size; i++) {
            int id = 0;
            Element elementProfessions = (Element) professionsList.item(i);
            Profession profession = new Profession();
            profession.setName(elementValue(elementProfessions, "Name"));
            professionList.addProfessionToList(profession);
            id++;
        }
        return professionList;
    }

    /**
     * Returns the value of an element surround by the tag elementName
     *
     * @param element     - element
     * @param elementName - elementName
     * @return
     */
    public static String elementValue(Element element, String elementName) {
        NodeList listElements = element.getElementsByTagName(elementName);
        if (listElements == null) {
            return null;
        }
        Element noElement = (Element) listElements.item(0);
        if (noElement == null) {
            return null;
        }
        Node no = noElement.getFirstChild();
        if (no == null) {
            return "";
        }
        return no.getNodeValue();
    }
}
