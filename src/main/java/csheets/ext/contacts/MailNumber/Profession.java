/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Guilherme
 */
@Entity
public class Profession {

    /**
     * Profession's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Profession designation
     */
    private String name;

    /**
     * CRM Purposes Constructor
     */
    public Profession() {

    }

    /**
     * Data attribution constructor(just name)
     *
     * @param name - designation
     */
    public Profession(String name) {
        this.name = name;
    }

    /**
     * Data attribution constructor(id and name)
     *
     * @param id   - id
     * @param name - name
     */
    public Profession(long id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @return the id
     */
    public Long Id() {
        return id;
    }

    /**
     * @return the name
     */
    public String Name() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return toString of a Profession
     */
    @Override
    public String toString() {
        return this.Name();
    }

}
