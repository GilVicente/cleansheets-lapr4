/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Daniela Ferreira <1130430@isep.ipp.pt>
 */
@Entity
public class PhoneNumber {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private long id;
    private String workNumber;
    private String home;
    private String celular1;

    private String celular2;

    public PhoneNumber() {
    }

    public PhoneNumber(String workNumber) {
        this.workNumber = workNumber;
    }
    
    
    public PhoneNumber(String workNumber, String home, String celular1, String celular2) {
        this.workNumber = workNumber;
        this.home = home;
        this.celular1 = celular1;
        this.celular2 = celular2;
    }
    
    


    public boolean workNumberIsValid() {
        Pattern p = Pattern.compile("9[1236][0-9]{7}");

        Matcher m = p.matcher(this.workNumber);

        boolean mat = m.matches();

        if (mat) {
            return true;
        }

        return false;
    }

    public boolean homeIsValid() {
        Pattern p = Pattern.compile("2[1-9]{1,2}[0-9]{6}");

        Matcher m = p.matcher(this.home);

        boolean mat = m.matches();

        if (mat) {
            return true;
        }

        return false;
    }

    public boolean celular1Valid() {
        Pattern p = Pattern.compile("9[1236][0-9]{7}");

        Matcher m = p.matcher(this.celular1);

        boolean mat = m.matches();

        if (mat) {
            return true;
        }

        return false;
    }

    public boolean celular2Valid() {
        Pattern p = Pattern.compile("9[1236][0-9]{7}");

        Matcher m = p.matcher(this.celular2);

        boolean mat = m.matches();

        if (mat) {
            return true;
        }

        return false;
    }

    public String WorkNumber() {
        return workNumber;
    }

    public String Home() {
        return home;
    }

    public String Celular1() {
        return celular1;
    }

    public String Celular2() {
        return celular2;
    }

    public long Id() {
        return id;
    }

    public void defineWork(String workNumber) {
        this.workNumber = workNumber;
    }

    /**
     * @param home the home to set
     */
    public void defineHome(String home) {
        this.home = home;
    }

    /**
     * @param celular1 the celular1 to set
     */
    public void defineCelular1(String celular1) {
        this.celular1 = celular1;
    }

    /**
     * @param celular2 the celular2 to set
     */
    public void defineCelular2(String celular2) {
        this.celular2 = celular2;
    }
}
