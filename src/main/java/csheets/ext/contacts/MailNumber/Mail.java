/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;

import csheets.ext.contacts.Contact;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author dmaia
 */

@Entity
public class Mail {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private String email;
    private boolean principal;
  
    
    
    //associado a um contact 
    @ManyToOne (cascade = CascadeType.ALL)
    private Contact contact;

    public Mail() {
    }

    public Mail(String email, boolean principal, Contact contact) {
        this.email = email;
        this.principal = principal;
        this.contact = contact;
    }
    
    public Mail(String email, boolean principal) {
        this.email = email;
        this.principal = principal;
        
    }
    
    /**
     * @return the email
     */
    public String Email() {
        return this.email;
    }

    /**
     * @return the principal
     */
    public boolean Principal() {
        return this.principal;
    }

   
     /**
     * @return the id
     */
    public long Id() {
        return id;
    }

    /**
     * @param email the email to set
     */
    public void defineEmail(String email) {
        this.email = email;
    }

    /**
     * @param principal the email to set 
     */
    public void definePrincipal(boolean principal) {
        this.principal=principal;
    }
    
    public boolean mailIsValid() {

        Pattern p = Pattern.compile("[a-z]+@[a-z]+\\.com|\\.pt");

        Matcher m = p.matcher(this.email);

        boolean mat = m.matches();

        if (mat) {
            return true;
        }

        return false;

          

    }

    @Override
    public String toString() {
        return "Mail{" + "id=" + id + ", email=" + email + ", principal=" + principal + '}';
    }
       
}
