/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;

import csheets.ext.contacts.Contact;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Guilherme
 */
@Entity
@Table(name = "Person")
public class Person extends Contact implements Serializable {

    /**
     * Person's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Person's company
     */
    @OneToOne(cascade = CascadeType.ALL)
    private Company company;

    /**
     * Person's profession
     */
    @OneToOne(cascade = CascadeType.ALL)
    private Profession profession;

    /***
     * CRM Purposes Constructor
     */
    public Person() {
        super();
    }

    /**
     * Constructor for data attribution
     *
     * @param firstname - firstname
     * @param lastname  - lastname
     * @param picture   - picture
     */
    public Person(String firstname, String lastname, String picture) {
        super(firstname, lastname, picture);
        this.company = null;
        this.profession = null;
    }

    /**
     * Constructor for data attribution(with profession,no company)
     *
     * @param firstname  - firstname
     * @param lastname   - lastname
     * @param picture    - picture
     * @param profession - profession
     */
    public Person(String firstname, String lastname, String picture, Profession profession) {
        super(firstname, lastname, picture);
        this.company = null;
        this.profession = profession;
    }

    /**
     * Constructor for data attribution(with profession and company)
     *
     * @param firstname  - firstname
     * @param lastname   - lastname
     * @param picture    - picture
     * @param company    - company
     * @param profession - profession
     */
    public Person(String firstname, String lastname, String picture, Company company, Profession profession) {
        super(firstname, lastname, picture);
        this.company = company;
        this.profession = profession;
    }

    /**
     * Constructor for data attribution(with phone number,company and profession)
     *
     * @param firstname   - firstname
     * @param lastname    - lastname
     * @param picture     - picture
     * @param phoneNumber - phoneNumber
     * @param company     - company
     * @param profession  - profession
     */
    public Person(String firstname, String lastname, String picture, PhoneNumber phoneNumber, Company company, Profession profession) {
        super(picture, firstname, lastname, phoneNumber);
        this.company = company;
        this.profession = profession;

    }

    /**
     * @return the company
     */
    public Company Company() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void defineCompany(Company company) {
        this.company = company;
    }

    /**
     * @return toString of a Person
     */
    @Override
    public String toString() {
        if (Company() != null) {
            return this.Firstname() + " " + this.Lastname() + " ; Works For: " + this.company.Firstname();
        }
        return this.Firstname() + " " + this.Lastname();
    }

    /**
     * @return the profession
     */
    public Profession Profession() {
        return profession;
    }

    /**
     * @param profession the profession to set
     */
    public void defineProfession(Profession profession) {
        this.profession = profession;
    }
}
