/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber.UI;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.MailController;
import csheets.ext.contacts.MailNumber.NumberController;
import csheets.ext.contacts.address.ui.ContactAddressController;
import csheets.ext.contacts.ui.ClientCellRenderer;
import csheets.ui.ctrl.UIController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author dmaia
 */
public class MailAndNumberEditionPanel extends JPanel {

    /**
     * The Use Case Controller.
     */
    private MailController controller;

    /**
     * The Use Case Controller.
     */
    private NumberController controller2;
    /**
     * Panel to display a list of Contacts and ContactNotes.
     */
    private JPanel mailNumberPanel;

    private List<Contact> contactsList;
    private DefaultListModel<Contact> modelContactList = new DefaultListModel();
    private JTree tree;

    private JList listContacts;
    
    Contact contact;

    /**
     * Creates a new notes panel.
     *
     * @param uiController the user interface controller
     */
    public MailAndNumberEditionPanel(UIController uiController) {
        super(new BorderLayout());
        setName(MailNumberExtension.NAME);

        controller = new MailController(uiController, this);
        controller2 = new NumberController(uiController, this);

        mailNumberPanel = new JPanel();
        mailNumberPanel.setLayout(new BorderLayout());
        mailNumberPanel.setPreferredSize(new Dimension(130, 336));
        mailNumberPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));        // width, height

        mailNumberPanel.add(panelContacts(), BorderLayout.NORTH);
        mailNumberPanel.add(panelButtons(), BorderLayout.SOUTH);

        add(mailNumberPanel);

    }

    public JPanel panelContacts() {
        JPanel p = new JPanel();
        showContacts();
        listContacts = new JList(modelContactList);
        listContacts.setCellRenderer(new ClientCellRenderer());
        JScrollPane pane = new JScrollPane(listContacts);
        p.add(pane);
        return p;
    }

    public void showContacts() {
        contactsList = controller.getContacts();
        if (contactsList.size() > 0) {
            for (Contact c : contactsList) {
                modelContactList.addElement(c);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Contacts not found", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No contacts found.");
        }
    }

    private JPanel panelButtons() {
        JPanel p = new JPanel(new GridLayout(4, 1));
        //p.setLayout(new BorderLayout());
        p.add(buttonSelectContact());
        p.add(window());
//        p.add(buttonCancel());
        return p;

    }
    
    /**
     * JButton to aid the removal of a contact
     *
     * @return
     */
    public JButton buttonSelectContact() {
        JButton buttonSelect = new JButton("Select Contact");
        buttonSelect.addActionListener(new ActionListener() {
                                           @Override
                                           public void actionPerformed(ActionEvent ae) {
                                               try {
                                                   contact = (Contact) listContacts.getSelectedValue();
                                               } catch (NullPointerException ex) {
                                                   JOptionPane.showMessageDialog(null, "No Contact Was Selected");
                                               }
                                           }
                                       }
        );
        return buttonSelect;
    }

    
    

    public JButton window() {
        JButton btnOpen = new JButton("Open");
        btnOpen.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                MailAndPhoneNumberEditionUI ui = new MailAndPhoneNumberEditionUI(contact);
                ui.setVisible(true);

            }
        });
           
        btnOpen.setVisible(true);
                
        return btnOpen;

    }
    
}
