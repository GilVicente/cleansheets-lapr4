/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber.UI;

import csheets.ext.contacts.address.ui.*;
import csheets.ui.ctrl.UIController;

import javax.swing.*;
import java.awt.event.KeyEvent;


class MailNumberMenu extends JMenu {
    public MailNumberMenu(UIController uiController) {
        super("Contact");

        setMnemonic(KeyEvent.VK_A);

        // Adds font actions
        add(new MailNumberAction(uiController));
    }
}
