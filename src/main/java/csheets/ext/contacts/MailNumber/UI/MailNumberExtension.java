/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber.UI;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * @author dmaia
 */
public class MailNumberExtension extends Extension {

    /**
     * The name of the extension
     */
    public static final String NAME = "Mails and Phone Numbers";

    /**
     * Creates a new SeeExtensionsExtension
     */
    public MailNumberExtension() {
        super(NAME);
    }

    /**
     * Returns the user interface extension of this extension
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIMailAndNumberExtension(this, uiController);
    }

}
