/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.MailNumber;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Guilherme
 */
public class ProfessionsList {
    /**
     * A list of professions
     */
    private List<Profession> professionsList;

    /**
     * Constructor for the initialization of the professions list
     */
    public ProfessionsList() {
        professionsList = new ArrayList();
    }

    /**
     * @param profession - profession
     * @return true if profession is added to list
     */
    public boolean addProfessionToList(Profession profession) {
        if (!this.professionsList.contains(profession)) {
            return this.professionsList.add(profession);
        }
        return false;
    }

    /**
     * @return the size of the professions list
     */
    public int size() {
        if (!professionsList.isEmpty()) {
            return this.professionsList.size();
        } else {
            return 0;
        }
    }

    /**
     * @param index - index
     * @return profession at the index position in the professions list
     */
    public Profession getProfessionFromList(int index) {
        return professionsList.get(index);
    }

    /**
     * @return the professionList
     */
    public List<Profession> professionList() {
        return professionsList;
    }


}
