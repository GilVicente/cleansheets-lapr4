/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts;

import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Guilherme
 */
@Entity
public class Event{
    
    /** Event's id*/
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    /** Event's date*/
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dueDate;
    
    /** Event's description*/
    private String description;

    /**
     * CRM Purposes Constructor
     */
    public Event() {
    }
    
    /**
     * Event constructor 
     * @param dueDate - dueDate
     * @param description - description
     */
    Event(Calendar dueDate,String description){
        this.dueDate=dueDate;
        this.description=description;
    }

    /**
     * @return the dueDate
     */
    public Calendar DueDate() {
        return dueDate;
    }

    /**
     * @param dueDate the dueDate to set
     */
    public void defineDueDateByCalendar(Calendar dueDate) {
        this.dueDate = dueDate;
    }
    
    /**
     * Sets the date to the three int parameters day,month,year
     *
     * @param month The month to set
     * @param day The day to set
     * @param year The year to set
     */
    public void seteventDate(int day, int month, int year) {
        this.dueDate.set(year, month, day);
    }

    /**
     * @return the description
     */
    public String Description() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void defineDescription(String description) {
        this.description = description;
    }

    /**
     * @return the id
     */
    public long Id() {
        return id;
    }

    /**
     * 
     * @return toString of an Event
     */
    @Override
    public String toString(){
        return this.Id()+" - " + this.description + " Date: " + this.DueDate().getTime().toString();
    }    
}
