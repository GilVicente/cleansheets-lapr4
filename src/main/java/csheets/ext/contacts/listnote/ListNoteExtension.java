/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote;

import csheets.ext.contacts.listnote.ui.UIExtensionListNotes;
import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author bie
 */
public class ListNoteExtension extends Extension {
    
    public static final String NAME = "ListNotes";
    
    public ListNoteExtension() {
        super(NAME);
    }
    
    @Override
    public UIExtension getUIExtension(UIController uiController){
        if (this.uiExtension == null) {
            this.uiExtension = new UIExtensionListNotes(this, uiController);
        }
        return this.uiExtension;
    }
    
}
