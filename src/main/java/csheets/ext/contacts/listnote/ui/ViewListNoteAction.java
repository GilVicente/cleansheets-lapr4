/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 *
 * @author bie
 */
public class ViewListNoteAction extends JMenuItem{
    
    public ViewListNoteAction(UIController uiController, ListNotesController controller){
        super("View List notes");
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ContactChooser chooser = new ContactChooser(controller);
            }
        });
    }
    
}
