/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote.ui;

import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author bie
 * @author Francisco
 */
class ListNotesMenu extends JMenu {
    
    ListNotesController controller;
    SearchListNoteController searchListNotesController;
    SearchTextNoteController searchTextNotesController;
    
    public ListNotesMenu(UIController uiController) {
        super("List Notes");
        this.controller = new ListNotesController(uiController);
        this.searchListNotesController = new SearchListNoteController(uiController);
        this.searchTextNotesController = new SearchTextNoteController(uiController);
        this.add(new CreateListNoteAction(uiController, controller));
        this.add(new ViewListNoteAction(uiController, controller));
        this.add(new SearchListNoteAction(uiController, searchListNotesController));
        this.add(new SearchTextNoteAction(uiController, searchTextNotesController));
    }
    
}
