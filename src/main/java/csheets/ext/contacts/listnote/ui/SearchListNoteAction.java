/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 *
 * @author Francisco
 */
public class SearchListNoteAction extends JMenuItem {


    public SearchListNoteAction (UIController uiController, SearchListNoteController controller) {
        super("Search list note");
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                SearchListNoteDialog dialog = new SearchListNoteDialog(controller);
            }
        });
    }
}
