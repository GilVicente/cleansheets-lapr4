/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 *
 * @author Francisco
 */
public class SearchTextNoteAction extends JMenuItem {


    public SearchTextNoteAction (UIController uiController, SearchTextNoteController controller) {
        super("Search text note");
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                SearchTextNoteDialog dialog = new SearchTextNoteDialog(controller);
            }
        });
    }
}
