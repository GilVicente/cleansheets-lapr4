package csheets.ext.contacts.listnote.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.listnote.ListNote;
import csheets.ext.contacts.listnote.ListNoteElementRegistry;
import csheets.ext.contacts.persistence.ContactsJpaRepository;
import csheets.ext.contacts.persistence.PersistenceContext;
import csheets.persistence.jpa.JpaListNoteRepository;
import csheets.ui.ctrl.UIController;
import java.util.List;

public class ListNotesController {

    private UIController uiController;
    private List<Contact> contactList;
    private Contact selectedContact;
    private JpaListNoteRepository repository;

    public ListNotesController(UIController uiController) {
        this.uiController = uiController;
        ContactsJpaRepository repo = (ContactsJpaRepository) PersistenceContext.repositories().contacts();
        contactList = repo.allContacts();
        repository = (JpaListNoteRepository) PersistenceContext.repositories().listNotes();
    }

    public List<Contact> getContacts() {
        return this.contactList;
    }

    public void setSelectedContact(Contact c) {
        this.selectedContact = c;
    }

    public boolean saveListNote(String note) {
        ListNote ln = new ListNote();
        for (String textLine : note.split("\n")) {
            if (ln.getTitle() == null) {
                ln.setTitle(textLine);
                ln.addListNoteElement(textLine);
            }
            ln.addListNoteElement(textLine);  
        }
        selectedContact.addListNote(ln);
        repository.save(ln);
        return true;
    }
    
    public boolean modifyListNote(ListNote ln, String note){
        ListNoteElementRegistry r = new ListNoteElementRegistry();
        for (String line : note.split("\n")) {
            r.addListNoteElement(line);
        }
        ln.setElements(r);
        ln.setTitle(r.getElements().get(0).getNote());
        return true;
    }

}
