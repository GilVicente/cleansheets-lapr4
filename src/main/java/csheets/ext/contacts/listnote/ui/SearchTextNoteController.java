/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.listnote.ListNote;
import csheets.ext.contacts.listnote.ListNoteElement;
import csheets.ext.contacts.persistence.ContactsJpaRepository;
import csheets.ext.contacts.persistence.PersistenceContext;
import csheets.ext.editNotes.ContactNote;
import csheets.persistence.jpa.JpaListNoteRepository;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Francisco
 */
public class SearchTextNoteController {

    private List<Contact> contactList;
    private UIController uiController;
    private ContactsJpaRepository rep;

    public SearchTextNoteController(UIController uiController) {

        this.uiController = uiController;
        this.rep = (ContactsJpaRepository) PersistenceContext.repositories().contacts();

    }

    public List<ContactNote> searchListNoteByString(String regex) {
        List<Contact> listOfContacts = this.rep.allContacts();
        List<ContactNote> newList = new ArrayList<>();
        for (Contact c : listOfContacts) {
            List<ContactNote> notes = c.getContactNotes();
            for (ContactNote cn : notes) {
                if (cn.getTitle().matches(regex)) {
                    newList.add(cn);
                }
            }
        }

        return newList;

    }

    public List<ContactNote> searchListNoteByTime(Calendar inferiorLimit, Calendar superiorLimit) {
        List<Contact> listOfContacts = this.rep.allContacts();
        List<ContactNote> newList = new ArrayList<>();
        for (Contact c : listOfContacts) {
            List<ContactNote> notes = c.getContactNotes();
            for (ContactNote note : notes) {
                if (note.getLatestTimestamp().after(inferiorLimit) && note.getLatestTimestamp().before(superiorLimit)) {
                    newList.add(note);
                }
            }

        }

        return newList;
    }
}
