 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote.ui;

import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 *
 * @author bie
 */
class CreateListNoteAction extends JMenuItem{

    public CreateListNoteAction(UIController uiController, ListNotesController controller) {
        super("Create list note");
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CreateListNoteDialog dialog = new CreateListNoteDialog(controller);
            }
        });
    }

    
    
}
