/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote.ui;

import csheets.ext.contacts.listnote.ListNote;
import csheets.ext.contacts.listnote.ListNoteElement;
import csheets.ext.contacts.persistence.PersistenceContext;
import csheets.persistence.jpa.JpaListNoteRepository;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Francisco
 */
public class SearchListNoteController {

    private JpaListNoteRepository repository;
    private UIController uiController;

    public SearchListNoteController(UIController uiController) {

        this.uiController = uiController;
        this.repository = (JpaListNoteRepository) PersistenceContext.repositories().listNotes();

    }

    public List<ListNote> searchListNoteByString(String regex) {
        List<ListNote> listOfListNotes = this.repository.all();
        List<ListNote> newList = new ArrayList<>();
        for (ListNote listOfNotes : listOfListNotes) {
            for (ListNoteElement element : listOfNotes.getElements().getElements()) {
                if (element.getNote().matches(regex)) {
                    newList.add(listOfNotes);
                }
            }
        }

        return newList;

    }

    public List<ListNote> searchListNoteByTime(Calendar inferiorLimit, Calendar superiorLimit) {
        List<ListNote> listOfListNotes = this.repository.all();
        List<ListNote> newList = new ArrayList<>();
        for (ListNote listOfNotes : listOfListNotes) {
            if (listOfNotes.getLastUpdated().after(inferiorLimit) && listOfNotes.getLastUpdated().before(superiorLimit)) {
                newList.add(listOfNotes);
            }
        }
        
        return newList;
    }
}
