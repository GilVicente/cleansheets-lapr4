/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote;

import java.io.Serializable;
import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author 1141277
 */
@Entity
public class ListNoteElement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    /**
     * Textual description of the note
     */
    private String note;
    /**
     * Boolean representig the checked status
     */
    private boolean isChecked;
    
    /**
     * Database purpose constructor
     */
    public ListNoteElement() {
    }

    public ListNoteElement(String note) {
        this.note = note;
        this.isChecked = false;
    }

    public String getNote() {
        return this.note;
    }

    public boolean isChecked() {
        return this.isChecked;
    }
    
    public boolean check(){
        System.out.println("Elemento Checked");
        return this.isChecked = true;
    }
    
    public boolean uncheck(){
        System.out.println("Elemento Unchecked");
        return this.isChecked = false;
    }
    
    @Override
    public boolean equals(Object other){
        if (other.getClass() == this.getClass()) {
            ListNoteElement otherElement = (ListNoteElement) other;
            return otherElement.getNote().equals(this.note);
        }
        return false;
    }

}
