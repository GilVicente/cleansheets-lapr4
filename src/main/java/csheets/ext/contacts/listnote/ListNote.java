package csheets.ext.contacts.listnote;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author 1141277
 */
@Entity
public class ListNote implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    /**List of elements of this list note*/
    @OneToOne(cascade = CascadeType.ALL)
    private ListNoteElementRegistry elements;
    
    /**I need to save he date of the alteration*/
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar lastUpdated;
    
    /**Title of the List Note*/
    private String title;

    public ListNote() {
        this.elements = new ListNoteElementRegistry();
    }

    public ListNote(String listNoteElement) {
        this.elements = new ListNoteElementRegistry();
        this.elements.addListNoteElement(listNoteElement);
        this.lastUpdated = Calendar.getInstance();
    }

    private void setTitle() {
        
    }

    public String getTitle() {
        return this.title;
    }
    
    public ListNoteElementRegistry getElements(){
        return this.elements;
    }
    
    public boolean addListNoteElement(String note){
        this.updateLUpdated();
        return this.elements.addListNoteElement(note);
    }
    
    public boolean removeListNoteElement(ListNoteElement element){
        this.updateLUpdated();
        return this.elements.removeListNoteElement(element);
    }
    
    public Calendar getLastUpdated(){
        return this.lastUpdated;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public void setElements(ListNoteElementRegistry lner){
        this.elements = lner;
    }
    
    public void setLastUpdated(Calendar timestamp){
        this.lastUpdated = timestamp;
    }
    
    public void updateLUpdated(){
        this.lastUpdated = Calendar.getInstance();
    }
    
    public ListNote getClone(){
        ListNote clone = new ListNote();
        clone.setTitle(this.getTitle());
        clone.setElements(this.getElements());
        clone.setLastUpdated(this.getLastUpdated());
        return clone;
    }
    
    @Override
    public boolean equals(Object other){
        if (other.getClass() == this.getClass()) {
            boolean flag = false;
            ListNote otherln = (ListNote)other;
            return otherln.getElements().getElements().equals(this.getElements().getElements());
        }
        return false;
    }
    
    @Override
    public String toString(){
        String s = "";
        for (ListNoteElement element : this.getElements().getElements()) {
            s+=element.getNote()+"\n";
        }
        s+="Last modified: "+this.lastUpdated.getTime().toString();
        return s;
    }
    
    
}
