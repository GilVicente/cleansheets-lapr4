/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts.listnote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author bie
 */
@Entity
public class ListNoteElementRegistry implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToMany(cascade = CascadeType.ALL)
    private List<ListNoteElement> elementList;
    
    public ListNoteElementRegistry(){
        elementList = new ArrayList<>();
    }
    
    public List<ListNoteElement> getElements(){
        return this.elementList;
    }
    
    public ListNoteElementRegistry(List<ListNoteElement> elementList){
        this.elementList = elementList;
    }
    
    public boolean addListNoteElement(String note){
        ListNoteElement element = new ListNoteElement(note);
        if (!elementList.contains(element)) {
            return elementList.add(element);
        }
        return false;
    }
    
    public boolean removeListNoteElement(ListNoteElement elementToRemove){
        
        for (ListNoteElement noteElement : elementList) {
            if (noteElement.equals(elementToRemove)) {
                return this.elementList.remove(elementToRemove);
            }
        }
        return false;
    }
    
    public boolean removeListNoteElement(String noteToRemove){
        ListNoteElement elementToRemove = new ListNoteElement(noteToRemove);
        return removeListNoteElement(elementToRemove);
    }
    
    
}
