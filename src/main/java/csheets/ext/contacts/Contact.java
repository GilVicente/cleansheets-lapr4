/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts;

import csheets.ext.contacts.MailNumber.Mail;
import csheets.ext.contacts.MailNumber.PhoneNumber;
import csheets.ext.contacts.address.ContactAddress;
import csheets.ext.contacts.listnote.ListNote;
import csheets.ext.editNotes.ContactNote;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Guilherme
 */
@Entity
@Table(name = "Contact")
@Inheritance(strategy = InheritanceType.JOINED)
public class Contact implements Serializable {

    /**
     * Contact's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    /**
     * Contact's firstname
     */
    private String firstname;
    /**
     * Contact's lastname
     */
    private String lastname;
    /**
     * Contact's picture
     */
    private String picture;

    /**
     * Contact's agenda
     */
    @OneToOne(cascade = CascadeType.MERGE)
    private Agenda agenda;

    /**
     * List of mails of the Contact
     */
    @OneToMany(cascade = CascadeType.ALL)
    private List<Mail> maillist;

    /**
     * Contact's phone number
     */
    @OneToOne(cascade = CascadeType.ALL)
    private PhoneNumber number;
    /**
     * List of Notes of the Contact
     */
    @OneToMany(cascade = CascadeType.ALL)
    private List<ContactNote> notes;

    /**
     * List of ListNotes of the contact
     */
    @OneToMany(cascade = CascadeType.ALL)
    private List<ListNote> listnotes;

    /**
     * Contact's primary address
     */
    @OneToOne(cascade = CascadeType.PERSIST)
    private ContactAddress primaryContactAdress;
    /**
     * Contact's secondary address
     */
    @OneToOne(cascade = CascadeType.PERSIST)
    private ContactAddress secundaryContactAdress;
//    

    /**
     * *
     * CRM Purposes Constructor
     */
    public Contact() {
    }

    /**
     * Contact constructor without picture
     *
     * @param firstname - firstname
     * @param lastname - lastname
     */
    public Contact(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.agenda = new Agenda();
        this.listnotes = new ArrayList<>();
        this.picture = "";

    }

    /**
     * Contact constructor with picture
     *
     * @param firstname - firsname
     * @param lastname - lastname
     * @param picture - picture
     */
    public Contact(String firstname, String lastname, String picture) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.agenda = new Agenda();
        this.picture = picture;
        this.listnotes = new ArrayList<>();
        this.number = new PhoneNumber();
    }

    /**
     * Contact constructor with all fields
     *
     * @param firstname - firstname
     * @param picture - picture
     * @param lastName - lastname
     * @param phoneNumber - phone number
     */
    public Contact(String picture, String firstname, String lastName, PhoneNumber phoneNumber) {
        this.agenda = new Agenda();
        this.picture = picture;
        this.number = new PhoneNumber();
        this.listnotes = new ArrayList<>();
        this.firstname = firstname;
        this.lastname = lastName;

    }

    public Contact(String firstname, String lastname, String picture, Agenda agenda, List<Mail> maillist, PhoneNumber number, List<ContactNote> notes, ContactAddress primaryContactAdress, ContactAddress secundaryContactAdress) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.picture = picture;
        this.agenda = agenda;
        this.maillist = maillist;
        this.number = number;
        this.notes = notes;
        this.primaryContactAdress = primaryContactAdress;
        this.secundaryContactAdress = secundaryContactAdress;
        this.listnotes = new ArrayList<>();
    }

    /**
     * @return the firstname
     */
    public String Firstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void defineFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String Lastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void defineLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the agendaList
     */
    public Agenda agendaList() {
        return agenda;
    }

    /**
     * @param agendaList the agendaList to set
     */
    public void defineAgendaList(Agenda agendaList) {
        this.agenda = agendaList;
    }

    /**
     * @return the picture
     */
    public String Picture() {
        return picture;
    }

    /**
     * @param picture the picture to set
     */
    public void definePicture(String picture) {
        this.picture = picture;
    }

    /**
     * Gets list os contact Notes
     *
     * @return list of notes
     */
    public List<ContactNote> getContactNotes() {
        return this.notes;
    }

    /**
     * Adds note
     *
     * @param note of Contact
     * @return list of notes with the given note added
     */
    public boolean addNote(ContactNote note) {
        return this.notes.add(note);
    }

    /**
     * Remove from the list the given note
     *
     * @param note of contact
     */
    public void removeNote(ContactNote note) {
        if (this.notes.size() == 1) {
            this.notes = new ArrayList<>();
        } else {
            this.notes.remove(note);
        }
    }

    /**
     * @return the id
     */
    public long Id() {
        return id;
    }

    /**
     * @return the number
     */
    public PhoneNumber number() {
        return number;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @param pNumber - phone Number
     */
    public void defineNumber(PhoneNumber pNumber) {
        this.number = pNumber;
    }

    /**
     * @return Contact's mail list
     */
    public List<Mail> getMaillist() {
        return maillist;
    }

    /**
     *
     * @return Contact's primary address
     */
    public ContactAddress getPrimaryContactAdress() {
        return this.primaryContactAdress;
    }

    /**
     * Set's the contact's primary address
     *
     * @param primaryContactAdress - primaryContactAddress
     */
    public void setPrimaryContactAdress(ContactAddress primaryContactAdress) {
        this.primaryContactAdress = primaryContactAdress;
    }

    /**
     *
     * @return Contact's secondary address
     */
    public ContactAddress getSecundaryContactAdress() {
        return this.secundaryContactAdress;
    }

    /**
     * Set's the contac's secondary address
     *
     * @param secundaryContactAdress - secondaryContactAddress
     */
    public void setSecundaryContactAdress(ContactAddress secundaryContactAdress) {
        this.secundaryContactAdress = secundaryContactAdress;
    }

    /**
     * Gets the contact's list notes.
     *
     * @return List with the list notes
     */
    public List<ListNote> getListNotes() {
        return this.listnotes;
    }

    /**
     * Gets the contacts list notes.
     *
     * @param listnotes list notes to set
     */
    public void setListNotes(List<ListNote> listnotes) {
        this.listnotes = listnotes;
    }

    public void addListNote(ListNote ln) {
        System.out.println("Added List note#############33");
        this.listnotes.add(ln);
    }

    public boolean removeListNote(ListNote ln) {
        return this.listnotes.remove(ln);
    }

    /**
     *
     * @return Contact's toString
     */
    @Override
    public String toString() {
        return this.Id() + " - " + this.Firstname() + " " + this.Lastname();
    }

}
