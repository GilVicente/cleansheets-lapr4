/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.contacts;


import csheets.ext.contacts.MailNumber.Person;
import csheets.ext.contacts.persistence.EventsJpaRepository;
import java.util.Calendar;
import java.util.List;
import csheets.ext.contacts.persistence.PersistenceContext;

/**
 *
 * @author Guilherme
 */
public class EventController {
    
    public EventController(){
        
    }
    
    /**
     * Registration of an event(database)
     * @param description - description
     * @param year - year
     * @param month - month
     * @param date - date
     * @param contact - contact
     */
    public void registerEvent(String description,int year,int month,int date,Contact contact){
        Calendar dueDate = Calendar.getInstance();
        dueDate.set(year, month, date);
        Event event = new Event(dueDate,description);
        PersistenceContext.repositories().events().addEvent(event, contact);
//        EventsJpaRepository rep= new EventsJpaRepository();
//        rep.addEvent(event,contact);
     }
    
    /**
     * Removal of an event(database)
     * @param event - event
     * @param contact - contact
     */
    public void removeEvent(Event event,Contact contact) {
        PersistenceContext.repositories().events().deleteEvent(event, contact);
//        EventsJpaRepository rep = new EventsJpaRepository();
//        rep.deleteEvent(event,contact);
    }
    
    /**
     * Editing an event(database)
     * @param event - event
     * @param description - description
     * @param year - year
     * @param month - month 
     * @param date - date
     * @param contact - contact
     */
    public void editEvent(Event event,String description,int year,int month,int date,Contact contact){
        Calendar dueDate = Calendar.getInstance();
        dueDate.set(year, month, date);
        event.defineDescription(description);
        event.defineDueDateByCalendar(dueDate);
        PersistenceContext.repositories().events().updateEditEvent(event, contact);
//        EventsJpaRepository rep= new EventsJpaRepository();
//        rep.updateEditEvent(event,contact);
     }
    
    /**
     * Get a contact's agenda
     * @param contact - contact to get agenda from
     * @return a contact's agenda
     */
    public List<Event> getContactAgenda(Contact contact){
        EventsJpaRepository rep = new EventsJpaRepository();
        return rep.showContactAgenda(contact);
    }
    
    /**
     * Get a company's agenda
     * @param contact - contact(company) to get agenda from
     * @return company's agenda
     */
   public List<Person> getCompanyAgenda(Contact contact){
       EventsJpaRepository rep = new EventsJpaRepository();
       return rep.getCompanyEmployees(contact);
   }
}
