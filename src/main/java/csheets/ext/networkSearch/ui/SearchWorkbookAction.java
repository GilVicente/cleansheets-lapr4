/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkSearch.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author Daniela Ferreira
 */
public class SearchWorkbookAction extends BaseAction {

    /**
     * The User Interface Controller
     */
    protected UIController uiController;
    private SearchWorkbookController searchWorkbookController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public SearchWorkbookAction(UIController uiController, SearchWorkbookController searchController) {
        this.uiController = uiController;
        this.searchWorkbookController = searchController;
    }

    @Override
    protected String getName() {
        return "Search workbooks";
    }

    @Override
    protected void defineProperties() {
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        SearchWorkbooksUI searchWorkbooks = new SearchWorkbooksUI(null, uiController, searchWorkbookController);
    }

}
