/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkSearch.ui;

import csheets.core.Workbook;
import csheets.ext.networkSearch.NetworkSearchExtension;

import csheets.ui.ctrl.UIController;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 *
 * @author Daniela Ferreira
 */
public class SearchWorkbooksPanel extends JPanel {

    private UIController uiController;
    private SearchWorkbooksUI searchUI;
    private SearchWorkbookController searchController;
    private JList workbookList;
    private DefaultListModel workbookListModel;

    /**
     * Constructor of the class FindWorkbookPanel
     *
     * @param Controller the UI Controller
     */
    public SearchWorkbooksPanel(UIController uiController, SearchWorkbookController searchController) {

        this.uiController = uiController;
        this.searchController = searchController;
        initComponents();
        actions();
    }

    public void initComponents() {
        setName(NetworkSearchExtension.NAME);
        setLayout(new GridLayout(1, 1));

        JPanel workbookListWrap = new JPanel(new FlowLayout());
        JLabel workbookListLabel = new JLabel("Found Workbooks:");
        workbookList = new JList();
        workbookListModel = new DefaultListModel();
        workbookList.setSize(150, 300);
        workbookList.setModel(workbookListModel);

        workbookListWrap.add(workbookListLabel);
        workbookListWrap.add(workbookList);

        this.add(workbookListWrap);
    }

    public void actions() {
        workbookList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent me) {
                WorkbookDetailsUI workbookDetailsUI = new WorkbookDetailsUI(null, (Workbook) workbookList.getSelectedValue());
            }
        });
    }

    void updateList(Map<Workbook,String> workbookList) {
        for (Workbook workbook : workbookList.keySet()) {
            this.workbookListModel.addElement(workbook);
        }
    }
}
