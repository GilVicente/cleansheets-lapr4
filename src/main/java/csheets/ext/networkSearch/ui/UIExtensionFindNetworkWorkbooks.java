/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkSearch.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.CellDecorator;
import csheets.ui.ext.TableDecorator;
import csheets.ui.ext.UIExtension;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JToolBar;

/**
 *
 * @author Daniela Ferreira
 */
public class UIExtensionFindNetworkWorkbooks extends UIExtension {

    /**
     * The icon to display with the extension's name
     */
    private Icon icon;

    /**
     * The menu of the extension
     */
    private FindNetworkWorkbooksMenu menu;

    private SearchWorkbooksPanel sideBar;

    private SearchWorkbookController searchController;

    /**
     * Creates a new instance of UIExtensionFindNetworkWorkbooks
     *
     * @param extension
     * @param uiController the user interface controller
     */
    public UIExtensionFindNetworkWorkbooks(Extension extension, UIController uiController) {
        super(extension, uiController);
        searchController = new SearchWorkbookController(uiController);
        searchController.findAllLocalWorkbooks();
    }

    public InetAddress getMyIPAddress() throws UnknownHostException, SocketException {
        Enumeration e = NetworkInterface.getNetworkInterfaces();

        while (e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                String[] address = i.getHostAddress().replace(".", ",").
                        split(",");
                if (address.length == 4) {
                    int add = Integer.parseInt(address[0]);
                    if (add >= 0 && add <= 255 && add != 127) {
                        return i;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Returns an icon to display with the extension's name.
     *
     * @return an icon with style
     */
    public Icon getIcon() {
        return null;
    }

    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @see FindNetworkWorkbooksMenu
     * @return a JMenu component
     */
    public JMenu getMenu() {
        if (menu == null) {
            menu = new FindNetworkWorkbooksMenu(uiController, searchController);
        }
        return menu;
    }

    /**
     * Returns a cell decorator that visualizes the data added by the extension.
     *
     * @return a cell decorator, or null if the extension does not provide one
     */
    public CellDecorator getCellDecorator() {
        return null;
    }

    /**
     * Returns a table decorator that visualizes the data added by the
     * extension.
     *
     * @return a table decorator, or null if the extension does not provide one
     */
    public TableDecorator getTableDecorator() {
        return null;
    }

    /**
     * Returns a toolbar that gives access to extension-specific functionality.
     *
     * @return a JToolBar component, or null if the extension does not provide
     * one
     */
    public JToolBar getToolBar() {
        return null;
    }

    /**
     * Returns a side bar that gives access to extension-specific functionality.
     *
     * @return a component, or null if the extension does not provide one
     */
    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new SearchWorkbooksPanel(uiController, searchController);
        }
        return sideBar;
    }
}
