/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkSearch.ui;

import csheets.core.Workbook;
import csheets.ext.networkSearch.LocalWorkbookSearch;
import csheets.ext.networkSearch.UDPgetWorkbookList;

import csheets.ui.ctrl.UIController;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Daniela Ferreira
 */
public class SearchWorkbookController {

    /**
     * The User Interface Controller
     */
    private UIController uiController;

    /**
     * List of workbooks found
     */
    private Map<Workbook, String> localWorkbooksList;
    private Map<Workbook, String> receivedWorkbookList;
    private UDPgetWorkbookList runningSearch;

    private SearchWorkbooksUI searchUI;

    /**
     * Creates an instance of SearchWorkbookController
     *
     * @param uiController The User Interface Controller
     */
    public SearchWorkbookController(UIController uiController) {
        this.uiController = uiController;
        localWorkbooksList = new HashMap<Workbook, String>();
        receivedWorkbookList = new HashMap<Workbook, String>();
        UDPgetWorkbookList udpWorkbooks = new UDPgetWorkbookList(this, 8888);
        this.runningSearch = udpWorkbooks;
        Thread search = new Thread(udpWorkbooks);
        search.start();
    }

    public void setActiveUI(SearchWorkbooksUI searchUI) {
        this.searchUI = searchUI;
    }

    public SearchWorkbooksUI getActiveUI() {
        return this.searchUI;
    }

    /**
     * Search for workbooks avaliable in the network
     *
     * @param portNumber port of destination
     * @throws Exception
     */
    public void searchWorkbooks(String name, String content) throws Exception {
        UDPgetWorkbookList.name = name;
        UDPgetWorkbookList.content = content;
        UDPgetWorkbookList.wantToRequest = true;
    }

    public Map<Workbook, String> getLocalWorkbookList() {
        return localWorkbooksList;
    }

    public Map<Workbook, String> getReceiveWorkbookList() {
        return receivedWorkbookList;
    }

    public void findAllLocalWorkbooks() {
        LocalWorkbookSearch workbookSearcher = new LocalWorkbookSearch("cls", this);
        Thread t = new Thread(workbookSearcher);
        t.start();
    }

    public void addWorkbook(File f) {
        Workbook w;
        try {
            uiController.loadFile(f);
            w = uiController.getWorkbook(f);
            if (w != null) {
                this.localWorkbooksList.put(w, f.getName());
            }

        } catch (IOException ex) {

        } catch (ClassNotFoundException ex) {

        }

    }

    public void setReceivedWorkbookList(Map<Workbook, String> workbooks) {
        this.receivedWorkbookList = new HashMap(workbooks);
        searchUI.updateList(workbooks);
    }
}
