/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkSearch;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Daniela Ferreira
 */
public abstract class FileSearcher extends Observable {

    /**
     * Search directory
     */
    private File dirToSearch;

    /**
     * List of files inside search directory
     */
    private List<File> fileList;

    /**
     * Constructor
     * @param path search directory's path
     * @throws SecurityException
     * @throws NullPointerException 
     */
    public FileSearcher(String path) throws SecurityException, NullPointerException {

        File dirPath = new File(path);

        if (dirPath.isDirectory()) {
            this.dirToSearch = dirPath;
            this.fileList = new ArrayList<File>();
        }

    }

    /**
     * Gives this class an observer
     * @param obs observer
     */
    public void defineObserver(Observer obs) {
        this.addObserver(obs);
    }

    /**
     * Notifies observers that a change occured
     * @param file 
     */
    public void setChangedAndNotifyObservers(File file) {
        this.setChanged();
        this.notifyObservers(file);
    }

    /**
     * Starts searching
     */
    public void beginSearch() {
        this.executeSearch(this.dirToSearch);
    }

    /**
     * Adds a file to the result of the search
     * @param file file to add to the list
     * @return 
     */
    public boolean addFileToList(File file) {
        if (!this.fileList.contains(file)) {
            setChangedAndNotifyObservers(file);
            return this.fileList.add(file);
        }
        return false;
    }

    /**
     * Returns the number of files found on this directory
     * @return 
     */
    public int numberOfFilesFound() {
        return this.fileList.size();
    }

    /**
     * Methos that searchs inside this directory
     * @param fileInSearch
     * @throws SecurityException
     * @throws NullPointerException 
     */
    public abstract void executeSearch(File fileInSearch) throws SecurityException, NullPointerException;

}
