/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkSearch;

import csheets.ext.Extension;
import csheets.ext.networkSearch.ui.UIExtensionFindNetworkWorkbooks;

import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Daniela Ferreira 
 */
public class NetworkSearchExtension extends Extension{
    
    /**
     * Name of extension
     */
    public static final String NAME ="Find network Workbooks";

    /**
     *Creates a new instance NetworkSearchExtension 
     */
    public NetworkSearchExtension() {
        super(NAME);
    }
    
    public UIExtension getUIExtension(UIController uiController){
        if(this.uiExtension==null){
            this.uiExtension=new UIExtensionFindNetworkWorkbooks(this, uiController);
        }
        return this.uiExtension;
    }
    
}
