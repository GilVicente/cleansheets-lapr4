/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkSearch;

import csheets.core.Workbook;
import csheets.ext.networkSearch.ui.SearchWorkbookController;


import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniela Ferreira
 */
public class UDPgetWorkbookList implements Runnable {

    private SearchWorkbookController searchController;
    private Object request;
    private int port;
    public static String name;
    public static String content;
    public static boolean wantToRequest = false;
    private ByteArrayOutputStream baos;
    private ObjectOutputStream oos;
    private ByteArrayInputStream byteArray;
    private ObjectInputStream objectInput;

    public UDPgetWorkbookList(SearchWorkbookController searchController, int port) {
        this.searchController = searchController;
        this.request = new String("REQUEST_LIST");
        this.port = port;

    }

    @Override
    public void run() {

        try {
            DatagramSocket socket = new DatagramSocket(port);
            this.baos = new ByteArrayOutputStream();
            try {
                this.oos = new ObjectOutputStream(new BufferedOutputStream(baos));
                socket.setSoTimeout(500);
            } catch (IOException ex) {

            }
            while (true) {

                try {
                    if (wantToRequest) {
                        oos.writeObject("REQUEST_LIST");
                        oos.flush();
                        byte[] buffer = baos.toByteArray();
                        socket.setBroadcast(true);
                        DatagramPacket toSend = new DatagramPacket(buffer, buffer.length, InetAddress.getByName("255.255.255.255"), port);
                        socket.send(toSend);
                        socket.setBroadcast(false);
                        wantToRequest = false;
                        oos.close();
                        baos.close();
                    }
                    byte[] incomingData = new byte[63000];
                    DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);
                    socket.receive(incomingPacket);
                    objectInput = null;
                    byteArray = null;
                    Object received;
                    try {
                        byteArray = new ByteArrayInputStream(incomingPacket.getData());
                        objectInput = new ObjectInputStream(byteArray);
                        received = objectInput.readObject();
                    } finally {
                        if (byteArray != null) {
                            byteArray.close();
                        }
                        if (objectInput != null) {
                            objectInput.close();
                        }
                    }
                    if (received instanceof String) {
                        if (((String) received).equals("REQUEST_LIST")) {
                            baos = new ByteArrayOutputStream();
                            oos = new ObjectOutputStream(baos);
                            oos.writeObject(searchController.getLocalWorkbookList());
                            oos.flush();
                            byte[] buffer = baos.toByteArray();
                            DatagramPacket toSend = new DatagramPacket(buffer, buffer.length, incomingPacket.getAddress(), port);
                            socket.send(toSend);
                            oos.close();
                            baos.close();
                        }
                    } else if (received instanceof Map) {
                        Map<Workbook, String> unFilteredBooks = (Map<Workbook, String>) received;
                        searchController.setReceivedWorkbookList(filterReceivedFiles(unFilteredBooks));
                    }
                } catch (SocketTimeoutException ex) {
                }
            }

        } catch (ClassCastException ex) {

        } catch (IOException ex) {
            System.out.println(ex.toString());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }

    }

    private Map<Workbook, String> filterReceivedFiles(Map<Workbook, String> unFiltered) {
        Map<Workbook, String> filteredResults = new HashMap();
        boolean flag = false;
        if (this.name == null && this.content != null) {
            for (Map.Entry<Workbook, String> entry : unFiltered.entrySet()) {
                for (int i = 0; i < entry.getKey().getSpreadsheetCount(); i++) {
                    for (int j = 0; j < entry.getKey().getSpreadsheet(i).getRowCount(); j++) {
                        for (int k = 0; k < entry.getKey().getSpreadsheet(i).getColumnCount(); k++) {
                            if (entry.getKey().getSpreadsheet(i).getCell(j, k).getContent().equals(this.content)) {
                                filteredResults.put(entry.getKey(), entry.getValue());
                                flag = true;
                            }
                            if (flag) {
                                break;
                            }
                        }
                        if (flag) {
                            break;
                        }
                    }
                    if (flag) {
                        break;
                    }
                }
                flag = false;
            }
        } else if (this.name != null && this.content == null) {
            for (Map.Entry<Workbook, String> entry : unFiltered.entrySet()) {
                if (entry.getValue().contains(this.name)) {
                    filteredResults.put(entry.getKey(), entry.getValue());
                }
            }

        } else if (this.name != null && this.content != null) {
            for (Map.Entry<Workbook, String> entry : unFiltered.entrySet()) {
                if (entry.getValue().equals(this.name)) {
                    for (int i = 0; i < entry.getKey().getSpreadsheetCount(); i++) {
                        for (int j = 0; j < entry.getKey().getSpreadsheet(i).getRowCount(); j++) {
                            for (int k = 0; k < entry.getKey().getSpreadsheet(i).getColumnCount(); k++) {
                                if (entry.getKey().getSpreadsheet(i).getCell(j, k).getContent().equals(this.content)) {
                                    filteredResults.put(entry.getKey(), entry.getValue());
                                    flag = true;
                                    break;
                                }
                            }
                            if (flag) {
                                break;
                            }
                        }
                        if (flag) {
                            break;
                        }
                    }
                    if (flag) {
                        break;
                    }
                }
                flag = false;
            }
        }
        return filteredResults;
    }
}
