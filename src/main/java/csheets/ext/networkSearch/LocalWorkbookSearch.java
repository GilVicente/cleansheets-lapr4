/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkSearch;


import csheets.ext.networkSearch.ui.SearchWorkbookController;
import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author Daniela Ferreira
 */
public class LocalWorkbookSearch extends FileSearcher implements Runnable {

    public static boolean searchEnd;
    private SearchWorkbookController searchController;

    public LocalWorkbookSearch(String path, SearchWorkbookController searchController) {
        super(path);
        searchEnd = false;
        this.searchController = searchController;
    }

    @Override
    public void run() {
        beginSearch();
    }

    @Override
    public void beginSearch() {
        super.beginSearch();
    }

    @Override
    public void executeSearch(File directory) {

        File f = new File(directory.getAbsolutePath());
        File[] matchingFiles = f.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".cls");
            }
        });

        for (File matchingFile : matchingFiles) {
            searchController.addWorkbook(matchingFile);
        }
    }
}
