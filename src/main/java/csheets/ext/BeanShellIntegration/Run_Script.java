/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.BeanShellIntegration;

import bsh.EvalError;
import bsh.Interpreter;
import bsh.util.JConsole;
import csheets.ui.ctrl.UIController;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;


/**
 *
 * @author dmaia
 */
public class Run_Script {

    private static UIController uIController;
    private static JConsole area;

    public Run_Script(UIController uicontroller) {
        area = new JConsole();
        uIController = uicontroller;
    }

    public static int run(String pathFile) {
        /**
         * Calling the console from the BSH Library.A new interactive
         * interpreter attached to the specified console.
         *
         */
        Interpreter interpreter = new Interpreter(area);

        /**
         * Create an interpreter for evaluation only.
         *
         */
        Interpreter i = new Interpreter();

        try {
            interpreter.set("controller", uIController);
        } catch (EvalError e) {
            JOptionPane.showMessageDialog(null, e, "Not Found!", JOptionPane.ERROR_MESSAGE);
        }
        int result = 0;
        try {

            result = (int) i.source(pathFile);
            System.out.println("Result script: " + result);
            //=RUN("beanshellScripts/test.bsh";0)
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, e, "Not Found!", JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e, "Not Found!", JOptionPane.ERROR_MESSAGE);
        } catch (EvalError e) {
            JOptionPane.showMessageDialog(null, e, "Evaluation Failed!", JOptionPane.ERROR_MESSAGE);
        }

        new Thread(interpreter).start();

        return result;

    }

}
