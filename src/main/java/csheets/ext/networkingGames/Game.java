/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkingGames;

/**
 *
 * @author 1130750
 */
public class Game implements Runnable {

    private String identifier;
    private String againstUser;

    public Game(String identifier, String againstUser) {
        this.identifier = identifier;
        this.againstUser = againstUser;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @return the againstUser
     */
    public String getAgainstUser() {
        return againstUser;
    }

}
