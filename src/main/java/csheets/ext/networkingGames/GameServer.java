package csheets.ext.networkingGames;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author 1130750
 */
public class GameServer {

    private int port = 9831;

    private NetworkingGamesController controller;

    private Thread server;

    public GameServer(NetworkingGamesController controller) {
        this.controller = controller;
        server = new Thread(new Server());
        server.start();
    }

    public boolean sendInviteToUser(UserProfile userProfile, String game) {
        Client client = new Client(userProfile, game,controller);
        Thread t = new Thread(client);
        t.start();
        return false;
    }

    private class Server implements Runnable {

        @Override
        public void run() {
            try {
                String invite;
                String inviteResponse;
                ServerSocket welcomeSocket = new ServerSocket(port);

                while (true) {
                    Socket connectionSocket = welcomeSocket.accept();
                    BufferedReader inFromClient
                            = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

                    DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                    invite = inFromClient.readLine();
                    int response = JOptionPane.showConfirmDialog(null, invite + "\nDo you wish to accept", "Invite", JOptionPane.YES_NO_OPTION);

                    if (response == JOptionPane.YES_OPTION) {
                        inviteResponse = "S" + '\n';
                        String[] aux = invite.split(" ");
                        String game = aux[aux.length-3];
                        controller.addActiveGame(new Game(game, connectionSocket.getInetAddress().getHostName()));
                    } else {
                        inviteResponse = "N" + '\n';
                    }

                    outToClient.writeBytes(inviteResponse);
                }
            } catch (IOException ex) {
                Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public class Client implements Runnable {

        private UserProfile userProfile;
        private String game;
        private NetworkingGamesController controller;
        public Client(UserProfile userProfile, String game,NetworkingGamesController controller) {
            this.userProfile = userProfile;
            this.game = game;
            this.controller=controller;
        }

        @Override
        public void run() {
            try {
                String invite = "User " +controller.getUserProfile().getUserName() + " wants to play " + game + " with you";
                String inviteResponse;
                Socket clientSocket = new Socket(userProfile.getAddress(), port);
                DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                outToServer.writeBytes(invite + '\n');
                inviteResponse = inFromServer.readLine();
                String message;
                if (inviteResponse.startsWith("S")) {
                    message = "User " + userProfile.getUserName() + " accepted your request";
                    controller.addActiveGame(new Game(game, userProfile.getUserName()));
                } else {
                    message = "User " + userProfile.getUserName() + " declined your request";
                }

                JOptionPane.showMessageDialog(null, message);
                clientSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
