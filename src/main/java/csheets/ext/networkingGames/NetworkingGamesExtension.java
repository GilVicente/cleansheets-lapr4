/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkingGames;

import csheets.ext.Extension;
import csheets.ext.networkingGames.ui.UIExtensionNetworkingGames;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author 1130750
 */
public class NetworkingGamesExtension extends Extension {

    /**
     * The name of the extension
     */
    public static final String NAME = "Networking Games";

    /**
     * Creates a new Networking Games extension.
     */
    public NetworkingGamesExtension() {
        super(NAME);
    }

    /**
     * Returns the user interface extension of this extension
     *
     * @param uiController the user interface controller
     * @return a user interface extension, or null if none is provided
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {
        return new UIExtensionNetworkingGames(this, uiController);
    }
}
