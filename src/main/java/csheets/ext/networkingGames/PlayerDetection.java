package csheets.ext.networkingGames;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 *
 * @author 1130750
 */
public class PlayerDetection {

    private NetworkingGamesController controller;

    private static final byte REQUEST = 10;

    private static final byte RESPONSE = 11;

    int port = 9001;

    private DatagramSocket broadcastSocket;

    private final InetSocketAddress broadcastAddress;

    private boolean connected = false;

    private Thread broadcastListen = new Thread() {
        @Override
        public void run() {
            try {
                byte[] buffy = new byte[900];
                DatagramPacket rx = new DatagramPacket(buffy, buffy.length);

                while (!connected) {
                    try {
                        broadcastSocket.receive(rx);
                        if (buffy[0] == REQUEST) {

                            byte[] data = serialize(controller.getUserProfile());
                            data = encode(data, RESPONSE);

                            if (!rx.getAddress().equals(InetAddress.getLocalHost())) {
                                DatagramPacket tx
                                        = new DatagramPacket(data, data.length, rx.getAddress(), port);
                               
                                broadcastSocket.send(tx);
                            }
                        } else if (buffy[0] == RESPONSE) {
                            synchronized (controller) {
                                buffy = decode(buffy);
                                UserProfile userToAdd=((UserProfile)deserialize(buffy));
                                userToAdd.setAddress(rx.getAddress());
                                controller.addUser(userToAdd);
                            }

                        }
                    } catch (SocketException se) {
                        se.printStackTrace();
                    }
                }

                broadcastSocket.disconnect();
                broadcastSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public PlayerDetection(NetworkingGamesController controller) {
        this.controller = controller;
        broadcastAddress = new InetSocketAddress("255.255.255.255", port);
        try {
            broadcastSocket = new DatagramSocket(port);
            broadcastSocket.setBroadcast(true);
        } catch (SocketException ex) {
            Logger.getLogger(PlayerDetection.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean isConnected() {
        return connected;
    }

    public void connect() {
        broadcastListen.setDaemon(true);
        broadcastListen.start();
    }

    public void disconnect() {
        connected = true;

        broadcastSocket.close();
        broadcastSocket.disconnect();

        try {
            broadcastListen.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void update(int timeout) throws IOException {

        byte[] data = new byte[900];
        data[0] = REQUEST;

        DatagramPacket tx = new DatagramPacket(data, data.length, broadcastAddress);
        broadcastSocket.send(tx);
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
        }
    }

    public static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream zipOutStream = new GZIPOutputStream(baOutputStream);
        ObjectOutputStream objOut = new ObjectOutputStream(zipOutStream);
        objOut.writeObject(obj);
        objOut.close();
        byte[] bytes = baOutputStream.toByteArray();
        return bytes;
    }

    public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream baInputStream = new ByteArrayInputStream(data);
        GZIPInputStream zipInStream = new GZIPInputStream(baInputStream);
        ObjectInputStream objectIn = new ObjectInputStream(zipInStream);
        Object obj = objectIn.readObject();
        objectIn.close();
        return obj;
    }

    public static byte[] encode(byte[] array, byte type) {
        byte newArray[] = new byte[array.length + 1];

        newArray[0] = type;
        for (int i = 1; i < newArray.length; i++) {
            newArray[i] = array[i - 1];
        }
        return newArray;
    }

    public static byte[] decode(byte[] array) {
        byte newArray[] = new byte[array.length - 1];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array[i + 1];
        }
        return newArray;
    }
}
