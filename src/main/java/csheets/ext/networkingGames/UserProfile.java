package csheets.ext.networkingGames;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author 1130750
 */
public class UserProfile implements Serializable {

    private String userName;
    private ImageIcon image;
    private InetAddress address;
    private static final String SYSTEM_PROPERTY="user.name";
    
    
    public UserProfile(ImageIcon img) {
        ImageIcon scaledImage = new ImageIcon(img.getImage().getScaledInstance(10, 10, 0));
        this.image = scaledImage;
        this.userName = System.getProperty(SYSTEM_PROPERTY);   
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @return the imagePath
     */
    public ImageIcon getImage() {
        return image;
    }

    /**
     * @return the address
     */
    public InetAddress getAddress() {
        return address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        UserProfile user = (UserProfile) o;
        return this.getUserName().equals(user.getUserName());
    }

    @Override
    public String toString() {
        return "user name: " + userName;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(InetAddress address) {
        this.address = address;
    }

}
