package csheets.ext.networkingGames;

import java.awt.Image;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author 1130750
 */
public class NetworkingGamesController {

    private UserProfile userProfile;
    private List<UserProfile> availableUsers;
    private PlayerDetection playerDescovery;
    private GameServer gameServer;
    private List<Game> activeGames;

    public NetworkingGamesController() {
        availableUsers = new ArrayList<>();
        this.activeGames=new ArrayList<>();
    }

    /**
     * @return the userProfile
     */
    public UserProfile getUserProfile() {
        return userProfile;
    }

    public boolean addUser(UserProfile user) {
        if (!availableUsers.contains(user)) {
            return this.availableUsers.add(user);
        } else {
            return false;
        }
    }

    /**
     * @return the availableUsers
     */
    public List<UserProfile> getAvailableUsers() {
        return availableUsers;
    }

    /**
     * @param img
     */
    public void setUserProfile(ImageIcon img) {
        this.userProfile = new UserProfile(img);
    }

    public void setOnline() {
        playerDescovery = new PlayerDetection(this);
        gameServer = new GameServer(this);
        this.playerDescovery.connect();
    }

    public void setOffline() {
        this.playerDescovery.disconnect();
    }

    public boolean isUserProfileSet() {
        return this.userProfile != null;
    }

    public void update() {
        try {
            this.playerDescovery.update(3000);
        } catch (IOException ex) {
            Logger.getLogger(NetworkingGamesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendInvite(UserProfile userProfile, String game) {
        this.gameServer.sendInviteToUser(userProfile, game);
    }

    public List<Game> getActiveGames(){
        return activeGames;
    }
    
    public boolean addActiveGame(Game game){
        return this.activeGames.add(game);
    }
    public boolean removeActiveGame(Game game){
        return this.activeGames.remove(game);
    }
}
