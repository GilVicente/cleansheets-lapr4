/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.networkingGames.ui;

import csheets.ext.networkingGames.NetworkingGamesController;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author 1130750
 */
public class SetUserProfileAction extends BaseAction{
    
    private UIController uiController;
    private NetworkingGamesController controller;
    public SetUserProfileAction(NetworkingGamesController controller,UIController uiController) {
        this.uiController=uiController;
        this.controller=controller;
    }
    
    
    @Override
    protected String getName() {
        return "Set User Profile";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new SetUserProfileUI(controller);
    }
    
}
