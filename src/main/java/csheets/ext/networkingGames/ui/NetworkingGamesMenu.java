package csheets.ext.networkingGames.ui;

import csheets.ext.networkingGames.NetworkingGamesController;
import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author 1130750
 */
public class NetworkingGamesMenu extends JMenu{
            
    public NetworkingGamesMenu(NetworkingGamesController controller,UIController uiController) {
        super("Networking Games");
        add(new SetUserProfileAction(controller,uiController));
    }    
}
