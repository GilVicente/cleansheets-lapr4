package csheets.ext.networkingGames.ui;

import csheets.ext.Extension;
import csheets.ext.comments.ui.CommentPanel;
import csheets.ext.emailConfiguration.ui.EmailMenu;
import csheets.ext.networkingGames.NetworkingGamesController;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 *
 * @author 1130750
 */
public class UIExtensionNetworkingGames extends UIExtension {

    /**
     * The menu of the extension
     */
    private JMenu menu;
    
    private JComponent sideBar;
    
    private NetworkingGamesController controller;//=new NetworkingGamesController();
    
    public UIExtensionNetworkingGames(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @see NetworkingGamesMenu
     * @return a JMenu component
     */
    @Override
    public JMenu getMenu() {
        if(controller==null) controller=new NetworkingGamesController();
        if (menu == null) {
            menu = new NetworkingGamesMenu(controller,uiController);
        }
        return menu;
    }
    
    @Override
    	public JComponent getSideBar() {
            if(controller==null) controller=new NetworkingGamesController();
		if (sideBar == null)
			sideBar = new NetworkingGamesPanel(controller);
		return sideBar;
	}
}
