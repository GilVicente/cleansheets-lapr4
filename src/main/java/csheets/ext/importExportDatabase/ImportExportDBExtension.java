/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportDatabase;

import csheets.ext.Extension;
import csheets.ext.importExportDatabase.ui.ImportExportDBExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * 
 * @author Gil
 */
public class ImportExportDBExtension extends Extension {

    public static final String NAME = "ImportExport Database";

    public ImportExportDBExtension() {
        super(NAME);
    }

    public UIExtension getUIExtension(UIController uiController) {
        return new ImportExportDBExtensionUI(this, uiController);
    }
}
