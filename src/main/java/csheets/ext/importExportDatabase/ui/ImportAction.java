/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportDatabase.ui;

import csheets.core.Cell;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 *
 * @author Gil
 */
public class ImportAction extends FocusOwnerAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public ImportAction(UIController uiController) {
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Import DataBase";
    }

    @Override
    protected void defineProperties() {
    }

    /**
     * Creates a panel to send a messagem
     *
     * @param event the event that was fired
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        try {
            ImportDataBasePanel e = new ImportDataBasePanel(uiController);
        } catch (Exception ex) {
            System.out.println(ex.getCause());
        }
    }
    
    public SpreadsheetTable getSpreadSheetTable(){
        return super.focusOwner;
    }
}