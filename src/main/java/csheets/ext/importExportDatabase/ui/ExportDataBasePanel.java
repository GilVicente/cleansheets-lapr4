/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportDatabase.ui;

import csheets.core.Spreadsheet;
import csheets.ext.importExportFile.JTextFieldLimit;
import csheets.ext.importExportFile.ui.ExportFileController;
import csheets.ui.ctrl.UIController;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Gil
 */
public class ExportDataBasePanel extends JDialog {

    private static final String DIALOG_TITLE = "Export to DataBase";
    private JTextField pathField;
    private final ExportDataBaseController controller;
    private final Spreadsheet sheet;
    private String dir;
    private String separator;
    private String headerText = "";
    private boolean headerLine;
    private JTextField sep;
    private JCheckBox header;
    private JCheckBox linked;
    private JPanel pathP;
    private File f;
    private JPanel headerPanel;
    private JTextField hed;
    private JPanel p;
    private JPanel aux, aux2, aux3, aux4;

    public ExportDataBasePanel(UIController uiController) {
        controller = new ExportDataBaseController(uiController);
        sheet = uiController.getActiveSpreadsheet();
        aux = new JPanel();
        aux2 = new JPanel();
        aux3 = new JPanel();
        aux4 = new JPanel();
        
        setSize(380, 460);
        setTitle(DIALOG_TITLE);
        JTextField tableName = new JTextField();
        tableName.setText("Table name:");
        Dimension dim = new Dimension(120,40);
        tableName.setPreferredSize(dim);
        tableName.setHorizontalAlignment(JTextField.CENTER);
        
        JTextField dbConnection = new JTextField();
        dbConnection.setText("Database URL:");
        dbConnection.setPreferredSize(dim);
        dbConnection.setHorizontalAlignment(JTextField.CENTER);
        
        JButton buttonOk = new JButton();
        buttonOk.setPreferredSize(dim);
        buttonOk.setText("EXPORT");
        buttonOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                
                controller.buttonOkActionPerformed(evt, ImportExportDBMenu.expAction.getSpreadSheetTable().getSelectedCells());
            }
        });
        
        //aux.add(Box.createHorizontalGlue());
        aux2.add(tableName);
        aux2.add(dbConnection);
        //aux.add(Box.createHorizontalGlue());
        aux3.add(buttonOk);
        
        aux.add(aux4);
        aux.add(aux2);
        aux.add(aux3);
        
        aux.setVisible(true);
        aux2.setVisible(true);
        aux3.setVisible(true);
        aux.setLayout(new GridLayout(3,1));
        
        
        super.add(aux);
        setLocationRelativeTo(null);
        setVisible(true);
        System.out.println("DATABASE!");
    }

}
