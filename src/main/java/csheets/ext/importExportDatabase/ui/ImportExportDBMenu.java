/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.importExportDatabase.ui;



import csheets.ext.share.ui.*;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author Gil
 */
public class ImportExportDBMenu extends JMenu {
    public static ExportAction expAction = null;
    public static ImportAction impAction = null;

    public ImportExportDBMenu(UIController uiController) {
        super("Import/Export DataBase");
        ExportAction exaction = new ExportAction(uiController);
        add(expAction = exaction);
        
        ImportAction imaction = new ImportAction(uiController);
        add(impAction = imaction);   
       
        
    }

}
