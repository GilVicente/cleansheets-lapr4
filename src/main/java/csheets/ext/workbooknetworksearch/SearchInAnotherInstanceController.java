/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbooknetworksearch;

import csheets.core.Workbook;
import csheets.ipc.connection.Broadcast;
import csheets.ipc.connection.TCPConnection;
import csheets.ui.ctrl.UIController;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Filipe
 */
public class SearchInAnotherInstanceController {

    private UIController uiController;
    private int port = Broadcast.PORT;
    private Socket connectionSocket;

    public SearchInAnotherInstanceController() {

    }

    public SearchInAnotherInstanceController(UIController uiController) {
        this.uiController = uiController;
    }

    public void makeConnection(String ip) throws IOException {
        try {
            InetAddress ipToConnect = InetAddress.getByName(ip);
            TCPConnection.sendTCPRequest(ipToConnect);
            connectionSocket = new Socket(ipToConnect, port);

        } catch (UnknownHostException ex) {
            System.out.println("Destinantion ip unknown");
        }
    }

    public List<Workbook> searchOpenedWorkbooks() {
        List<Workbook> listWorkbooks = new ArrayList<>();
        Workbook[] openWorkbooks = uiController.app().getWorkbooks();
        for (Workbook openWorkbook : openWorkbooks) {
            if (uiController.app().isWorkbookStored(openWorkbook)) {
                listWorkbooks.add(openWorkbook);
            }
        }

        return listWorkbooks;
    }

    public List<Workbook> getWorkbooks() throws IOException, ClassNotFoundException {
        ObjectInputStream inFromShare = new ObjectInputStream(connectionSocket.getInputStream());
        List<Workbook> listWorkbooks = (ArrayList<Workbook>) inFromShare.readObject();
        connectionSocket.close();
        
        return listWorkbooks;
    }
}
