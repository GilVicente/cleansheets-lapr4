/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbooknetworksearch.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.awt.event.ActionEvent;

/**
 *
 * @author Filipe
 */
public class SearchInAnotherInstanceAction extends FocusOwnerAction {
    
   protected UIController uiController;
   
   public SearchInAnotherInstanceAction(UIController uiController){
       this.uiController = uiController;
   }

    @Override
    protected String getName() {
        return "Search Another Instance";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       
    }
}
