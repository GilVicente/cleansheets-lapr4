/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbooknetworksearch.ui;

import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author Filipe
 */
public class WorkbookNetworkSearchMenu extends JMenu{
    
    public WorkbookNetworkSearchMenu(UIController uiController){
        super("Workbook Search Menu...");
          add(new SearchInAnotherInstanceAction(uiController));
    }
    
    
}
