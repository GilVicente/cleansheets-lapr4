/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbooknetworksearch.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JMenu;

/**
 *
 * @author Filipe
 */
public class SearchInAnotherInstanceExtensionUI extends UIExtension{
    
    
    private Icon icon;
    
    WorkbookNetworkSearchMenu menu;
    
    public SearchInAnotherInstanceExtensionUI(Extension extension, UIController uiController){
        super(extension, uiController);
    }
    
    public Icon getIcon(){
        return null;
    }
    
    public JMenu getMenu(){
       return null;
    }
}
