/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbooknetworksearch.ui;

import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author Filipe
 */
public class WorkbookNetworkSearchAction extends FocusOwnerAction {

    protected UIController uiController;
    
    public WorkbookNetworkSearchAction(UIController uiController){
        this.uiController = uiController;
    }
    @Override
    protected String getName() {
       return "Workbook Search";
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
    
    
}
