/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbooknetworksearch.ui;


import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 *
 * @author Filipe
 */
public class WorkbookNetworkSearchExtensionUI extends UIExtension{
    
    private Icon icon;
    
    private WorkbookNetworkSearchMenu menu;
    
    
    public WorkbookNetworkSearchExtensionUI(Extension extension, UIController uiControler){
        super(extension, uiControler);
    }
    
    public Icon getIcon(){
        return null;
    }
    
    public JMenu getMenu(){
       if(menu == null)
           menu = new WorkbookNetworkSearchMenu(uiController);
       return menu;
        
    }
    
    public JComponent getSideBar(){
        return null;
    }
    
}

