/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbooknetworksearch;

import csheets.ext.Extension;
import csheets.ext.workbooknetworksearch.ui.SearchInAnotherInstanceExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;

/**
 *
 * @author Filipe
 */
public class SearchInAnotherInstanceExtension extends Extension{
    /**
     * The name of the extension
     */
    public static final String NAME = "Search in Another Instance";
    
    /**
     * 
     */
    public SearchInAnotherInstanceExtension(){
        super(NAME);
    }
    /**
     * 
     * @param uIController
     * @return 
     */
    public UIExtension getUIExtension(UIController uIController){
        return new SearchInAnotherInstanceExtensionUI(this, uIController);
    }
    
   
    
}
