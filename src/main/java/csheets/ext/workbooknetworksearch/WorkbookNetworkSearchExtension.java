/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbooknetworksearch;

import csheets.ext.Extension;
import csheets.ext.workbooknetworksearch.ui.WorkbookNetworkSearchExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Filipe
 */
public class WorkbookNetworkSearchExtension extends Extension{
    
    /**
     * The name of the extension
     */
    public static final String NAME = "Workbook Search";
    
   /**
    * The constructor
    */
    public WorkbookNetworkSearchExtension(){
        super(NAME);
    }
    
    /**
     * 
     * @param uiController
     * @return 
     */
    public UIExtension getUIExtension(UIController uiController){
        return new WorkbookNetworkSearchExtensionUI(this, uiController);
                
    }
}
