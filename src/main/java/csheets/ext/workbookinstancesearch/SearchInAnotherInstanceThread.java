/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbookinstancesearch;

import csheets.core.Workbook;
import csheets.ext.workbooknetworksearch.SearchInAnotherInstanceController;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Filipe
 */
public class SearchInAnotherInstanceThread implements Runnable{
    

    private SearchInAnotherInstanceController controller;
    
    
    public SearchInAnotherInstanceThread(){
        controller = new SearchInAnotherInstanceController();
        
    }

    @Override
    public void run() {
        
        String ip = JOptionPane.showInputDialog(null, "IP?");
        
        try {
            controller.makeConnection(ip);
        } catch (IOException ex) {
            System.out.println("Unknown Destination Host");
        }
        
        System.out.println("Connection sucessfull");
        
        try {
            List<Workbook> instanceWorkbooksLst = controller.getWorkbooks();
            
        } catch (IOException ex) {
            Logger.getLogger(SearchInAnotherInstanceThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SearchInAnotherInstanceThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        
    }
    
}
