/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.workbookinstancesearch;

import java.net.InetAddress;

/**
 *
 * @author Filipe
 */
public class CleansheetsInstanceConnection {
    
    private int port;
    private InetAddress address;
    
    /**
     * Empty Constructor
     */
    public CleansheetsInstanceConnection(){
        
    }
    
    public CleansheetsInstanceConnection(InetAddress address, int port){
        this.address = address;
        this.port = port;
    }
    
    public int getCleansheetInstancePort(){
        return port;
    }
    
    public InetAddress getCleansheetInstanceAddress(){
        return address;
    }
    
    
}
