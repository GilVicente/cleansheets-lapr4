package csheets.ext.navigationWindow.ui;

import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.Spreadsheet;
import csheets.core.SpreadsheetImpl;
import csheets.ext.Extension;
import csheets.ext.navigationWindow.ExtensionNavigation;
import csheets.ext.navigationWindow.ui.NavigationController;
import csheets.ui.ctrl.EditEvent;
import csheets.ui.ctrl.EditListener;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * @author Antonio Soutinho
 *
 */
/**
 * A panel with the ability to navigate between the elements of a Workbook
 *
 */
@SuppressWarnings("serial")
public class NavigationWindowPanel extends JPanel implements EditListener {

    /**
     * The navigation window controller
     */
    private NavigationController controller;
    
    private UIController uicontroller;
    /**
     * JTree to display the elements of the workbook
     */
    private JTree tree = new JTree();
    private JTree extensionTree = new JTree();

    /**
     * Main panel of the sidebar
     */
    private JPanel navigationPanel = new JPanel();

    /**
     * Position
     */
    private int pos;

    /**
     * JScrollPane
     */
    private JScrollPane globalScroll = new JScrollPane(navigationPanel);

    /**
     * Creates a new navigation window.
     *
     * @param uiController the user interface controller
     */
    public NavigationWindowPanel(UIController uiController) {
        // Configures panel
        super(new BorderLayout());
        setName(ExtensionNavigation.NAME);
        navigationPanel.setLayout(new GridLayout(2, 1));
        // Creates controller
        controller = new NavigationController(uiController);
        this.uicontroller = uiController;
        uiController.addEditListener(this);
        globalScroll.setAlignmentX(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        add(globalScroll, BorderLayout.CENTER);
        
    }
    
    private void addTreeNodes() {
        
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Workbook");
        
        List<Spreadsheet> listSpreadsheet = controller.getSpreadsheetList();
        
        for (Spreadsheet s : listSpreadsheet) {
            DefaultMutableTreeNode sNode = new DefaultMutableTreeNode(s.getTitle());
            
            List<CellImpl> listCellValues = controller.getCellsValues(s);
            DefaultMutableTreeNode nameValues = new DefaultMutableTreeNode("Values");
            for (CellImpl cell : listCellValues) {
                DefaultMutableTreeNode values = new DefaultMutableTreeNode(cell.getValue());
                sNode.add(nameValues);
                nameValues.add(values);
            }
            List<String> listCellFormulas = controller.getCellsFormulas(s);
            DefaultMutableTreeNode nameOfFormulas = new DefaultMutableTreeNode("Formulas");
            for (String formula : listCellFormulas) {
                DefaultMutableTreeNode formulas = new DefaultMutableTreeNode(formula);
                sNode.add(nameOfFormulas);
                nameOfFormulas.add(formulas);
            }
            root.add(sNode);
        }
        
        DefaultMutableTreeNode extensionRoot = new DefaultMutableTreeNode("Extensions");
        List<UIExtension> listActiveExtensions = controller.getExtensionList();
        List<Extension> allExtensions = controller.getAllExtensions();
        
        List<UIExtension> unusedExtensions = controller.getInactiveExtensions(listActiveExtensions, allExtensions);
        
        DefaultMutableTreeNode activeNode = new DefaultMutableTreeNode("Active Extensions");
        extensionRoot.add(activeNode);
        for (UIExtension e : listActiveExtensions) {
            
            DefaultMutableTreeNode eNode = new DefaultMutableTreeNode(e.getExtension().getName());
            
            activeNode.add(eNode);
        }
        DefaultMutableTreeNode unactiveNode = new DefaultMutableTreeNode("All Extensions");
        extensionRoot.add(unactiveNode);
        for (UIExtension ue : unusedExtensions) {
            DefaultMutableTreeNode ueNode = new DefaultMutableTreeNode(ue.getExtension().getName());
            unactiveNode.add(ueNode);
        }
        
        tree = new JTree(root);
        extensionTree = new JTree(extensionRoot);
        
        navigationPanel.add(tree);
        navigationPanel.add(extensionTree);
        
        navigationPanel.revalidate();
        
        navigationPanel.repaint();
        
        navigationPanel.updateUI();
        MouseListener ml = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
                if (e.getClickCount() == 2) {
                    List<Spreadsheet> listSpreadsheet = controller.getSpreadsheetList();
                    for (Spreadsheet s : listSpreadsheet) {
                        List<CellImpl> listCellValues = controller.getCellsValues(s);
                        for (CellImpl f : listCellValues) {
                            if (selPath.getLastPathComponent().toString().trim().equals(f.getContent().toString().trim())) {
                                System.out.println("entrou");
                                uicontroller.setActiveCell((Cell) f);
                            }
                        }
                    }
                }
            }
        };
        
         MouseListener ml2 = new MouseAdapter() {
         public void mousePressed(MouseEvent e) {
                  TreePath path = extensionTree.getPathForLocation(e.getX(), e.getY());
                if (e.getClickCount() == 2) {
                    List<UIExtension> extensionList = controller.getExtensionList();
                    for (UIExtension uIExtension : extensionList) {
                        if (path.getLastPathComponent().toString().equals(uIExtension.getExtension().getName())){
                            JOptionPane.showMessageDialog(null, "Extension that allows the visualization of " +uIExtension.getExtension().getName());
                        }
                    }
                }
         }
         };
        
        
        
        extensionTree.addMouseListener(ml2);
        tree.addMouseListener(ml);
    }

    /**
     * Updates the tree when the Workbook is changed
     */
    public void updateTree() {
        tree.setVisible(false);
        navigationPanel.remove(tree);
        addTreeNodes();
    }
    
    @Override
    public void workbookModified(EditEvent event) {
        updateTree();
    }
//
//    public void activeInactiveExtensions() {
//
//        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Extensions");
//        List<UIExtension> listExtensions = controller.getExtensionList();
//
//        root.add((MutableTreeNode) listExtensions);
//
//        ExtensionTree = new JTree(root);
//        navigationPanel.add(ExtensionTree, BorderLayout.CENTER);
//        navigationPanel.revalidate();
//        navigationPanel.repaint();
//        navigationPanel.updateUI();
//        MouseListener ml = new MouseAdapter() {
//            public void mousePressed(MouseEvent e) {
//                TreePath selPath = ExtensionTree.getPathForLocation(e.getX(), e.getY());
//                if (e.getClickCount() == 2) {
//                    List<UIExtension> listExtensions = controller.getExtensionList();
//
//                }
//            }
//
//        };
//        ExtensionTree.addMouseListener(ml);
//    }

}
