package csheets.ext.navigationWindow.ui;

import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.Spreadsheet;
import csheets.core.SpreadsheetImpl;
import csheets.core.Value;
import csheets.core.formula.Formula;
import csheets.ext.Extension;
import csheets.ext.ExtensionManager;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

/**
 * @author Antonio Soutinho
 */
/**
 * Use Case 'Navigation Window' Controller.
 */
public class NavigationController {

    /**
     * The user interface controller
     */
    private UIController uiController;

    /**
     * The name of the files in which extension properties are stored
     */
    private static final String PROPERTIES_FILENAME = "extensions.props";
    private static final String EXTENSIONS_METADATA = "res/extensions_metadata.props";

    /**
     * User interface panel
     */
    private NavigationWindowPanel uiPanel;

    /**
     * Creates a new Navigation Window controller.
     *
     * @param uiController the user interface controller
     */
    public NavigationController(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * Return the list of spreadsheets in the current Workbook.
     *
     * @return list of spreadsheets
     */
    public List<Spreadsheet> getSpreadsheetList() {
        List<Spreadsheet> listSpreadsheets = new ArrayList<>();
        for (int i = 0; i < this.uiController.getActiveWorkbook().getSpreadsheetCount(); i++) {
            Spreadsheet sheet = this.uiController.getActiveWorkbook().getSpreadsheet(i);
            listSpreadsheets.add(sheet);
        }
        return listSpreadsheets;
    }

    /**
     * Returns the list of cells with the values in the spreadsheet received.
     *
     * @param s Spreadsheet
     * @return list of cells with values
     */
    public List<CellImpl> getCellsValues(Spreadsheet s) {
        List<CellImpl> listCellValue = new ArrayList<>();
        for (Spreadsheet spreadsheet : this.uiController.getActiveWorkbook()) {
            if (spreadsheet.equals(s)) {
                Iterator iteratorCell = spreadsheet.iterator();
                while (iteratorCell.hasNext()) {
                    CellImpl cell = (CellImpl) iteratorCell.next();
                    Value cellValue = (Value) cell.getValue();
                    if (cellValue.cellEmptyOrFilled()) {
                        listCellValue.add(cell);
                    }
                }
            }
        }

        return listCellValue;
    }

    public List<String> getCellsFormulas(Spreadsheet s) {
        List<String> listCellFormula = new ArrayList<>();
        for (Spreadsheet spreadsheet : this.uiController.getActiveWorkbook()) {
            if (spreadsheet.equals(s)) {
                Iterator iteratorCell = spreadsheet.iterator();
                while (iteratorCell.hasNext()) {
                    CellImpl cell = (CellImpl) iteratorCell.next();
                    String cellFormula = cell.getContent();
                    if (cellFormula != null && !cellFormula.isEmpty() && cellFormula.startsWith("=")) {
                        listCellFormula.add(cellFormula);
                    }
                }
            }
        }

        return listCellFormula;
    }

    public List<UIExtension> getExtensionList() {
        List<UIExtension> listExtensions = new ArrayList<>();
        ExtensionManager em = ExtensionManager.getInstance();
        em.getExtensions();

        UIExtension[] a = uiController.getExtensions();

        for (int i = 0; i < a.length; i++) {
            listExtensions.add(a[i]);

        }

        return listExtensions;
    }

    public ArrayList<Extension> getAllExtensions() {

        ExtensionManager em = ExtensionManager.getInstance();
        ArrayList<Extension> allExtensions = new ArrayList<>();

        InputStream stream = CleanSheets.class.getResourceAsStream(EXTENSIONS_METADATA);
        String inputStreamString = new Scanner(stream, "UTF-8").useDelimiter("\\A").next();

        String[] str = inputStreamString.split("\n");
        for (int i = 0; i < str.length; i++) {
            String temp = "";
            for (int j = 0; j < str[i].length(); j++) {
                char c = str[i].charAt(j);
                if (c != '-') {
                    temp += c;
                } else {
                    break;
                }

            }
            str[i] = temp;
            try {
                Class extensionClass = Class.forName(temp);

                Extension extension = (Extension) extensionClass.newInstance();
                allExtensions.add(extension);

            } catch (Exception err) {
                System.out.println("um");
            }

//           
        }

        return allExtensions;
    }

    public List<UIExtension> getInactiveExtensions(List<UIExtension> listActiveExtensions, List<Extension> allExtensions) {

        ArrayList<UIExtension> unusedExtensions = new ArrayList<>();
             

        for (Extension extension : allExtensions) {

            unusedExtensions.add(extension.getUIExtension(uiController));
        }

        for (UIExtension unusedExtension : unusedExtensions) {
            for (UIExtension activeExtension : listActiveExtensions) {

                if (activeExtension.toString().equals(unusedExtension.toString())) {
                    unusedExtensions.remove(activeExtension);
                }
            }
        }
        return unusedExtensions;
    }
}
