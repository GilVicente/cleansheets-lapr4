/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.navigationWindow.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.SideBarAction;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;

/**
 *
 * @author Antonio Soutinho
 */
public class UIExtensionNavigation extends UIExtension{
   /** The icon to display with the extension's name */
	private Icon icon;

        /** A panel in which the contacts are displayed */
	private JComponent sideBar;
        
        
        private JCheckBoxMenuItem menuSideBar;
        
    /**
     * Creates a new user interface extension.
     * @param extension the extension for which components are provided
     * @param uiController the user interface controller
     */
    public UIExtensionNavigation(Extension extension,UIController uiController) {
        super(extension, uiController);
    }
    
    /**
	 * Returns an icon to display with the extension's name.
	 * @return an icon with style
	 */
	public Icon getIcon() {
		return null;
	}


	/**
	 * Returns a side bar that gives access to extension-specific
	 * functionality.
	 * @return a component, or null if the extension does not provide one
	 */
	public JComponent getSideBar(){
            if (sideBar == null){
                sideBar = new NavigationWindowPanel(uiController); 
            }
			
		return sideBar;
	}	
              
        public JCheckBoxMenuItem getCheckBoxMenuItemSidebar() {
        if (menuSideBar == null) {
            menuSideBar = new JCheckBoxMenuItem(new SideBarAction(this, sideBar));
        }
        return menuSideBar;
    }
}
