/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.MessageSend.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.CellDecorator;
import csheets.ui.ext.TableDecorator;
import csheets.ui.ext.UIExtension;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 *
 * @author Tiago
 */
class MessageUIExtension extends UIExtension {

       /**
     * SubMenu which will be added to the application.
     */
    private MessageSubMenu submenu;

    /**
     * A side bar that shows the received messages
     */
    private JComponent sideBar;
    /**
     * The icon to display with the extension's name
     */
    private Icon icon;

    /**
     * Constructor of the class ShareCellUIExtension
     *
     * @param extension Extension which this UI refers to
     * @param uiController UIController
     */
    public MessageUIExtension(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    /**
     * Returns an icon to display with the extension's name.
     *
     * @return an icon with style
     */
    @Override
    public Icon getIcon() {
        return null;
    }

    /**
     * Method which will return the subMenu.
     *
     * @return the submenu which was created from the ShareCellSubMenu
     */
    @Override
    public JMenu getMenu() {
        if (submenu == null) {
            submenu = new MessageSubMenu(uiController);

        }
        return submenu;
    }


    /**
     * Returns a side bar that gives access to extension-specific functionality.
     *
     * @return a component, or null if the extension does not provide one
     */
    @Override
    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new MessageReceiveUI(uiController);
        }
        return sideBar;
    }
    
}
