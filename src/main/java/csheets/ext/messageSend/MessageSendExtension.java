/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.MessageSend.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Tiago
 */
public class MessageSendExtension extends Extension {
    
    public MessageSendExtension() {
        super("Message Send");
    }
    
    
      /**
     * Method which returns the UIExtension corresponding to the SendMessage
     * extension
     *
     * @param uiController The UIController used
     * @return UIExtension
     */
    @Override
    public UIExtension getUIExtension(UIController uiController) {

        return new MessageUIExtension(this, uiController);
    }
    
}
