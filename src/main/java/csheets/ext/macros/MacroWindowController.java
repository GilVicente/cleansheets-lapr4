/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.core.macro.Macro;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller of the MacroWindow
 *
 * @author 1140234 and Antonio Soutinho
 */
public class MacroWindowController {

    private UIController uiController;

    /**
     * Creates a new controller for this use case.
     *
     * @param uiController the main ui controller
     */
    public MacroWindowController(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * Creates a new macro with the given instructions and adds him to the macro Repo.
     *
     * @param name Name for the new Macro
     * @param instructions The macro's instructions 
     * @throws FormulaCompilationException if the instructions have wrong syntax
     */
    public void newMacro(String name, String instructions) throws FormulaCompilationException {
        validateMacroName(name);
        MacroWorkbookRepo mWorkbookRepo = (MacroWorkbookRepo) uiController.getActiveSpreadsheet().getExtension(MacrosExtension.NAME);
        Macro m = createMacro(mWorkbookRepo, name, instructions);
        mWorkbookRepo.addMacro(m);
    }
    
    /**
     * Creates a new macro
     * @param name name of the created macro
     * @param instructions instructions given to the created macro
     * @return Returns the new macro created
     * @throws FormulaCompilationException 
     */
    private Macro createMacro(MacroWorkbookRepo repo,String name, String instructions) throws FormulaCompilationException {
        Macro newMacro = new Macro(repo,name, uiController.getActiveSpreadsheet());
        String instructionsArr[] = instructions.split("\\n");
        for (String str : instructionsArr) {
            newMacro.addLine(str);
        }

        return newMacro;
    }
    
    /**
     * validates the macro's name
     * @param macroName Macro's given name
     */
    private void validateMacroName(String macroName) {
        if (getMacro(macroName) != null) {
            throw new IllegalArgumentException("Macro with identical name already exists");
        }
    }

    /**
     * Runs a macro
     *
     * @param instructions The instruction to run.
     * @param name
     * @return The end value of the macro.
     */

    public Value runMacro(String instructions, String name) throws FormulaCompilationException, IllegalValueTypeException {
        Macro m = createMacro((MacroWorkbookRepo) uiController.getActiveSpreadsheet().getExtension(MacrosExtension.NAME),name, instructions);
         //send repository macros
         return m.run();
     }

    /**
     * Returns the macro with the given name.
     *
     * @param macroName The name of the macro that will be returned.
     * @return The macro specified by the user
     */
    public Macro getMacro(String macroName) {
        Workbook book = uiController.getActiveWorkbook();
        MacroWorkbookRepo mWorkbookRepo;
        Macro m;
        int spCount = book.getSpreadsheetCount();
        for (int i = 0; i < spCount; i++) {
            mWorkbookRepo = (MacroWorkbookRepo) book.getSpreadsheet(i).getExtension(MacrosExtension.NAME);
            m = mWorkbookRepo.getMacro(macroName);
            if (m != null) {
                return m;
            }
        }

        return null;
    }

    /**
     * Returns a list of all available macros.
     *
     * @return List of available macros.
     */
    public List<String> listAvailableMacros() {
        Workbook book = uiController.getActiveWorkbook();
        MacroWorkbookRepo mWorkbookRepo;
        List<String> macroNames = new ArrayList<String>();
        int spCount = book.getSpreadsheetCount();
        for (int i = 0; i < spCount; i++) {
            mWorkbookRepo = (MacroWorkbookRepo) book.getSpreadsheet(i).getExtension(MacrosExtension.NAME);
            macroNames.addAll(mWorkbookRepo.listAvailableMacros());

        }

        return macroNames;
    }
}
