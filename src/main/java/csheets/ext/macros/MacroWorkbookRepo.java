/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros;

import csheets.core.Spreadsheet;
import csheets.core.macro.Macro;
import csheets.ext.SpreadsheetExtension;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class will persist the macros in the workbook that the user will open/load 
 * @author Antonio Soutinho
 * changed by Patrícia Monteiro (1140807)
 */
public class MacroWorkbookRepo extends SpreadsheetExtension implements Serializable {
    
    /**
    * Alocates the macros
    */
    private Map<String, Macro> macros;

	/**
	 * Creates a new spreadsheet extension capable of holding macros.
	 *
	 * @param delegate The spreadsheet that will hold the implementation
	 */
	public MacroWorkbookRepo(Spreadsheet delegate) {
                super(delegate, MacrosExtension.NAME);
		this.macros = new HashMap<String, Macro>();
	}

	/**
	 * Adds a macro to the current spreadsheet.
	 *
	 * @param macro The macro to be added.
	 */
	public void addMacro(Macro macro) {
		this.macros.put(macro.getName(), macro);
	}

	/**
	 * Returns a macro with the given name that the user wants
	 *
	 * @param macroName The name of the macro.
	 * @return The macro.
	 */
	public Macro getMacro(String macroName) {
		return this.macros.get(macroName);
	}

        
        public Collection<Macro> values()
        {
            return this.macros.values();
        }
	/**
	 * Lists all the available macros in the spreadsheet.
	 *
	 * @return List of the name of all the available macros.
	 */
	public List<String> listAvailableMacros() {
		return new ArrayList<String>(this.macros.keySet());
	}

    
}
