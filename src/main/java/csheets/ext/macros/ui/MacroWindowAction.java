/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.ui;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;

/**
 *
 * @author 1140234
 */
public class MacroWindowAction extends BaseAction {

    /**
     * The User Interface Controller
     */
    protected UIController uiController;

    /**
     * Creates a new MacroEditor action.
     *
     * @param uiController the user interface controller
     */
    public MacroWindowAction(UIController uiController) {
        this.uiController = uiController;
    }

    /**
     * Returns the name of the action to be used in the user interface
     *
     * @return the name of the action
     */
    @Override
    protected String getName() {
        return "MacroWindow";
    }

    /**
     * The code fired when action is performed
     *
     * @param event the action event
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        MacroWindowUI ui = new MacroWindowUI(null, uiController);
    }

}
