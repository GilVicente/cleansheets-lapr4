/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JMenu;

/**
 *
 * @author 1140234
 */
public class UIExtensionMacros extends UIExtension {

    /**
     * The menu of the extension
     */
    private MacrosMenu menu;

    /**
     * The constructor of the UIExtensionMacros
     *
     * @param extension the extension
     * @param uiController the main user interface controller
     */
    public UIExtensionMacros(Extension extension,
            UIController uiController) {
        super(extension, uiController);
    }

    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @return a JMenu component
     */
    @Override
    public JMenu getMenu() {
        if (menu == null) {
            menu = new MacrosMenu(uiController);
        }
        return menu;
    }
}
