/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macros.ui;

import csheets.ext.macros.MacrosExtension;
import csheets.ui.ctrl.UIController;
import javax.swing.JMenu;

/**
 *
 * @author brafa
 */
public class MacrosMenu extends JMenu {

    /**
     * Creates a new MacrosMenu menu.
     *
     * @param uiController the user interface controller
     */
    public MacrosMenu(UIController uiController) {
        super(MacrosExtension.NAME);
        add(new MacroWindowAction(uiController));
    }
}
