/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.editNotes;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 * Class ContactNote featured by last title , the last text and hair last timestamp
 *
 * @author Daniela Ferreira 
 */
@Entity
public class ContactNote implements Serializable {

    @Id
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /** The latest title */
    private String latestTitle;

    /**
     * The Latest text
     */
    private String latestText;

    /**
     * Date of note
     */
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar latestTimestamp;

    
    /**
     * Tree with versions of notes
     */
    private HashMap<Calendar, String> versions;

    public ContactNote() {
    }

    /**
     * Creates a note with the given text
     * @param text of note
     */
    public ContactNote(String text) {
        versions = new HashMap<>();
        setLatestVersion(text);
    }

    /**
     * Adds the latest version with the given text
     * @param text of note
     */
    public void addVersion(String text) {
        setLatestVersion(text);
    }

    /**
     * Returns Tree with versions of notes
     * @return Tree with versions of notes
     */
    public HashMap<Calendar, String> getNotes() {
        return this.versions;
    }

    /**
     * Gets latest timeStamp
     * @return latestsTimeStamp
     */
    public Calendar getLatestTimestamp() {
        return this.latestTimestamp;
    }

    /**
     * Gets Title of note
     * @return latestTitle
     */
    public String getTitle() {
        return this.latestTitle;
    }

    /**
     * Assigns the last note as the latest version
     * @param text note
     */
    private void setLatestVersion(String text) {
        Calendar now = Calendar.getInstance();
        this.latestTimestamp = now;

        String[] tmp = text.split("\n");
        if (tmp.length == 1) {
            this.latestTitle = text;
            this.latestText = "";
        } else {
            this.latestTitle = tmp[0];
            this.latestText = "";
            for (int i = 1; i < tmp.length; i++) {
                this.latestText += tmp[i];
            }
        }
        versions.put(now, text);
    }

}
