/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.editNotes.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.editNotes.ContactNote;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.tree.TreePath;

/**
 * Window that allows the user to create a new ContactNote associated with a
 * Contact.
 *
 * @author Daniela Ferreira
 */
public class NewNoteDialog extends JDialog {

    /**
     * List of contacts of the user.
     */
    private List<Contact> contacts;

    /**
     * Select/Option that allow the user to select a specific version.
     */
    private JComboBox selectContacts;

    /**
     * Text area for the user to write the ContactNote.
     */
    private JTextArea noteText;

    /**
     * The Use Case Controller.
     */
    private EditNotesController controller;

    public NewNoteDialog(final EditNotesController controller, final List<Contact> contacts, TreePath selectedContact) {

        this.controller = controller;
        this.contacts = contacts;

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        String selectedContactString = "";
        String[] contactsList = new String[contacts.size()];
        if (selectedContact != null) {

            //Rever funcionamento treePath
            Object[] path = selectedContact.getPath();
            selectedContactString = path[1].toString();
        }

        int selectIndex = 0;
        for (int i = 0; i < contacts.size(); i++) {
            contactsList[i] = contacts.get(i).Firstname() + " " + contacts.get(i).Lastname();
            if (selectedContact != null) {
                if (contactsList[i].equals(selectedContactString)) {
                    selectIndex = i;
                }
            } else {
                selectIndex = 0;
            }
        }

        selectContacts = new JComboBox(contactsList);
        selectContacts.setSelectedIndex(selectIndex);

//        selectContacts.addActionListener(this);
        noteText = new JTextArea();
        JPanel bottomPanel = new JPanel();
        //bottomPanel.setLayout(new BorderLayout());

        JButton addNote = new JButton("Add Note");

        addNote.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Contact selectedContact = contacts.get(selectContacts.getSelectedIndex());
                ContactNote note = new ContactNote(noteText.getText());
                if (selectedContact.addNote(note)) {
                    controller.addNote(selectedContact, note);
                   
                    JOptionPane.showMessageDialog(null, "Contact Note was successefuly added");
                    controller.refreshPanel();
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "An error has occured, the note could not be created");
                    dispose();
                }
            }
        });

        JButton cancelNote = new JButton("Cancel");
        cancelNote.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        bottomPanel.add(addNote);
        bottomPanel.add(cancelNote);

        add(selectContacts, BorderLayout.NORTH);
        add(noteText, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);

        setLocationRelativeTo(null);
        setSize(500, 500);
        setVisible(true);
    }

}
