/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.editNotes.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.editNotes.ContactNote;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.tree.TreePath;

/**
 *
 * @author Daniela Ferreira 
 */
public class OpenNoteDialog extends JDialog {

    /**
     * List of contacts of the user.
     */
    private List<Contact> contacts;

    /**
     * Select/Option that allow the user to select a specific version.
     */
    private JComboBox selectVersion;

    /**
     * Text area to show the Note's text.
     */
    private JTextArea noteText;
    
    /**
     * Use Case Controller.
     */
    private EditNotesController controller;

    /**
     * The selected Note to be viewed or edited.
     */
    private ContactNote note;
    
    /**
     * Stores a list of all the timestamps in order for the user to select one in the ComboBox.
     */
    private List<Calendar> calendarVersions;
    
    /**
     * A Maximum limit to the note's versions.
     */
    private static final int MAX_VERSIONS = 1000;

    
    public OpenNoteDialog(final EditNotesController controller, final List<Contact> contacts, TreePath selectedContact) {

        this.controller = controller;
        this.contacts = contacts;
        TreePath selContact = selectedContact;
        
        setLayout(new BorderLayout());
        
        if (findNote(selectedContact)) {
            showNoteAndVersions();
        } else {
            JOptionPane.showMessageDialog(null, "An error has occured trying to find the Contact Note. Aborting ...");
        }

        setLocationRelativeTo(null);
        setSize(500, 500);
        setVisible(true);

    }

    /**
     * Searches for the particular note selected by the user.
     * @param selectedContact
     * @return bool
     */
    private boolean findNote(TreePath selectedContact) {
        Object[] path = selectedContact.getPath();
        String selectedContactString = path[2].toString();
        String[] tmp = selectedContactString.split(",");

        for (Contact contact : this.contacts) {
            List<ContactNote> notes = contact.getContactNotes();

            for (ContactNote note : notes) {
                if (tmp[0].equals(note.getLatestTimestamp().getTime().toString())) {
                    this.note = note;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Shows the user all the versions of the note, and allows him to edit any version.
     */
    private void showNoteAndVersions() {
        
        Iterator entries = note.getNotes().entrySet().iterator();
        
        String[] tmp = new String[MAX_VERSIONS];
        int i=0;
        calendarVersions = new ArrayList<>();
        
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            Calendar timestampObj = (Calendar) thisEntry.getKey();
            Object textObj = thisEntry.getValue();

            tmp[i] = timestampObj.getTime().toString();
            calendarVersions.add((Calendar) thisEntry.getKey());
            i++;
        }
        
        String[] versions = new String[i];
        for (int j=0; j < i; j++) {
            versions[j] = tmp[j];
        }
        
        selectVersion = new JComboBox(versions);
        selectVersion.setSelectedIndex(versions.length-1);
        
        selectVersion.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                noteText.setVisible(false);
                remove(noteText);
                noteText = new JTextArea((String) note.getNotes().get(calendarVersions.get(selectVersion.getSelectedIndex())));
                add(noteText, BorderLayout.CENTER);
            }
        });
        
        
        noteText = new JTextArea((String) note.getNotes().get(calendarVersions.get(versions.length-1)));
        
        JPanel bottomPanel = new JPanel();
        JButton editButton = new JButton("Save");
        editButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                note.addVersion(noteText.getText());
                selectVersion.setVisible(false);
                noteText.setVisible(false);
                
                remove(selectVersion);
                remove(noteText);
                
                showNoteAndVersions();
                controller.refreshPanel();
            }
        });
        
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.refreshPanel();
                dispose();
            }
        });
        
        bottomPanel.add(editButton);
        bottomPanel.add(cancelButton);
        
        add(selectVersion, BorderLayout.NORTH);
        add(noteText, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);

    }
}
