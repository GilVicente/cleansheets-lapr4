/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.editNotes.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.persistence.ContactsJpaRepository;
import csheets.ext.editNotes.ContactNote;
import csheets.ui.ctrl.UIController;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniela Ferreira
 */
public class EditNotesController {

    /**
     * The user interface controller
     */
    private UIController uiController;

    /**
     * User interface panel *
     */
    private EditNotesPanel uiPanel;
    /**
     * List of contacts of the user.
     */
    private List<Contact> contactsList;

    /**
     * Creates a new MangeExtensions controller.
     *
     * @param uiController the user interface controller
     * @param uiPanel the user interface panel
     */
    public EditNotesController(UIController uiController, EditNotesPanel uiPanel) {
        this.uiController = uiController;
        this.uiPanel = uiPanel;
//        createDummyContactList();
        ContactsJpaRepository rep = new ContactsJpaRepository();
        rep.allContacts();
    }

    public EditNotesController() {
    }

    /**
     * Returns a list of Extensions and their status (on/off).
     *
     * @return extList
     */
    public String[] getExtensionsListWithStatus() {
        return null;
    }

    /**
     * Returns the list of Contacts of the User.
     *
     * @return contactsList
     */
    public List<Contact> getContactsList() {
        return this.contactsList;
    }

    public List<Contact> getContacts() {

        ContactsJpaRepository rep = new ContactsJpaRepository();
        return rep.allContacts();

    }

    /**
     * Since the DB connection is not working, it was necessary to create some
     * dummy contacts in order to develop the Use Case functionality.
     */
    public void createDummyContactList() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Sara", "Costa", null));
        contacts.add(new Contact("Ken", "Follet", null));
        contacts.add(new Contact("Lars", "Kepler", null));
        this.contactsList = contacts;
    }

    /**
     * Adds a new ContactNote to a Contact.
     *
     * @param contact Contact
     * @param note from Contact
     */
    public void addNote(Contact contact, ContactNote note) {
        contact.addNote(note);
    }

    /**
     * Refreshes the 'Notes' Sidebar after updates.
     */
    public void refreshPanel() {
        this.uiPanel.refreshNotes();
    }
}
