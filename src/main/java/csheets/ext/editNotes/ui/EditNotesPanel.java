/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.editNotes.ui;

import csheets.ext.contacts.Contact;
import csheets.ext.editNotes.ContactNote;
import csheets.ext.editNotes.EditNotesExtension;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author Daniela Ferreira
 */
public class EditNotesPanel extends JPanel {

    /**
     * The Use Case Controller.
     */
    private EditNotesController controller;
    /**
     * Panel to display a list of Contacts and ContactNotes.
     */
    private JPanel notesPanel;
    /**
     * JTree to display the Notes inside the Contact.
     */
    private JTree tree;

    /**
     * List of contacts of the user.
     */
    private List<Contact> contactsList;

    /**
     * Creates a new notes panel.
     *
     * @param uiController the user interface controller
     */
    public EditNotesPanel(UIController uiController) {
        super(new BorderLayout());
        setName(EditNotesExtension.NAME);

        // Creates controller
        controller = new EditNotesController(uiController, this);
        String[] extList = {""};

        notesPanel = new JPanel();
        notesPanel.setLayout(new BorderLayout());
        notesPanel.setPreferredSize(new Dimension(130, 336));
        notesPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height

        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Contacts");
        List<Contact> contactsList = controller.getContacts();
        for (Contact contact : contactsList) {
            DefaultMutableTreeNode c = new DefaultMutableTreeNode(contact.Firstname() + " " + contact.Lastname());
            if (contact.getContactNotes() != null) {
                for (ContactNote note : contact.getContactNotes()) {
                    DefaultMutableTreeNode n = new DefaultMutableTreeNode(note.getLatestTimestamp().getTime() + ", " + note.getTitle());
                    c.add(n);
                }
            }
            root.add(c);
        }

        tree = new JTree(root);
        notesPanel.add(tree, BorderLayout.CENTER);

        JButton newBtn = new JButton("New Note");
        newBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new NewNoteDialog(controller, contactsList, tree.getSelectionPath());
                refreshNotes();
            }
        });

        JButton openBtn = new JButton("Open Note");
        openBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int size = tree.getSelectionPath().getPath().length;
                if (size != 3) {
                    JOptionPane.showMessageDialog(null, "No Note selected. Please select a Note before trying to open it.");
                } else {
                   new OpenNoteDialog(controller, contactsList, tree.getSelectionPath());
                }
            }
        });

        JButton removeBtn = new JButton("Remove Note");
        removeBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int size = tree.getSelectionPath().getPath().length;
                if (size != 3) {
                    JOptionPane.showMessageDialog(null, "No Note selected. Please select a Note before trying to open it.");
                } else {
                    Object[] path = tree.getSelectionPath().getPath();
                    String[] tmp = path[2].toString().split(",");
                    for (Contact c : contactsList) {
                        for (ContactNote note : c.getContactNotes()) {
                            if (tmp[0].equals(note.getLatestTimestamp().getTime().toString())) {
                                c.removeNote(note);
                                refreshNotes();
                            }

                        }
                    }
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BorderLayout());

        buttonsPanel.add(newBtn, BorderLayout.NORTH);
        buttonsPanel.add(openBtn, BorderLayout.CENTER);
        buttonsPanel.add(removeBtn, BorderLayout.SOUTH);

        notesPanel.add(buttonsPanel, BorderLayout.SOUTH);

        // Adds borders
        TitledBorder border = BorderFactory.createTitledBorder("Notes");
        border.setTitleJustification(TitledBorder.CENTER);
        notesPanel.setBorder(border);

        // Adds panels
        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(notesPanel, BorderLayout.NORTH);
        add(northPanel, BorderLayout.NORTH);
    }

    /**
     * Refreshes the 'Notes' info after an update.
     */
    public void refreshNotes() {
        tree.setVisible(false);
        notesPanel.remove(tree);
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Contacts");
        List<Contact> contactsList = controller.getContacts();

        for (Contact contact : contactsList) {
            DefaultMutableTreeNode c = new DefaultMutableTreeNode(contact.Firstname() + " " + contact.Lastname());
            for (ContactNote note : contact.getContactNotes()) {
                //System.out.println("New Note: " + note.getLatestTimestamp().toString() + ", " + note.getTitle());
                DefaultMutableTreeNode n = new DefaultMutableTreeNode(note.getLatestTimestamp().getTime() + ", " + note.getTitle());
                c.add(n);
            }
            root.add(c);
        }

        tree = new JTree(root);
        notesPanel.add(tree, BorderLayout.CENTER);
    }

}
