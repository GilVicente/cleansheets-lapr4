/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.findFiles;

import csheets.ext.Extension;
import csheets.ext.share.ui.ActiveInstancesPanel;
import csheets.ext.share.ui.ShareMenu;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 *
 * @author Tiago Lacerda
 */
public class FindFilesUI extends UIExtension {

    private FindFileMenu menu;
    private JComponent sideBar;

    public FindFilesUI(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    public JMenu getMenu() {
        if (menu == null) {
            menu = new FindFileMenu();
        }
        return menu;
    }

    public JComponent getSideBar() {
        if (sideBar == null) {
            sideBar = new FindFilesPanel();
        }
        return sideBar;
    }
}
