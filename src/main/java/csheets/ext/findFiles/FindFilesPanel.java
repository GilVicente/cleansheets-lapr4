/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.findFiles;

/**
 *
 * @author Tiago Lacerda
 */
import csheets.CleanSheets;
import csheets.core.Cell;
import csheets.core.CellImpl;
import csheets.core.CellListener;
import csheets.core.Workbook;
import csheets.ext.style.StylableCell;
import csheets.ipc.connection.Broadcast;
import csheets.ipc.connection.TCPConnection;
import csheets.ui.FileChooser;
import csheets.ui.ctrl.SelectionEvent;
import csheets.ui.ctrl.SelectionListener;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTableModel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.EventListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.Marshaller.Listener;

public class FindFilesPanel extends JPanel implements SelectionListener {

    private FindFilesController controller = new FindFilesController(this);
    private JComponent sideBar;
    private JList listBox = new JList();
    protected CleanSheets app;

    public FindFilesPanel() {
        super(new BorderLayout());
        setName(FindFilesExtension.NAME);
        controller = new FindFilesController( this);
        this.app= CleanSheets.getCleansheets();
        String answer = askRoot();
        controller.setDirectory(answer);

       

        ApplyAction applyAction = new ApplyAction();
        listBox.setPreferredSize(new Dimension(120, 240));		// width, height
        listBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        listBox.addFocusListener(applyAction);
        listBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        instancesListUpdate();
        

        listBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() % 2 == 0 && !e.isConsumed()) {
                    e.consume();
                    
                    File f = (File) listBox.getSelectedValue();
                    Workbook workbook = app.getWorkbook(f);
                    String location = f.getAbsolutePath();
            
			
				try {
					// Opens the file
					app.load(f);
				} catch (FileNotFoundException fe) {
					showErrorDialog("The file you requested was not found.");
			
				} catch (InvalidClassException err) {
					showErrorDialog("Something is wrong with a class used by serialization.");
				} catch (StreamCorruptedException err) {
					showErrorDialog("Control information in the input stream is inconsistent.");
				} catch (OptionalDataException err) {
					showErrorDialog("Primitive data was found in the input stream instead of objects.");
				} catch (Exception err) {
					showErrorDialog("An I/O error occurred when loading the file.");
				}
                

                }
            }

            @Override
            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }
        });

        // Lays out comment components
        JPanel commentPanel = new JPanel();
        commentPanel.setLayout(new BoxLayout(commentPanel, BoxLayout.PAGE_AXIS));
        commentPanel.setPreferredSize(new Dimension(130, 336));
        commentPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        commentPanel.add(listBox);

        // Adds borders
        TitledBorder border = BorderFactory.createTitledBorder("Files found in directory:");
        border.setTitleJustification(TitledBorder.CENTER);
        commentPanel.setBorder(border);

        // Adds panels
        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(commentPanel, BorderLayout.NORTH);
        add(northPanel, BorderLayout.NORTH);

//        InstancesListUpdate t = new InstancesListUpdate();
//        t.start();
    }

    @Override
    public void selectionChanged(SelectionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String askRoot() {
        
        JFileChooser f = new JFileChooser();
        
        f.setDialogTitle("Import File");
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int userSelection = f.showOpenDialog(null);

        if (userSelection != JFileChooser.APPROVE_OPTION) {
            return null;
        }

        File folder = f.getSelectedFile();

        return folder.getAbsolutePath();
    }

    protected class ApplyAction implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            //TODO
        }

        @Override
        public void focusLost(FocusEvent e) {
            //Do nothing
        }

    }

    public void instancesListUpdate() {

        DefaultListModel model = new DefaultListModel();
        for (File f : controller.getCleanSheetsInstances()) {
            model.addElement(f);
        }
        listBox.setModel(model);

    }
    
    protected void showErrorDialog(Object message) {
		JOptionPane.showMessageDialog(
			null,
			message,
			"Error",
			JOptionPane.ERROR_MESSAGE
		);
	}

}
