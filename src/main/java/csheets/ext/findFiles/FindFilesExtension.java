/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.findFiles;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Tiago Lacerda
 */
public class FindFilesExtension extends Extension {
    
    public static final String NAME = "FindFilesExtension";
    
    public FindFilesExtension(){
        super(NAME);
    }
    
    @Override
    public UIExtension getUIExtension(UIController uiController) {
		return new FindFilesUI(this, uiController);
	}
}

    
