/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.findFiles;

import csheets.CleanSheets;
import csheets.io.FilenameExtensionFilter;
import csheets.ui.ctrl.UIController;
import java.io.File;
import java.io.FilenameFilter;
import javax.mail.Folder;

/**
 *
 * @author Tiago Lacerda
 */
public class FindFilesController {

   

    private FindFilesPanel uiPanel;
    File directory ;

    public FindFilesController(FindFilesPanel uiPanel) {

        this.uiPanel = uiPanel;
    }

    public void setDirectory(String root){
        this.directory= new File(root);
    }
    
    public File[] getCleanSheetsInstances() {
       
        File[] matchedFiles = directory.listFiles(new FilenameFilter(){
            public boolean accept (File dir, String name){
                return name.endsWith(".cls");
            }
        });
        
        return matchedFiles;
    }

}
