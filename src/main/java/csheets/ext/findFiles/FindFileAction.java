/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.findFiles;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;

/**
 *
 * @author Tiago Lacerda
 */
public class FindFileAction extends BaseAction {

    @Override
    protected String getName() {
        return "FindFileAction";
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        new FindFilesPanel();
    }
    
    
}
