/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.findFiles;

import javax.swing.JMenu;

/**
 *
 * @author Tiago Lacerda
 */
public class FindFileMenu extends JMenu{
    public static FindFileAction action =null;
    
    public FindFileMenu(){
        super("Find Files");
        add(action = new FindFileAction());

        action.setEnabled(true);
        
    }
}
        
