/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chatMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author António
 */
public class MessagesReceived {

    public static List<String> listOfReceivedMessages;

    public MessagesReceived() {
        this.listOfReceivedMessages = new ArrayList<>();
    }

    public static void addNewMessage(String msg) {
        listOfReceivedMessages.add(msg);
    }

    public List<String> listOfReceivedMsg() {
        return listOfReceivedMessages;
    }
    
    public void setListOfReceivedMsg(List<String> listOfReceivedMessages) {
        this.listOfReceivedMessages = listOfReceivedMessages;
    }
}
