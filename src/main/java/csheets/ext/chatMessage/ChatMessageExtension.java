/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chatMessage;

import csheets.ext.Extension;
import csheets.ext.chatMessage.ui.MessageExtensionUI;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Tiago
 */
public class ChatMessageExtension extends Extension{
    
    
    public static final String NAME = "Message chat";
    
    
    public ChatMessageExtension() {
        super(NAME);
    }
    
    public UIExtension getUIExtension(UIController uiController) {
		return new MessageExtensionUI(this,uiController);
	}
}
