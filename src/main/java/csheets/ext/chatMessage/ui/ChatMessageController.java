/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chatMessage.ui;

import csheets.ext.chatMessage.MessagesReceived;
import java.util.List;

/**
 *
 * @author António
 */
public class ChatMessageController {
    
    private MessagesReceived msgReceived;
    
    public ChatMessageController(MessagesReceived msgReceived) {
        this.msgReceived = msgReceived;
    }
    
    public void addNewMessage(String msg){
        msgReceived.addNewMessage(msg);
    }
    
    public List<String> listOfMessages(){
        return msgReceived.listOfReceivedMsg();
    }
    
    
}
