/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chatMessage.ui;


import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.SideBarAction;
import csheets.ui.ext.UIExtension;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;

/**
 *
 * @author Tiago
 */
public class MessageExtensionUI extends UIExtension{
    
    private JCheckBoxMenuItem menuSideBar;
    
    public static String NAME = "Message chat";
    
    private JComponent sideBar;

    public MessageExtensionUI(Extension extension, UIController uiController) {
        super(extension, uiController);
    }
    
    public JMenu getMenu(){
        return null;
    }
    
    public JComponent getSideBar(){
        if (sideBar == null)
			sideBar = new ChatMessagePanel();
		return sideBar;
    }
    
    public JCheckBoxMenuItem getCheckBoxMenuItemSidebar() {
        if (menuSideBar == null) {
            menuSideBar = new JCheckBoxMenuItem(new SideBarAction(this, sideBar));
        }
        return menuSideBar;
    }
    
}
