/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.chatMessage.ui;

import csheets.ext.chatMessage.ChatMessageExtension;
import csheets.ext.chatMessage.MessagesReceived;
import csheets.ipc.connection.ServerClientTcp;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

/**
 *
 * @author António
 */
public class ChatMessagePanel extends JPanel {

    private JPanel chatMsgPanel = new JPanel();
    private JLabel messages = new JLabel("--- Messages in chat ----");
    private JTextArea msgReceived = new JTextArea(10, 20);
    private JButton buttonSendMsg = new JButton("Send Message");
    private JButton refreshMsg = new JButton("Refresh Messages");
    /**
     * JScrollPane
     */
    private JScrollPane globalScroll = new JScrollPane(chatMsgPanel);

    private ChatMessageController controller;
    private MessagesReceived msgRvd;
    private DefaultListModel<String> msgList;
    private JList jListMsgs = new JList();

    public ChatMessagePanel() {
        super(new BorderLayout());
        setName(ChatMessageExtension.NAME);
        this.msgRvd = new MessagesReceived();
        this.controller = new ChatMessageController(msgRvd);
        this.msgList = new DefaultListModel<>();
        chatMsgPanel.setLayout(new BoxLayout(chatMsgPanel, BoxLayout.Y_AXIS));
        chatMsgPanel.setOpaque(true);
        globalScroll.setAlignmentX(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        msgReceived.setAlignmentX(CENTER_ALIGNMENT);
        buttonSendMsg.setAlignmentX(TOP_ALIGNMENT);
        msgReceived.setAlignmentX(BOTTOM_ALIGNMENT);
        buttonSendMsg.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                ServerClientTcp t = new ServerClientTcp();
//                t.setVar(1);
//                Thread b = new Thread(t.getServer());
//                b.start();
                new SendMessage(controller).setVisible(true);
            }
        });
        refreshMsg.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateMessages();
            }
        });
        msgList.addElement("");
        jListMsgs.setModel(msgList);
        add(globalScroll, BorderLayout.CENTER);
        add(buttonSendMsg, BorderLayout.NORTH);
        add(jListMsgs, BorderLayout.CENTER);
        add(refreshMsg, BorderLayout.PAGE_END);
        MouseListener mouseList = new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 2) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (index >= 0) {
//                        Object o = theList.getModel().getElementAt(index);
//                        System.out.println("Double-clicked on: " + o.toString());
                        ServerClientTcp t = new ServerClientTcp();
                        t.setVar(1);
                        Thread b = new Thread(t.getServer());
                        b.start();
                        new SendMessage(controller).setVisible(true);
                    }
                }
            }
        };
        jListMsgs.addMouseListener(mouseList);
    }

    private void updateMessages() {
        msgList.addElement(controller.listOfMessages().toString());
        jListMsgs.setModel(msgList);
    }
}
