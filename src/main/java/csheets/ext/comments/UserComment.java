package csheets.ext.comments;

import java.awt.Font;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * This class represents an user associated comment
 */
public class UserComment implements Serializable, Cloneable {

    private String user;
    private String comment;
    private Font font;
    private Date modificationDate;

    public UserComment(String comment) {
        this.comment = comment;
        updateUser();
        this.poke();
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        if (!commentWithUser().equals(comment)) {
            this.comment = comment;
            updateUser();
        }
    }

    /**
     * updates the user in case it has changed
     */
    public void updateUser() {
        this.user = System.getProperty("user.name");
    }

    /**
     *
     * @return comment with reference to the user that created it
     */
    public String commentWithUser() {
        String userReference=" by user "+getUser();
        String uc = getComment();
        if (uc == null) return "";
        if (uc.endsWith(userReference)) {
            return uc;
        } else {
            return uc + userReference;
        }
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Returns the current Font.
     * @return Font.
     */
    public Font getFont() {
        return font;
    }
    
    /**
     * Gets the date that the user comment was modificated.
     * @return Modification date.
     */
    public Date getDate(){
        return this.modificationDate;
    }

    /**
     * Sets a new current Font.
     * @param font new Font.
     */
    public void setFont(Font font) {
        this.font = font;
    }
    
    /**
     * Pokes the UserComment, in order to update the modification time to the system's current time.
     */
    public void poke(){
        this.modificationDate = new Date();
    }
    
    
    @Override
    /**
     * Returns a clone of this instance.
     * @return New UserComment.
     */
    public UserComment clone() throws CloneNotSupportedException{
        UserComment u = new UserComment(this.comment);
        u.setFont(this.font);
        
        return u;
    }
    
    @Override
    public boolean equals(Object obj){
        if (obj == null || !(obj instanceof UserComment)) return false;
        
        UserComment u = (UserComment)obj;
        
        if (!u.getComment().equals(this.getComment()) ||
                !u.getUser().equals(this.getUser())) return false;
        
        if (this.getFont() == null && u.getFont() != null) return false;
        if (this.getFont() != null && u.getFont() == null) return false;
        if (this.getFont() == null && u.getFont() == null) return true;
        
        return this.getFont().equals(u.getFont());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.user);
        hash = 97 * hash + Objects.hashCode(this.comment);
        hash = 97 * hash + Objects.hashCode(this.font);
        return hash;
    }
    
    @Override
    public String toString(){
        return String.format("Autor: %s\n%s\n%s\nModificated date: %s",
                this.user, this.comment, this.font == null ? "<no Font>" :
                        this.font.toString(), this.modificationDate);
    }
}
