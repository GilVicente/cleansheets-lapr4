/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.comments.ui;

import csheets.ext.comments.CommentableCell;
import csheets.ext.comments.UserComment;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author JOSENUNO
 */
public class CommentHistoryDialog extends JDialog {

    /**
     * Selected Cell.
     */
    private CommentableCell cell;

    /**
     * Panel containing all other components.
     */
    private JPanel panel;

    /**
     * Parent's Panel.
     */
    private CommentPanel parent;

    /**
     * List that shows the previous modifications of the comments, at the
     * selected Cell.
     */
    private JList historyList = new JList(new DefaultListModel());

    /**
     * Creates a JDialog that shows the history log of the comments of the
     * selected Cell.
     *
     * @param parent Parent that shows this dialog.
     * @param panel Panel that invokes this dialog.
     * @param cell Selected Cell.
     */
    public CommentHistoryDialog(Frame parent, CommentPanel panel, CommentableCell cell) {
        super(parent, "History", true);

        this.parent = panel;
        this.cell = cell;
        this.initializeComponents();
    }

    /**
     * Gets the JList that has stored the history log.
     */
    public JList getHistory(){
        return this.historyList;
    }
    
    /**
     * Initializes all the components of this JDialog.
     */
    private void initializeComponents() {

        DefaultListModel f = new DefaultListModel();
        ArrayList<UserComment> h = cell.getHistory();
        boolean first = true;
        for (UserComment u : h) {
            String s = u.toString();
            String[] a = s.split("\n");
            String total = "<html>";
            for (String line : a) {
                total += String.format("%s%s", first ? "" : "<br>", line);
                first = false;
            }
            total += "</span></html>";
            first = true;

            f.addElement(total);
        }

        this.historyList.setModel(f);
        this.historyList.setVisibleRowCount(5);
        this.historyList.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if ((e.getClickCount() & 1) == 0 && !e.isConsumed()) {
                    e.consume();

                    int i = historyList.getSelectedIndex();
                    
                    selectComment(i);

                    JOptionPane.showMessageDialog(null, "Comment updated");
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        });

        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(this.historyList);

        this.panel = new JPanel(new GridLayout(1, 2));

        this.panel.add(scroll);

        super.add(this.panel);

        super.setMinimumSize(new Dimension(500, 300));
        super.setPreferredSize(new Dimension(500, 300));
        super.setVisible(true);
    }

    /**
     * Must be invoked when the user selects a Comment.
     * @param index Index of the Comment at the JList.
     */
    public void selectComment(int index) {
        CommentableCell c = parent.getCell();
        c.setUserComment(cell.getHistory().get(index));
        parent.setCell(c);
        parent.update();
    }
}
