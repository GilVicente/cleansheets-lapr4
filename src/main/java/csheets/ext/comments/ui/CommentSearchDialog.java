/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.comments.ui;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.ext.comments.CommentableCell;
import csheets.ext.comments.CommentsExtension;
import csheets.ext.comments.UserComment;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author JOSENUNO
 */
public class CommentSearchDialog extends JDialog {
    
    /**
     * The controller.
     */
    private CommentController controller;
    
    /**
     * Comment Action.
     */
    private CommentAction action;
    
    /**
     * History log containing the Cells with the comments.
     */
    private ArrayList<CommentableCell> historyCells;

    /**
     * Panel containing all other components.
     */
    private JPanel panel;

    /**
     * TextField to search comments.
     */
    private TextField searchBar = new TextField();

    /**
     * JButton to search comments.
     */
    private JButton searchButton = new JButton();

    /**
     * List that shows the results of the search.
     */
    private JList resultList = new JList(new DefaultListModel());

    public static Dimension BUTTON_SIZE = new Dimension(60, 35);

    /**
     * Empty Constructor for unit tests purposes.
     */
    public CommentSearchDialog() {

    }

    /**
     * Creates a JDialog to search comments.
     *
     * @param parent Parent that shows this dialog.
     * @param act Comment Action.
     * @param ctr Controller.
     */
    public CommentSearchDialog(Frame parent, CommentAction act, CommentController ctr) {
        super(parent, "Search comments", true);

        this.controller = ctr;
        this.action = act;

        this.initializeComponents();
    }

    /**
     * Initializes all the components of this JDialog.
     */
    private void initializeComponents() {
        searchButton.setPreferredSize(new Dimension(BUTTON_SIZE.width * 2, BUTTON_SIZE.height));		// width, height
        searchButton.setMaximumSize(new Dimension(BUTTON_SIZE.width * 2, BUTTON_SIZE.height));		// width, height
        searchButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        searchButton.setFont(new Font("arial", Font.BOLD, 12));
        searchButton.setText("Search");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Spreadsheet s = action.getSpreadSheetTable().getSpreadsheet();

                ArrayList<CommentableCell> l = controller.searchCells(s, searchBar.getText());

                DefaultListModel lm = new DefaultListModel();

                ArrayList<String> a = fillSearchList(l);
                
                for (String str : a){
                    lm.addElement(str);
                }

                historyCells = l;
                resultList.setModel(lm);
            }
        });
        
        
        this.resultList.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if ((e.getClickCount() & 1) == 0 && !e.isConsumed()) {
                    e.consume();
                    
                    int i = resultList.getSelectedIndex();
                    
                    CommentableCell c = historyCells.get(i);
                    
                    controller.selectCell(c);
                    
                    JOptionPane.showMessageDialog(null, "Cell selected");
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        });

        JPanel topPanel = new JPanel(new GridLayout(1, 2));
        topPanel.add(this.searchBar);
        topPanel.add(this.searchButton);

        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(this.resultList);

        this.panel = new JPanel(new BorderLayout());
        this.panel.add(topPanel, BorderLayout.NORTH);
        this.panel.add(scroll, BorderLayout.CENTER);

        super.add(this.panel);

        super.setMinimumSize(new Dimension(500, 300));
        super.setPreferredSize(new Dimension(500, 300));
        super.setVisible(true);
    }

    /**
     * Fills the search list with each characteristic of the cells.
     * @param l Commentable Cells.
     * @return Strings already formatted.
     */
    public ArrayList<String> fillSearchList(ArrayList<CommentableCell> l) {
        ArrayList<String> ls = new ArrayList<>();
        
        boolean first = false;
        for (CommentableCell c : l) {
            String str = c.getUserComment().toString();
            String[] a = str.split("\n");
            String total = "<html>";
            for (String line : a) {
                total += String.format("%s%s", first ? "" : "<br>", line);
                first = false;
            }
            total += "</span></html>";
            first = true;

            ls.add(total);
        }
        
        return ls;
    }
}
