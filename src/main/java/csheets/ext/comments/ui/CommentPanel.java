package csheets.ext.comments.ui;

/*
 * Copyright (c) 2013 Alexandre Braganca, Einar Pehrson
 *
 * This file is part of
 * CleanSheets Extension for Comments
 *
 * CleanSheets Extension for Assertions is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * CleanSheets Extension for Assertions is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CleanSheets Extension for Assertions; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA  02111-1307  USA
 */
import com.sun.glass.ui.Size;
import csheets.CleanSheets;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import csheets.core.Cell;
import csheets.ext.comments.CommentableCell;
import csheets.ext.comments.CommentableCellListener;
import csheets.ext.comments.CommentsExtension;
import csheets.ext.style.ui.FontChooser;
import csheets.ui.ctrl.SelectionEvent;
import csheets.ui.ctrl.SelectionListener;
import csheets.ui.ctrl.UIController;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JEditorPane;

/**
 * A panel for adding or editing a comment for a cell
 *
 * @author Alexandre Braganca
 * @author Einar Pehrson
 */
@SuppressWarnings("serial")
public class CommentPanel extends JPanel implements SelectionListener,
        CommentableCellListener {

    /**
     * The assertion controller
     */
    private CommentController controller;

    /**
     * The commentable cell currently being displayed in the panel
     */
    private CommentableCell cell;

    /**
     * Current selected font.
     */
    private Font currentFont = DEFAULT_FONT;

    /**
     * The text field in which the comment of the cell is displayed.
     */
    private JEditorPane commentField = new JEditorPane();

    private JButton searchButton = new JButton();
    
    private JButton formatButton = new JButton();

    private JButton historyButton = new JButton();

    private static Size BUTTONS_SIZE = new Size(60, 30);

    public static final Font DEFAULT_FONT = new Font("arial", Font.PLAIN, 12);
    
    public static CommentAction action = null;
    
    public static ArrayList<CommentableCell> allCells = new ArrayList<>();

    /**
     * Creates a new comment panel.
     *
     * @param uiController the user interface controller
     */
    public CommentPanel(UIController uiController) {
        // Configures panel
        super(new BorderLayout());
        setName(CommentsExtension.NAME);
        action = new CommentAction(uiController);

        // Creates controller
        controller = new CommentController(uiController, this);
        uiController.addSelectionListener(this);

        // Creates comment components
        ApplyAction applyAction = new ApplyAction();

        commentField.setPreferredSize(new Dimension(120, 240));		// width, height
        commentField.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));		// width, height
        commentField.addFocusListener(applyAction);
        commentField.setAlignmentX(Component.CENTER_ALIGNMENT);

        formatButton.setPreferredSize(new Dimension(BUTTONS_SIZE.width, BUTTONS_SIZE.height));		// width, height
        formatButton.setMaximumSize(new Dimension(BUTTONS_SIZE.width, BUTTONS_SIZE.height));		// width, height
        formatButton.addFocusListener(applyAction);
        formatButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        formatButton.setFont(new Font("arial", Font.PLAIN, 10));
        formatButton.setText("Format");
        formatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Lets user select a font
                Font font = FontChooser.showDialog(
                        null,
                        "Choose Font",
                        currentFont);

                if (font != null) {
                    commentField.setFont(font);
                    currentFont = font;
                }
            }
        });

        historyButton.setPreferredSize(new Dimension(BUTTONS_SIZE.width, BUTTONS_SIZE.height));		// width, height
        historyButton.setMaximumSize(new Dimension(BUTTONS_SIZE.width, BUTTONS_SIZE.height));		// width, height
        historyButton.addFocusListener(applyAction);
        historyButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        historyButton.setFont(new Font("arial", Font.PLAIN, 10));
        historyButton.setText("History");
        CommentPanel temp = this;
        historyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CommentHistoryDialog(CleanSheets.mainFrame, temp, getCell());

            }
        });

        searchButton.setPreferredSize(new Dimension(BUTTONS_SIZE.width * 2, BUTTONS_SIZE.height));		// width, height
        searchButton.setMaximumSize(new Dimension(BUTTONS_SIZE.width * 2, BUTTONS_SIZE.height));		// width, height
        searchButton.addFocusListener(applyAction);
        searchButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        searchButton.setFont(new Font("arial", Font.PLAIN, 10));
        searchButton.setText("Search comment");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CommentSearchDialog(CleanSheets.mainFrame, action, controller);
            }
        });

        // Lays out comment components
        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new GridLayout());
        searchPanel.setPreferredSize(new Dimension(BUTTONS_SIZE.width * 2, BUTTONS_SIZE.height));
        searchPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        searchPanel.add(searchButton);
        
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 2));
        buttonsPanel.setPreferredSize(new Dimension(BUTTONS_SIZE.width * 2, BUTTONS_SIZE.height));
        buttonsPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        buttonsPanel.add(formatButton);
        buttonsPanel.add(historyButton);

        JPanel commentPanel = new JPanel();
        commentPanel.setLayout(new BoxLayout(commentPanel, BoxLayout.PAGE_AXIS));
        commentPanel.setPreferredSize(new Dimension(130, 300));
        commentPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        commentPanel.add(commentField);

        // Adds borders
        TitledBorder border = BorderFactory.createTitledBorder("Comment");
        border.setTitleJustification(TitledBorder.CENTER);
        commentPanel.setBorder(border);

        // Adds panels
        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(searchPanel, BorderLayout.NORTH);
        northPanel.add(buttonsPanel, BorderLayout.CENTER);
        northPanel.add(commentPanel, BorderLayout.SOUTH);
        add(northPanel, BorderLayout.NORTH);
    }

    /**
     * Updates the comments field
     *
     * @param event the selection event that was fired
     */
    public void selectionChanged(SelectionEvent event) {
        Cell cell = event.getCell();
        if (cell != null) {
            CommentableCell activeCell
                    = (CommentableCell) cell.getExtension(CommentsExtension.NAME);
            activeCell.addCommentableCellListener(this);

            commentChanged(activeCell);
        } else {
            commentField.setText("");
        }

        // Stops listening to previous active cell
        if (event.getPreviousCell() != null) {
            ((CommentableCell) event.getPreviousCell().getExtension(CommentsExtension.NAME))
                    .removeCommentableCellListener(this);
        }
    }

    /**
     * Updates the comment field when the comments of the active cell is
     * changed.
     *
     * @param cell the cell whose comments changed
     */
    @Override
    public void commentChanged(CommentableCell cell) {
        // Stores the cell for use when applying comments
        this.cell = cell;

        // The controller must decide what to do...
        controller.cellSelected(cell);
    }

    public void setCommentText(String text) {
        commentField.setText(text);
    }

    /**
     * Sets the current Font of the Panel.
     *
     * @param currentFont new Font.
     */
    public void setCommentFont(Font currentFont) {
        commentField.setFont(currentFont);
    }

    public CommentableCell getCell() {
        return cell;
    }

    public void setCell(CommentableCell cell) {
        this.cell = cell;
    }

    public void update() {
        if (getCell() != null) {
            controller.setComment(getCell(), commentField.getText().trim());
            if (currentFont != null && controller.setFont(getCell(), currentFont)) {
                currentFont = null;
            }
        }
    }

    protected class ApplyAction implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub

        }

        @Override
        public void focusLost(FocusEvent e) {
            // TODO Auto-generated method stub
            update();
        }
    }
}
