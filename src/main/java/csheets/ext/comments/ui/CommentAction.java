/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.comments.ui;

import csheets.ext.share.ui.*;
import csheets.core.Cell;
import csheets.ipc.connection.Broadcast;
import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.event.ActionEvent;

/**
 * 
 * @author JOSENUNO
 */
public class CommentAction extends FocusOwnerAction {
    

    @Override
    public void setEnabled(boolean newValue) {
        super.setEnabled(newValue); //To change body of generated methods, choose Tools | Templates.
    }
    
    protected UIController uiController;
    
    public CommentAction(UIController uiController){
        this.uiController = uiController;
    }

    @Override
    protected String getName() {
        return "Comment Action";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        
    }
    
    public SpreadsheetTable getSpreadSheetTable(){
        return super.focusOwner;
    }
}
