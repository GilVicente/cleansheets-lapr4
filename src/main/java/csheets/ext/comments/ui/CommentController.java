package csheets.ext.comments.ui;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.ext.comments.CommentableCell;
import csheets.ext.comments.CommentsExtension;
import csheets.ext.comments.UserComment;
import csheets.ui.ctrl.UIController;
import java.awt.Font;
import java.util.ArrayList;

/**
 * A controller for updating the user-specified comment of a cell.
 *
 * @author Alexandre Braganca
 * @author Einar Pehrson
 */
public class CommentController {

    /**
     * The user interface controller
     */
    private UIController uiController;

    /**
     * User interface panel *
     */
    private CommentPanel uiPanel;


    /**
     * Creates a new comment controller.
     *
     * @param uiController the user interface controller
     * @param uiPanel the user interface panel
     */
    public CommentController(UIController uiController, CommentPanel uiPanel) {
        this.uiController = uiController;
        this.uiPanel = uiPanel;
    }


    
    /**
     * Sets a new current Font.
     * @param cell the cell for which the Font should be set.
     * @param font new Font.
     * @return true if the cell's Font was changed succesfully.
     */
    public boolean setFont(CommentableCell cell, Font font) {
        if (font == null) return false;
        
        // Stores the comment
        boolean b = cell.setUserCommentFont(font);
        uiController.setWorkbookModified(cell.getSpreadsheet().getWorkbook());

        return b;
    }
    
    /**
     * Attempts to create a new comment from the given string. If successful,
     * adds the comment to the given cell. If the input string is empty or null,
     * the comment is set to null.
     *
     * @param cell the cell for which the comment should be set
     * @param commentString the comment, as entered by the user
     * @return true if the cell's comment was changed
     */
    public boolean setComment(CommentableCell cell, String commentString) {
        // Clears comment, if insufficient input
        if (commentString == null || commentString.equals("")) {
            cell.setUserComment(new UserComment(null));
            return true;
        }

        // Stores the comment
        cell.setUserComment(commentString);
        uiController.setWorkbookModified(cell.getSpreadsheet().getWorkbook());

        return true;
    }

    /**
     * A cell is selected.
     *
     * @param cell the cell whose comments changed
     */
    public void cellSelected(CommentableCell cell) {
        // Updates the text field and validates the comment, if any
        if (cell.hasComment()) {
            uiPanel.setCommentText(cell.getCommentsWithUser());
            uiPanel.setCommentFont(cell.getFont());
        } else {
            uiPanel.setCommentText("");
            uiPanel.setCommentFont(CommentPanel.DEFAULT_FONT);
        }
    }
    
    /**
     * Selects a cell at the spreadsheet.
     * @param cell Cell to selct.
     */
    public void selectCell(CommentableCell cell){
        uiController.setActiveCell(cell);
    }
    
    /**
     * Searches for comments.
     *
     * @param s Active Spreadsheet.
     * @param pattern Text pattern.
     * @return Results.
     */
    public ArrayList<CommentableCell> searchCells(Spreadsheet s, String pattern) {
        ArrayList<CommentableCell> l = new ArrayList<>();

        int szx = s.getColumnCount() + 1;
        int szy = s.getRowCount() + 1;
        for (int x = 0; x < szx; x++) {
            for (int y = 0; y < szy; y++) {
                Cell c = s.getCell(x, y);

                CommentableCell cc
                        = (CommentableCell) c.getExtension(CommentsExtension.NAME);

                if (cc == null) {
                    continue;
                }

                ArrayList<UserComment> ucs = new ArrayList<>();
                ucs.addAll(cc.getHistory());
                ucs.add(cc.getUserComment());

                for (UserComment uc : ucs) {
                    if (uc == null) {
                        continue;
                    }
                    String comment = uc.getComment();
                    if (comment == null) {
                        continue;
                    }

                    if (comment.contains(pattern)) {
                        l.add(cc);
                    }
                }
            }
        }

        return l;
    }
}
