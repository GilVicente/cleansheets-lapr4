package csheets.ext.comments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import csheets.core.Cell;
import csheets.ext.CellExtension;
import java.awt.Font;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An extension of a cell in a spreadsheet, with support for comments.
 *
 * @author Alexandre Braganca
 * @author Einar Pehrson
 */
public class CommentableCell extends CellExtension {

    /**
     * The unique version identifier used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The cell's user-specified comment
     */
    private UserComment userComment;

    /**
     * The previous modificated user-specified comments
     */
    private ArrayList<UserComment> history;

    /**
     * The listeners registered to receive events from the comentable cell
     */
    private transient List<CommentableCellListener> listeners
            = new ArrayList<CommentableCellListener>();

    /**
     * Creates a comentable cell extension for the given cell.
     *
     * @param cell the cell to extend
     */
    CommentableCell(Cell cell) {
        super(cell, CommentsExtension.NAME);

        this.history = new ArrayList<>();
    }


    /*
     * DATA UPDATES
     */
//	public void contentChanged(Cell cell) {
//	}
    /*
     * COMMENT ACCESSORS
     */
    /**
     * Get the cell's user comment.
     *
     * @return The user supplied comment for the cell or <code>null</code> if no
     * user supplied comment exists.
     */
    public UserComment getUserComment() {
        return userComment;
    }

    public String getCommentsWithUser() {
        return userComment.commentWithUser();
    }

    /**
     * Returns whether the cell has a comment.
     *
     * @return true if the cell has a comment
     */
    public boolean hasComment() {
        return userComment != null;
    }

    /*
     * COMMENT MODIFIERS
     */
    /**
     * Sets a new UserComment.
     * @param uc New UserComment.
     */
    public void setUserComment(UserComment uc){
        this.userComment = uc;
    }
    
    /**
     * Sets the user-specified comment for the cell.
     *
     * @param comment the user-specified comment
     */
    public void setUserComment(String comment) {
        if (comment != null) {
            if (hasComment()) {
                this.userComment.setComment(comment);
            } else {
                this.userComment = new UserComment(comment);
            }
            
            if (this.userComment != null) {
                try {
                    if (!this.history.contains(this.userComment) && this.userComment.getFont() != null) {
                        this.history.add(this.userComment.clone());
                    }
                } catch (CloneNotSupportedException ex) {
                    return;
                }
            }
        }
        // Notifies listeners
        fireCommentsChanged();
    }

    /**
     * Sets the font for the cell.
     *
     * @param font Font.
     * @return True if the font is added sucessfully.
     */
    public boolean setUserCommentFont(Font font) {
        boolean b = false;
        if (font != null) {
            if (this.userComment != null) {
                this.userComment.setFont(font);
                
                try {
                    if (!this.history.contains(this.userComment)) {
                        this.history.add(this.userComment.clone());
                    }
                } catch (CloneNotSupportedException ex) {
                    return false;
                }

                b = true;
            }
        }
        //Notifies listeners
        fireCommentsChanged();

        return b;
    }

    /**
     * Returns the Font of the User Comment.
     *
     * @return Font.
     */
    public Font getFont() {
        if (this.userComment == null) {
            return null;
        }
        return this.userComment.getFont();
    }

    /**
     * Gets the History of previous modifications at the cell.
     *
     * @return History List.
     */
    public ArrayList<UserComment> getHistory() {
        return this.history;
    }

    /*
     * EVENT LISTENING SUPPORT
     */
    /**
     * Registers the given listener on the cell.
     *
     * @param listener the listener to be added
     */
    public void addCommentableCellListener(CommentableCellListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes the given listener from the cell.
     *
     * @param listener the listener to be removed
     */
    public void removeCommentableCellListener(CommentableCellListener listener) {
        listeners.remove(listener);
    }

    /**
     * Notifies all registered listeners that the cell's comments changed.
     */
    protected void fireCommentsChanged() {
        for (CommentableCellListener listener : listeners) {
            listener.commentChanged(this);
        }
    }

    /**
     * Customizes serialization, by recreating the listener list.
     *
     * @param stream the object input stream from which the object is to be read
     * @throws IOException If any of the usual Input/Output related exceptions
     * occur
     * @throws ClassNotFoundException If the class of a serialized object cannot
     * be found.
     */
    private void readObject(java.io.ObjectInputStream stream)
            throws java.io.IOException, ClassNotFoundException {
        stream.defaultReadObject();
        listeners = new ArrayList<CommentableCellListener>();
    }
}
