/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FindOpenWorkbooksNetwork.ui;


import csheets.ui.ctrl.UIController;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;

/**
 *
 * @author dmaia
 */
public class FindOpenWorkbooksMenu extends JMenu{
    
    public FindOpenWorkbooksMenu(UIController uiController) {
        super("WorkBook");

        setMnemonic(KeyEvent.VK_A);

        // Adds font actions
        add(new FindOpenWorkBookAction(uiController));
    }
    
}
