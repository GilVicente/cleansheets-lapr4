/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FindOpenWorkbooksNetwork.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.CellDecorator;
import csheets.ui.ext.SideBarAction;
import csheets.ui.ext.TableDecorator;
import csheets.ui.ext.UIExtension;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JToolBar;

/**
 *
 * @author dmaia
 */
public class UIFindOpenWorkbooksExtension extends UIExtension{
    
    /**
     * A side bar that provides editing of comments
     */
    private JComponent sideBar;

    private JCheckBoxMenuItem menuSideBar;
    
    private Icon icon;

    private JButton btnOpen;

 
    public UIFindOpenWorkbooksExtension(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    /**
     * Returns an icon to display with the extension's name.
     *
     * @return an icon with style
     */
    public Icon getIcon() {
        return null;
    }

    /**
     * Returns an instance of a class that implements JMenu. In this simple case
     * this class only supplies one menu option.
     *
     * @return a JMenu component
     */
    public JMenu getMenu() {
        return null;
    }

    /**
     * Returns a cell decorator that visualizes comments on cells.
     *
     * @return decorator for cells with comments
     */
    public CellDecorator getCellDecorator() {
        return null;
    }

    /**
     * Returns a table decorator that visualizes the data added by the
     * extension.
     *
     * @return a table decorator, or null if the extension does not provide one
     */
    public TableDecorator getTableDecorator() {
        return null;
    }

    /**
     * Returns a toolbar that gives access to extension-specific functionality.
     *
     * @return a JToolBar component, or null if the extension does not provide
     * one
     */
    public JToolBar getToolBar() {
        return null;
    }

    /**
     * Returns a side bar that shows the extensions and their status.
     *
     * @return a side bar
     */
    @Override
    public JComponent getSideBar() {
        if (sideBar == null) {
            try {
                sideBar = new FindOpenWorkbooksNetworkPanel(uiController);
            } catch (IOException ex) {
                System.out.println("Side bar controller error");
            }
        }
        return sideBar;
    }


    public JCheckBoxMenuItem getCheckBoxMenuItemSidebar() {
        if (menuSideBar == null) {
            menuSideBar = new JCheckBoxMenuItem(new SideBarAction(this, sideBar));
        }
        return menuSideBar;
    }

    
}
