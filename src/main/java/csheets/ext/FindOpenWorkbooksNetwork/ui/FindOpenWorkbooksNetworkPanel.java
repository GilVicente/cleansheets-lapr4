/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FindOpenWorkbooksNetwork.ui;

import csheets.ext.FindOpeWorkbooksNetwork.FindOpenWorkBooksExtension;
import csheets.ext.FindOpeWorkbooksNetwork.FindOpenWorbooksController;
import csheets.ext.contacts.MailNumber.UI.MailAndNumberEditionPanel;
import csheets.ext.contacts.ui.ClientCellRenderer;
import csheets.ui.ctrl.UIController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author dmaia
 */
public class FindOpenWorkbooksNetworkPanel extends JPanel {

   
    private FindOpenWorbooksController controller;
    
    private JPanel workbookPanel; 
    //para o outro uc
    private JList resultList;
    
    private DefaultListModel<String> modelWorkbookList = new DefaultListModel();
    
    private List<String> workbookList =new ArrayList();
    
     private JLabel pathLabel;
    private JTextField path;
    
  
    /**
     * Creates a new notes panel.
     *
     * @param uiController the user interface controller
     */
    public FindOpenWorkbooksNetworkPanel(UIController uiController) throws IOException {
        super(new BorderLayout());
        setName(FindOpenWorkBooksExtension.NAME);
        this.workbookList=new ArrayList();
          
        workbookPanel = new JPanel();
        workbookPanel.setLayout(new BorderLayout());
        workbookPanel.setPreferredSize(new Dimension(130, 336));
        workbookPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));        // width, height

        workbookPanel.add(panelPath(),BorderLayout.NORTH);
        
        if (path.getText() == null) {
            System.out.println("No path");
        } else {

            controller = new FindOpenWorbooksController(uiController, this, path.getText());

        }
        workbookPanel.add(panelResultList(), BorderLayout.CENTER);
        
        workbookPanel.add(panelButtons(), BorderLayout.SOUTH);

        add(workbookPanel);

    }


    private JPanel panelButtons() {
        JPanel p = new JPanel(new GridLayout(4, 1));
        //p.setLayout(new BorderLayout());
        p.add(buttonSearchOpenWorkbooks());
        p.add(buttonClearWorkbooks());
        return p;

    }
    
     public JPanel panelPath() {
        JPanel p = new JPanel();
        pathLabel = new JLabel();
        pathLabel.setText("Directory Path");
        path = new JTextField();
        path.setPreferredSize(new Dimension(90, 20));
        p.add(path);
        p.add(pathLabel);
        return p;
    }
    
     public JPanel panelResultList() {
        JPanel p = new JPanel();
        resultList = new JList(modelWorkbookList);
        JScrollPane pane = new JScrollPane(resultList);
        p.add(pane);
        return p;
    }
    
    public void showWorkbooks() {
        
        if (workbookList.size() > 0) {
            for (String workbook : workbookList) {
                modelWorkbookList.addElement(workbook.toString());
            }
        } else {
            JOptionPane.showMessageDialog(this, "Workbooks not found", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("No workbooks found.");
        }
    }
//    
    
     /**
     * JButton to find open workbooks
     *
     * @return
     */
    public JButton buttonSearchOpenWorkbooks() {
        JButton buttonSearch = new JButton("Search");
        buttonSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                
                
               
                   try {
                    
                    
                           controller.searchNetworkWorkbooks();
                           workbookList = controller.infoWorkbook();
                       
                    
                } catch (IOException ex) {
                       System.out.println("Nada a apresentar!");
                }
                   
                
               showWorkbooks();

                
               

            }
        }
        );
        return buttonSearch;
    }

     /**
     * JButton to find open workbooks
     *
     * @return
     */
    public JButton buttonClearWorkbooks() {
        JButton buttonSearch = new JButton("Clear");
        buttonSearch.addActionListener(new ActionListener() {
                                           @Override
                                           public void actionPerformed(ActionEvent ae) {
                                              delete();
                                           }
                                       }
        );
        return buttonSearch;
    }
    

    public void delete() {
		DefaultListModel model = (DefaultListModel) resultList.getModel();
		if (!model.isEmpty()) {
			model.removeAllElements();
		}
	}
   
    
    

   
    
}
