package csheets.ext.email;

import csheets.ext.email.SendEmailDialog;
import csheets.ext.emailConfiguration.EmailConfigurationController;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import csheets.ui.ctrl.BaseAction;
import csheets.ui.ctrl.UIController;
import java.awt.Frame;

/**
 * The action to Send an email
 *
 * @author hugo1140465
 */
public class SendEmailAction extends BaseAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public SendEmailAction(UIController uiController) {
        this.uiController = uiController;
    }

    protected String getName() {
        return "Send an email";
    }

    protected void defineProperties() {
    }

    /**
     * the action permorfed
     *
     * @param event the event that was fired
     */
    public void actionPerformed(ActionEvent event) {

        new SendEmailDialog((Frame)null, this.uiController);

    }
}