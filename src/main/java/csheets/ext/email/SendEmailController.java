/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.email;

import csheets.core.Cell;
import csheets.ext.emailConfiguration.EmailConfiguration;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;

/**
 *The controller class to send an email
 * @author hugoc
 */
public class SendEmailController {
    /**
     * SendEmailHelper - to help the controller with some functions
     */
    private SendEmailHelper sendEmailHelper;
    
    /**
     * creates a new controller
     */
    public SendEmailController(){
        this.sendEmailHelper = new SendEmailHelper();
    
    }
    /**
     * adds an email sent to the workbook by calling the addEmail method on SendEmailHelper
     * @param e the sent email to add to the workbook
     */
    public void addEmail(EmailConfiguration e){
        this.sendEmailHelper.addEmail(e);
    }
    
    /**
     * uses the Send Email Helper to get the selected cells
     * @return the selected cells
     */
    public Cell[][]getSelectedCells(){
        return this.sendEmailHelper.getSelectedCells();
    }

}
