/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.email;

import csheets.core.Cell;
import csheets.ext.emailConfiguration.EmailConfiguration;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.sheet.SpreadsheetTable;
import java.awt.event.ActionEvent;

/**
 * a support class that can help the controller access functions only accessable by the FocusOwner
 * @author hugoc
 */
public class SendEmailHelper extends FocusOwnerAction{
    
    /**
     * returns the cells selected by the user
     * @return selected cells
     */
    public Cell[][] getSelectedCells(){
       return getSpreadSheetTable().getSelectedCells();
    }
    
    /**
     * returns the current spreadSheetTable
     * @return  current spreadSheetTable
     */
    public SpreadsheetTable getSpreadSheetTable(){
        return super.focusOwner;
    }
    
    /**
     * adds a sent email to the workbook
     * @param e sent email to be added
     */
    public void addEmail(EmailConfiguration e){
        super.focusOwner.getSpreadsheet().getWorkbook().addOutbox(e);
    }
    
    /**
     * returns the name
     * @return name
     */
    @Override
    protected String getName() {
        return "SendEmailHelper";
    }
    /**
     * overrides action performed
     * @param ae event
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
       
    }
    
}
