/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.style;

import csheets.core.Cell;
import csheets.ext.CellExtension;
import csheets.ext.conditionalformatting.ui.ConditionalStyle;
import csheets.ext.conditionalformatting.ui.ConditionalStyleCellListener;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import javax.swing.border.Border;

public class ConditionalStyleCell extends CellExtension {

    private StylableCell cell;
    private ConditionalStyle trueStyle;
    private ConditionalStyle falseStyle;
    /**
     * The listeners registered to receive events from the comentable cell
     */
    private transient List<ConditionalStyleCellListener> listeners
            = new ArrayList<ConditionalStyleCellListener>();

    /**
     * Creates a conditional stylable cell extension for the given cell.
     *
     * @param cell the cell to extend
     */
    public ConditionalStyleCell(Cell cell) {

        super(cell, StyleExtension.NAME);

        this.cell = new StylableCell(cell);

    }

    public void setFalseStyle(Font font, int hAlignment, int vAlignment, Color fgColor, Color bgColor, Border border) {
        falseStyle = new ConditionalStyle(font, hAlignment, vAlignment, fgColor, bgColor, border);
    }

    public void setTrueStyle(Font font, int hAlignment, int vAlignment, Color fgColor, Color bgColor, Border border) {
        trueStyle = new ConditionalStyle(font, hAlignment, vAlignment, fgColor, bgColor, border);
    }

    public ConditionalStyle getTrueStyle() {
        return trueStyle;

    }

    public ConditionalStyle getFalseStyle() {

        return falseStyle;

    }

    public boolean stylesAreValid() {

        if (trueStyle != null && falseStyle != null) {

            return true;

        }

        return false;

    }

    /*
 * EVENT LISTENING SUPPORT
     */
    /**
     * Registers the given listener on the cell.
     *
     * @param listener the listener to be added
     */
    public void addConditionalStyleCellListener(ConditionalStyleCellListener listener) {

        listeners.add(listener);

    }

    /**
     * Removes the given listener from the cell.
     *
     * @param listener the listener to be removed
     */
    public void removeConditionalStyleCellListener(ConditionalStyleCellListener listener) {

        listeners.remove(listener);

    }

    /**
     * Notifies all registered listeners that the cell's comments changed.
     */
    protected void fireContentChanged() {

        for (ConditionalStyleCellListener listener : listeners) {
            listener.stylableCellChanged(this);
        }
    }

    public StylableCell getStylableCell() {
        return cell;
    }
}
