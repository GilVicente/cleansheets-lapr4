/*
 * Copyright (c) 2005 Einar Pehrson <einar@pehrson.nu>.
 *
 * This file is part of
 * CleanSheets - a spreadsheet application for the Java platform.
 *
 * CleanSheets is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CleanSheets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CleanSheets; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package csheets.ext.style.ui;

import csheets.core.Cell;
import csheets.ext.share.ui.ActiveInstancesPanel;
import java.awt.Font;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ipc.connection.Shared;
import csheets.ipc.connection.TCPConnection;
import csheets.ui.ctrl.UIController;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.Action.MNEMONIC_KEY;
import static javax.swing.Action.SMALL_ICON;

/**
 * A font weight changing operation.
 *
 * @author Einar Pehrson
 */
@SuppressWarnings("serial")
public class BoldAction extends StyleAction {

    /**
     * Creates a new bold action.
     *
     * @param uiController the user interface controller
     */
    public BoldAction(UIController uiController) {
        super(uiController);
    }

    protected String getName() {
        return "Bold";
    }

    protected void defineProperties() {
        putValue(MNEMONIC_KEY, KeyEvent.VK_B);
        putValue(SMALL_ICON, new ImageIcon(StyleExtension.class.getResource("res/img/font_bold.gif")));
    }

    /**
     * Toggles the boldness of the selected cells in the focus owner table.
     *
     * @param cell the cell to which style should be applied
     */
    protected void applyStyle(StylableCell cell) {
        cell.setFont(new Font(cell.getFont().getFamily(),
                cell.getFont().getStyle() ^ Font.BOLD, cell.getFont().getSize()));

//        if (TCPConnection.realTimeSharing) {
//            System.out.println(cell.getExtension("Style"));
//            Cell cells[][] = new Cell[1][1];
//            cells[0][0] = cell.getDelegate();
//            TCPConnection.sharingSendData(TCPConnection.Ip, cells);
//        }
        if (TCPConnection.realTimeSharing) {

            boolean flag = false;
            for (Shared s : ActiveInstancesPanel.listOfShares.geList()) {

                Cell[][] cl = s.cellsToShare;
                for (Cell[] cl1 : cl) {

                    for (Cell cl11 : cl1) {
                        if (cl11.getAddress().getColumn() == cell.getAddress().getColumn()
                                && cl11.getAddress().getRow() == cell.getAddress().getRow()) {

                            Cell[][] cc = new Cell[1][1];
                            cc[0][0] = cell.getDelegate();
                            flag = true;
                            TCPConnection.sharingSendData(s.getIp(), cc);
                            break;
                        }
                    }
                    if (flag) {
                        break;
                    }
                }

                System.out.println(cell.getExtension("Style"));

            }
        }

    }
}
