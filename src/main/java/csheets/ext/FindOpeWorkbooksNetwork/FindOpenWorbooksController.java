/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FindOpeWorkbooksNetwork;


import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.ext.FindOpenWorkbooksNetwork.ui.FindOpenWorkbooksNetworkPanel;
import csheets.ext.networkSearch.ui.SearchWorkbooksUI;

import csheets.ui.ctrl.UIController;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;




/**
 *
 * @author dmaia
 */
public class FindOpenWorbooksController {
    
    
    
    
      /**
     * The user interface controller
     */
    private UIController uiController;

    private FindOpenWorkbooksNetworkPanel uiPanel;
    List<String> resultList;
    List<Workbook> openWorkbookList;
    
    private SearchWorkbooksUI searchUI;
     /**
     * List of workbooks found
     */
    List<Workbook> localWorkbooksList;
    List<Workbook> receivedWorkbookList;
    private Map<Workbook, String> receivedWorkBookList;
    List<File> files;
    private NetworkWorkbookList netWorkSearcher;
    LocalWorkbookSearch LocalSearcher;
    private String path;
   

    public FindOpenWorbooksController(UIController uiController, FindOpenWorkbooksNetworkPanel mailPanel,String path) throws IOException {
        this.uiController = uiController;
        this.uiPanel = mailPanel;
        this.resultList = new ArrayList();
        this.openWorkbookList = new ArrayList();
        this.localWorkbooksList = new ArrayList();
        this.receivedWorkbookList = new ArrayList();
        this.path = path;
        
    }
    public void setActiveUI(SearchWorkbooksUI searchUI) {
        this.searchUI = searchUI;
    }

    public List<File> getFiles() {
        return files;
    }
    
    public void searchNetworkWorkbooks() throws IOException {
        //encontra na rede
        NetworkWorkbookList udpWorkbooks = new NetworkWorkbookList(this);
        this.netWorkSearcher = udpWorkbooks;
        this.netWorkSearcher.run();
        
    }
    public void searchWorkbooks(String name, String content) throws Exception {
        NetworkWorkbookList.name = name;
        NetworkWorkbookList.content = content;
        NetworkWorkbookList.wantToRequest = true;
        netWorkSearcher.run();
    }
    
   
    public void searchLocalWorkbooks() throws IOException {
        //encontra localmente
        LocalWorkbookSearch workbookSearcher = new LocalWorkbookSearch(this,this.uiController,this.path);
        this.LocalSearcher = workbookSearcher;
        this.LocalSearcher.run();
      
    }
    
    public List<Workbook> getLocalWorkbookList() throws IOException {
        return this.LocalSearcher.getLocalList();
    }

    public Map<Workbook, String> getReceiveWorkbookList() {
        return receivedWorkBookList;
    }
   
    public List<Workbook> getWorkBookList(){
        return this.netWorkSearcher.getWorkbookList();
    }
    
     /**
     * This method sends an broadcast signal with the workbooks
     *
     * @return
     */
    public List<String> infoWorkbook() throws IOException {
        String content = "";
        String result = "";
        

        for (Workbook w : getWorkBookList()) {
            if (uiController.getFile(w) != null) {
                String nameWorkbook = uiController.getFile(w).getName();
                //
                String[] filename = nameWorkbook.split("\\.", 2);
                
               content = content.concat("\nName:" + nameWorkbook + " ");
                    int cont = 0;
                    for (Spreadsheet s : w) {
                        cont++;
                        int contCell = 0;
                        content = content.concat("\nTitle "
                                + cont + ": " + s.getTitle() + " \n\n");
                        for (int i = 0; i < 100; i++) {
                            for (int j = 0; j < 100; j++) {
                                if (!s.getCell(j, i).getContent().isEmpty()) {
                                    if (contCell < 5) {
                                        content = content.concat("Cell information"
                                                + ": " + s.getCell(j, i).getContent() + " \n");
                                        contCell++;
                                    }
                                }
                            }
                        }
                    }      
            }
            
            result = result.concat(content);
            this.resultList.add(result);
            
        }
        
       return resultList;
                
        
    }
    
        public List<Workbook> searchOpenedWorkbooks() {
        List<Workbook> workbooks = new ArrayList<>();
        workbooks = getWorkBookList();
        for (Workbook openWorkbook : workbooks) {
            if (uiController.app().isWorkbookStored(openWorkbook)) {
                this.openWorkbookList.add(openWorkbook);
            }
        }

        return this.openWorkbookList;
    }

}


