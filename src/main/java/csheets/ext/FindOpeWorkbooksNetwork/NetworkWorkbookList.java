/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FindOpeWorkbooksNetwork;

import csheets.core.Workbook;


import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author dmaia
 */
public class NetworkWorkbookList implements Runnable {

    private FindOpenWorbooksController searchController;
    private Object request = "REQUEST_LIST";
    public static String name;
    public static String content;
    public static boolean wantToRequest = false;
    private ByteArrayOutputStream baos;
    private ObjectOutputStream oos;
    private ByteArrayInputStream byteArray;
    private ObjectInputStream objectInput;
    private int port;
    Object received;
   
    
    List<Workbook> workbookList = new ArrayList();

    public NetworkWorkbookList(FindOpenWorbooksController searchController) {
        this.searchController = searchController;
        this.port = 30101;
        

    }

    @Override
    public void run() {
        
        try {
            DatagramSocket socket = new DatagramSocket();
            this.baos = new ByteArrayOutputStream();
            try {
                this.oos = new ObjectOutputStream(new BufferedOutputStream(baos));
                socket.setSoTimeout(50000);
            } catch (IOException ex) {
                System.out.println(ex.toString());
            }
            while (true) {

                try {
                    
                       //server 
                       System.out.println("Loading Server ...");
                       ServerSocket srvSocket = new ServerSocket(this.port);
                       System.out.println("Network connected ...");
                      
                    
                        if (!wantToRequest) {
                            
                        //request for all machines
                        oos.writeObject("REQUEST_LIST");
                        oos.flush();
                        byte[] buffer = baos.toByteArray();
                        socket.setBroadcast(true);
                        DatagramPacket toSend = new DatagramPacket(buffer, buffer.length, InetAddress.getByName("255.255.255.255"), port);
                        socket.send(toSend);
                        System.out.println("Sent");
                        received = this.request;
                        wantToRequest=true;
                        oos.close();
                        baos.close();
                        
                    }else{
                       
                       // receive
                        byte[] incomingData = new byte[63000];
                        DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length, InetAddress.getLocalHost(), port);
                        System.out.println("Sent to: " + InetAddress.getLocalHost().getHostName());
                        socket.receive(incomingPacket);
                       
                         
                        
                        
                    try {
                        objectInput = null;
                        byteArray = null;
                        byteArray = new ByteArrayInputStream(incomingPacket.getData());
                        objectInput = new ObjectInputStream(byteArray);
                        received = objectInput;
                    } finally {
                        if (byteArray != null) {
                            byteArray.close();
                        }
                        if (objectInput != null) {
                            objectInput.close();
                        }
                    }
                    
                    wantToRequest=false;
                    }
                    if (received instanceof String) {
                        if (((String) received).equals("REQUEST_LIST")) {
                            baos = new ByteArrayOutputStream();
                            oos = new ObjectOutputStream(baos);
                            // escreve no fluxo de rede os workbooks locais onde foi recebido o pedido
                            searchController.searchLocalWorkbooks();
                            oos.writeObject(searchController.getLocalWorkbookList());
                            oos.flush();
                            byte[] bufferr = baos.toByteArray();
                            DatagramPacket toSendR = new DatagramPacket(bufferr, bufferr.length, InetAddress.getByName("255.255.255.255"), port);
                            socket.send(toSendR);
                            System.out.println("Local list sent....");
                            oos.close();
                            baos.close();
                            this.workbookList = searchController.getLocalWorkbookList(); 
                            
                        }
                    } else if (received instanceof List) {
                        this.workbookList = (List<Workbook>) received;
                        
                    }
                } catch (SocketTimeoutException ex) {
                    System.out.println("Socket error");
                }
                
                
                
                
                
            }

        } catch (ClassCastException ex) {
            System.out.println("Class not found");
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }

    }

    public List<Workbook> getWorkbookList() {
        return workbookList;
    }
    
       
}
