/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.FindOpeWorkbooksNetwork;


import csheets.core.Workbook;
import csheets.ui.ctrl.UIController;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dmaia
 */
public class LocalWorkbookSearch implements Runnable {

   
    private UIController uicontroller;
    List<Workbook> localList;
    List<File> files;
    private String path;
     public static boolean searchEnd;
    

    public LocalWorkbookSearch(FindOpenWorbooksController searchController, UIController uicontroller, String path) throws IOException {
        this.localList = new ArrayList();
        this.files = new ArrayList();
        this.uicontroller = uicontroller;
        this.path = path;
        searchEnd = false;
       
        
        
        
    }


    public void executeSearch() throws IOException {
        
            // diretório a ser procurado
            //C:\Users\dmaia\Desktop\LAPR4_este\cls (this.path)
            File baseFolder = new File("C:\\Users\\toshiba-pc\\Documents\\lapr4-2016-2db\\cls");
            
            // obtem a lista de arquivos
            File[] files = baseFolder.listFiles();
            
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.getName().endsWith(".cls")) {
                    String name = file.getName();
                    addWorkbook(file);
                }
            }

        
       
    }
    
     public void addWorkbook(File f) {
        Workbook w;
        try {
            
            w = uicontroller.getWorkbook(f);
            if (w != null) {
                this.localList.add(w);
            }

        } catch (IOException ex) {

        } catch (ClassNotFoundException ex) {

        }

    }

    public List<Workbook> getLocalList() {
        return localList;
    }

  
   
   
    @Override
    public void run() {
        try {
            executeSearch();
        } catch (IOException ex) {
            System.out.println("Workbooks not found!!!");
        }   
    }
     
     
     
}
