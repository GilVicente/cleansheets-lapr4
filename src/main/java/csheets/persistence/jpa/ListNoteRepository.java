/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.listnote.ListNote;

/**
 *
 * @author bie
 */
public interface ListNoteRepository {
    
    boolean addListNote(Contact c, ListNote ln);
    boolean removeListNote(Contact c, ListNote ln);
    boolean editListNote(Contact c, ListNote ln);
    
}
