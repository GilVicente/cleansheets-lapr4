/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.Mail;
import csheets.persistence.JpaRepository;
import java.util.List;

/**
 *
 * @author dmaia
 */
public class JpaMailsRepository extends JpaRepository<Mail, Long> implements MailsRepository {
    
     @Override
    protected String persistenceUnitName() {
        return "csheets_jar_1.0-SNAPSHOTPU";
    }

    @Override
    public boolean addMail(Mail e, Contact c) {
        try {
            List<Mail> listMail = c.getMaillist();
            listMail.add(e);
            this.save(e);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteMail(Mail e, Contact c) {
        try {

            List<Mail> listMail = c.getMaillist();
            //deleteAgenda(e,c);
            int toRemove = listMail.indexOf(e);
            listMail.remove(toRemove);
            this.delete(e);

        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean updateEditMail(Mail e, Contact c) {
        try {
             List<Mail> listMail = c.getMaillist();
            listMail.remove(e);
            this.update(e);
            this.save(e);
            listMail.add(e);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public List<Mail> showAll() {
        return all();
    }

 
    
}
