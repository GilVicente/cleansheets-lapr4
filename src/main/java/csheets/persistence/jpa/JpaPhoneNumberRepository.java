/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.PhoneNumber;
import csheets.persistence.JpaRepository;


/**
 *
 * @author dmaia
 */
public class JpaPhoneNumberRepository extends JpaRepository<PhoneNumber, Long> implements PhoneNumberRepository{

    @Override
    protected String persistenceUnitName() {
        return "csheets_jar_1.0-SNAPSHOTPU";
    }
 

    @Override
    public boolean addNumber(PhoneNumber e, Contact c) {
        try {
            c.defineNumber(e);
            this.save(e);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteNumber(PhoneNumber e, Contact c) {
        try {

           c.defineNumber(e);
           this.delete(e);

        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean updateEditNumber(PhoneNumber e, Contact c) {
        try {
             c.defineNumber(e);
            this.update(e);
            this.save(e);;
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

   

   

}
