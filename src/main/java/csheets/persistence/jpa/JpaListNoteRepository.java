/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.listnote.ListNote;
import csheets.persistence.JpaRepository;

/**
 *
 * @author bie
 */
public class JpaListNoteRepository extends JpaRepository<ListNote, Long> implements ListNoteRepository {

    @Override
    protected String persistenceUnitName() {
        return "csheets_jar_1.0-SNAPSHOTPU";
    }

    @Override
    public boolean addListNote(Contact c, ListNote ln) {
        try {
            c.addListNote(ln);
            this.save(ln);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean removeListNote(Contact c, ListNote ln) {
        try {
            c.removeListNote(ln);
            this.delete(ln);

        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean editListNote(Contact c, ListNote ln) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
