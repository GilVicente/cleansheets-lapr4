/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.persistence.*;
import csheets.ext.contacts.persistence.*;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.PhoneNumber;

import java.util.List;

/**
 * @author dmaia
 */
public interface PhoneNumberRepository {


    boolean addNumber(PhoneNumber fN, Contact c);

    boolean deleteNumber(PhoneNumber fN, Contact c);

    boolean updateEditNumber(PhoneNumber fN, Contact c);

    


}
