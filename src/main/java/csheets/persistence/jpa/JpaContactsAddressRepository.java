/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.ext.contacts.address.ContactAddress;
import csheets.ext.contacts.persistence.ContactAddressRepository;
import csheets.persistence.JpaRepository;

/**
 *
 * @author Tixa
 */
public class JpaContactsAddressRepository extends JpaRepository<ContactAddress, Long> implements ContactAddressRepository{

    @Override
    protected String persistenceUnitName() {
         return "csheets_jar_1.0-SNAPSHOTPU";
    }

    @Override
    public boolean addNewContactAddress(ContactAddress ac) {
       try {
            this.save(ac);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteContactAddress(ContactAddress ac) {
        try {
            this.remove(ac);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }

    @Override
    public boolean editUpdateContactAddress(ContactAddress ac) {
        try {
            this.update(ac);
        } catch (IllegalArgumentException exception) {
            return false;
        }
        return true;
    }
    
}
