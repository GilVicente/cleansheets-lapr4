/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.persistence.jpa;

import csheets.persistence.*;
import csheets.ext.contacts.persistence.*;
import csheets.ext.contacts.Contact;
import csheets.ext.contacts.MailNumber.Mail;

import java.util.List;

/**
 * @author dmaia
 */
public interface MailsRepository {

    boolean addMail(Mail m, Contact c);

    boolean deleteMail(Mail m, Contact c);

    boolean updateEditMail(Mail m, Contact c);

    List<Mail> showAll();

}
