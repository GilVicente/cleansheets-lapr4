package csheets.persistence.jpa;

import csheets.ext.contacts.Contact;
import csheets.ext.contacts.listnote.ListNote;
import csheets.ext.contacts.persistence.ContactAddressRepository;
import csheets.ext.contacts.persistence.ContactsJpaRepository;
import csheets.ext.contacts.persistence.EventsRepository;
import csheets.ext.contacts.persistence.ContactsRepository;
import csheets.ext.contacts.persistence.EventsJpaRepository;
import csheets.ext.contacts.persistence.RepositoryFactory;
import csheets.ext.contacts.persistence.TagJpaRepository;
import csheets.ext.contacts.persistence.TagRepository;

/**
 * @author Rui Batista 1130880
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public ContactsRepository contacts() {
        return new ContactsJpaRepository();
    }
    
     @Override
    public ContactAddressRepository contactAddress() {
        return new JpaContactsAddressRepository();
    }

    @Override
    public EventsRepository events() {
        return new EventsJpaRepository();
    }

    @Override
    public MailsRepository mails() {
        return new JpaMailsRepository();
    }

    
    //ALTERAR
    @Override
    public PhoneNumberRepository numbers() {
        return new JpaPhoneNumberRepository();
    }

    @Override
    public TagRepository tags() {
        return new TagJpaRepository();
    }

    @Override
    public ListNoteRepository listNotes() {
       return new JpaListNoteRepository();
    }

    
}
