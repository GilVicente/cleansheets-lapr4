/**
 * Technical documentation regarding the work of the team member (1130638) Guilherme Sousa during week4.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: Lang04.3</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-283<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-284<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-285<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-286
 *
 *
 * <p>
 * - Lang04.3- Insert Formula Advanced Wizard -The wizard should now have an edit box where the formula 
 * is gradually constructed. The user should be able to edit the formula text freely. 
 * The functions or operators (and the values of its parameters/operands) selected from the list 
 * should now be inserted in the position of the cursor in the new editbox. 
 * The wizard should continue to have an area to display the evaluation of the formula 
 * (that should be produced dynamically, as the user edits the formula). 
 * The wizard should also have a new window that should display the structure of the formula expression 
 * like an abstract syntax tree (i.e., the structure resulting from the formula compilation). 
 * When the user clicks a tree element its respective text in the edit box should appear highlighted.
 *
 * <h2>3. Requirement</h2>
 * The wizard's window should now contain an optional area to display a structured
 * tree of the chosen formula. As this area is optional, there should be a button
 * or check box to activate or deactivate this functionality. This button or check box should be 
 * on the wizard's window.
 *
 * <p>
 * <b>Use Case "Insert Formula Advanced Wizard":</b> The user opens the wizard. 
 * The system displays wizard's window with the tree option disabled. The user
 * selects the "tree check box". The system displays the formula tree. 
 * The user selects a node of the tree, the respective text of the node becomes
 * highlighted.
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * In this iteration a check box was made so that when it is selected, 
 * the system generates the tree on runtime and shows it,
 * otherwise the tree stays hidden and is not updated. The tree used was a 
 * JTree swing component to display the generated tree. This makes it 
 * easier to associate Tree nodes with parts of the expression, so I can 
 * highlight this components edit boxes when they are clicked/selected on 
 * the tree.
 * <p>
 * <img src="doc-files/lang_04.3_InsertFormulaAdvancedWizard_SequenceDiagram.png" alt="image">
 * 
 * 
 *
 <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to be able to display a JTree with the introduced expression,and then 
 * produce the result while at the same time highlighting the nodes in the edit box. 
 * Following this approach we can start by coding a couple of unit tests one for the listing part, and
 * the other for the preview.The idea is that the tests will pass in the end.
 * <p>
 * see: <code>csheets.ext.function.WizardFunctionController</code>
 *      <code>csheets.ext.function.ui.WizardFunctionUI</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create and develop the following classes :
 * WizardFunctionUI, WizardFunctionController
 * <h3>5.3. Classes</h3>
 *<p>
 * Class Diagram that illustrates how the classes related to this use case,
 * interact with each other.</p>
 *
 * <img src="doc-files/lang_04.3_InsertFormulaAdvancedWizardClassDiagram.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 
 *
 * <h2>6. Implementation</h2>
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/ext/function/WizardFunctionController/package-summary.html">csheets.ext.function.WizardFunctionController</a><p>
 * <a href="../../../../csheets/ext/function/ui/WizardFunctionUI/package-summary.html">csheets.ext.function.ui.WizardFunctionUI</a><p>
 * <a href="../../../../csheets/ext/function/ui/WizardFunctionButton/package-summary.html">csheets.ext.function.ui.WizardFunctionButton</a><p>
 * 
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 * 
 * <h2>9. Work Log</h2>
 *
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Use Case Design and Implementation
 *
 *
 * 
 * <p>
 * Today
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Use case tests and Implementation 
 * 
 * Blocking:
 * <p>
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Even though I had difficulties this week's use case is correctly implemented.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: -
 *
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130638.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130638
 */
class _Dummy_ {
}
