/**
 * Technical documentation regarding the work of the team member (1130638) Guilherme Sousa during week3.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: CRM02.2</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-219<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-221<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-222<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-223
 *
 *
 * <p>
 * - IPC02.2- Advanced Workbook Search -The sidebar window that displays the results of the search should now 
 * include an area to display a preview of the contents of a workbook when the user selects it (i.e., clicks on it). 
 * The preview should be based on the values of the first non-empty cells of the workbook. 
 * This preview should be produced without open the worksheet (at least without the worksheet 
 * been opened in the user interface). The search should now be based on a pattern and not 
 * only on the file name extension.
 *
 * <h2>3. Requirement</h2>
 * Sidebar window to aid in the display of the list of files in a directory chosen by the user
 * being that the latter can also choose to filter his file search by pattern and extension.
 * There should be a preview of a file based on the values of the first non-empty cells of the workbook
 * (without opening it in the UI).
 *
 * <p>
 * <b>Use Case "Advanced Workbook Search":</b> The user selects the button in the sidebar window
 * for Advanced Workbook Search. System requires pattern ,extension, and directory. User introduces
 * it. System displays the found file(s).User selects the one for which he wants to have a preview.
 * System shows the preview.
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * <p>
 * <img src="doc-files/ipc02_2.AdvancedWorkbookSearch.png" alt="image">
 * 
 * 
 *
 <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to be able to list files with the introduced extension,pattern, and then 
 * produce the preview of one of them. 
 * Following this approach we can start by coding a couple of unit tests one for the listing part, and
 * the other for the preview.The idea is that the tests will pass in the end.
 * <p>
 * see: <code>csheets.ipc.advancedworkbooksearch.AdvancedWorkbookSearchRunnable</code>
 *      <code>csheets.ipc.advancedworkbooksearch.WorkbookPreviewGenerator</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create and develop the following classes :
 * AdvancedWorkbookSearchExtension, AdvancedWorkbookSearchRunnable , AdvancedWorkbookSearchPanel, AdvancedWorkbookSearch UI,
 * SearchMainFrame,WorkbookPreviewGenerator
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 
 *
 * <h2>6. Implementation</h2>
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/ipc/advancedworkbooksearch/AdvancedWorkbookSearchExtension/package-summary.html">csheets.ipc.advancedworkbooksearch.AdvancedWorkbookSearchExtension</a><p>
 * <a href="../../../../csheets/ipc/advancedworkbooksearch/AdvancedWorkbookSearchRunnable/package-summary.html">csheets.ipc.advancedworkbooksearch.AdvancedWorkbookSearchRunnable</a><p>
 * <a href="../../../../csheets/ipc/advancedworkbooksearch/ui/AdvancedWorkbookSearchPanel/package-summary.html">csheets.ipc.advancedworkbooksearch.ui.AdvancedWorkbookSearchPanel</a><p>
 * <a href="../../../../csheets/ipc/advancedworkbooksearch/ui/AdvancedWorkbookSearchUI/package-summary.html">csheets.ipc.advancedworkbooksearch.ui.AdvancedWorkbookSearchUI</a><p>
 * <a href="../../../../csheets/ipc/advancedworkbooksearch/ui/SearchMainFrame/package-summary.html">csheets.ipc.advancedworkbooksearch.ui.SearchMainFrame</a><p>
 * <a href="../../../../csheets/ipc/advancedworkbooksearch/ui/WorkbookPreviewGenerator/package-summary.html">csheets.ipc.advancedworkbooksearch.ui.WorkbookPreviewGenerator</a><p>
 * 
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 *-In this iteration the only thing that isnt functional is the preview due to an error in the CellExtension class
 * 
 * <h2>9. Work Log</h2>
 *
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Use Case Design and Implementation
 *
 *
 * 
 * <p>
 * Today
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Use case tests and Implementation 
 * 
 * Blocking:
 * <p>
 * 1. Can't show the preview.
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Even though I had a hard time with the coding during this week 50% of the use case is functional.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: -
 *
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130638.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130638
 */
class _Dummy_ {
}
