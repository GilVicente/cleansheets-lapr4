/**
 * Technical documentation regarding the work of the team member (1130638) Guilherme Sousa during week2.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: CRM01.2</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-161<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-162<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-163<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-164
 *
 *
 * <p>
 * - CRM01.2- Company Edition - A contact may also be a company. If a contact is a company then it has a name 
 * (no first and no last name). A person contact may now be related to a company contact. 
 * A person contact may have also a profession. The profession should be selected from a list.
 * The list of professions should be loaded (and/or updated) from a external xml file or an existing 
 * configuration file of Cleansheets. The window for company contacts should display all the person contacts 
 * that are related to it. The company window should also have an agenda. 
 * The agenda of a company should be read only and display all the events of the individual
contacts that are related to it.
 *
 * <h2>3. Requirement</h2>
 * The agenda may be displayed in a different sidebar. This sidebar should
 * display a list of all events: past, present and future for.One of the contacts
 * should be the user of the session in the computer where Cleansheets is
 * running. If this user has events then, when their due date arrives,
 * Cleansheets should display a popup window notifying the user about the
 * events. This popup window should automatically disappear after a small time
 * interval (e.g., 5 seconds)
 *
 * <p>
 * <b>Use Case "Company Edition":</b> The user selects the the siderbar window
 * for creating contact funcionality. System requires first and last name and a
 * picture as well as if it is a Company or Personal type of Contact, job and if company for which he works. 
 * User inserts the requirements previously required . System
 * saves contact. System displays a sidebar window with a list of all events.
 * Each event is associated with a contact and has a due date and a textual
 * description. System allows the user the following functionalities: create,
 * edit and remove events.Events for the company are all the events of their employees.
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * <p>
 * <img src="doc-files/crm01_2.AddPersonEventSequenceDiagram.png" alt="image">
 * <img src="doc-files/crm01_2.EditPersonEventSequenceDiagram.png" alt="image">
 * <img src="doc-files/crm01_2.RegisterCompanySequenceDiagram.png" alt="image">
 * <img src="doc-files/crm01_2.RegisterPersonCompanySequenceDiagram.png" alt="image">
 * <img src="doc-files/crm01_2.RemovePersonEventSequenceDiagram.png" alt="image">
 * <img src="doc-files/crm01_2.SeeCompanyEventsSequenceDiagram.png" alt="image">
 * <img src="doc-files/crm01_2.SeePersonEventsSequenceDiagram.png" alt="image">
 * 
 *
 <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to be able to remove, edit and add events
 * and contacts. Following this approach we can start by coding a unit test that
 * uses a subclass of <code>CellExtensionContacts</code>.The idea is that the
 * tests will pass in the end.
 * <p>
 * see: <code>csheets.ext.contacts</code>
 *      <code>csheets.ext.contacts.company</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to improve the following classes
 * Agenda, Contact, ContactList, ContaxtExtension, Event and Picture,and create Company,Person,Profession,ProfessionsList,ProfessionsReader. For the
 * sidebar we need to implement a JPanel.
 *
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 
 *
 * <h2>6. Implementation</h2>
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/ext/contacts/Agenda/package-summary.html">csheets.ext.contacts.Agenda</a><p>
 * <a href="../../../../csheets/ext/contacts/Contact/package-summary.html">csheets.ext.contacts.Contact</a><p>
 * <a href="../../../../csheets/ext/contacts/ContactController/package-summary.html">csheets.ext.contacts.ContactController</a><p>
 * <a href="../../../../csheets/ext/contacts/ContactList/package-summary.html">csheets.ext.contacts.ContactList</a><p>
 * <a href="../../../../csheets/ext/contacts/ContactsExtension/package-summary.html">csheets.ext.contacts.ContactsExtension</a><p>
 * <a href="../../../../csheets/ext/contacts/Event/package-summary.html">csheets.ext.contacts.Event</a><p>
 * <a href="../../../../csheets/ext/contacts/EventController/package-summary.html">csheets.ext.contacts.EventController</a><p>
 * <a href="../../../../csheets/ext/contacts/Picture/package-summary.html">csheets.ext.contacts.Picture</a>
 * <a href="../../../../csheets/ext/contacts/company/Company/package-summary.html">csheets.ext.contacts.company.Company</a><p>
 * <a href="../../../../csheets/ext/contacts/company/Person/package-summary.html">csheets.ext.contacts.company.Persony</a><p>
 * <a href="../../../../csheets/ext/contacts/company/Profession/package-summary.html">csheets.ext.contacts.company.Profession</a><p>
 * <a href="../../../../csheets/ext/contacts/company/ProfessionsList/package-summary.html">csheets.ext.contacts.ProfessionsList</a><p>
 * <a href="../../../../csheets/ext/contacts/company/ProfessionsReader/package-summary.html">csheets.ext.contacts.company.ProfessionsReadery</a><p>
 * 
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 *-In this iteration the only thing that isnt functional is the removal of the event from the database
 * 
 * <h2>9. Work Log</h2>
 *
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Use Case Design and Implementation
 *
 *
 * 
 * <p>
 * Today
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Use case tests and Implementation 
 * 
 * Blocking:
 * <p>
 * 1. Can't remove event from database.
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Even though mistakes we're made I believe that at least 90% of the functionality is working.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: -
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/d658cea67016203907dc033f495089a2a8cae50f<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/8280c07b2b9ddf869b1a04cc2fd97e74eb5a76c0<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/37a91727587bfd9266afb1910b0a0a159c066289<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/9a4f23bfcef025adf640a5c5897946b85304d9a0
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130638.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130638
 */
class _Dummy_ {
}
