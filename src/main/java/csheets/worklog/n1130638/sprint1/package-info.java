/**
 * Technical documentation regarding the work of the team member (1130638) Guilherme Sousa during week1.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: CRM01.1</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-117<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-120<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-123<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-114
 *
 *
 * <p>
 * - CRM01.1- Contact Edition - A sidebar window that provides functionalities
 * for creating, editing and removing contacts. Each contact should have a first
 * and last name and also a photograph. Each contact should also have one agenda
 * in which events related to the contact should be displayed. For the moment,
 * events have only a due date (i.e., timestamp) and a textual description. It
 * should be possible to create, edit and remove events. The agenda may be
 * displayed in a different sidebar. This sidebar should display a list of all
 * events: past, present and future. One of the contacts should be the user of
 * the session in the computer where Cleansheets is running. If this user has
 * events then, when their due date arrives, Cleansheets should display a popup
 * window notifying the user about the events. This popup window should
 * automatically disappear after a small time interval (e.g., 5 seconds).-
 *
 * <h2>3. Requirement</h2>
 * The agenda may be displayed in a different sidebar. This sidebar should
 * display a list of all events: past, present and future.One of the contacts
 * should be the user of the session in the computer where Cleansheets is
 * running. If this user has events then, when their due date arrives,
 * Cleansheets should display a popup window notifying the user about the
 * events. This popup window should automatically disappear after a small time
 * interval (e.g., 5 seconds)
 *
 * <p>
 * <b>Use Case "Contact Edition":</b> The user selects the the siderbar window
 * for creating contact funcionality. System requires first and last name and a
 * picture as well. User inserts the requirements previously required. System
 * saves contact. System displays a sidebar window with a list of all events.
 * Each event is associated with a contact and has a due date and a textual
 * description. System allows the user the following functionalities: create,
 * edit and remove events.
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * <p>
 * <img src="doc-files/cmr01_1.ContactEditionClassDiagram.png" alt="image">
 * <img src="doc-files/cmr01_1.ContactRegisterSequenceDiagram.png" alt="image">
 * <img src="doc-files/cmr01_1.CreateEventSequenceDiagram.png" alt="image">
 * <img src="doc-files/cmr01_1.EditContactSequenceDiagram.png" alt="image">
 * <img src="doc-files/cmr01_1.EditEventSequenceDiagram.png" alt="image">
 * <img src="doc-files/cmr01_1.RemoveContactSequenceDiagram.png" alt="image">
 * <img src="doc-files/cmr01_1.RemoveEventsequenceDiagram.png" alt="image">
 * 
 *
 <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to be able to remove, edit and add events
 * and contacts. Following this approach we can start by coding a unit test that
 * uses a subclass of <code>CellExtensionContacts</code>.The idea is that the
 * tests will pass in the end.
 * <p>
 * see: <code>csheets.ext.contacts</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create the following classes
 * Agenda, Contact, ContactList, ContaxtExtension, Event and Picture. For the
 * sidebar we need to implement a JPanel.
 *
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 
 *
 * <h2>6. Implementation</h2>
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/ext/contacts/Agenda/package-summary.html">csheets.ext.contacts.Agenda</a><p>
 * <a href="../../../../csheets/ext/contacts/Contact/package-summary.html">csheets.ext.contacts.Contact</a><p>
 * <a href="../../../../csheets/ext/contacts/ContactController/package-summary.html">csheets.ext.contacts.ContactController</a><p>
 * <a href="../../../../csheets/ext/contacts/ContactList/package-summary.html">csheets.ext.contacts.ContactList</a><p>
 * <a href="../../../../csheets/ext/contacts/ContactsExtension/package-summary.html">csheets.ext.contacts.ContactsExtension</a><p>
 * <a href="../../../../csheets/ext/contacts/Event/package-summary.html">csheets.ext.contacts.Event</a><p>
 * <a href="../../../../csheets/ext/contacts/EventController/package-summary.html">csheets.ext.contacts.EventController</a><p>
 * <a href="../../../../csheets/ext/contacts/Picture/package-summary.html">csheets.ext.contacts.Picture</a>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 *-In this iteration was not used persistence, but all classes are allocated in
 * local memory.
 *
 * <h2>9. Work Log</h2>
 *
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Use Case Design and Implementation
 *
 *
 * 
 * <p>
 * Today
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Use case tests and Implementation 
 * 
 * Blocking:
 * <p>
 * 1. Picture does not load.
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: -
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/d658cea67016203907dc033f495089a2a8cae50f<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/8280c07b2b9ddf869b1a04cc2fd97e74eb5a76c0<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/37a91727587bfd9266afb1910b0a0a159c066289<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/9a4f23bfcef025adf640a5c5897946b85304d9a0
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130638.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130638
 */
class _Dummy_ {
}
