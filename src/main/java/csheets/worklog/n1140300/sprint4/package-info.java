/**
 * Technical documentation regarding the work of the team member 1140300, Tiago 
 * Lacerda during week4.
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the third week of work. Sprint4 week. In this week every developer
 * has a functional increment to do. Things are getting serious.
 * <p>
 * Lang02.2 - Global Variables
 *<p>
 * This weak i started with the analysis and understanding of the functional
 * increment, Lang02.2 - Global Variables
 * <p>
 * Grammar will have to be studied as well as expressions/formulas validation.
 *
 * <h2>2. Lang02.2 - Global Variables</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-31
 * <p>
 * Lang02.2 - Global Variables
 *
 * <h2>3. Requirements</h2>
 * <b>Functional Increment Lang02.2 - Global Variables:</b>
 * Add support for Global variables. The name of temporary variables must
 * start with the "@" sign. When a variable is referred in a formula for the
 * first time it is created. To set the value of a variable it must be used on
 * the left of the assign operator (":="). Global variables are variables
 * that are used in all the WorkBook instance. Therefore, it
 * is possible to use several operations on all the cells using the same global
 * variable and it will be the same instance. 
 * <h2>4. Analysis</h2>
 * It should be possible to create and use a Global variable. That variable
 * will be available on all the Workbook and its formula scopes.
 * <p>
 * <img src="\main\java\csheets\worklog\n1140300\sprint4.analysis.jpg" alt="image">
 *
 * <h2>5. Design</h2>
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets.
 * <p>
 * 2- Select cell
 * <p>
 * 3- Declare and use global variables at cell and formula scope
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this functional increment i'll have to study the project's grammar
 * ,and will afterwords create several classes and edit several more. 
 * GlobalVariable, GlobalVariableRef,GlobalVariableRepository are created.
 * ExcelExpressionCompiler, Assign, ExpressionBuilder etc were edited.
 *
 * <h3>5.3. Classes</h3>
 * The following Sequence Diagram is a preview of the structure of the
 * development of the issue Lang02.1 - Temporary Variables
 * <p>
 * <img src="doc-files/lang02_2_sd.png" alt="image">
 * <p>
 * Class Diagram
 * <p>
 * <img src="doc-files/lang02_2_analysisCD.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * Creator, Information Expert, low coupling, high cohesion, polymorphism.
 *
 * <h2>6. Implementation</h2>
 * <p>
 * csheets.core.GlobalVariable
 * <p>
 * csheets.core.formula.lang.GlobalVariableRef
 * <p>
 * csheets.core.formula.persistence.inmemory.GlobalVariableRepository
 * <p>
 * csheets.core.formula.lang.Assign
 * <p>
 * csheets.core.formula.compiler.ExcelExpressionCompiler
 * <p>
 * csheets.core.formula.persistence.inmemory.GlobalVariableRepository
 * <p>
 * <p>
 * Unit tests in Test Packages at:
 * <p>
 * csheets.core.GlobalVariableTest
 * <p>
 * csheets.core.GlobalVariableRefTest
 * <p>
 * csheets.core.formula.persistence.inmemory
 * <h2>7. Integration/Demonstration</h2>
 *
 * <h2>8. Final Remarks</h2>
 *
 * After full implementation and some grammar issues, the functional increment is working.
 *
 * <h2>9. Work Log</h2>
 *
 * <b>Monday</b>
 * <p>
 * Understanding the client requirements. And started the analysis.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Analysis of the Functional Increment - Lang02.1 - Temporary Variables
 * <p>
 * Started tests, design and implementation
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Tests, design and implementation
 * <p>
 * <b>Thursday</b>
 * <p>
 * Finished tests, design and implementation
 * <h2>10. Self Assessment</h2>
 * - 4
 * <h3>10.1. Design and Implementation:3</h3>
 * <b>Evidences:</b>
 * <p>
 * - url of commit:
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/3ced41e225d5999df67aa57c5d306c7b061243ee
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/ca3e8cd046ced2396a1d2adee922248d5cfdc2ba
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/7a76aba89970a101550267bec4961d41c7c18a6f
 * 
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140300.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
