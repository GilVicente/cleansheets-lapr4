/**
 * Technical documentation regarding the work of the team member (1130430) Daniela Ferreira during week2.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: CRM04.1</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-137
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-139
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-146
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-147
 *

 * 

 *
 * <h2>3. Requirement</h2>
 * <b>Use Case "Edit Notes":</b> the user should be able to create, edit and
 * remove notes associated with contacts. A note consists on free text where its
 * first line defines its title. It should have a timestamp, and there should be
 * a history of modifications to the note. When the note is selected, a list of
 * all its versions (with the timestamp and the notes first line) should appear.
 * By default, editing a note should show the latest version for editing, but
 * there also should be an option to edit another version and if so, the
 * selected version should become the latest version.
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 * Since <b>CRM - Cutomer Relationship Management</b> should be expanded with
 * the use of Extensions, after a first analysis of the requirements, it was
 * defined that the best approach to implement the Use Case would be to create a
 * new Extension of Cleansheets to Edit Notes. Also, since it is not specified
 * by the requirements of the Use Case, the type of presentation of the notes,
 * as the presentation method, will be a Sidebar, containing a tree list, with
 * all the user contacts and their associated notes. On the sidebar, there
 * should be two buttons, 'View Note' and 'Remove Note'. By clicking 'View Note'
 * with a note selected, a window should appear, listing all versions of the
 * note, and presenting an 'Edit button' and a 'View button'. If no version of
 * the note is selected when 'Edit Note' is pressed, the version that should be
 * edited should be the lastest one.
 *
 * <h3>First "analysis" sequence diagram</h3>
 * <p>
 * <img src="doc-files/SequenceDiagram_1.png" alt="image">
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the main
 * functionality of this use case is to be able to create, edit, and remove
 * Notes. We need to be able to get a list of Contacts, their notes, and be able
 * to create new ones, change existing notes, and remove notes. To do that, we
 * need to have a database for test purposes to allow us to test the
 * funcionallity of the Extension, and test function in order to determine if
 * the Extension in question is working properly.
 * <p>
 * see: <code>csheets.ext.contacts</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story, the Extension has the following classes:
 * ContactNote, EditNotesExtension, EditNotesController, EditNotesPanel,
 * NewNoteDialog, OpenNoteDialog and UIEditNotesExtension. Also, the Contact
 * class on the Edit Contact Extension was modified to have a list of Contact
 * notes and the respective functions to interact with it.
 * <p>
 *
 * <img src="doc-files/EditNotes_SequenceDiagram.png" alt="image" >
 * <p>
 * <img src="doc-files/EditNotes_ClassDiagram.png" alt="image" >
 *
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 *
 * see:
 * <a href="../../../../csheets/ext/contacts/Agenda/package-summary.html">csheets.ext.contacts.Agenda</a>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 *
 * -In this iteration was not used persistence, but all classes are allocated in
 * local memory.
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings. Example
 * <b>Tuesday</b>
 * <p>
 * 1. Use case Analysis and design
 * <p>
 *
 * <b>Wednesday</b>
 * <p>
 * 1. Implementation and Tests
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * - url of commit:
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1140300.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130430
 */
class _Dummy_ {
}
