/**
 * Technical documentation regarding the work of the team member (1140300) Tiago Lacerda during week3.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Notes about the week's work.-
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: Core02.1</h2>
 *
 * Issue in Jira:http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-10
 * <p>
 * Core04.2) Extensions in Navigator
 *
 * <h2>3. Requirement</h2>
 * The navigator should now include information regarding the extensions. 
 * Active and inactive extensions should appear in the navigator.
 * For each extension the navigator should display its properties: menus; cell extension; sidebar, etc.
 * When feasible, a double click should change the focus of the mouse to the clicked element
 *
 * 
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts how the user interacts with the system when its
 * using the navigation sidebar to view extensions
 * <p>
 * <img src="t\src\main\java\csheets\worklog\n1140300\sprint3" alt="image">
 * <p>
 * 
 *
 *
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 *First off a new Tree would have to be added to the navigation window, this one containing active and inactive extensions, which
 * would need to check all the existing extensions in order to write on it.
 * 
 * o
 * <p>
 * see:
 * <code>csheets.ext.function.Controller.WizardFunctionControllerTest</code>
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High Coesion and Low Coupling
 * <p>
 * <h2>6. Implementation</h2>
 *
 * all of the classes in the NavigationWindow packages were updated
 * <p>
 * 
 * <p>
 * see:
 * <p>
 * <a </a><p>
 * <a> </a>
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * -In this section present your views regarding alternatives, extra work and
 * future work on the issue.-
 * <p>
 * As an extra this use case also implements a small cell visual decorator if
 * the cell has a comment. This "feature" is not documented in this page.
 *
 *
 * <h2>9. Work Log</h2>
 * <p>
 * <b>Monday</b>
 * On monday I worked on analysis of the requirements of this issue.
 * </p>
 * <p>
 * <b>Tuesday</b>
 * On Tuestady I worked on the analysis, testing and design.
 * </p>
 * <p>
 * <b>Wednesday</b>
 * On Wednesday I worked on implementation, and finishing certain aspets of design.
 * </p>
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 2-  o código cobre uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.), embora apresente algumas lacunas. Mais será feito com o tempo.
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 * @author alexandrebraganca
 */
package csheets.worklog.n1140300.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
