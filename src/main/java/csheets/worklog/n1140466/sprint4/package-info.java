/**
 * Technical documentation regarding the work of the team member (1140466) José Cardoso during week4.
 * 
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: yes</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * Analysis of the features already developed on the Project.
 *
 * <h2>2. Use Case/Feature: Lang 1.3</h2>
 * 
 * Issue in Jira:
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-261<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-262<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-263<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-264<p>
 * 
 * 
 * <h2>3. Requirement</h2>
 * Setup language for Eval, DoWhile and WhileDo. The user should be able to use three more functions (Eval, DoWhile and WhileDo).
 * 
 * <p>
 * <b>Use Case "Lang Features":</b> The user selects the cell where he/she wants to write the function. The user then writes a function with a determinated syntax. The "Eval" function will require 1 parameter and it will process the parameter written. The DoWhile and WhileDo functions will work at a similiar way, they both will execute a statement while a boolean condition is true. The main difference between the last two functions is that the DoWhile function will execute the statement in the first place, and only after will check the condition, while with the WhileDo the check will be done otherwise.
 * 
 *  
 * <h2>4. Analysis</h2>
 * Since "Eval and While loops" will be a new function of cleansheets we need to study how functions are created and loaded by cleansheets and how they work.
 * 
 * 
 * 
 * 
 * <h3>First "analysis" sequence diagrams</h3>
 * The following diagrams depict a proposal for the realization of the previously described use case. We call this diagrams an "analysis" use case realization because they function like a draft that we can do during analysis or early design in order to get a previous approach to the design. For that reason we mark the elements of the diagrams with the stereotype "analysis" that states that the element is not a design element and, therefore, does not exists as such in the code of the application (at least at the moment that this diagram was created).
 * <p>
 * <img src="doc-files/LANG01_03_analysis_Eval.png" alt="image">
 * <p>
 * <img src="doc-files/LANG01_03_analysis_DoWhile.png" alt="image">
 * <p>
 * <img src="doc-files/LANG01_03_analysis_WhileDo.png" alt="image">
 * <p>
 * 
 * From the previous diagrams we see that we need to create a new fucntion and get the parameters inserted by the user.
 * Therefore, at this point, we need to study how the functions are created. This is the core technical problem regarding this issue.
 * <h3>Analysis of Core Technical Problem</h3>
 * We can see an example of a creation of a function at {@code csheets.core.formula.lang.If} which implements {@code csheets.core.formula.Function}.
 * We can see that there is a method {@code applyTo} which receives the parameters inserted by the user. This function should then reproduce the action of the fucntion itself, given those parameters.
 * Therefore, we can create a similiar class, that also implements Function, for the new required functions, and will start to implement tests for this use case.
 * 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Execute cleansheets.<p>
 * 2- Select a Cell.<p>
 * 3- Write a DoWhile/WhileDo/Eval function.<p>
 * 4- Verify the outcome.<p>
 * <p>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will develop three new Functions (Eval, DoWhile and WhileDo) (<code>csheets.core.formula.lang</code>).
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * 
 * <h3>User changes font of the comment.</h3>
 * The following diagram illustrates what happens when the user writes a function at a cell.
 * <p>
 * <img src="doc-files/LANG01_03_design.png" alt="image">
 * <p>
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <p>
 * <img src="doc-files/LANG01_03_dc.png" alt = "image">
 * <p>
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -High cohesion and low coupling.<p>
 * -Polimorphism.<p>
 * -Information Expert.<p>
 * -Creator.<p>
 * -Controller.<p>
 * 
 * <p>
 * 
 * <h2>6. Implementation</h2>
 * 
 * <a href="../../../../csheets/core/formula/compiler/package-summary.html">csheets.core.formula.compiler</a><p>
 * <a href="../../../../csheets/core/formula/lang/package-summary.html">csheets.core.formula.lang</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -Analysis, design, implementation and tests. 
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Analysis of the issue and realization of the design
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Implementation of the source code, and unit tests.
 * <p>
 * Blocking:
 * <p>
 * <b>Thursday</b>
 * <p>
 * Finalization of implementation, unit and functional tests.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit:
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/00c16163b333ef219b02e4ac6d56c4672625766a<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/0352b9bc8b72da34df213f1183f4fe23c7a4c7ab<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/db26b7344f9612e6d754ca65b6f72bf1eeaf044c<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/45f3514fa7696525cfbd963bfb607eb216af3393<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/e88b3d3ea6ddb88d27f15622fc3c2ce594e49178<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/6c84e0ca067f675f0d152b9b33c79c408cc0f17a<p>
 * 
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author JOSENUNO
 */

package csheets.worklog.n1140466.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

