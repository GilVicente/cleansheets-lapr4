/**
 * Technical documentation regarding the work of the team member (1140466) José Cardoso during week3.
 * 
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: yes</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * Analysis of the features already developed on the Project.
 *
 * <h2>2. Use Case/Feature: Core 2.3</h2>
 * 
 * Issue in Jira:
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-208<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-210<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-212<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-213<p>
 * 
 * 
 * <h2>3. Requirement</h2>
 * Setup extension for Core. The user should be able to write rich comments at the CommentPanel. By this, it means that the user should be able to change the font and style of the comments. The User should also be able to view and select previous modifications.
 * 
 * <p>
 * <b>Use Case "Core Features":</b> The user selects the cell where he/she wants to write a comment. The user is allowed to configure font style of the written comments. It should also be provided to the user a history log, which contains all the modifications of the comments. The user is able to select one, and update the comment accordingly.
 * 
 *  
 * <h2>4. Analysis</h2>
 * Since "Rich Comments and History" will be supported in a new extension to cleansheets we need to study how extensions are loaded by cleansheets and how they work.
 * The first sequence diagram in the section <a href="../../../../overview-summary.html#arranque_da_aplicacao">Application Startup</a> tells us that extensions must be subclasses of the Extension abstract class and need to be registered in special files.
 * The Extension class has a method called getUIExtension that should be implemented and return an instance of a class that is a subclass of UIExtension.
 * 
 * 
 * 
 * <h3>First "analysis" sequence diagrams</h3>
 * The following diagrams depict a proposal for the realization of the previously described use case. We call this diagrams an "analysis" use case realization because they function like a draft that we can do during analysis or early design in order to get a previous approach to the design. For that reason we mark the elements of the diagrams with the stereotype "analysis" that states that the element is not a design element and, therefore, does not exists as such in the code of the application (at least at the moment that this diagram was created).
 * <p>
 * <img src="doc-files/CORE02_03_analysis_format.png" alt="image">
 * <p>
 * <img src="doc-files/CORE02_03_analysis_select_history.png" alt="image">
 * <p>
 * <img src="doc-files/CORE02_03_analysis_search_history.png" alt="image">
 * <p>
 * 
 * From the previous diagrams we see that we need to update the characteristics of the comments, when the user changes font configurations.
 * Therefore, at this point, we need to study how to update those characteristics. This is the core technical problem regarding this issue.
 * <h3>Analysis of Core Technical Problem</h3>
 * We can see that there is a JEditorPane object on the CommentPanel. This defines the visual interface of the Comment text area.
 * If we check the {@code JEditorPane} we see that there is a method {@code setFont} which sets the font of the written text at the {@code JEditorPane}, including font style and its size.
 * Therefore, we can update the characteristics of the text at the {@code JEditorPane}, and will start to implement tests for this use case.
 * <img src="doc-files/core02_01_analysis_cell_delegate.png" alt="image"> 
 * 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Execute cleansheets.<p>
 * 2- Select a Cell.<p>
 * 3- Select CommentsPanel.<p>
 * 4- Write a Comment and click at the bold/italic button.<p>
 * 5- The Text should become bold/italic.<p>
 * 6- Reselect the cell (So the comment can be saved).
 * 7- Write a new Comment.
 * 8- Click the View history button at the CommentsPanel.<p>
 * 9- Select the last modification.<p>
 * 10- The Comment should be updated according to the last modification.<p>
 * 11- Use the search bar to find a certain key-word, that was contained at the last modification.<p>
 * 12- It should display a list with the found key-words.<p>
 * <p>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will use the already developed Extension at (<code>csheets.ext.comments</code>) and (<code>csheets.ext.comments.ui</code>).
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * 
 * <h3>User changes font of the comment.</h3>
 * The following diagram illustrates what happens when the user changes the font.
 * <p>
 * <img src="doc-files/Core02_03_design_format.png" alt="image">
 * <p>
 * 
 * The following diagram illustrates what happens when the user selects a item at the history log.
 * <p>
 * <img src="doc-files/Core02_03_design_select_history.png" alt="image">
 * 
 * The following diagram illustrates what happens when the user searches a key-word of a comment.
 * <p>
 * <img src="doc-files/Core02_03_design_search_history.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <p>
 * <img src="doc-files/Core02_03_dc.png" alt = "image">
 * <p>
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -High cohesion and low coupling.<p>
 * -Polimorphism.<p>
 * -Information Expert.<p>
 * -Creator.<p>
 * -Controller.<p>
 * 
 * <p>
 * 
 * <h2>6. Implementation</h2>
 * 
 * <a href="../../../../csheets/ext/comments/package-summary.html">csheets.ext.comments</a><p>
 * <a href="../../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.comments.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -Analysis, design, implementation and tests. 
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Analysis of the issue and realization of the design
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Implementation of the source code, and unit tests.
 * <p>
 * Blocking:
 * <p>
 * <b>Thursday</b>
 * <p>
 * Finalization of implementation, unit and functional tests.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit:
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/be09d2ac4d9fa2ff07ea9d080307331f0821e135<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/aec66f2ac53a20df48396de7faa1c158414d22f8<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/338405ff5507bdc9edfa005457a566bb6dbf68d2<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/3548aa4b55636e590d55a160550e382acf7981f7<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/4327f826b626ed7cb4d12e78f5689534b48b206d<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/fb724de755ec07dfbbf49109399e1722fe7bc8ba<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/54cfc0a61928b4946f404fde4e1a58580f0ccf68<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/6d6e67cba1ac24e7be703d400222f32d7e1e1bf4<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/290fe7cfb9afb8764de0d143d63c20c21fd23b3e<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/25dd6101b271502816344c7b9631e53ad292c212<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/c7a3ff2b181656666f7ce9ee6fa706b4184e9629<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/b331a4454d4116d67985421b84be794bd44c1cb2<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/932cba287e6424f160fd9b8ed51c9a6c99f08721<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/5a0459ecd8299498e75a71668998511ecf724c1c<p>
 * 
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author JOSENUNO
 */

package csheets.worklog.n1140466.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

