/**
 * Technical documentation regarding the work of the team member (1140466) José Cardoso during week2.
 * 
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * Analysis of the features already developed on the Project.
 *
 * <h2>2. Use Case/Feature: IPC 4.1</h2>
 * 
 * Issue in Jira:
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-170<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-171<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-172<p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-173<p>
 * 
 * 
 * <h2>3. Requirement</h2>
 * Setup extension for Inter-Process Comunication. The user should be able to import or export data of a range of cells, selected at the active spreadsheetable. After the selection of the cells, the user should be able to export data to a file, or to import to those cells the data of a file.
 * 
 * <p>
 * <b>Use Case "Inter-process Communication Features":</b> The user selects the cell(s) where he/she wants to import/export. The user selects if he/she wants to import or to export a file. The system prompts for the location of the file to save/load and other requires options (choose special character, choose if the header should be at the first row on the file, etc). The user selects the file and chooses the options. The System updates the selected cells according to the file (if import option was selected) or saves the file (if export option was selected).
 * 
 *  
 * <h2>4. Analysis</h2>
 * Since "Import/Export text file" will be supported in a new extension to cleansheets we need to study how extensions are loaded by cleansheets and how they work.
 * The first sequence diagram in the section <a href="../../../../overview-summary.html#arranque_da_aplicacao">Application Startup</a> tells us that extensions must be subclasses of the Extension abstract class and need to be registered in special files.
 * The Extension class has a method called getUIExtension that should be implemented and return an instance of a class that is a subclass of UIExtension.
 * 
 * 
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depict a proposal for the realization of the previously described use case. We call this diagrams an "analysis" use case realization because they function like a draft that we can do during analysis or early design in order to get a previous approach to the design. For that reason we mark the elements of the diagrams with the stereotype "analysis" that states that the element is not a design element and, therefore, does not exists as such in the code of the application (at least at the moment that this diagram was created).
 * <p>
 * <img src="doc-files/IPC04_01_analysis_export.png" alt="image">
 * <p>
 * <img src="doc-files/IPC04_01_analysis_import.png" alt="image">
 * <p>
 * 
 * From the previous diagrams we see that we need to get the selected cells at the current spreadsheet.
 * Therefore, at this point, we need to study how to get the selected cells. This is the core technical problem regarding this issue.
 * <h3>Analysis of Core Technical Problem</h3>
 * We can see a class diagram of the domain model of the application <a href="../../../../overview-summary.html#modelo_de_dominio">here</a>
 * From the domain model we see that there is a SpreadSheetTable class. This defines the visual interface of the spreadsheet.
 * If we open the {@link csheets.ui.sheet.SheetTable} code we see that there is a method {@code getSelectedCells} which returns the selected cells at that spreadsheettable.
 * Therefore, we can obtain the selected cells for the given SpreadSheetTable, and will start to implement tests for this use case.
 * <img src="doc-files/core02_01_analysis_cell_delegate.png" alt="image"> 
 * 
 * <p>
 * One important aspect, is the need of implementing a class that extends FocusOwnerAction to get the active spreadsheet table at the moment, in order to get the selected cells on that spreadsheet table.
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Execute cleansheets.<p>
 * 2- Select a range of Cells.<p>
 * 3- Select Extensions, into Import/Export File and select export.<p>
 * 4- The following windows should request the user the file location and other contents referenced at the manual.<p>
 * 5- After confirmation, the file should be written correctly.<p>
 * 
 * 1- Execute cleansheets.<p>
 * 2- Select a range of Cells.<p>
 * 3- Select Extensions, into Import/Export File and select import.<p>
 * 4- The following windows should request the user the file location.<p>
 * 5- After confirmation, the selected cells should contain the information stored at the file.<p>
 * <p>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create a subclass of Extension (<code>csheets.ext.importExportFile.ImportExportFileExtension</code>). We will also need to create a subclass of UIExtension (<code>csheets.ext.ImportExportFile.ui.ImportExportFileExtensionUI</code>).
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * 
 * <h3>User exports file, from the selected cells.</h3>
 * The following diagram illustrates what happens when the user selects the Export file option.
 * <p>
 * <img src="doc-files/IPC04_01_design_export.png" alt="image">
 * <p>
 * 
 * The following diagram illustrates what happens when the user selects the Import file option.
 * <p>
 * <img src="doc-files/IPC04_01_design_import.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <p>
 * <img src="doc-files/IPC04_01_dc.png" alt = "image">
 * <p>
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -High cohesion and low coupling.<p>
 * -Polimorphism.<p>
 * -Information Expert.<p>
 * -Creator.<p>
 * -Controller.<p>
 * 
 * <p>
 * 
 * <h2>6. Implementation</h2>
 * 
 * <a href="../../../../csheets/ext/importExportFile/package-summary.html">csheets.ext.importExportFile</a><p>
 * <a href="../../../../csheets/ext/importExportFile/ui/package-summary.html">csheets.ext.importExportFile.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -Analysis, design, implementation and tests. 
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Analysis of the issue and beginning of design
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Implementation of the source code, and unit tests.
 * Finalization of the design.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit:
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/716ed7af62d9035c3e3d542daba718f1ea0f86b7<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/2287a2cfca978eb43fa99015cd4b6ab43d5180cf<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/04ee04b0bd3918fd2e8dc5e82262b462d3474014<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/520d5e4f62a682b2803126772fdd2b4df15f9af6<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/cd682e29a5394d81c2f06a14c33174c493708e9f<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/808b01d8ba013c2d920603830ec199442ca68b41<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/9234d5d0b14d4ea8cd94d438d26546a340986614<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/10bdd1f81f440e68925e983a20d861b81a18294d<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/348bd131f221f7b61fe3608cfe56fca60bd9f49e<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/d1903814702e7783b4484a13470b49c0c6e27588<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/212a2dacff8d5178b58f7b45b0008bdcc80e747c<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/f91aa0c46f4787c1819ba4e10c9ecd28c18cd993<p>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author JOSENUNO
 */

package csheets.worklog.n1140466.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

