/**
 * Technical documentation regarding the work of the team member (1140807) Patrícia Monteiro during week1.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Notes about the week's work.-
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: IPC 04.2</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-203
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-204
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-205
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-206
 *
 *
 *
 * <p>
 * - IPC 04.2- Import/Export Text Link - The process of creating a link is 
 * simular to the one described in IPC04.1 but the import or export should be 
 * always active (until it is removed by the user). Being active means that 
 * the process will be repeated automatically when the source of the data is 
 * updated. This should happen for imports and exports.
 *
 * <h2>3. Requirement</h2>
 * Instead of importing/exporting (like on part 1 of this use case), it should
 * be possible to link cells to a text file. This two components should always
 * be associated and synchronized.
 * 
 * <p>
 * <b>Use Case "Import/Export Text Link ":</b> The user selects one ore more cells
 * from the active spreadsheet. The user selects the Import/Export Link option 
 * on Import/Export menu. The system displays a window asking what file the user
 * pretends to link to the previously selected cells. The user selects the file
 * and the connection is done.
 *
 *
 *
 * <h2>4. Analysis</h2>
 * <h3>Importation link </h3>
 * First the user must select the Extensions menu, and then click "Import TXT
 * File". A dialog window will appear and it will show the data that will be
 * required (separator, header line option, file). The user must complete all of
 * the requested data. Finally, the user must select the linked option and click
 * import. From this moment, any changes to the file must be reflected on the
 * working sheet.
 * <h3>Exportation link </h3>
 * First the user must select the Extensions menu, and then click "Export to TXT
 * File". A dialog window will appear and it will show the data that will be
 * required (separator, header line option, file). The user must complete all of
 * the requested data. Finally, the user must select the linked option and click
 * export. From this moment any change performed on the spreadsheet will be
 * automatically updated in the file.
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * <p>
 * <h3>Exportation link </h3>
 * <img src="doc-files/exportation_link_uc_realization.png" alt="image">
 *  
 * <h3>Importation link </h3>
 *  <img src="doc-files/importation_link_uc_realization.png" alt="image">
 *
 * 
 * 
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is import and export 
 * Following this approach we can start by coding a unit test that
 * uses a subclass of <code>CellExtensionimportExportFile</code>. The idea is that the
 * tests will pass in the end.
 * <p>
 * see: <code>csheets.ext.importExportFile</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create the following classes
 * ExportTXT, ExportThread, FileLink, ImportTXT, ImportThread, linkingFileToExport
 * LinkingFileToImport. 
 *
 * <h3>5.3. Classes</h3>
 * The following diagrams illustrate the core aspects of the design of the
 * solution for this use case.
 * <h3>Sequence Diagram</h3>
 * <h3>Importation link </h3>
 * <img src="doc-files/importation_link_design.png" alt="image">
 * 
 * <h3>Exportation link </h3>
 * <img src="doc-files/exportation_link_design.png" alt="image">
 * 
 * <h3>Class Diagram</h3>
 * <h3>Importation link </h3>
 * <img src="doc-files/importation_link_design_CD.png" alt="image">
 * 
 * <h3>Exportation link </h3>
  * <img src="doc-files/exportation_link_design_CD.png" alt="image">
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/ext/importExportFile/ExportTXT/package-summary.html">csheets.ext.importExportFile</a><p>
 * <a href="../../../../csheets/ext/importExportFile/ExportThread/package-summary.html">csheets.ext.importExportFile</a><p>
 * <a href="../../../../csheets/ext/importExportFile/ImportThread/package-summary.html">csheets.ext.importExportFile</a><p>
 * <a href="../../../../csheets/ext/importExportFile/ImportTXT/package-summary.html">csheets.ext.importExportFile</a><p>
 * <a href="../../../../csheets/ext/importExportFile/FileLink/package-summary.html">csheets.ext.importExportFile</a><p>
 * <a href="../../../../csheets/ext/importExportFile/LinkingFileToExport/package-summary.html">csheets.ext.importExportFile</a><p>
 * <a href="../../../../csheets/ext/importExportFile/LinkingFileToImport/package-summary.html">csheets.ext.importExportFile</a><p>

 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 *
 * 
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Use case analysis and design
 * <p>
 * Blocking:  
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Use case tests and implementation
 * <p>
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * )
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ..
 *
 * 
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1140807.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1140807
 */
class _Dummy_ {
}
