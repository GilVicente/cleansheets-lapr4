/**
 * Technical documentation regarding the work of the team member (1140807) Patrícia Monteiro during week4.
 *
 * 
 * 
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Notes about the week's work.-
 * 
 * This week I spent aditional time resolving a bug in lang 5.2
 * (inability to invoke macros of the same workbook), 
 * as it was blocking the progress of my issue.
 * <p>
 * This week 
 *
 * <h2>2. Use Case/Feature: LANG 05.3</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-41
 * <p>
 * Sub-task:
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-303
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-304
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-305
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-306
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-307
 *
 *
 *
 * <p>
 * - Lang 05.3- Macros with Parameters - The process of creating macros now have
 * parameters. The syntax for macros should now include an header that should 
 * include the name of the macro and its parameters (all parameters should 
 * have a distinct name).
 *
 * <h2>3. Requirement</h2>
 * The parameters should be considered only input parameters.
 * However, it should be possible to freely reference parameters inside the macro.
 * That is to say that, inside a macro, parameters should be used like variables.
 * Macros should support local variables that exist only in the context of a macro.
 * 
 * <p>
 * <b>Use Case "Macros with Parameters":</b> The user opens the extension window
 * of macros and system exhibits the window of macros. The user wrtites a new macro
 * with parameters and saves it within the active workbook. 
 * The system saves the macro that was created.
 * The user finishes creating the macro(s).
 * The user call the macro ther was created and change the parametrs.
 *
 *
 *
 * <h2>4. Analysis</h2>
 * <h3>Macros with Parameters</h3>
 * The objective is to save the macros with parameters to a specific workbook
 * so that the macro persists within the workbook. 
 * Inside a macro, parameters should be used like variables.
 * Macros should support local variables that exist only in the context of a macro.
 * This local variables should have a syntax similar to the one described in
 * Lang 02.1 for the temporary variables of formulas. The invocation of macros 
 * must now include the values for its parameters.
 * 
 *  
 * 
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * <p>
 * <h3>Macros with parameters</h3>
 * <img src="doc-files/lang05_03_SSD.png" alt="image">
 *  
 *  
 * 
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets.
 * 2- Select extension macros window.
 * 3- The macros window should now be able to save macros in the active workbook.
 * 4- The result should be the saving of the macros.
 * 5- The user can invoke the macros that were saved in the active workbook in the creation of new macros.
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need changed the following classes
 * MacroWindowController, MacroWorkbookRepo, Macro. 
 *
 * <h3>5.3. Classes</h3>
 * The following diagrams illustrate the core aspects of the design of the
 * solution for this use case.
 * <h3>Sequence Diagram</h3> 
 * <img src="doc-files/lang05_03_designSD.png" alt="image">
 * 
 * 
 * 
 * <h3>Class Diagram</h3>
 * <h3>Macros with Parameters </h3>
 * <img src="doc-files/lang05_03_designDC.png" alt="image">
 *  
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/core/macro/Macro/package-summary.html">csheets.core.macro.Macro</a><p>
 * <a href="../../../../csheets/ext/macros/MacroWindowController/package-summary.html">csheets.ext.macros.MacroWindowController</a><p>
 * <a href="../../../../csheets/ext/macros/MacroWorkbookRepo/package-summary.html">csheets.ext.macros.MacroWorkbookRepo</a><p>
 *
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 *
 * 
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Use case analysis and design
 * <p>
 * Blocking:  
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Use case tests and implementation
 * <p>
 * 
 * <b>Thursday</b>
 * <p>
 * Finished some adjustments to the implementation and concluded the tests. Updated the worklog as well.
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * )
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ..
 *
 * 
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1140807.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1140807
 */
class _Dummy_ {
}
