/**
 * Technical documentation regarding the work of the team member (1140807) Patrícia Monteiro during week2.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -Notes about the week's work.-
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: CRM 02.1</h2>
 *
 * Issue in Jira: 
 *http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-167
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-152
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-168
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-169
 *
 * <p>
 * - CRM02.1- Address Edition - A sidebar window that provides functionalities
 * to create, edit and remove addresses associated with contacts (individuals or companies). 

 *
 * <h2>3. Requirement</h2>
 * The adress edition may be displayed in a different sidebar. 
 * Each address must include: street, town, postal code, city and country. Each
 * contact should have to addresses: the main address and a secondary address. 
 * In the case of a Portuguese address the postal code should be validated. 
 * It should be possible to import and update the the list of
 * valid Portuguese postal codes from an external file (xml file or other format).
 *
 * <p>
 * <b>Use Case "Address Edition":</b> The user selects the the siderbar window
 * for creating address funcionality. System requires the main address and a secondary address.
 * Address include: street, town, postal code, city and country.
 * User inserts the requirements previously required. System saves addresses. 
 * System allows the user the following functionalities: create, edit and remove addresses.
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagrams depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design. For that
 * reason we mark the elements of the diagram with the stereotype "analysis"
 * that states that the element is not a design element and, therefore, does not
 * exists as such in the code of the application (at least at the moment that
 * this diagram was created).
 * <img src="doc-files/crm_2.1.CreateAddressSD.png" alt="image">
 *
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the core
 * functionality of this use case is to remove, edit and add events and
 * contacts. Following this approach we can start by coding a unit test that
 * uses a subclass of <code>CellExtensionContact</code>. The idea is that the
 * tests will pass in the end.
 * <p>
 * see: <code>csheets.ext.contacts</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create the following classes
 * ContactAddress, PostalCode. For the sidebar we need to implement a JPanel.
 *
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 * <p>
 * see:
 * 
 *
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 * 
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Tuesday</b>
 * <p>
 * 1. Use case analysis
 * <p>
 * Blocking: 
 * <p>
 * <b>Wednesday</b>
 * <p>
 * 1. Use case tests and implementation
 * <p>
 * Blocking:BD
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:</h3>
 *
 * 
 * <p>
 * <b>Evidences:</b>
 *
 * 
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1140807.sprint2;
/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1140807
 */
class _Dummy_ {
}
