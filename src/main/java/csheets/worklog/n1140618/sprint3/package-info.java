/**
 * Technical documentation regarding the work of the team member (1140618) Tiago Carvalho during week3
 *
 * <b>Scrum Master: -no</b>
 *
 * <p>
 * <b>Area Leader: -no</b>
 *
 * <h2>1. Notes</h2>
 *
 *
 *
 * <h2>2. Use Case/Feature: IPC05.1</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-63?filter=-1
 * <p>
 * Analysis. Do the analysis of the IPC5.1
 *
 * <h2>3. Requirement</h2>
 *
 *
 * It should exist an option to send a message to other instances of
 * cleansheets. Also, it should have a new sidebar to show those messages.
 *
 *
 * <p>
 * <b>Use Case "Chat Send Message":</b>
 * The user selects the option to send a message. The system shows a textbox to
 * write the message and show a list of instances that can receive the message.
 * The user selects the instance. The system send the message to the other
 * instance that shows for 5seconds.
 *
 *
 *
 * <h2>4. Analysis</h2>
 * <h3>Objectives<h3/>
 *
 * Create a new sidebar that show all the messages received. Create the option
 * to send a message to another instance using cleansheets.
 *
 * <h3>Problem Resolve Proposal<h3/>
 *
 * Get all the instances using cleansheet and display them on a list, the user
 * selects it write a message to them.
 *
 *
 * <p>
 *
 * <pre>
 *
 * </pre>
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * N/A
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create a subclass of Extension. We
 * will also need to create a subclass of UIExtension. For the sidebar we need
 * to implement a JPanel. In the code of the extension
 * <code>csheets.ext.style</code> we can find examples that illustrate how to
 * implement these technical requirements. The following diagrams illustrate
 * core aspects of the design of the solution for this use case.
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical
 * documentation the elements depicted in these design diagrams exist in the
 * code!
 *
 * <h3>Message SideBar</h3>
 * The following diagram shows the setup of the Messages side bar.
 * <p>
 * <img src="doc-files/designMessageSideBar.png" alt="image">
 *
 *
 * <h3>User Selects to send a Message</h3>
 * The following diagram illustrates what happens when the user sends a message.
 * <p>
 * <img src="doc-files/designMessage.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Describe new or existing design patterns used in the issue-
 * <p>
 * -You can also add other artifacts to document the design, for instance,
 * database models or updates to the domain model-
 *
 * <h2>6. Implementation</h2>
 *
 * -Reference the code elements that where updated or added-
 * <p>
 * -Also refer all other artifacts that are related to the implementation and
 * where used in this issue. As far as possible you should use links to the
 * commits of your work-
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/ext/comments/package-summary.html">csheets.ext.comments</a><p>
 * <a href="../../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.comments.ui</a>
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * -In this section present your views regarding alternatives, extra work and
 * future work on the issue.-
 * <p>
 * As an extra this use case also implements a small cell visual decorator if
 * the cell has a comment. This "feature" is not documented in this page.
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. ...
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 * @author 1140618
 */
package csheets.worklog.n1140618.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1140618
 */
class _Dummy_ {
}
