/**
 * Technical documentation regarding the work of the team member (1140618) Tiago Carvalho during week2. 
 * 
 * <b>Scrum Master: -no</b>
 * 
 * <p>
 * <b>Area Leader: -no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * All members of the group interacted with the various aspects of UC and helped each other.
 * 
 * <h2>2. Use Case/Feature: Core01.1</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-103
 * <p>
 * Analysis.
 * Do the analysis of the core01.1
 * 
 * <h2>3. Requirement</h2>
 * It should exist a new window that allows to enable and disable extensions of Cleansheets. A disabled extension means that all its functionalities are disabled.
 * 
 * <p>
 * <b>Use Case "Activate/Deactivate Extensions":</b> The system displays the extensions avaliable. The user selects if he wants to activate/deactivate extension. The system saves the choice.
 * 
 *  
 * <h2>4. Analysis</h2>
 * <h3>Objectives<h3/>
 * Create a new window that allows to enable and disable extensions of Cleansheets. A disabled extension means that all its functionalities are disabled. To do so, it’s needed get a full list of the extensions and to do a method that returns the list of extensions in order to choose ativate or deactivate.
 * 
 * <h3>Problem Resolve Proposal<h3/>
 * Study the best ways to ativate or deativate in order to follow the structure of the project and do the minimal impact in the project’s output when selecting the menu Extensions, adding a new sub-menu (Activate/Deactivate Extension), that will alow choose the state of the extension, allowing the use of the extension or not. The best way to implemente its create a Controller that allow the UI to change the state of a selected extension.
 * 
 * <p>
 * One important aspect is how extensions are dynamically activated and deactivated. Everytime that the user select activate or deactivate the following method its implemented:
 * <pre>
 *public void setEnableOrDisableExtension(UIExtension uie, boolean flag){
 *       
 *      if (uie.getMenu() != null) {
 *          uie.getMenu().setEnabled(!flag);
 *      }
 *      
 *      if (uie.getToolBar() != null) {
 *          for (Component comp : uie.getToolBar().getComponents()) {
 *              comp.setEnabled(!flag);
 *              //comp.setVisible(!flag);
 *          }
 *      }
 *      
 *      if (uie.getSideBar() != null) {
 *          for (Component comp : uie.getSideBar().getComponents()) {
 *              //comp.setEnabled(!flag);
 *              comp.setVisible(!flag);
 *          }
 *      }
 *  }
 * </pre>
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * N/A
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story we will need to create a subclass of Extension. We will also need to create a subclass of UIExtension. For the sidebar we need to implement a JPanel. In the code of the extension <code>csheets.ext.style</code> we can find examples that illustrate how to implement these technical requirements.
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * <b>Note:</b> It is very important that in the final version of this technical documentation the elements depicted in these design diagrams exist in the code!
 * 
 * <h3>Extension Setup</h3>
 * The following diagram shows the setup of the "comments" extension when cleansheets is run.
 * <p>
 * <img src="doc-files/core02_01_design.png" alt="image">
 * 
 *
 * <h3>User Selects a Cell</h3>
 * The following diagram illustrates what happens when the user selects a cell. The idea is that when this happens the extension must display in the sidebar the comment of that cell (if it exists).
 * <p>
 * <img src="doc-files/core02_01_design2.png" alt="image">
 * 
 * <h3>User Updates the Comment of a Cell</h3>
 * The following diagram illustrates what happens when the user updates the text of the comment of the current cell. To be noticed that this diagram does not depict the actual selection of a cell (that is illustrated in the previous diagram).
 * <p>
 * <img src="doc-files/core02_01_design3.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 * 
 * -Document the implementation with class diagrams illustrating the new and the modified classes-
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -Describe new or existing design patterns used in the issue-
 * <p>
 * -You can also add other artifacts to document the design, for instance, database models or updates to the domain model-
 * 
 * <h2>6. Implementation</h2>
 * 
 * -Reference the code elements that where updated or added-
 * <p>
 * -Also refer all other artifacts that are related to the implementation and where used in this issue. As far as possible you should use links to the commits of your work-
 * <p>
 * see:<p>
 * <a href="../../../../csheets/ext/comments/package-summary.html">csheets.ext.comments</a><p>
 * <a href="../../../../csheets/ext/comments/ui/package-summary.html">csheets.ext.comments.ui</a>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * -In this section present your views regarding alternatives, extra work and future work on the issue.-
 * <p>
 * As an extra this use case also implements a small cell visual decorator if the cell has a comment. This "feature" is not documented in this page.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * -Insert here a log of you daily work. This is in essence the log of your daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. ...
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * -Insert here your self-assessment of the work during this sprint.-
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex: mais de 50%) e apresentam código que para além de não ir contra a arquitetura do cleansheets segue ainda as boas práticas da área técnica (ex: sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
* @author 1140618
 */

package csheets.worklog.n1140618.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1140618
 */
class _Dummy_ {}
