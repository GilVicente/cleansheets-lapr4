/**
 * Technical documentation regarding the user story ipc03_01: Search in another instance.
 *<b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 * 
 * 
 * <h2>1. Notes</h2>
 * - Finishing Sprint1 implementation
 * - Analysis of Sprint2
 * - Design
 * - Parcial implementation
 * <p>
 *
 * <h2>2. Use Case/Feature: IPC03.1</h2>
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-57
 * </p>
 * 
 * Identification: 
 * <p>
 * -Search Workbooks on Another Instance
 * </p>
 * Description:
 * <p>
 * -Connect to an instance of cleansheets, input a workbook name to search.
 * </p>
 * <p>
 * -Presents a prewview of the workbooks that matches that name (found)
 * </p>
 * 
 * <h2>1. Requirement</h2>
 * It should be possible for a user to search for a workbook of another instance of Cleansheets. The search should include workbooks that are open in the remote instance. The system must inform
 * if the workbook was found or not and if found the reply must include a summary of the contents of the workbook and the name of the worksheets and the values of the non-empty cells
 * of each worksheet.
 * <br/>
 * 
 * 
 * <b>Use case "Search in Another Instance"</b>
 *
 * <h2>2. Analysis</h2>
 * There should be another extension/option with "Workbook Search" and then  "search in another instance". The system should ask for a input from the user as the name of the workbook 
 * to be found. If found, it replay the user with a preview or summary of contents wich should include: name of the worksheets and first non-empty values of each worksheet.
 * 
 * 
 * 
 * <h3>4. Analysis</h3>
 * This use case will be supported in a new extension to cleansheets. We need to choose an instance to connect and then send a request. the response 
 * will return a list of opened workbooks that match the user workbook input name file. If found, it should present the user a preview of the worksheets and
 * first non empty cells.
 * <br>
 * <img src="doc-files/searchworkbookwnotherinstance_uc_realization.png"/>
 * <br/>
 * 
 * From the previous diagrams we see that we need to create a list of workbooks found.<br/>
 * 
 * <h2>3. Tests</h2>
 * 
 * <br/>
 * 
 * <h2>4. Design</h2>
 * 
 * <br/>
 * 
 * <br/>
 * <br/>
 * <img src="doc-files/searchworkbookwnotherinstance_uc_realization_design.png"/>
 * <br/>
 * <br/>
 * <br/>
 * <img src="doc-files/searchworkbookwnotherinstance_uc_realization_design2.png"/>
 * <br/>
 * <br/>
 * @author 1040523
 */
package csheets.worklog.n1040523.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130516
 */

