/**
 * Technical documentation regarding the work of the team member (1130430) Daniela Ferreira during week3.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 *
 * <h2>2. Use Case/Feature: Lang 7.1</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-218
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-220
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-224
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-225
 *
 *
 *
 *
 *
 * <h2>3. Requirement</h2>
 * Create a new extension, where user can open a window to write macros using
 * <a href="http://www.beanshell.org/home.html">Java BeanShell</a>. This feature
 * will be integrated with the others macros. User can select what language we
 * want to write a macro, beanshell or cleansheets.
 * <p>
 * In this phase the user will should be able to write a script in beanshell
 * that:
 * <ol>
 * <li>Open a new Workbook</li>
 * <li>Create a new cleansheet's macro</li>
 * <li>run the macro </li>
 * <li>show in a new window the macro's result </li>
 * </ol>
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 * The BeanShell should be implemented as an extension to cleansheets using a
 * subclass of UIExtension class. <p>
 * With this subclass we will override a method to create a menu, on wich a new
 * window will appear, and asks the user, what script he wants to use. Then the
 * editor window will appear.
 *
 * <h3>First "analysis" sequence diagram</h3>
 * <p>
 * <img src="doc-files/firstAnalysis_sd_lang8.1.png" alt="image">
 *
 * <h2>5. Design</h2>
 * <p>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the main
 * functionality of this use case is to be able to write macros using BeanShell.
 * To test the BeanShellWindow we need to follow these steps:<p><p>
 * 1 - Select the Extensions menu<p>
 * 2 - Select the BeanShell option <p>
 * 3 - Select language wanted <p>
 * 4 - Costumize CleanSheets<p>
 * 
 *
 * <h3>5.2. UC Realization</h3>
 *
 * To realize this user story we will need to create a subclass of Extension. We
 * will also need to create a subclass of UIExtension. For the menu we need to
 * implement a JMenu.
 * <p>
 *
 * <img src="doc-files/Design_lang8.1.png" alt="image" >
 * <p>
 * <img src="doc-files/doc-files/window.png" alt="image" >
 *
 * <h3>5.3. Classes</h3>
 * <img src="doc-files/classDiagram_lang8.1.png" alt="image" >
 *
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 *
 * see:
 *
 * <a href="../../../../csheets/ext/beanshell/ui/BeanShellWindow.html">csheets.ext.beanshell.ui.BeanShellWindow</a><p>
 * <a href="../../../../csheets/ext/beanshell/ui/UIBeanShell.html">csheets.ext.beanshell.ui.UIBeanShell</a><p>
 * <a href="../../../../csheets/ext/beanshell/ui/BeanShellAction.html">csheets.ext.beanshell.ui.BeanShellAction</a><p>
 * <a href="../../../../csheets/ext/beanshell/ui/UIBeanShellExtension.html">csheets.ext.beanshell.ui.UIBeanShellExtension</a><p>
 * <a href="../../../../csheets/ext/beanshell/ui/BeanShellMenu.html">csheets.ext.beanshell.ui.BeanShellMenu</a><p>
 * <a href="../../../../csheets/ext/beanshell/BeanShellExtension.html">csheets.ext.beanshell.BeanShellExtension</a>
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 * Basically, from requirements and also analysis, we see that the main
 * functionality of this use case is to be able to write macros using BeanShell.
 * However, this use case was only developed with UserInterfaces and Menus what
 * means that is not possible to make Unit Tests.
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings. Example
 * <b>Tuesday</b>
 * <p>
 * 1. Use case Analysis and design
 * <p>
 *
 * <b>Wednesday</b>
 * <p>
 * 1. Implementation and Tests
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <p>
 * <b>Evidences:</b>
 * - url of commit:
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130430.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130430
 */
class _Dummy_ {
}
