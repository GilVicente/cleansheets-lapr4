/**
 * Technical documentation regarding the work of the team member (1130430) Daniela Ferreira during week4.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * --
 * <p>
 *
 * <h2>2. Use Case/Feature: IPC03.3</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-275
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-276
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-277
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-278
 *
 *
 * <h2>3. Requirement</h2>
 * The user must be able to request for all disk workbooks on the LAN filtered
 * by name or content, or both. The list must be available in the extension
 * sideBar and if he clicks one workbook some basic details appear (first lines
 * of content).
 *
 * <h2>4. Analysis</h2>
 * The user must be able to request for all disk workbooks on the LAN filtered
 * by name or content, or both. The list must be available in the extension
 * sideBar and if he clicks one workbook some basic details appear (first lines
 * of content).
 *
 * <img src="doc-files/SSD_IPC03.3.png" alt="image">
 *
 * <h2>5. Design</h2>
 *
 * <p>
 * <h3>5.1. Functional Tests</h3>
 *
 * To test the Network Search by File Contents we need to follow these steps:
 * <p>
 * <p>
 * 1 - Select the Extensions menu<p>
 * 2 - Select the BeanShell option
 * <p>
 * 3 - Select language wanted
 * <p>
 * 4 - Costumize CleanSheets<p>
 *
 *
 * <h3>5.2. UC Realization</h3>
 *
 * To realize this user story we will need to create a subclass of Extension. We
 * will also need to create a subclass of UIExtension.
 * <p>
 *
 * <img src="doc-files/SD_IPC03.3.png" alt="image" >
 * <p>
 *
 * <h3>5.3. Classes</h3>
 * <img src="doc-files/DC_IPC03.3.png" alt="image" >
 *
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 *
 * see:
 *
 * <a href="../../../../csheets/ext/networkSearch/LocalWorkbookSearch.html">csheets.ext.networkSearch.LocalWorkbookSearch</a><p>
 * <a href="../../../../csheets/ext/networkSearch/NetworkSearchExtension.html">csheets.ext.networkSearch.NetworkSearchExtension</a><p>
 * <a href="../../../../csheets/ext/networkSearch/UDPgetWorkbookList.html">csheets.ext.networkSearch.UDPgetWorkbookList</a><p>
 * <a href="../../../../csheets/ext/networkSearch/ui/FindNetworkWorkbooksMenu.html">csheets.ext.networkSearch.ui.FindNetworkWorkbooksMenu</a><p>
 * <a href="../../../../csheets/ext/networkSearch/ui/SearchWorkbookAction.html">csheets.ext.networkSearch.ui.SearchWorkbookAction</a><p>
 * <a href="../../../../csheets/ext/networkSearch/ui/SearchWorkbookController.html">csheets.ext.networkSearch.ui.SearchWorkbookController</a><p>
 * <a href="../../../../csheets/ext/networkSearch/ui/SearchWorkbooksPanel.html">csheets.ext.networkSearch.ui.SearchWorkbooksPanel</a><p>
 * <a href="../../../../csheets/ext/networkSearch/ui/SearchWorkbooksUI.html">csheets.ext.networkSearch.ui.SearchWorkbooksUI</a><p>
 * <a href="../../../../csheets/ext/networkSearch/ui/UIExtensionFindNetworkWorkbooks.html">csheets.ext.networkSearch.ui.UIExtensionFindNetworkWorkbooks</a><p>
 * <a href="../../../../csheets/ext/networkSearch/ui/WorkbookDetailsUI.html">csheets.ext.networkSearch.ui.WorkbookDetailsUI</a><p>
 *
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 * -It was possible to conclude this use case in time. There is a bug when
 * search is invoked that displays a message error of nullPointException. Other
 * situation is when the user tries to open saved workbooks. This option
 * displays a message, but this issue is a problem that already came with the
 * project since the beginning. Therefore, is not possible to show the workbooks
 * that are stored in local or network level.
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings. Example
 * <b>Tuesday</b>
 * <p>
 * 1. Use case Analysis and design
 * <p>
 *
 * <b>Wednesday</b>
 * <p>
 * 1. Implementation and Tests
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <p>
 * <b>Evidences:</b>
 * - url of commit:
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130430.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130430
 */
class _Dummy_ {
}
