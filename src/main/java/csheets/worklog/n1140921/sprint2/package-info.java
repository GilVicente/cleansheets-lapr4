/**
 * Technical documentation regarding the work of the team member (1140921) Jose Santos during week2.
 *
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the second week of work. Sprint2 week. This week I'll worked on the
 * Core05.1) Email Configuration.
 *
 * <h2>2. Use Case/Feature: Core05.1</h2>
 *
 * Issue in Jira:http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-133
 * <p>
 * Core05.1)Email Configuration
 * </p>
 * <h2>3. Requirements</h2>
 * Implement a new extension window to setup email. The user should have the
 * possibility to setup the configurations for email . All configuration data
 * will be saved in a proper file .
 *
 * <b>Use Case "Email Configuration":</b>
 * <h2>4. Analysis</h2>
 * This functional should allow the user configurate is email. All the
 * configuration data should be saved in a proper file(used to save global
 * data). The extension should display a new window which the user select the
 * content ( destination, subject and body) from the contents of specific cells
 * (to be selected when the user select the test email button).After press the
 * button to send a test email should display a preview of the email and the
 * result of the test.
 *
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design.
 * <img src="doc-files/core05_01_analysisSSD.png" alt="image">
 *
 *
 *
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets. 2- Go to extensions 3- Select configuration Email 4- The
 * user choose the cells to get the content of destiny,subject and body.5-User
 * press Test Email.6-Show the email configuration
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Sequence Diagram is a preview of the structure of the
 * development of the issue Email Configuration
 * <p>
 * <img src="doc-files/core05.1_image2.png" alt="image">
 * <p>
 * <p>
 * <img src="doc-files/core05.1_image3.png" alt="image">
 * <p>
 * <p>
 * <img src="doc-files/core05.1_image5.png" alt="image">
 * <p>
 * 
 * The following Class Diagram is a preview of the structure and conection
 * between our classes of our issue (Email Configuration).
 * <p>
 * <img src="doc-files/core05.1_image1.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * This issue could be implemented in a better way over the time of the first
 * sprint and it will be worked on in this last days, before starting the new
 * sprint.
 *
 *
 * <h2>9. Work Log</h2>
 *
 * 

 * <b>Monday</b>
 *
 * Made analysis OO.

 * <b>Tuesday</b>
 *
 * Made Test design OO , and Start implementation
 *
 *
 *
 * <p>
 * <b>Wednesday</b>
 * </p>
 * Updating DesignOO and continuing the implementation
 * <p>
 * <b>Thursday</b>
 * </p>
 * 
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 90%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <&p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140921.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
