/**
 * Technical documentation regarding the work of the team member (1140921) Jose Santos during week1.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the first week of work. Sprint1 week. This week we started with the 4
 * mandatory functional increments. Core01.1-Enable and Disable Extensions
 * Lang01.1-Instructions Block IPC01.1-Start Sharing CRM01.1-Contact Edition
 * <p>
 * This weak i started with the analysis and understanding of the functional
 * increment, lang01.1 - Instructions Block. And also the all application
 * itself.
 *
 * <h2>2. Use Case/Feature: Lang01.1</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-108
 * <p>
 * Lang01.1-Instructions Block
 *
 * <h2>3. Requirements</h2>
 * Setup extension for formulas of CleanSheets. The user should have the
 * possibility of writing blocks of instructions. A block should be delimited by
 * curly braces and it's instructions separated by a tip-colon. Instructions are
 * executed sequentially and the result of the block is the result of the last
 * executed instruction.
 * <p>
 * <b>Use Case "Block of Instructions":</b>
 * <h2>4. Analysis</h2>
 * This functional increment should result in the execution of all the block
 * instructions and the block result should be the last instruction result. The
 * file formula.g has the grammar of the project and should be adapted in a way
 * that accepts the open curly brace ( { ) and recognizes it as the start of a
 * block of instructions. It will also recognize the tip-colong ( ; ) as a
 * delimiter of instructions and the close curly brace ( } ) as the end of the
 * block of instructions. Changes are also needed in the
 * ExcelExpressionsCompiler, so it can process an expression at the time for our
 * block of instructions.
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design.
 * <p>
 * <img src="doc-files/lang01_01_analysisSSD.png" alt="image">
 *
 * <p>
 * The <a href="http://en.wikipedia.org/wiki/Delegation_pattern">delegation
 * design pattern</a> is used in the cell extension mechanism of cleansheets.
 * The following class diagram depicts the relations between classes in the
 * "Cell" hierarchy.
 * <p>
 * <img src="doc-files/core02_01_analysis_cell_delegate.png" alt="image">
 *
 * As we can see from the code, if we are requesting a extension that is not
 * already present in the cell, it is applied at the moment and then returned.
 * The extension class (that implements the <code>Extension</code> interface)
 * what will do is to create a new instance of its cell extension class (this
 * will be the <b>delegator</b> in the pattern). The constructor receives the
 * instance of the cell to extend (the <b>delegate</b> in the pattern). For
 * instance, <code>StylableCell</code> (the delegator) will delegate to
 * <code>CellImpl</code> all the method invocations regarding methods of the
 * <code>Cell</code> interface. Obviously, methods specific to
 * <code>StylableCell</code> must be implemented by it. Therefore, to implement
 * a cell that can have a associated comment we need to implement a class
 * similar to <code>StylableCell</code>.
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets. 2- Select cell 3- Use a formula using a block of
 * instructions. 4- The result value in the cell should
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Sequence Diagram is a preview of the structure of the
 * development of the issue Block of Instructions
 * <p>
 * <img src="doc-files/lang01_01_analysis.png" alt="image">
 * <p>
 * The following Class Diagram is a preview of the structure and conection
 * between our classes of our issue (Block of Instructions).
 * <p>
 * <img src="doc-files/lang01_01_analysisDC.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * This issue could be implemented in a better way over the time of the first
 * sprint and it will be worked on in this last days, before starting the new
 * sprint.
 *
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * Example
 * <b>Monday</b>
 * <p>
 * Me and my team studied the project and the implementation of it.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * I analysed the issue that my team and I were going to work on this week and I
 * worked on the analysis and requirements of the issue Block of Instructions.
 * <p>
 * Blocking:I had some difficulties on understanding the implementation of this
 * application and how we should start implementing this issue.
 *
 * <p>
 * <b>Wednesday</b>
 * <p>
 * I worked on the analysis and the design of the issue Block of Instructions
 * and the implementation as well.
 * <p>
 * <b>Thursday</b>
 * <p>
 * Finished some adjustments to the design of the issue Block of instructions
 * and worked on implementation.
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 90%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140921.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
