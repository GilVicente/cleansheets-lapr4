/**
 * Technical documentation regarding the work of the team member (1140921) Jose Santos during week3.
 *
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the second week of work. Sprint3 week. This week I'll worked on the
 * Lang03.2)Conditional Formatting of Ranges
 *
 * <h2>2. Use Case/Feature: Lange03.2</h2>
 *
 * Issue in Jira:http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-7
 * <p>
 * Lang03.2)Conditional Formatting of Ranges
 * </p>
 * <h2>3. Requirements</h2>
 * Enable Cleansheets to apply conditional formatting to a range of cells.
 *
 * <b>Use Case "Conditional Formatting of Ranges":</b>
 * <h2>4. Analysis</h2>
 * Enable Cleansheets to apply conditional formatting to a range of cells (also
 * in the style extension). The idea is that a single formula could applied to
 * all the cells in the range (one at a time) in order to evaluate what style to
 * apply. For that to be possible it is necessary to add a new special kind of
 * variable to the formulas that represents the "current" cell. This special
 * variable could be named "_cell". For instance, the formula "=_cell >= 10"
 * could be associated to a range format. In this case, Cleansheets would
 * evaluate the formula for each cell in the range and apply the formatting
 * style in accordance with the result of the formula. In this example, all cell
 * in the range with a value greater or equal to 10 would receive the style
 * associated with the true result and the others the style associated with the
 * false result. The window in the sidebar should also be updated so that it is
 * clear if the format is for a single cell or for a range. Within the sidebar
 * window it should also be possible to remove existing conditional style
 * formatting.
 *
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design.
 * <img src="lang03.2analysisSSD.png" alt="image">
 *
 *
 *
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets. 2- Set color to true and false 3-Select range of cells 4-
 * insert the exprecion "=_cell > 10" 4- set colours True and false.
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Class Diagram is a preview of the structure of the development
 * of the issue Conditional Formatting of Ranges
 * <p>
 * <img src="doc-files/lang03.2_image1.png " alt="image">
 * <p>
 *
 *
 * The following Class Diagram is a preview of the structure and conection
 * between our classes of our issue (Conditional Formatting of Ranges).
 * <p>
 * <img src="doc-files/core03_02_SD.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 *
 *
 * <h2>8. Final Remarks</h2>
 *
 * This issue could be implemented in a better way over the time of the first
 * sprint and it will be worked on in this last days, before starting the new
 * sprint.
 *
 *
 * <h2>9. Work Log</h2>
 *
 *
 *
 * <b>Monday</b>
 *
 * Made analysis OO.
 *
 * <b>Tuesday</b>
 *
 * Made Test design OO , and Start implementation
 *
 *
 *
 * <p>
 * <b>Wednesday</b>
 * </p>
 * Updating DesignOO and continuing the implementation
 * <p>
 * <b>Thursday</b>
 * </p>
 * Fixed some erros on the implementation finish DesignOO
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 90%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <&p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140921.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
