/**
 *
 * Technical documentation regarding the work of the team member (1140921) Jose Santos during week4.
 *
 *
 *
 * <b>Scrum Master: -(yes/no)- yes</b>
 *
 *
 *
 * <p>
 *
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 *
 *
 * <h2>1. Notes</h2>
 *
 *
 *
 * This week I'll be working on the feature Ipc 06.1 The rest of the members
 *
 * will work on : Ipc05.1-Chat send message; Ipc07.1-Choose game and partner;
 *
 * Ipc04.3-Import and export database; * #bug on the feature ipc1.2 and 01.3
 * there
 *
 * are some bugs on that issue the code is a little limited so I'll try to make
 *
 * some modifications so everyone can use this same classes
 *
 *
 *
 * <h2>2. Use Case/Feature:IPC06.1</h2>
 *
 *
 *
 * Issue in Jira:http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-66
 *
 * <p>
 *
 * Lang06.1)Secure Communication
 *
 * </p>
 *
 * <h2>3. Requirements</h2>
 *
 * There should be a new mechanism to add secure communications (encrypted
 *
 * communications) between instances of Cleansheets. It is not required at this
 *
 * moment that the cypher should be 'professional', only that it should not be
 *
 * trivial to break it. It should be possible to establish secure and unsecure
 *
 * communications with other instances. Cleansheets should now have a new window
 *
 * that logs all the incoming and outgoing communications. Therefore, when this
 *
 * window is activated it should be possible to see encrypted and unsecure data
 *
 * being exchanged. For testing purposes it should be possible for the user to
 *
 * send simple text messages either unsecure or encrypted.
 *
 *
 *
 * <b>Use Case "Secure Communication":</b>
 *
 * <h2>4. Analysis</h2>
 *
 * To this feature, first it is necessary to do a mechanism to encrypt and
 *
 * decrypt communications. To do so, I will use a simple process where a key is
 *
 * generated a symetric key (a random value) which is passed in the begin of the
 * connection and
 *
 * is used to encrypt and decrypt the data sent/received. It is necessary to add
 *
 * a new windows to the menu, to decide if the communication must be
 *
 * secure or unsecure, to show all the data that is being
 *
 * transfered (encrypted or not).
 *
 *
 *
 *
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 *
 *
 *
 * <img src="doc-files/ipc06.1analysisSSD.png" alt="image">
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * <h2>5. Design</h2>
 *
 *
 *
 * <h3>5.1. Functional Tests</h3>
 * For functional testing, navigate to window to Secure communication and
 * stablish secur communication on port 3079 the result expect is the
 * information was encrypted and only can be decrypted with password
 * "Lapr4Encrypt"
 * <h3>6.1 UC Realization</h3>
 *
 *
 *
 *
 *
 * <h3>5.3. Classes</h3>
 *
 *
 *
 *
 * <img src="doc-files/ipc06.1_image1.png" alt="image">
 *
 *
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 *
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 *
 * <p>
 * The following diagram shows the setup of the "Secure Communications"
 * extension when cleansheets is run
 * <img src="doc-files/design06.1.png" alt="image">
 * <img src="doc-files/extension-setup.png alt="image">
 *
 * <p>
 *
 * <h2>6. Implementation</h2>
 *
 *
 *
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 *
 *
 *
 *
 *
 *
 * <h2>8. Final Remarks</h2>
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 *
 *
 *
 *
 *
 *
 * <b>Monday</b>
 *
 *
 *
 * Made analysis OO.
 *
 *
 *
 * <b>Tuesday</b>
 *
 *
 *
 * Made Test design OO , and Start implementation
 *
 *
 *
 *
 *
 *
 *
 * <p>
 *
 * <b>Wednesday</b>
 *
 * </p>
 *
 * Updating DesignOO and continuing the implementation
 *
 * <p>
 *
 * <b>Thursday</b>
 *
 * </p>
 *
 * Fixed some erros on the implementation finish DesignOO Finish implementation
 * code and fixed precious code erros from IPC tcp connection
 *
 *
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 *
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 *
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 *
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 *
 * mais de 90%) e apresentam código que para além de não ir contra a arquitetura
 *
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 *
 * sincronização, padrões de eapli, etc.)
 *
 * <p>
 *
 * <b>Evidences:</b>
 *
 * <&p>
 *
 * - url of commit: ... - description: this commit is related to the
 *
 * implementation of the design pattern ...-
 *
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 *
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 *
 *
 *
 */
package csheets.worklog.n1140921.sprint4;

/**
 *
 * This class is only here so that javadoc includes the documentation about this
 *
 * EMPTY package! Do not remove this class!
 *
 *
 *
 */
class _Dummy_ {

}
