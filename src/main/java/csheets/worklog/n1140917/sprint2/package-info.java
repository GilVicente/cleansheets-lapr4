/**
 * Technical documentation regarding the work of the team member (1140917) Antonio Soutinho during week1. 
 * 
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that each team member must produce each week/sprint. Suggestions on how to build this documentation will appear between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 * 
 * <h2>1. Notes</h2>
 *
 * This is the second week of work. Sprint2 week.
 * This week each functional area has 4 new functional increments, 1 for each student.
 * My functional area is working on the Core division of issues.
 * Core01.2-Auto-description of Extensions;
 * Core02.2-Tooltip and User Associated to Comment;
 * Core04.1-Navigation Window;
 * Core05.1-Email Configuration;
 * <p>
 * This weak I started with the analysis and understanding of the functional increment, Core04.1 - Navigation Window.
 * And also the all application itself.
 *
 * <h2>2. Use Case/Feature: Core04.1</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-110?filter=-1
 * <p>
 * Core04.1 - Navigation Window
 *
 * <h2>3. Requirements</h2>
 * A 'navigator' window must appear with information of the contents.
 * The user should have the possibility of viewing the contents: writing workbooks; spreadsheets; non-empty cells; formulas and values.
 * The user should also be able to double click an element and it will update the mouse focus to show the user the element he/she clicked on.
 * Like the manual of the project presents, The contents of the navigator should be automatically updated.
 * 
 * <p>
 * <b>Use Case "Navigation Window":</b>  
 * <h2>4. Analysis</h2>
 * Detailed analysis of the User Story 013: Navigation Window.
 * The objective is to create an extension that will present in a window information about some contents of the project (writing workbooks, spreadsheets, non-empty cells, formulas and values).
 * That information will be the names of the sheets existing in the workbook, cells that have some content written on them, definition of the formulas of the project and values.
 * This functional increment should result in the presentation of all the contents asked in the manual of the project, that will be provided by the navigation window.
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the previously described use case. We call this diagram an "analysis" use case realization because it functions like a draft that we can do during analysis or early design in order to get a previous approach to the design. 
 * <p>
 * <img src="doc-files/core04_01_SSD.png" alt="image"> 
 * 
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets.
 * 2- Select navigation window.
 * 3- The navigation window should display the contents referenced in the issue of the manual.
 * 4- The result should be the display of those contents.
 * 5- The user can double click on an element to show the element isolated.
 * 
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Sequence Diagram is a preview of the structure of the development of the issue Navigation Window.
 * <p>
 * <img src="doc-files/core04_01_SD.png" alt="image"> 
 * <p>
 * The following Class Diagram is a preview of the structure and conection between our classes of our issue (Navigation Window).
 * <p>
 * <img src="doc-files/core04_01_DC.png" alt="image"> 
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 * 
 * 
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * This issue could be implemented in a better way like in the mouse listener focus of the element, the refresh of the JPanel and the search for formulas. However, I will continue to work on this issue till I get the best performance of the task that was asked on the manual.
 * 
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * Example
 * <b>Monday</b>
 * <p>
 * I created my subtasks of the issue and planed all the work for the week and analysed the issue and started doing the design also.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * I finished the design and started implementing the issue following my analysis and design.
 * <p>
 * Blocking:I had some difficulties on understanding the implementation of the issue, because I wasn't sure if the issue asked for an extension or not.
 * 
 * <p>
 * <b>Wednesday</b>
 * <p>
 * I worked on the implementation and began to implement the issue in another way that suit the manual better. Re opened the design and analysis and updated them with this new way that I was going to implement the code.
 * <p>
 *  Blocking:I had some difficulties on the implementation of tests because the methods that I created was on a controller and in a Panel and all of them was getLists of content, and I wasn't so sure I could make any test of that.
 * 
 * 
 * <b>Thursday</b>
 * <p>
 * Finished some adjustments to the implementation and concluded the tests. Updated the worklog as well.
 * 
 * 
 * <h2>10. Self Assessment</h2>
 *
 * I think that in this sprint I fill almost all the requirements that were asked on the manual to do, just couldn't finished the implementation of the mouse focus and I implemented some tests even though they were a little redundant because it was all about methods gets. In this sprint I think I deserve a 4 out of 5.
 *
 *
 *
 */
 
package csheets.worklog.n1140917.sprint2;
 
/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {}