/**
 * Technical documentation regarding the work of the team member (1140917) Antonio Soutinho during week4. 
 * 
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that each team member must produce each week/sprint. Suggestions on how to build this documentation will appear between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 *
 * This is the fourth week of work. Sprint4 week.
 * My functional area is working on the ipc division of issues.
 * Ipc05.1-Chat send message;
 * Ipc07.1-Choose game and partner;
 * Ipc04.3-Import and export database;
 * Ipc06.1-Secure Communication;
 * <p>
 * This weak I started with the analysis and understanding of the functional increment, Ipc05.1-Chat send Message.
 *
 * <h2>2. Use Case/Feature: Ipc05.1</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-40
 * <p>
 * Ipc05.1-Chat send Message
 *
 * <h2>3. Requirements</h2>
 * 
 * The cleansheets will support a new extension that will allow the user to send a new message to another instance of cleansheets.
 * The address of that instance should be known and the message send to that new instance should appear in a popup window in the new instance.
 * 
 * <p>
 * <b>Use Case "Chat send Message":</b>  
 * 
 * <h2>4. Analysis</h2>
 * 
 * The objective is to initiate two instances of Cleansheets in the same machine. 
 * When the user writes something in an instance, the user should be able to send that information to the other instance of cleansheets. 
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the previously described use case. We call this diagram an "analysis" use case realization because it functions like a draft that we can do during analysis or early design in order to get a previous approach to the design. 
 * <p>
 * <img src="doc-files/ipc05_01_analysisSSD.png" alt="image"> 
 * 
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets.
 * 2- Select extension chat send message.
 * 3- The user selects the sidebar window to send a message.
 * 4- The window should display all the messages.
 * 5- The user should be able to send a message to another instance of cleansheets with the proper address for that instance.
 * 6- The other instance of cleansheets should have a popup window that will popup with the message sent from the user.
 * 
 * 
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Sequence Diagram is a preview of the structure of the development of the issue Multiple Macros.
 * <p>
 * <img src="doc-files/ipc05_01_designSD.png" alt="image"> 
 * <p>
 * The following Class Diagram is a preview of the structure and conection between our classes of our issue (Multiple Macros).
 * <p>
 * <img src="doc-files/ipc05_01_designDC.png" alt="image"> 
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 * 
 * 
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * This issue was could be implemented in a better way. For instance, I could have done a tree instead of a jtextArea to save the  reply messages in a better design.
 * 
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * Example
 * <b>Monday</b>
 * <p>
 * I created my subtasks of the issue and planed all the work for the week and analysed the issue and started doing the design also.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * I continued working on design and started implementing the issue following my analysis and my thoughts on the design of the issue.
 * <p>
 * Blocking:I had some difficulties on working on my issue because of the ipc fuctional area wasn't implemented in the best way to work on my issue, so me and some of my colleagues were working on that area as well.
 * 
 * <p>
 * <b>Wednesday</b>
 * <p>
 * I worked on the implementation and began to implement the issue in another way that suit the new implementation of the tcp conection, that could support my issue. 
 * <p>
 * 
 * 
 * <b>Thursday</b>
 * <p>
 * Finished some adjustments to the implementation and concluded the tests. Updated the worklog as well.
 * 
 * 
 * <h2>10. Self Assessment</h2>
 *
 * I think that in this sprint I fill allmost all the requirements that were asked on the manual to do, could not finish the feature that would save the replies in a tree. Couldn't find the time to implement some tests to prove that my methods were 100%. In this sprint I think I deserve a 3 out of 5 even though I didn't create tests to support my work.
 *
 *
 *
 */
 
package csheets.worklog.n1140917.sprint4;
 
/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {}