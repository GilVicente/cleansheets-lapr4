/**
 * Technical documentation regarding the work of the team member (1140917) Antonio Soutinho during week1. 
 * 
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that each team member must produce each week/sprint. Suggestions on how to build this documentation will appear between '-' like this one. You should remove these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * 
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * 
 * <h2>1. Notes</h2>
 *
 * This is the second week of work. Sprint2 week.
 * This week each functional area has 4 new functional increments, 1 for each student.
 * My functional area is working on the Lang division of issues.
 * Lang04.2-Insert Function Intermediate Wizard;
 * Lang03.2-Conditional Formatting of Ranges;
 * Lang02.1-Temporary Variables;
 * Lang05.2-Multiple Macros;
 * <p>
 * This weak I started with the analysis and understanding of the functional increment, Lang05.2-Multiple Macros.
 *
 * <h2>2. Use Case/Feature: Lang05.2</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-40
 * <p>
 * Lang05.2 - Multiple Macros
 *
 * <h2>3. Requirements</h2>
 * The cleansheets should support multiple macros.
 * Each macro should have a name and be associated with a specific workbook (persistence of macro).
 * The macros can also support the invocation of other macros.
 * Like the manual of the project presents, The contents of the navigator should be automatically updated.
 * 
 * <p>
 * <b>Use Case "Multiple Macros":</b>  
 * 
 * <h2>4. Analysis</h2>
 * The objective is to save the macros to a specific workbook so that the macro persists within the workbook. 
 * The grammar of the macros should also have a mechanism that will support the invocation of a macro, that is saved in the active workbook.
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the previously described use case. We call this diagram an "analysis" use case realization because it functions like a draft that we can do during analysis or early design in order to get a previous approach to the design. 
 * <p>
 * <img src="doc-files/lang05_02_analysisSSD.png" alt="image"> 
 * 
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets.
 * 2- Select extension macros window.
 * 3- The macros window should now be able to save macros in the active workbook.
 * 4- The result should be the saving of the macros.
 * 5- The user can invoke the macros that were saved in the active workbook in the creation of new macros.
 * 
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Sequence Diagram is a preview of the structure of the development of the issue Multiple Macros.
 * <p>
 * <img src="doc-files/lang05_02_designSD.png" alt="image"> 
 * <p>
 * The following Class Diagram is a preview of the structure and conection between our classes of our issue (Multiple Macros).
 * <p>
 * <img src="doc-files/lang05_02_designDC.png" alt="image"> 
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 * 
 * 
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * This issue was could be implemented in a better way. For instance, I could have done a better way to persist the macros like an inmemmory repository in a repository factory, but with the last minutes that I had for this issue, I wasn't able to modified the code to those standards. However, maybe I will continue to work on this issue and try to implement a better way to persist the macros.
 * 
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * Example
 * <b>Monday</b>
 * <p>
 * I created my subtasks of the issue and planed all the work for the week and analysed the issue and started doing the design also.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * I continued working on design and started implementing the issue following my analysis and my thoughts on the design of the issue.
 * <p>
 * Blocking:I had some difficulties on understanding the implementation of the issue, because I wasn't so sure on how to persist the macros.
 * 
 * <p>
 * <b>Wednesday</b>
 * <p>
 * I worked on the implementation and began to implement the issue in another way that suit the manual better (my colleague had made the issue before mine and I had to rearrange some classes and create another way to create macros cause the issue before mine was only to run temporary macros). Worked on the design and analysis and updated them with this new way that I was going to implement the code. Started working on tests as well.
 * <p>
 * 
 * 
 * <b>Thursday</b>
 * <p>
 * Finished some adjustments to the implementation and concluded the tests. Updated the worklog as well.
 * 
 * 
 * <h2>10. Self Assessment</h2>
 *
 * I think that in this sprint I fill all the requirements that were asked on the manual to do, just couldn't find a better way to persist the macros with the given time on the issue and I implemented some tests to prove that my methods work and were 100%. In this sprint I think I deserve a 4 out of 5.
 *
 *
 *
 */
 
package csheets.worklog.n1140917.sprint3;
 
/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {}