/**
 * Technical documentation regarding the work of the team member 1140984, Gil
 * Vieira during week4.
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the fourth week of work. Sprint4 week.
 * <p>
 * IPC04.3 - Import/Export Database
 * <p>
 * This weak i started with the analysis and understanding of the functional
 * increment, IPC04.3 - Import/Export Database
 * <p>
 * I will use a local database, so a local database must be created as well as
 * jdbc connection.
 *
 * <h2>2. IPC04.3 - Import/Export Database</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-62
 * <p>
 * IPC04.3 - Import/Export Database
 *
 * <h2>3. Requirements</h2>
 * <b>IPC04.3 - Import/Export Database:</b>
 * It should be possible to export and import data to/from a table in a
 * database. Each row in the table corresponds to a row in Cleansheets and each
 * column in the table corresponds to a column in Cleansheets. The user should
 * enter a range of cells to be used as source (export) or destination (import)
 * for the operation. The first row of the range should be treated as a header.
 * Each column in the first row is used as the name of the corresponding
 * database column. This feature should be based in jdbc (Java Database
 * Connectivity). The user should specify a database connection to be used and
 * the name of the table.
 * <h2>4. Analysis</h2>
 * It should be possible to import and export to a DataBase a range of cells,
 * selected by the user. The user should input the table's name and database
 * connection(url, username, password). There will be needed a Local Database 
 * for this functional increment and the application will import/export 
 * the data using JDBC.
 * <p>
 * <img src="doc-files/ipc04_3_analysisSSD_Export.png" alt="image">
 * <img src="doc-files/ipc04_3_analysisSSD_Import.png" alt="image">
 *
 * <h2>5. Design</h2>
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets.
 * <p>
 * 2- Select a range of cells.
 * <p>
 * 3- Select Extensions, into Import/Export Database and select export.
 * <p>
 * 4- The following window should request the Database URL and tables name.
 * <p>
 * 5- After confirmation, the export action should be sucessfull.
 * <p>
 * 1- Run cleansheets.
 * <p>
 * 2- Select a range of Cells.
 * <p>
 * 3- Select Extensions, into Import/Export Database and select import.
 * <p>
 * 4- The following window should request the Database Connection and tables 
 * name.
 * <p>
 * 5- After confirmation, the selected cells should contain the information
 * stored at the Database.
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this uc i'll have to create several classes, 2 UI's and a local
 * database.
 * ImportExportDBExtension > ImportExportDBExtensionUI > 
 * ImportAction > ImportDataBasePanel > ImportDataController
 * ExportAction > ExportDataBasePanel > ExportDataController >
 * JDBCConnection
 * <h3>5.3. Classes</h3>
 * The following Sequence Diagram is a preview of the structure of the
 * development of the issue IPC04.3 - Import/Export Database
 * <p>
 * <img src="doc-files/ipc04_3_sdImportDB.png" alt="image">
 * <p>
 * <img src="doc-files/ipc04_3_sdExportDB.png" alt="image">
 * Class Diagram
 * <p>
 * <img src="doc-files/ipc04_03_dc.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * Creator,low coupling, high cohesion, polymorphism.
 *
 * <h2>6. Implementation</h2>
 * <p>
 * csheets.ext.ImportExportDatabase.ImportExportDBExtension
 * <p>
 * csheets.ext.ImportExportDatabase.ui.ImportExportDBExtensionUI
 * <p>
 * csheets.ext.ImportExportDatabase.ui.ImportExportDBMenu
 * <p>
 * csheets.ext.ImportExportDatabase.ui.ImportAction
 * <p>
 * csheets.ext.ImportExportDatabase.ui.ImportDataBasePanel
 * <p>
 * csheets.ext.ImportExportDatabase.ui.ImportDataBaseController
 * <p>
 * csheets.ext.ImportExportDatabase.ui.ExportAction
 * <p>
 * csheets.ext.ImportExportDatabase.ui.ExportDataBasePanel
 * <p>
 * csheets.ext.ImportExportDatabase.ui.ExportDataBaseController
 * <p>
 * csheets.ext.ImportExportDatabase.persistance.JdbcConnection
 * <p>
 * <h2>7. Integration/Demonstration</h2>
 *
 * <h2>8. Final Remarks</h2>
 *
 * 
 *
 * <h2>9. Work Log</h2>
 *
 * <b>Monday</b>
 * <p>
 * Understanding the client requirements. And started the analysis.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Analysis of the Functional Increment - IPC04.3 - Import/Export Database
 * <p>
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Started tests, design and implementation
 * <p>
 * <b>Thursday</b>
 * <p>
 * Continued with tests, design and implementation
 * <h2>10. Self Assessment</h2>
 * <h3>10.1. Design and Implementation:3</h3>
 * <b>Evidences:</b>
 * <p>
 * - url of commit:
 * <p>
 * <p>
 * <p>
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140984.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
