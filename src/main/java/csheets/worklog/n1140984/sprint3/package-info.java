/**
 * Technical documentation regarding the work of the team member 1140984, Gil
 * Vieira during week3.
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the third week of work. Sprint3 week. In this week every developer
 * has a functional increment to do. Things are getting serious.
 * <p>
 * Lang02.1 - Temporary Variables
 *<p>
 * This weak i started with the analysis and understanding of the functional
 * increment, Lang02.1 - Temporary Variables
 * <p>
 * Grammar will have to be studied as well as expressions/formulas validation.
 *
 * <h2>2. Lang02.1 - Temporary Variables</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-30
 * <p>
 * Lang02.1 - Temporary Variables
 *
 * <h2>3. Requirements</h2>
 * <b>Functional Increment Lang02.1 - Temporary Variables:</b>
 * Add support for temporary variables. The name of temporary variables must
 * start with the "_" sign. When a variable is referred in a formula for the
 * first time it is created. To set the value of a variable it must be used on
 * the left of the assign operator (":="). Temporary variables are variables
 * that only exist in the context of the execution of a formula. Therefore, it
 * is possible for several formulas to use temporary variables with the same
 * name and they will be different instances. 
 * <h2>4. Analysis</h2>
 * It should be possible to create and use a temporary variable. That variable
 * will only be available at a cell and formula scope.
 * <p>
 * <img src="doc-files/core01_2_analysisSSD.png" alt="image">
 *
 * <h2>5. Design</h2>
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets.
 * <p>
 * 2- Select cell
 * <p>
 * 3- Declare and use temporary variables at cell and formula scope
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this functional increment i'll have to study the project's grammar
 * ,and will afterwords create several classes and edit several more. 
 * TemporaryVariable, TempVariableRef,TemporaryVariableRepository are created.
 * ExcelExpressionCompiler, Assign, ExpressionBuilder etc were edited.
 *
 * <h3>5.3. Classes</h3>
 * The following Sequence Diagram is a preview of the structure of the
 * development of the issue Lang02.1 - Temporary Variables
 * <p>
 * <img src="doc-files/lang02_1_sd.png" alt="image">
 * <p>
 * Class Diagram
 * <p>
 * <img src="doc-files/lang02_1_analysisDC.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * Creator, Information Expert, low coupling, high cohesion, polymorphism.
 *
 * <h2>6. Implementation</h2>
 * <p>
 * csheets.core.TemporaryVariable
 * <p>
 * csheets.core.formula.lang.TempVariableRef
 * <p>
 * csheets.core.formula.persistence.inmemory.TemporaryVariableRepository
 * <p>
 * csheets.core.formula.lang.Assign
 * <p>
 * csheets.core.formula.compiler.ExcelExpressionCompiler
 * <p>
 * csheets.core.formula.persistence.inmemory.TemporaryVariableRepository
 * <p>
 * <p>
 * Unit tests in Test Packages at:
 * <p>
 * csheets.core.TemporaryVariableTest
 * <p>
 * csheets.core.TempVariableRefTest
 * <p>
 * csheets.core.formula.persistence.inmemory
 * <h2>7. Integration/Demonstration</h2>
 *
 * <h2>8. Final Remarks</h2>
 *
 * After full implementation and some painful grammar issues, the functional increment is working.
 *
 * <h2>9. Work Log</h2>
 *
 * <b>Monday</b>
 * <p>
 * Understanding the client requirements. And started the analysis.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Analysis of the Functional Increment - Lang02.1 - Temporary Variables
 * <p>
 * Started tests, design and implementation
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Tests, design and implementation
 * <p>
 * <b>Thursday</b>
 * <p>
 * Finished tests, design and implementation
 * <h2>10. Self Assessment</h2>
 * - 4
 * <h3>10.1. Design and Implementation:3</h3>
 * <b>Evidences:</b>
 * <p>
 * - url of commit:
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/0d58afe6a76176ead2c359550d191a821071d090
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/1152f163c0e3a1414a7ab62eebdbb738773fb531
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/b85cb97552e6f8009429b6fce528b571924eadf4
 * 
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140984.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
