/**
 * Technical documentation regarding the work of the team member 1140984, Gil
 * Vieira during week2.
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the second week of work. Sprint2 week. In this week every developer
 * has a functional increment to do.
 * <p>
 * Core01.2 - Auto-description of Extensions
 *
 * This weak i started with the analysis and understanding of the functional
 * increment, Core01.2 - Auto-description of Extensions
 *
 * <h2>2. Core01.2 - Auto-description of Extensions</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-2?filter=-1
 * <p>
 * Core01.2 - Auto-description of Extensions
 *
 * <h2>3. Requirements</h2>
 * <b>Use Case Core01.2 - Auto-description of Extensions:</b>
 * Extensions should have associated metadata. Particularly, extensions should
 * have a version number, a name and a description. Cleansheets should display
 * to the user the metadata of the extensions before loading them. The user
 * should be able to cancel the loading of an extension and also to select the
 * version of the extension to be loaded (if there are more than one).
 * <h2>4. Analysis</h2>
 * This functional increment should result in functionality that allows the user
 * of CleanSheets to see the name, version and description of the extensions
 * present in the application. The user will then select the extensions to be
 * loaded. After the user selections, the system will load the selected extends
 * into CleanSheets.
 * <p>
 * <img src="doc-files/core01_2_analysisSSD.png" alt="image">
 *
 * <h2>5. Design</h2>
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets.
 * <p>
 * 2- Observe the CleanSheets extensions metadata in a new frame, before
 * CleanSheets main frame.
 * <p>
 * 3- Select the extensions to be loaded.
 * <p>
 * 4- The main frame of CleanSheets will now have the selected extensions
 * loaded.
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this functional increment i will need to create the following classes
 * LoadExtensions, LoadExtensionsCheckBoxList, ToContinue. For the
 * window showing up before the main frame a JDialog will be implemented.
 * <h3>5.3. Classes</h3>
 * The following Sequence Diagram is a preview of the structure of the
 * development of the issue Core01.02 - Auto-description of Extensions
 * <p>
 * <img src="doc-files/core01_2_sd.png" alt="image">
 * <p>
 * Class Diagram
 * <p>
 * <img src="doc-files/core01_2_analysisDC.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * Creator, Information Expert
 *
 * <h2>6. Implementation</h2>
 * <a href="../../../../csheets/ui/LoadExtensions/package-summary.html">csheets.ui.LoadExtensions</a><p>
 * <a href="../../../../csheets/ui/LoadExtensionsCheckBoxList/package-summary.html">csheets.ui.LoadExtensionsCheckBoxList</a><p>
 * <a href="../../../../csheets/ext/ToContinue/package-summary.html">csheets.ext.ToContinue</a><p>
 * <h2>7. Integration/Demonstration</h2>
 *
 *
 * <h2>8. Final Remarks</h2>
 *
 * The functional increment is working.
 *
 * <h2>9. Work Log</h2>
 *
 * <b>Monday</b>
 * <p>
 * Understanding the client requirements. And started the analysis.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Analysis of the Functional Increment - Core01.2 - Auto-description of
 * Extensions
 * <p>
 * Started tests, design and implementation
 * <b>Wednesday</b>
 * <p>
 * Continued previous work
 * <p>
 * <b>Thursday</b>
 * <p>
 * Finished tests design and implementation
 * <h2>10. Self Assessment</h2>
 *
 * <h3>10.1. Design and Implementation:3</h3>
 * <b>Evidences:</b>
 * <p>
 * - url of commit:<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/d1ba43cc6057c1142231491a984003ce0938945c<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/3905672b4c513f76a0ad3153c99da1b3a1e799ea<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/234f526f8b2d138dea091915c2d50aa017e6f740<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/a6054853bfcb36910a76e4945bd7f4c5ff79b05b<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/c4fe9c5305cd0b1416364f2a0fa591abdc924f15
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140984.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
