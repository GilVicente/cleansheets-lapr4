/**
 * Technical documentation regarding the work of the team member (1130618) Daniela Figueiredo during week2.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: CRM04.1</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-157
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-158
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-159
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-160
 *

 * 

 *
 * <h2>3. Requirement</h2>
 * <b>Use Case "Mail and Phone Number Edition":</b> the user should be able to create, edit and
 * remove mails and phone numbers associated with contacts. A mail consists on a email and name 
 * of the person belongs to.
 * The number is divided into work number,home number,celular1,celular2.
 *  When the contact is selected all its versions mails and numbers should appear and be able to 
 * edit and remove or should be able to create.
 
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 * Since <b>CRM - Cutomer Relationship Management</b> should be expanded with
 * the use of Extensions, after a first analysis of the requirements, it was
 * defined that the best approach to implement the Use Case would be to create a
 * new Extension of Cleansheets to Edit Mails and Phone Numbers. Also, since it is not specified
 * by the requirements of the Use Case, the type of presentation of the mails and numbers,
 * as the presentation method, will be a window, containing a list, with
 * all the user contacts and their associated mails and phone numbers. On the window, there
 * should be four buttons, 'Select','Edit' , 'Remove' and 'Create'. By clicking 'Select'
 * a mail list and phone number should appear, making possible the removal and edition processes 
 * with the buttons 'Remove' and 'Edit'.
 * 
 * .
 *
 * <h3>First "analysis" sequence diagram</h3>
 * <p>
 * 
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the main
 * functionality of this use case is to be able to create, edit, and remove
 * mails and phone numbers. We need to be able to get a list of Contacts, their mails and phone number, and be able
 * to create new ones, change existing notes, and remove mails and numbers. To do that, we
 * need to have a database for test purposes to allow us to test the
 * funcionallity of the Extension, and test function in order to determine if
 * the Extension in question is working properly.
 * <p>
 * see: <code>csheets.ext.contacts</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story, the Extension has the following classes:
 * Contact, InBox,Mail,PhoneNumber MailsAndNumbersEditionExtension, MailsController, PhoneNumberController
 * .Also, the Contact class on the Edit Contact Extension was modified to have a list of Contact
 * mails,have a phone number and the respective functions to interact with it.
 * <p>
 *
 * <img src="doc-files/CreateMails_SequenceDiagram.png" alt="image" >
 * <p>
 * <img src="doc-files/CreatePhoneNumber_SequenceDiagram.png" alt="image" >
 * <p>
 * <img src="doc-files/EditMails_SequenceDiagram.png" alt="image" >
 * <p>
 * <img src="doc-files/EditPhoneNumber_SequenceDiagram.png" alt="image" >
 * <p>
 * <img src="doc-files/RemoveMails_SequenceDiagram.png" alt="image" >
 * <p>
 * <img src="doc-files/RemovePhoneNumber_SequenceDiagram.png" alt="image" >
 * <p>
 * <img src="doc-files/MailsAndPhoneNumberEdition_ClassDiagram.png" alt="image" >
 * 
 *
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 *
 * see:
 * <a href="../../../../csheets/ext/contacts/Agenda/package-summary.html">csheets.ext.contacts.Agenda</a>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 *
 * -In this iteration was not used persistence, but all classes are allocated in
 * local memory.
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings. Example
 * <b>Tuesday</b>
 * <p>
 * 1. Use case Analysis and design
 * <p>
 *
 * <b>Wednesday</b>
 * <p>
 * 1. Implementation and Tests
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * - url of commit:
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130618.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130618
 */
class _Dummy_ {
}
