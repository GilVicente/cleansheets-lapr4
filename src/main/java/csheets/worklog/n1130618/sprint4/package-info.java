/**
 * Technical documentation regarding the work of the team member (1130618) Daniela Figueiredo during week4.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 *
 * <h2>2. Use Case/Feature: Lang 7.2</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-299
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-300
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-301
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-302
 *
 *
 *
 *
 *
 * <h2>3. Requirement</h2>
 * It should now be possible to invoke Beanshell scripts from macros and formulas using a function.
 * The function should be able to execute Beanshell scripts synchronously or asynchronously and show the result.
 * If the execution mode is synchronous, then the function should wait for the script to end its execution
 * for show the result (value of the last expression of the Beanshell script). 
 * If the execution mode is asynchronous then the function should return immediately after launching the execution 
 * of the script (formula/macro will execute in parallel).
 * 
 * <p>
 * In this phase the user will should be able to run a script in beanshell
 * and show the synchronously and asynchronously results with other funtions and inside other
 * funtions.

 *
 *
 * <h2>4. Analysis</h2>
 * 
 * The user will be able to access the cell and put the script file path you want to run, other functions
 * or this function and other simultaneously.
 *  The user will be able to choose to get the result, synchronously or asynchronously with a '1' in
 * the second parameter of the function to synchronous
 * or 0 for asynchronous.
 * The results appeared in the cell and on the console.
* 
 * <h3>First "analysis" sequence diagram</h3>
 * <p>
 * <img src="doc-files/SequenceDiagram_lang7.2.png" alt="image">
 *
 * <h2>5. Design</h2>
 * <p>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the main
 * functionality of this use case is to be able to run BeanShell script with/without other funtions.
 * To test the BeanShell we need to follow these steps:<p><p>
 * 1 - Insert a unary expression using fucntion Beanshell<p>
 * 2 - Insert a binary expression using fucntion Beanshell and others<p>
 * 3 - Insert a binary expression using fucntion Beanshell and others inside in the others functions <p>
 *
 * 
 * 
 *
 * <h3>5.2. UC Realization</h3>
 *
 * To realize this user story we will need to put a function(s) 
 * in cell and visualize the results in the respective cell (synchronous and
 * unary expressions) and console (asynchronous).
 * <p>
 *
 *
 * <h3>5.3. Classes</h3>
 * <img src="doc-files/classDiagram_lang7.2.png" alt="image" >
 *
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 *
 * see:
 *
 *<a href="../../../../csheets/ext/BeanShellIntegration/BeanShelIntegrationFunction.html">csheets.ext.BeanShelIntegration.Run_Script</a><p>
 * <a href="../../../../csheets/core/formula/lang/Beanshell.html">csheets.core.formula.lang.Beanshell</a><p>
 *
 * 
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 * Basically, from requirements and also analysis, we see that the main
 * functionality of this use case is create a function to be able run beanshell scripts 
 * synchronously,in other words, show result only in the end of the execution
 * and asynchronously,that is, show the result of each function in parallel.
 * .
 * 
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings. Example
 * <b>Tuesday</b>
 * <p>
 * 1. Use case Analysis and design
 * <p>
 *
 * <b>Wednesday</b>
 * <p>
 * 1. Implementation and Tests
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <p>
 * <b>Evidences:</b>
 * - url of commit:
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130618.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130618
 */
class _Dummy_ {
}
