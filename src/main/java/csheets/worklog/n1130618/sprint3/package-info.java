/**
 * Technical documentation regarding the work of the team member (1130618) Daniela Figueiredo during week2.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * -
 * <p>
 * -In this section you should register important notes regarding your work
 * during the week. For instance, if you spend significant time helping a
 * colleague or if you work in more than a feature.-
 *
 * <h2>2. Use Case/Feature: CRM04.1</h2>
 *
 * Issues in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-254
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-255
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-256
 * <p>
 * http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-257
 *

 * 

 *
 * <h2>3. Requirement</h2>
 * <b>It must be possible to send a workbook (name) search request to every
 * instance of cleansheets in the local network. The search must only
 * contemplate workbooks that are open on other instances. There must be a
 * sidebar window with a list to contain all the search results. The list must
 * be updated as the files are being found. On that list there must be the
 * instance where the workbook was found, it's name and the summary of the
 * workbook's content (name of the sheets and values of the first cells with the
 * value from each sheet.
 
 *
 *
 *
 * <h2>4. Analysis</h2>
 *
 * Since <b>IPC</b> The user searches for a workbook on the local network. A window with all the
 * workbooks open on different instances that are found is presented to the
 * user, this list must be updated as soon as it founds a workbook that matches
 * the criteria and it must contain the info from that workbook.
 * 
 * .
 *
 * <h3>First "analysis" sequence diagram</h3>
 * <p>
 * 
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that the main
 * functionality of this use case is to be able to search all open worbooks in network.
 * 
 * <p>
 * see: <code>csheets.ext.contacts</code>
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story, the Extension has the following classes:
 * FileSearcher,LocalWorkSearch,FindOpenWorkbookExtension, NetworkWorkbookList, FindOpenWorkbookController
 * .
 * <p>
 *
 * <img src="doc-files/SearchWorkbooks_SequenceDiagram.png" alt="image" >
 * <p>
 * 
 * 
 * 
 *
 * <h3>5.3. Classes</h3>
 *
 * -Document the implementation with class diagrams illustrating the new and the
 * modified classes-
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * -Information Expert, Creator, Controller-
 *
 *
 * <h2>6. Implementation</h2>
 *
 * see:
 * <a href="../../../../csheets/ext/contacts/Agenda/package-summary.html">csheets.ext.contacts.Agenda</a>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -Implementation, tests, analysis and design.
 *
 * <h2>8. Final Remarks</h2>
 *
 * -In this iteration was not used persistence, but all classes are allocated in
 * local memory.
 *
 *
 *
 * <h2>9. Work Log</h2>
 *
 * -Insert here a log of you daily work. This is in essence the log of your
 * daily standup meetings. Example
 * <b>Tuesday</b>
 * <p>
 * 1. Use case Analysis and design
 * <p>
 *
 * <b>Wednesday</b>
 * <p>
 * 1. Implementation and Tests
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 50%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * - url of commit:
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130618.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author 1130618
 */
class _Dummy_ {
}
