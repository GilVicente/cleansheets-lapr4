/**
 * Technical documentation regarding the work of the team member (1130750) Fabio
 * Silva during week1.
 *<p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *</p>
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *</p>
 * <h2>1. Notes</h2>
 *
 * This is the first week of work. Sprint1 week. This week we started with the 4
 * mandatory functional increments. 
 * <p>Core01.1-Enable and Disable Extensions</p>
 * <p>Lang01.1-Instructions Block</p> 
 * <p>IPC01.1-Start Sharing</p> 
 * <p>CRM01.1-Contact Edition</p>
 * This week I started with the analysis and understanding of the functional
 * increment, lang01.1 - Instructions Block. And also the all application
 * itself.
 *
 * <h2>2. Use Case/Feature: Lang01.1</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-109
 * 
 * Lang01.1-Instructions Block
 *
 * <h2>3. Requirements</h2>
 * Setup extension for formulas of CleanSheets. The user should have the
 * possibility of writing blocks of instructions. A block should be delimited by
 * curly braces and it's instructions separated by a tip-colon. Instructions are
 * executed sequentially and the result of the block is the result of the last
 * executed instruction.
 * <p>
 * <b>Use Case "Block of Instructions":</b>
 * <h2>4. Analysis</h2>
 * This functional increment should result in the execution of all the block
 * instructions and the block result should be the last instruction result. The
 * file formula.g has the grammar of the project and should be adapted in a way
 * that accepts the open curly brace ( { ) and recognizes it as the start of a
 * block of instructions. It will also recognize the tip-colong ( ; ) as a
 * delimiter of instructions and the close curly brace ( } ) as the end of the
 * block of instructions. Changes are also needed in the
 * ExcelExpressionsCompiler, so it can process an expression at the time for our
 * block of instructions.
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design.
 *
 * <img src="doc-files/lang01_01_analysisSSD.png" alt="image">
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * <p>1- Run cleansheets.</p> 
 * <p>2- Select cell.</p> 
 * <p>3- Use a formula using a block of instructions.</p> 
 * <p>4- The result value in the cell should.</p>
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Sequence Diagram is a preview of the structure of the
 * development of the issue Block of Instructions
 * <img src="doc-files/lang01_01_analysis.png" alt="image">
 *<p>
 * The following Class Diagram is a preview of the structure and conection
 * between our classes of our issue (Block of Instructions).
 * <img src="doc-files/lang01_01_analysisDC.png" alt="image">
 *</p>
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * This issue could be implemented in a better way over the time of the first
 * sprint and it will be worked on in this last days, before starting the new
 * sprint.
 *
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * <b>Monday</b>
 * 
 * Me and my team studied the project and the implementation of it.
 * </p>
 * 
 * <p>
 * <b>Tuesday</b>
 * 
 * I analysed the issue that my team and I were going to work on this week and I
 * worked on the analysis and requirements of the issue Block of Instructions.
 * 
 * Blocking:I had some difficulties on understanding the implementation of this
 * application and how we should start implementing this issue.
 *
 * </p>
 * 
 * <p>
 * <b>Wednesday</b>
 * 
 * I worked on the analysis and the design of the issue Block of Instructions
 * and the implementation as well.
 *</p>
 * <p>
 * <b>Thursday</b>
 *
 * Finished some adjustments to the design of the issue Block of instructions
 * and worked on implementation.
 *
 *</p>
 * <h2>10. Self Assessment</h2>
 *
 * <h3>10.1. Design and Implementation:</h3>
 *
 * <b>Evidences:</b>
 * 
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1130750.sprint1;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
