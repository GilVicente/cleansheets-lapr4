/**
 * Technical documentation regarding the work of the team member (1130750) Fabio
 * Silva during week4.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * <h2>2. Use Case/Feature: IPC07.1</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-69
 * <p>
 * IPC07.1) Choose Game and Partner
 *
 * <h2>3. Requirement</h2>
 * With this extension it should be possible for two instances of Cleansheets to
 * establish a connection to play games. Cleansheets should display a list of
 * all instances of Cleansheets in the local network. It should be possible to
 * invite another instance to play one of the following games: tic-tac-toe
 * (https://en.wikipedia. org/wiki/Tic-tac-toe) or battleships
 * (https://en.wikipedia.org/wiki/Battleship_(game)). Each instance should be
 * able to define the profile of the user. The user name should be the user name
 * of the system. The user should be able to configure an icon or photograph to
 * be associated to his profile. The name of the users and icon should be
 * displayed in the list of available users to play with. The user should be
 * able to play several games at the same time. There should be a sidebar to
 * display all the active games and also all the online users (i.e., instances
 * of Cleansheets). It should be possible to end a game. At this moment it is
 * not required to effectively implement the games.
 *
 * <p>
 * <b>Use Case "Choose Game and Partner":</b> The user selects the set user
 * profile function. The system shows the user the respective user name (user
 * name of the system) and asks for the profile picture. The user selects the
 * profile picture and concludes the process. The system shows the success of
 * the operation. And displays on a sidebar the active games and active users.
 * The system selects an user from the list to invite to a game. The system
 * shows the list of games and asks the user to choose. The user chooses a game.
 * The system sends the invite to the selected user.
 *
 * <h2>4. Analysis</h2>
 * Since networking games will be supported in a new extension to cleansheets we
 * need to study how extensions are loaded by cleansheets and how they work. The
 * first sequence diagram in the section
 * <a href="../../../../overview-summary.html#arranque_da_aplicacao">Application
 * Startup</a> tells us that extensions must be subclasses of the Extension
 * abstract class and need to be registered in special files. The Extension
 * class has a method called getUIExtension that should be implemented and
 * return an instance of a class that is a subclass of UIExtension. In this
 * subclass of UIExtension there is a method (getSideBar) that returns the
 * sidebar for the extension. A sidebar is a JPanel.
 *
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts the user interacting with the system, when
 * using the networking games exteension.
 *
 * <p>
 * <img src="doc-files/ipc_07_01_SSD.png" alt="image">
 *
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * There will be two classes crucial to this user story, which are:
 * PlayerDetection and Game server. Since the threads that envolve sockets and
 * transfer of packets through the network cant be tested, i decided to, however
 * something we the compression, serialization/deserialization and
 * encoding/decoding methods used to transform the objects to be moved around in
 * the network, into arrays of bytes.
 * <p>
 * see: <code>csheets.ext.networkingGames.PlayerDetectionTest</code>
 *
 * <h3>5.2. UC Realization</h3>
 *
 * <h2>
 * Setting up user profile</h2>
 * <p>
 * <img src="doc-files/SequenceDiagramUserProfile.png" alt="image">
 *
 * <h2> View online users</h2>
 * <p>
 * <img src="doc-files/SequenceDiagramViewUsers.png" alt="image">
 *
 * <h2>
 * Create Game</h2>
 * <p>
 * <img src="doc-files/SequenceDiagramCreateGame.png" alt="image">
 *
 * <h3>5.3. Classes</h3>
 *
 * <p>
 * Class Diagram</p>
 * <p>
 * <img src="doc-files/ClassDiagram.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High Coesion and Low Coupling
 *
 * <h2>6. Implementation</h2>
 *
 * <p>
 * see:
 * <p>
 * <a href="../../../../csheets/ext/networkingGames/package-summary.html">csheets.ext.networkingGames</a><p>
 * <a href="../../../../csheets/ext/networkingGames/ui/package-summary.html">csheets.ext.networkingGames.ui</a>
 *
 * <h2>7. Integration/Demonstration</h2>
 * The demonstration to the client didnt go well, since there were bugs in the
 * PlayerDetection class, and it wasnt possible to detect other instances of
 * CleanSheets in the network, which is crucial to this user story. However
 * shortly afterwards this problem was solved, and the bugs were corrected.
 *
 * <h2>8. Final Remarks</h2>
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * <b>Monday</b>
 * I worked on analysis
 * <p>
 * <b>Tuesday</b>
 * I worked on design and implementation
 * <p>
 * <b>Wednesday</b>
 * I worked on implementation and design.
 * <p>
 * <b>Thurday</b>
 * I worked on implementation, design and tests.
 *
 * <h2>10. Self Assessment</h2>
 *
 *
 * <h3>10.1. Design and Implementation:</h3>
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130750.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
