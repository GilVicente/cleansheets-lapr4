/**
 * Technical documentation regarding the work of the team member (1130750) Fabio
 * Silva during week3.
 *
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * 
 * <p>
 *
 * <h2>2. Use Case/Feature: Lang04.2</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-230
 * <p>
 * Lang04.2) Insert Function Intermediate Wizard
 *
 * <h2>3. Requirements</h2>
 * The wizard window should display an edit box for each parameter of the
 * selected function. The user should use these edit boxes to enter the values
 * for each parameter of the function. As the user enters the values the wizard
 * should display (in a new region of the window) the result of the execution of
 * the formula or a message explaining the problem. The function list should now
 * include also the operators as well as the functions that are dynamically
 * loaded from java.lang.Math. The wizard should be now launched from an icon or
 * button located in the formula bar, between the label of the active cell and
 * the edit box of the formula/value of the current cell. The menu option should
 * be removed.
 *
 * <p>
 * <b>Use Case "Insert Function Intermediate Wizard":</b> The user selects the
 * cell where he/she wants to insert a formula, and then presses the wizard
 * button on the formula bar. The system displays the wizard window. The user
 * selects a function/operator from the list. The system displays the syntax of
 * the selected function/operator and an edit box for each parameter. The user
 * inserts the parameters. The system shows the result of the execution of the
 * formula. The user presses the insert button. The system inserts the formula
 * in the active cell.
 *
 * <h2>4. Analysis</h2>
 * In order to understand this use case, first it will be necessary to
 * understand the previous increment, and how the window and the controller
 * work. 
 * It will not be necessary to create nem classes, we will however need to
 * add methods to the <code>WizardFunctionController</code> and
 * <code>Language</code> classes, to be able to add operators to the function
 * list. 
 * It will also be necessary to create a method in the
 * <code>WizardFunctionController</code> that receives an identifier and a list
 * of strings and rebuilds it into a formula, since we will be inserting each
 * parameter in a different box. 
 * Considering that certain functions have an unlimited
 * number of parameters, it will be necessary allow the user to always insert
 * one more parameter in these functions, which means I will have to study how 
 * to insert a text field in the graphical interface during RunTime.
 *
 * To lauch the wizard from a button on the formula bar we will have to analyse
 * the class <code>Frame</code> and check where the formula bar is created.
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts how the user interacts with the system when its
 * using the function wizard feature.
 * <p>
 * <img src="doc-files/lang04_02_AnalysisSSD.png" alt="image">
 * <p>
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we see that there are two
 * major functionlities to this issue, which are add to the function list,
 * operators and functions from java.lang.Math, and also add text boxes to edit
 * the parameters of the functions/operators. Since this use case is focused
 * heavily on graphical interface, we will test mostly the
 * <code>wizardFunctionCotroller</code> class. To test if the list contains also
 * operators and functions from java.lang.Math we will get the list of objects
 * from the controller to load to the list, and check if it contains, operators
 * and functions from java.lang.Math. We can also test if the size of the list
 * is equal to the number of functions added to the number of operators that are
 * loaded by the <code>Language</code> class. To test the use of text boxes, we
 * will have to test the method<code>getSource()</code>, that will recreate a
 * function based on the identifier and the paramters, and check if it has a
 * valid syntax for functions, operators and functions from java.lang.Math.
 *
 * <p>
 * see:
 * <code>csheets.ext.function.Controller.WizardFunctionControllerTest</code>
 *
 * <h3>5.2. UC Realization</h3>
 * <p>
 * Sequence Diagram</p>
 * 
 * <img src="doc-files/lang04_02_DesignSD.png" alt="image">
 *
 * <h3>5.3. Classes</h3>
 * <p>
 * Class Diagram that illustrates how the classes related to this use case,
 * interact with each other.</p>
 *
 * <img src="doc-files/lang04_02_DesignCD.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High Coesion and Low Coupling
 * <p>
 * <h2>6. Implementation</h2>
 *
 *  
 * see:
 * <p>csheets.ext.function.Controller.WizardFunctionController</p>
 * <p>csheets.ext.function.ui.WizardDialog</p>
 * <p>csheets.ext.function.ui.WizardFunctionButton</p>
 * <h2>7. Integration/Demonstration</h2>
 *
 *
 * <h2>8. Final Remarks</h2>
 *
 * 
 * <p>
 *
 *
 * <h2>9. Work Log</h2>
 * <p>
 * <b>Monday</b>
 * On monday I worked on analysis of the requirements of this issue.
 * </p>
 * <p>
 * <b>Tuesday</b>
 * On Tuestady I worked on the analysis, testing and design.
 * </p>
 * <p>
 * <b>Wednesday</b>
 * On Wednesday I worked on implementation, and finishing certain aspets of
 * design.
 * </p>
 *
 * <h2>10. Self Assessment</h2>
 *
 * 
 * <h3>10.1. Design and Implementation:</h3>
 *
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130750.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * 
 */
class _Dummy_ {
}
