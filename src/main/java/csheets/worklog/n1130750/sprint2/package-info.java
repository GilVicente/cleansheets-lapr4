/**
 * Technical documentation regarding the work of the team member (1130750) Fábio
 * Silva during week2.
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 * </p>
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * </p>
 * <h2>1. Notes</h2>
 *
 *
 * <h2>2. Use Case/Feature: Core02.1</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-4
 * <p>
 * Core02.2) Tooltip and User Associated to Comment
 * </p>
 * <h2>3. Requirement</h2>
 * Cleansheets should register the name of the user that creates comments and
 * each cell should support several comments. When the mouse pointer is hovering
 * above a cell and the cell has comments then these comments should be
 * displayed in a form similar to a "tooltip". The name of the author of each
 * comment should also appear in all displays of comments. Comments should be
 * persisted with the workbook.
 *
 * <p>
 * <b>Use Case "Tooltip and User Associated to Comment":</b>
 * The user hovers the mouse pointer over a cell. If that cell contains
 * comments, they are displayed in a tooltip, with reference to the user that
 * created them.
 * </p>
 *
 * <h2>4. Analysis</h2>
 * To complete this use case, first it will be necessary to understand how the
 * classes in the csheets.ext.comments package. To create the tooltip
 * functionality it will be necessary to implement Java Tool Tip API on the UI
 * classes that represent the cells.
 *
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a user interacting with the system, using the
 * tooltip functionality that displays comments in a cell an the respective user
 * that created them.
 * <p>
 * <img src="doc-files/core02_02_AnalysisSSD.png" alt="image">
 * </p>
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Basically, from requirements and also analysis, we can see that there are two
 * core functionalities to this use case: each cell should have a tooltip with
 * its comments, and with each comment should also be registered the user that
 * created it. Since the tooltip is an ui element, it cant really be
 * tested with junit. The user associated comment however, can be tested. We need
 * to create a comment on a given cell and verifiy the user was registered
 * Following this approach we can start by coding a unit test that uses a
 * subclass of <code>CellExtension</code> with an existing attribute for user
 * comments with the corresponding method accessors (set and get). A simple test
 * can be to set this attribute with a simple string and to verify if the get
 * method returns the same string with the associated user. As usual, in a test
 * driven development approach tests normally fail in the beginning. The idea is
 * that the tests will pass in the end. see:
 * <code>csheets.ext.comments.CommentableCellTest</code>
 *
 * <h3>5.2. UC Realization</h3>
 * Since the CellRenderer class is responsible for rendering cells, we need to
 * make use of the setToolTipMethod(applicable to a JComponent) to realize this
 * user story. I decided to aplly this method in the
 * getTableCellRendererComponent, where there is already a section to apply a
 * tooltip, and we can just check if that cell has a comment and if so, set the
 * tool tip to the text in that comment.
 *
 * <h3>Setting ToolTip</h3>
 * The following diagram shows how the tooltip is set in Cleansheets.
 * 
 * <img src="doc-files/core02_02_design_tooltip.png" alt="image">
 *
 * <h3>Cleansheets registers user associated comment</h3>
 * The following diagram illustrates what happens when the user updates the text
 * of the comment of the current cell, and how the system registers the user
 * that created the comment.
 * 
 * <img src="doc-files/core02_02design_user_associated_comment.png" alt="image">
 *
 * <h3>5.3. Classes</h3>
 * The following class diagram illustrates how the major classes used in this
 * issue are connected.
 * 
 * <img src="doc-files/core02_02class_diagram_comments.png" alt="image">
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * Low Coupling and High coesion.
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 *
 * <h2>8. Final Remarks</h2>
 *
 * <h2>9. Work Log</h2>
 * <p>
 * <b>Monday</b>
 * On monday I worked on analysis of the requirements of this issue.
 * </p>
 * <p>
 * <b>Tuesday</b>
 * On Tuestady I worked on finishing the analysis, and began testing and design.
 * </p>
 * <p>
 * <b>wednesday</b>
 * On Wednesday I worked on implementation and finishing some aspects of the
 * design and testing.
 * </p>
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 */
package csheets.worklog.n1130750.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 * @author alexandrebraganca
 */
class _Dummy_ {
}
