/**
 * Technical documentation regarding the work of the team member (1140465) Hugo Cerdeira during week1. 
 * 
 *  * <p>

 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * Analysis of the features already developed on the Project.
 *
 * <h2>2. Use Case/Feature: IPC6_01.2</h2>
 * 
 * Implementation:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-186
 * <p>
 * Design:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-187
 * <p>
 * Analysis:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-188
 * <p>
 * Tests:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-189
 * <p>
 * -Include the identification and description of the feature-
 * 
 * <h2>3. Requirement</h2>
 * A user should be able to share cells with other user in real time.
 * 
 * <p>
 * <b>Use Case "Sharing's Automatic Update":</b> 
 * The user selects an instance to share his cells, after the user edits a cell the respective cell on the ohter instance should be edited as welll
 * 
 *  
 * <h2>4. Analysis</h2>
 * Everytime that a user edits a cell, other user, selected by the first one, needs to recive an automatic update on that cell.
 * In orther to achieve that a listener should run a method, that shares a cell, each time a cell is edited making it possible to share the cell in real time.
 * The method called by the listener is the method sendData() while the other instance is running thread is runing the code to recieve and replace the cells.
 * 
 * <h3>First "analysis" sequence diagram</h3>
 * The following diagram depicts a proposal for the realization of the previously described use case. We call this diagram an "analysis" use case realization because it functions like a draft that we can do during analysis or early design in order to get a previous approach to the design. For that reason we mark the elements of the diagram with the stereotype "analysis" that states that the element is not a design element and, therefore, does not exists as such in the code of the application (at least at the moment that this diagram was created).
 * <p>
 * <img src="doc-files/IPC6_01_2_analysis.png" alt="image">
 * <p>
 * 
 * From the previous diagram we see that we need to get the selected cells at the current spreadsheet.
 * Therefore, at this point, we need to study how to get the selected cells. This is the core technical problem regarding this issue.
 * <h3>Analysis of Core Technical Problem</h3>
 * We need to be able to add a new CellListener to the user spreadsheet we can do that cy getting the spreadsheetTable and then the spreadsheet itself, then, using a new class "ShareCellListener" we create a new listener and add it to the spreadsheet, using the method addCellListener
 * 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Instance sending cells:
 * 1-execute cleansheets
 * 2-configure a port
 * 3-activates sharing option
 * 4-double click on an instance
 * 5-eddit a cell
 * <p>
 * Instance recieving the cells:
 * 1-configure a port
 * <p>
 * 
 *
 * <h3>5.2. UC Realization</h3>
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * 
 * <h3>User Sends Cells in real time to specified instance</h3>
 * <p>
 * <img src="doc-files/IPC6_01_2_design.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 * 
 * -Document the implementation with class diagrams illustrating the new and the modified classes-
 * 
 *  <img src="doc-files/IPC04_01_dc.png" alt="image">
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -Describe new or existing design patterns used in the issue-
 * <p>
 * -You can also add other artifacts to document the design, for instance, database models or updates to the domain model-
 * 
 * <h2>6. Implementation</h2>
 * 
 * -Reference the code elements that where updated or added-
 * <p>
 * -Also refer all other artifacts that are related to the implementation and where used in this issue. As far as possible you should use links to the commits of your work-
 * <p>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 * 
 * <h2>8. Final Remarks</h2> 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <b>Tuesday</b>
 * <p>
 * Analysis
 * <p>
 *  Blocking: nothing
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Start of implementation, end of analysis
 * <p>
 * Blocking: nothing
 * <p>
 * Thursday
 * <p>
 * Finished implementation, design and tests
 * <p>
 * Blocking: nothing
 * <p>
 * <h2>10. Self Assessment</h2> 
 * 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * <p>
 * <b>Evidences:</b>
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/a3ac47daf7611ce443aa15e8ec7f45f1efdf59fc <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/db67cc48f65fb0013e791221d960cb39d2779877<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/2ac063eee03754b9908e9648260cd033d9b410a8<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/3326772aa57191ad82103f63634045340f02a875<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/879f773dabb12f35de8d23b7de77633049dbddeb<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/3383b6e1b712c1a6eb53768edbeba370aa169ae5<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/685962479a8dbe7a72bc3d15e6a247a1318b9b18<p>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author Hugo Ceridera
 */

package csheets.worklog.n1140465.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

