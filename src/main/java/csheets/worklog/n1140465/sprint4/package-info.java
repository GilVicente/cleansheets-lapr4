/**
 * Technical documentation regarding the work of the team member (1140465) Hugo Cerdeira during week4 
 * 
 *  
 * <p>
 * <b>Scrum Master: no</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * Analysis of the features already developed on the Project.
 *
 * <h2>2. Use Case/Feature: Lang 8.1</h2>
 * 
 * Implementation:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-308
 * <p>
 * Design:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-310
 * <p>
 * Analysis:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-311
 * <p>
 * Tests:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-309
 * <p>
 * 
 * <h2>3. Requirement</h2>
 * A user should be able to export is data to a xml file
 * 
 * <p>
 * <b>Use Case "Export XML":</b> 
 *  The user selects the option to export to XML
 *  The user selects what to export, tags to use on the xml and the xml file path
 *  A new file is createad where teh user specified
 
 *  
 * <h2>4. Analysis</h2>
 * To achieve the xml file we must have methods to build each individual part of the project, cells, spreadsheet and worklog.
 * Those methods would use the tags the the user specified before.*
 * 
 * 
 * <h3>First "analysis" sequence diagrams</h3>
 * The following diagrams depict a proposal for the realization of the previously described use case. We call this diagram an "analysis" use case realization because it functions like a draft that we can do during analysis or early design in order to get a previous approach to the design. For that reason we mark the elements of the diagram with the stereotype "analysis" that states that the element is not a design element and, therefore, does not exists as such in the code of the application (at least at the moment that this diagram was created).
 * <p>
 * Export XML analysis diagram
 * <p>
 * <img src="doc-files/Lang8_1_analysis.png" alt="image">
 * <p>
 
 * 
 * <h3>Analysis of Core Technical Problem</h3>
 * we need to be able to export the data to a xml file
 * 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Export to xml:
 * 1- Click on fle menu<p>
 * 2- Click on export to xml option<p>
 * 3- Fill the fields<p>
 * 4- press export<p>
 * <p>
 * 
 *
 * <h3>5.2. UC Realization</h3>
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * 
 * <h3>Exporting to Xml</h3>
 * <p>
 * <img src="doc-files/Lang8_1_design.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 * 
 *  <img src="doc-files/Lang8_1_cd.png" alt="image">
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -High cohesion and low coupling.<p>
 * -Polimorphism.<p>
 * -Information Expert.<p>
 * -Creator.<p>
 * -Controller.<p>
 * <p>
 * <h2>6. Implementation</h2>
 * 
 * <a href="../../../../csheets/ext/export/package-summary.html">csheets.ext.export</a><p>
 * <a href="../../../../csheets/ext/export/ui/package-summary.html">csheets.ext.export.ui</a>
 * <p>
 
 * <p>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 
 * -Analysis, design, implementation and tests. 
 * 
 * <h2>8. Final Remarks</h2> 
 * 
 * <h2>9. Work Log</h2> 
 * 
 ** <b>Tuesday</b>
 * <p>
 * Analysis of the issue
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Design and Implementation of the source code , started unit tests.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-<p>
 *<b> Thursday</b>
 *<p>
 * Tests and javadoc
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <p>
 * <h2>10. Self Assessment</h2> 
 * 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * <p>
 * <b>Evidences:</b>
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/4beb035740a3e45aea6d03e16fc839dee821c059 <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/433976d5423144b8453e2a0695a92d1051c90ba1<p>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author Hugo Ceridera
 */

package csheets.worklog.n1140465.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

