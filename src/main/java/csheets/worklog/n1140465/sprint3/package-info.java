/**
 * Technical documentation regarding the work of the team member (1140465) Hugo Cerdeira during week3 
 * 
 *  
 * <p>
 * <b>Scrum Master: yes</b>
 * 
 * <p>
 * <b>Area Leader: no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * <p>
 * Analysis of the features already developed on the Project.
 *
 * <h2>2. Use Case/Feature: Core05_2</h2>
 * 
 * Implementation:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-237
 * <p>
 * Design:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-236
 * <p>
 * Analysis:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-234
 * <p>
 * Tests:<p>
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-235
 * <p>
 * -Include the identification and description of the feature-
 * 
 * <h2>3. Requirement</h2>
 * A user should be able to send an email and see his email's outbox in a side bar
 * 
 * <p>
 * <b>Use Case "Send Email and Outbox":</b> 
 *  The user selects the option o write an email, fills the fields: destination, subject and body.
 *the user can select an option to add a cell, and after inserting the cell coordinates a cell is added to the email.
 * The user can also view a side bar that has all the sent emails, after double clicking on a sent email a new sidebar should be made visible showing the email content.
 * 
 *  
 * <h2>4. Analysis</h2>
 * Everytime that a user edits a cell, other user, selected by the first one, needs to recive an automatic update on that cell.
 * In orther to achieve that a listener should run a method, that shares a cell, each time a cell is edited making it possible to share the cell in real time.
 * The method called by the listener is the method sendData() while the other instance is running thread is runing the code to recieve and replace the cells.
 * 
 * <h3>First "analysis" sequence diagrams</h3>
 * The following diagrams depict a proposal for the realization of the previously described use case. We call this diagram an "analysis" use case realization because it functions like a draft that we can do during analysis or early design in order to get a previous approach to the design. For that reason we mark the elements of the diagram with the stereotype "analysis" that states that the element is not a design element and, therefore, does not exists as such in the code of the application (at least at the moment that this diagram was created).
 * <p>
 * Send an email analysis diagram
 * <p>
 * <img src="doc-files/Core05_2_sendMail_analysis.png" alt="image">
 * <p>
 * Shows outbox  
 * <p>
 * <img src="doc-files/Core05_2_showOutbox_analysis.png" alt="image">
 * <p>
 * 
 * From the previous diagram we see that we need to get the selected cells at the current spreadsheet.
 * Therefore, at this point, we need to study how to get the selected cells. This is the core technical problem regarding this issue.
 * <h3>Analysis of Core Technical Problem</h3>
 * we need to be able to validate an email and send it, after sending an email it needs to be stored on our data base in order to make the user able to see the outbox with old emails.
 * 
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Sending email:
 * 1- execute cleansheets<p>
 * 2- activate email extensio<p>
 * 3- on the email menu select send email<p>
 * 4- fill the fields, email, body and subject<p>
 * 5- selects cells you want to send<p>
 * 6- press add cells<p>
 * 7- press send email<p>
 * <p>
 * checking outbox:
 * 1- activate outbox sidebar<p>
 * 2- click on the upadte button<p>
 * 3- double click on a email<p>
 * <p>
 * 
 *
 * <h3>5.2. UC Realization</h3>
 * The following diagrams illustrate core aspects of the design of the solution for this use case.
 * <p>
 * 
 * <h3>Checking the outbox and view the sent emails</h3>
 * <p>
 * <img src="doc-files/Core05_2_check_outbox_design.png" alt="image">
 * 
 * <p>
 * <h3>Checking the outbox and view the sent emails</h3>
 * <p>
 * <img src="doc-files/Core05_2_sendEmail_design.png" alt="image">
 * 
 * <h3>5.3. Classes</h3>
 * 
 *  <img src="doc-files/Core05_2_cd.png" alt="image">
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * -High cohesion and low coupling.<p>
 * -Polimorphism.<p>
 * -Information Expert.<p>
 * -Creator.<p>
 * -Controller.<p>
 * <p>
 * <h2>6. Implementation</h2>
 * 
 * <a href="../../../../csheets/ext/email/package-summary.html">csheets.ext.email</a><p>
 * <a href="../../../../csheets/ext/outbox/package-summary.html">csheets.ext.outbox</a><p>
 * <a href="../../../../csheets/ext/outbox/ui/package-summary.html">csheets.ext.outbox.ui</a>
 * <p>
 
 * <p>
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 
 * -Analysis, design, implementation and tests. 
 * 
 * <h2>8. Final Remarks</h2> 
 * 
 * <h2>9. Work Log</h2> 
 * 
 ** <b>Tuesday</b>
 * <p>
 * Analysis of the issue
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Design and Implementation of the source code , started unit tests.
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-<p>
 *<b> Thursday</b>
 *<p>
 * Tests and javadoc
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <p>
 * <h2>10. Self Assessment</h2> 
 * 
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * <p>
 * <b>Evidences:</b>
 * <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/62733ba87586d781f6e923db8a1ac8e0c8b19467 <p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/5fc7aa20bc12d4319ac20eb9a5122ba5e1f677f8<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/fd79652ecd7bd5c3a6da6954d933c38b862bbb7a<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/d1767b16da2f9a1a144c23a70b0aea53eeb4926a<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/de727b710d460f70747bf47a1ba279da9a32f89b<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/e9bc0e109ac7b64e2ff2cdde033a7b8ffd3392f4<p>
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/5f7d9a65242610dff7f17f6f1de3225e5bdc5418<p>
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
 * @author Hugo Ceridera
 */

package csheets.worklog.n1140465.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author alexandrebraganca
 */
class _Dummy_ {}

