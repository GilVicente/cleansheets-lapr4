/**
 * Technical documentation regarding the work of the team member (1140439) Francisco Pinelas during week3.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the third week of work. Sprint3 week.
 * <p>
 * This weak I started with the analysis and understanding of the functional
 * increment, Ipc 01.3 - Multiple sharing.
 *
 * <h2>2. Use Case/Feature: Ipc 01.3</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-53
 * <p>
 * Ipc 01.3 - Multiple Sharing
 *
 * <h2>3. Requirements</h2>
 * It should be possible to have multiple cell shares active at the same time. Each of the shares should have
 * a unique name. The location (i.e., range address) of the share in each instance of Cleansheets may be
 * different. It should be possible to share ranges that include cells with formulas.
 * <p>
 * <b>Use Case "Multiple Sharing":</b>
 * <h2>4. Analysis</h2>

 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. I call this diagram an "analysis" use case
 * realization because it functions like a draft that I can do during analysis
 * or early design in order to get a previous approach to the design.
 * <p>
 * <img src="doc-files/ipc_01.3_SSD.png">
 * <p>
 * 
 *
 * <h2>5. Design</h2>
 * <h2>Sequence Diagram:<h2>
 * <p>
 * <img src="doc-files/ipc_01.3_SD.png">
 * <p>
 * 
 *
 * <h3>5.1. Functional Tests</h3>
 * 
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 * ActiveInstancesPanel - Had to make some updates on this class
 * ConnectionList - Has a hashmap with both ip and  a name associated for every connection
 * Shared - Has the information about a connection (ip address, name that was given by the user,  matrix of the cells shared with the ip address)
 * SharedList - Has a list of Shared class so I can run through every Shared to get its matrix and check if a edited cell is within the matrix range
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 *
 *
 * <h2>6. Implementation</h2>
 *  
 *  
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * 
 *
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * <b>Monday</b>
 * <p>
 * Reviewed the other two IPC use cases(01.1 and 01.2) already implemented so
 * I could understand how to implement this sprints IPC use case (01.3) 
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Worked on the analysis of the IPC 01.3 use case.
 * <p>
 * <b>Wednesday</b>
 * <p>
 * Worked on the implementation of the IPC 01.3 use case.
 * <p>
 * <b>Thursday</b>
 * <p>
 * Worked on the implementation of the IPC 01.3 use case and adjusted some diagrams.
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:</h3>
 *
 * <p>
 * <b>Evidences:</b>
 * <p>
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140439.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
