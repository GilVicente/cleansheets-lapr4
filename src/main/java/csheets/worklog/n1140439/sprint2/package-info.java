/**
 * Technical documentation regarding the work of the team member (1140439) Francisco Pinelas during week2.
 *
 * <p>
 * <b>-Note: this is a template/example of the individual documentation that
 * each team member must produce each week/sprint. Suggestions on how to build
 * this documentation will appear between '-' like this one. You should remove
 * these suggestions in your own technical documentation-</b>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the second week of work. Sprint2 week.
 * <p>
 * This weak I started with the analysis and understanding of the functional
 * increment, lang01.2 - Monetary Language.
 *
 * <h2>2. Use Case/Feature: Lang01.2</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-28
 * <p>
 * Lang01.2-Instructions Block
 *
 * <h2>3. Requirements</h2>
 * This user story adds a new language - monetary language. The character that begins formula should
 * be the '#' and the block must be delimited by braces - "{", "}".The
 * formula should only accept additions, subtractions, multiplications and
 * divisions. The operands are monetary values ​​only (eg 10.58€, 2,31£ or 1.20$ ).
 * <p>
 * <b>Use Case "Monetary Language":</b>
 * <h2>4. Analysis</h2>

 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. I call this diagram an "analysis" use case
 * realization because it functions like a draft that I can do during analysis
 * or early design in order to get a previous approach to the design.
 * <p>
 * <img src="doc-files/lang_01.2_SSD.png">
 * <p>
 * 
 *
 * <h2>5. Design</h2>
 * <h2>Sequence Diagram:<h2>
 * <p>
 * <img src="doc-files/lang_01.2_SD.png">
 * <p>
 * 
 *
 * <h3>5.1. Functional Tests</h3>
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * This issue could be implemented in a better way over the time of the second
 * sprint and it will be worked on in this last days, before starting the new
 * sprint.
 *
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * Example
 * <b>Monday</b>
 * <p>
 * Studied the implementation of this use case.
 * <p>
 * <b>Tuesday</b>
 * <p>
 * I analysed the issue that I was going to work on this week and made some diagrams about this particular use case.
 * <p>
 * I had some difficulties on understanding the implementation of this
 * application and how I should start implementing this issue as I didn't know 
 * quite well where to start or what I should analyze in the first place.
 *
 * <p>
 * <b>Wednesday</b>
 * <p>
 * I worked on implementation of this use case.
 * <p>
 * <b>Thursday</b>
 * <p>
 * I have made a class diagram about his particular use case and some implementation adjustments.
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:</h3>
 *
 * <p>
 * <b>Evidences:</b>
 * <p>
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140439.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
