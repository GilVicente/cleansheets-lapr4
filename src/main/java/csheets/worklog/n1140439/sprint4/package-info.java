/**
 * Technical documentation regarding the work of the team member (1140439) Francisco Pinelas during week4.
 *
 * <p>
 * <p>
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 * <h2>1. Notes</h2>
 *
 * This is the fourth week of work. Sprint 4 week.
 * <p>
 * This weak I started with the analysis and understanding of the functional
 * increment, CRM 04.3 - Search and export Notes.
 *
 * <h2>2. Use Case/Feature: Ipc 01.3</h2>
 *
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-86
 * <p>
 * CRM04.3 - Search and export Notes
 *
 * <h2>3. Requirements</h2>
 * It should be possible to search for notes (text and lists) within an time interval. The query expression
 * should also allow to search based on the contents of the notes. It should be possible to user regular
 * expressions to search the contents of the notes that are within the time interval. The result should list all
 * the notes that were found. It should be possible to open a specific note by double clicking on it in the
 * result list. It should be possible to export the search results to a range in an worksheet.
 * <p>
 * <b>Use Case "Search and export Notes":</b>
 * <h2>4. Analysis</h2>
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. I call this diagram an "analysis" use case
 * realization because it functions like a draft that I can do during analysis
 * or early design in order to get a previous approach to the design.
 * <p>
 * <img src="doc-files/crm_04.3_SSD.png">
 * <p>
 * 
 *
 * <h2>5. Design</h2>
 * <h2>Sequence Diagram:<h2>
 * <p>
 * <img src="doc-files/crm_04.3_SD.png">
 * <p>
 * 
 *
 * <h3>5.1. Functional Tests</h3>
 * The user selects the ListNotes extesion, clicks on Search List Notes or 
 * Search Text Notes, introduces either a regular expression or a date interval,
 * the system shows a list of the list of notes or the text notes on the system
 * according to the introduced conditions, the user selects a note, or not, and 
 * opens and/or exports the content of that note to a selected cell.
 * 
 * <h3>5.2. UC Realization</h3>
 * To realize this UC I had to apply an existing extion, I had to create a 
 * new controller with the searching methos, a new action and a new dialog.
 *
 * <h3>5.3. Classes</h3>
 * 
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * - High cohesion and low coupling.
 * - Polimorphism.
 * - Information Expert.
 * - Creator.
 * - Controller.
 * 
 * <h2>6. Implementation</h2>
 * <a href= "../../../../csheets/ext/contacts.listnote.ui/package-summery.html">csheets.ext.listnote.ui</a><p>
 * 
 *  
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 * 
 *
 *
 * <h2>9. Work Log</h2>
 *
 * <p>
 * <b>Monday</b>
 * Analysed the previous uses cases of point 4-CRM.
 * <p>

 * <p>
 * <b>Tuesday</b>
 * Worked on the use case diagrams.
 * <p>

 * <p>
 * <b>Wednesday</b>
 * Implemented the code.
 * <p>

 * <p>
 * <b>Thursday</b>
 * Fixed some bugs and validations on the implemented code.
 * <p>

 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:</h3>
 *
 * <p>
 * <b>Evidences:</b>
 * URL cmmits
 * https://bitbucket.org/lei-isep/lapr4-2016-2db/commits/8fd9011964c07dc52fa5f9325572339744ef7a24
 * 
 * <p>
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1140439.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
