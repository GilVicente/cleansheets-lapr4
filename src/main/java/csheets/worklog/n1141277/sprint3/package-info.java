/**
 * Technical documentation regarding the work of the team member (1141277) Miguel Freitas during week3.
 *
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- yes</b>
 *
 * <h2>1. Notes</h2>
 *
 * This week I will be working on the List Notes edition feature.
 *
 * <h2>2. Use Case/Feature: CRM04.2</h2>
 *
 * Issue in Jira:http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-85
 * <p>
 * CRM04.2) List Edition
 * </p>
 * <h2>3. Requirements</h2>
 * It should be possible to create, edit and remove list notes. A list note is similar to a textual note but each line is displayed as a
 * check box (that can be checked or unchecked). The first line is also interpreted as the title of the list note. It should be possible to generate a 
 * new version of a text note or list based on a old version of it. When this happens
 * Cleansheets should 'open' the new version for edit with the same contents of the old version. This is the only 'trace' that may eventually link to the old version.
 *
 * <b>Use Case "Lists Edition":</b>
 * <h2>4. Analysis</h2>
 * 
 *
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design.
 * <img src="doc-files/cmr04_2-CreateNoteList.png" alt="image">
 *
 *
 *
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Run cleansheets and observe the notes extension behaviour.
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Class Diagram is a preview of the structure that creates a listNote
 * <p>
 * <img src="doc-files/CRM04_02_dc.png" alt="image">
 * <p>
 * 
 * The following sequence diagram is how i tried to implement this feature.
 * <p>
 * <img src="doc-files/cmr04_2.png" alt="image">
 * <p>
 *
 * 
 * 
 *
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling, protected variations, polimorphism.
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 * -In this section document your contribution and efforts to the integration of
 * your work with the work of the other elements of the team and also your work
 * regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 *
 * <h2>8. Final Remarks</h2>
 *
 *I still have some minor changes to make to this feature, and will work on them this weekend, as the
 * time period was too short to complete the full feature.
 *
 * <h2>9. Work Log</h2>
 *
 * 

 * <b>Monday</b>
 *
 * Made analysis.

 * <b>Tuesday</b>
 *
 * Made Test design OO , and Started on a basic implementation
 *
 *
 *
 * <p>
 * <b>Wednesday</b>
 * </p>
 * Updating DesignOO and continuing the implementation
 * <p>
 * <b>Thursday</b>
 * </p>
 * 
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 90%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <&p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 * Corrected some things that were broken in the project, and made some minor visual fixes.
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1141277.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
