/**
 * Technical documentation regarding the work of the team member (1141277) Miguel Freitas during week4.
 *
 * <b>Scrum Master: -(yes/no)- no</b>
 *
 * <p>
 * <b>Area Leader: -(yes/no)- no</b>
 * </p>
 * <h2>1. Notes</h2>
 *
 * This is the last week of LAPR4. This week I will work on the  Multiple Realtime Workbook Search
 * feature
 *
 * <h2>2. Use Case/Feature: IPC02.3</h2>
 *
 * Issue in Jira:http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-56
 * <p>
 * IPC02.3)Multiple Realtime Workbook Search
 * </p>
 * <h2>3. Requirements</h2>
 * Enable Cleansheets to apply conditional formatting to a range of cells.
 *
 * <b>Use Case "Multiple Realtime Workbook Search":</b>
 * <h2>4. Analysis</h2>
 * It should now be possible to have several search windows. Each search window should 
 * have an option, for instance a check button, to indicate if the search is an active 
 * search. An active search will be a search that keeps updating the contents of the list 
 * that displays the results. The preview for each file should now be cached so that Cleansheets
 * only produces the preview the first time the user request it or when the contents of the file change.
 *
 *
 * <h3>First "analysis" Sequence System Diagram</h3>
 * The following diagram depicts a proposal for the realization of the
 * previously described use case. We call this diagram an "analysis" use case
 * realization because it functions like a draft that we can do during analysis
 * or early design in order to get a previous approach to the design.
 * <img src="doc-files/ipc_02_3_ssd.png" alt="image"/>
 *
 *
 *
 *
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * 1- Run cleansheets. 
 * 2- Select advanced workbook search
 * 3- Search for workbooks
 * 4- Modify contents of chosen folder
 * 5- Watch the result list reload and change results
 *
 * <h3>5.2. UC Realization</h3>
 *
 *
 * <h3>5.3. Classes</h3>
 *
 * The following Sequence Diagram is a preview of the structure of the development
 * of the issue Conditional Formatting of Ranges
 * <p>
 * <img src="doc-files/ipc02_3_sd.png" alt="image"/>
 * </p>
 *
 *
 * The following Class Diagram is a preview of the structure and conection
 * between our classes of our issue (Conditional Formatting of Ranges).
 * <p>
 * <img src="doc-files/ipc02_3_cd.png" alt="image"/>
 * </p>
 * <h3>5.4. Design Patterns and Best Practices</h3>
 *
 * High cohesion, low coupling
 *
 *
 * <h2>6. Implementation</h2>
 *
 *
 * <h2>7. Integration/Demonstration</h2>
 *
 *
 *
 * <h2>8. Final Remarks</h2>
 *
 * I think this feature was well implemented, as everything seems to work flawlessly.
 * There were some limitations code-wise but I fixed some problems that were coded before.
 * I still don't think that the management of threads is efficient on my part, but I didn't have
 * much time to make it better
 *
 *
 * <h2>9. Work Log</h2>
 *
 *
 *
 * <b>Monday</b>
 *
 * Made analysis OO.
 *
 * <b>Tuesday</b>
 *
 * Made Test design OO , and Started implementation
 *
 *
 *
 * <p>
 * <b>Wednesday</b>
 * </p>
 * Updating DesignOO and finished the implementation and made some tests that are not complete
 * <p>
 * <b>Thursday</b>
 * </p>
 *
 *
 * <h2>10. Self Assessment</h2>
 *
 * -Insert here your self-assessment of the work during this sprint.-
 *
 * <h3>10.1. Design and Implementation:3</h3>
 *
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex:
 * mais de 90%) e apresentam código que para além de não ir contra a arquitetura
 * do cleansheets segue ainda as boas práticas da área técnica (ex:
 * sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <&p>
 * </p>
 * - url of commit: ... - description: this commit is related to the
 * implementation of the design pattern ...-
 *
 * <h3>10.2. Teamwork: ...</h3>
 *
 * <h3>10.3. Technical Documentation: ...</h3>
 *
 *
 */
package csheets.worklog.n1141277.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this
 * EMPTY package! Do not remove this class!
 *
 */
class _Dummy_ {
}
