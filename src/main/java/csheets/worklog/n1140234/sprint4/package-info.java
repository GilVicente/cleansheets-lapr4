
/**
 * Technical documentation regarding the work of the team member 1140234 Luis Teixeira during week3. 
 * 
 * <b>Scrum Master: -no</b>
 * 
 * <p>
 * <b>Area Leader: -no</b>
 * 
 * <h2>1. Notes</h2>
 * I lapr2 at the same time, so I can not finish my uc
 * 
 * 
 * <h2>2. Use Case/Feature: ipc08.1</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-72
 * <p>
 * Analysis.
 * Create a new Extension that allows sharing files from a directory.
 * 
 * <h2>3. Requirement</h2>
 * 
 * A new extension that supports the sharing of files between instances of Cleansheets.
 * Cleansheets should have a new option to share the files contained in a specific directory. The user should
 *be able to specify the directory to share (output). These files should now be listed on other instances
 *of Cleansheets (in a specific window, for instance, in a sidebar). The list should include the name of
 *the files and its size. It is also required to configure the local directory that will receive the downloaded
 *files (input). The configuration of file sharing should be persistent. For the moment it is not required to
 *implement the download of files, however it is necessary to keep the list of files updated automatically.
 *It is also necessary to update the list of files that where selected for download in the input list. This list
 *should include the name of the file, its size, its source and its status (download in progress, up to date,
 *etc.).
 * 
 * <p>
 * <b>Use Case File Sharing:</b>
 * The user selects the extension file sharing. The system request the path of
 * the file. The user selects the path and share. The system establishes a
 * connection and request information necessary. The user input the information (ip and port) and select
 * connect. The system get the files and shows.
 * 
 * 
 * <h2>4. Analysis</h2>
 * It's needed to create an extension that allows file sharing, to do so it's needed to have two
 * classes, one to define what it's to share and to
 * define with whom it's to share.
 * 
 * 
 * <p>
 * One important aspect is how we choose the directory that we want to share. Everytime we share the system does:
 * <pre>
 *JFileChooser fileChooser = new JFileChooser();

 *      fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
 *      int returnValue = fileChooser.showOpenDialog(null);
 *
 *      if (returnValue == JFileChooser.APPROVE_OPTION) {
 *          File file = fileChooser.getSelectedFile();
 *          try {
 *              if (jListInstance.getSelectedValue() != null) {
 *                  lst.fillListFiles(file.getAbsolutePath());
 *                  Thread tcp_sender_thread = lst.sendListFiles((InetAddress) jListInstance.getSelectedValue());
 *              } else {
 *                  JOptionPane.showMessageDialog(null, "Select an Instance!");
 *              }
 *          } catch (IOException ex) {
 *              Logger.getLogger(FilesShareUI.class.getName()).log(Level.SEVERE, null, ex);
 *          }
 *      }
 * </pre>
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Yes
 *
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story its needed: send files to another instance and receive files from other instances
 * The following diagrams illustrate core aspects of the design of the solution for this
 * use case.
 * <p>
 * 
 * The following diagram shows interaction of the user with the interface and the interaction between classes of this use case.
 * <p>
 * vdoc-files/file_share.png
 * 
 * 
 * <h3>5.3. Classes</h3>
 * 
 * 
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * Controller pattern:
 * The controller call the classes(I.E.) responsible to execute their functions.
 * 
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * It was added new classes tcp and udp to fullfill my needs, new classes to save the 
 * objets that we need, such FileShare and ListFilesShare, new Ui for receive and share files.
 * 
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 *I had a hard time to understand what my colllegues had done before, so I start doing the classes I needed to me.
 * 
 * <h2>8. Final Remarks</h2>
 * I think by the time the IPC area should had be more organized.
 * 
 * <p>
 * I did everything that was requested and fixed some issues to fullfill my needs.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Team reunion
 * <p>
 * Today
 * <p>
 * 1. Analysis and design development
 * <p>
 * Blocking:
 * <p>
 *  * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Some implementation and analyses and design
 * <p>
 * Today
 * <p>
 * 1. Implementantion
 * <p>
 * Blocking:
 * <p>
 * 1. Bad organization at ipc area
 * 
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * Good Work, maybe not perfect, but this week I had some blockings int the implemantation, and I have Lapr2 to do.
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex: mais de 50%) e apresentam código que para além de não ir contra a arquitetura do cleansheets segue ainda as boas práticas da área técnica (ex: sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
* @author 1140234
 */

package csheets.worklog.n1140234.sprint4;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1140234
 */
class _Dummy_ {}