
/**
 * Technical documentation regarding the work of the team member 1140234 Luis Teixeira during week3. 
 * 
 * <b>Scrum Master: -no</b>
 * 
 * <p>
 * <b>Area Leader: -yes</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * 
 * <h2>2. Use Case/Feature: Lang05.1</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-77
 * <p>
 * Analysis.
 * Do the analysis of the crm01.3
 * 
 * <h2>3. Requirement</h2>
 *It should be possible to associated tags to contacts (individual or company contacts) and should have two windows: a window to search contacts based on tags and
 *a window with a list of tags that is automatically sorted (descending) based on the frequency of the tag
 *utilization.
 * <p>
 * <b>Use Case "Contacts with tags/create tag":</b> The user create a new tag. The system request the tag's information. The user input the information. The system save and inform the user.
 * <b>Use Case "Contacts with tags/update tag":</b> The user create a new tag. The system request the tag's information. The user input the information. The system save and inform the user.
 * <b>Use Case "Contacts with tags/Most Used Tags":</b> The user select search tags. The system show the most used tags.
 *  * <b>Use Case "Contacts with tags/Most Used Tags":</b> The user select search tags. The system request the tags name. The user input the tag's name. The system returns the contacts associated to the tag 
 * 
 * <h2>4. Analysis</h2>
 * 
 * It's needed create a new class Tag, and associate a contact to the tag to be possible find contacts by searching a tag. The window must provide an option to select and 
 * edit a contact from the list.
 * It's needed to elaborate too a window to display the most used tags.
 * 
 * <p>
 * One important aspect is how we see if the tag already exists. Everytime that the add a new tag to a contact the system does:
 * <pre>
 * if (!s.isEmpty()) {
 *
 *                      Tag t = contactController.verifyTag(s);
 *
 *                      if (t != null) {
 *
 *                          contactController.associateTag(t, contact);
 *
 *                          contactController.updateTag(t);
 *                          JOptionPane.showMessageDialog(null, "Tag " + s + " added too contact " + contact.Firstname() + ".", "Tag existent sucess!", JOptionPane.PLAIN_MESSAGE);
 *
 *                      } else {
 *                          boolean b;
 *                          b = contactController.newTag(s, contact);
 *                          if (b == false) {
 *                              JOptionPane.showMessageDialog(null, "The tag name is not valid.", "Tag exception", JOptionPane.ERROR_MESSAGE);
 *                          } else {
 *                              JOptionPane.showMessageDialog(null, "Tag " + s + " added too contact " + contact.Firstname() + ".", "Tag create sucess!", JOptionPane.PLAIN_MESSAGE);
 *                          }
 *                      }
 * </pre>
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * Yes
 *
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story its needed: (1/2)create/update new tags and associate to contacts, (3)show the most used tags and (4)edit contacts from searching tags.
 * The following diagrams illustrate core aspects of the design of the solution for this
 * use case.
 * <p>
 * 
 * The following diagram shows interaction of the user with the interface and the interaction between classes of this use case.
 * <p>
 * //images not working, see pumls
 * 
 * 
 * <h3>5.3. Classes</h3>
 * 
 * 
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * Controller pattern:
 * The controller call the classes(I.E.) responsible to execute their functions.
 * 
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * It was added new class tag, new Ui for search and new repository to save the tags
 * 
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * <p>
 * I did everything that was requested and helped some team mates to understand what I had done in lang05.1.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Team reunion
 * <p>
 * Today
 * <p>
 * 1. Analysis and design development
 * <p>
 * Blocking:
 * <p>
 *  * <b>Wednesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. Some implementation and analyses and design
 * <p>
 * Today
 * <p>
 * 1. Implementantion
 * <p>
 * Blocking:
 * <p>
 * 
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * Great work
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex: mais de 50%) e apresentam código que para além de não ir contra a arquitetura do cleansheets segue ainda as boas práticas da área técnica (ex: sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
* @author 1140234
 */

package csheets.worklog.n1140234.sprint3;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1140234
 */
class _Dummy_ {}