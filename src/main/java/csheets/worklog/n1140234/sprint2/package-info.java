
/**
 * Technical documentation regarding the work of the team member 1140234 Luis Teixeira during week1. 
 * 
 * <b>Scrum Master: -no</b>
 * 
 * <p>
 * <b>Area Leader: -no</b>
 * 
 * <h2>1. Notes</h2>
 * 
 * 
 * <h2>2. Use Case/Feature: Lang05.1</h2>
 * 
 * Issue in Jira: http://jira.dei.isep.ipp.pt:8080/browse/LPFOURDB-103
 * <p>
 * Analysis.
 * Do the analysis of the lang05.1
 * 
 * <h2>3. Requirement</h2>
 *The new extension should add a menu option to open a window to edit and execute a single macro. 
 * <p>
 * <b>Use Case "Macro Window":</b> The user initiate select MacroWindow. The system shows information. The user edit the macro. The user runs the macro. The system shows the result.
 * 
 *  
 * <h2>4. Analysis</h2>
 * 
 * Create a new extension Macros that open window that allows to run a single macro. The idea behind macros is that they are like mini-programs 
 * written in a language oriented to the end user. To do so, it’s needed to edit the macro's information in order to execute the macro with the desired effect.
 * 
 * <p>
 * One important aspect is how the macro is executed. Everytime that the user execute a macro the following method its implemented:
 * <pre>
 *public String executeMacro() throws FormulaCompilationException, IllegalValueTypeException {
 *      
 *      Formula frml;
 *     
 *     
 *      Value res = new Value("");
 *      for (String line : this.functions) {
 *          if (line != null) {
 *              if (!line.isEmpty()) {
 *                  frml = theCompiler.compile(this.cell, line);
 *                  try {
 *                      if (frml == null) {
 *                          throw new FormulaCompilationException("Formula not recongnizable: " + line);
 *                      }
 *                      res = frml.evaluate();
 *                  } catch (IllegalValueTypeException ex) {
 *                      throw new FormulaCompilationException(ex.toString());
 *                  }
 *              }
 *          }
 *      }
 *      return res.toString();
 *  }
 * </pre>
 * 
 * <h2>5. Design</h2>
 *
 * <h3>5.1. Functional Tests</h3>
 * N/A
 *
 *
 * <h3>5.2. UC Realization</h3>
 * To realize this user story its needed create a run a macro and show the outcome.
 * The following diagrams illustrate core aspects of the design of the solution for this
 * use case.
 * <p>
 * 
 * The following diagram shows interaction of the user with the interface and the interaction between classes of this use case.
 * <p>
 * <img src="lang05_1_ssd" alt="image">
 * 
 * 
 * <h3>5.3. Classes</h3>
 * 
 * <img src="doc-files/lang05_1DC.jpg" alt="image">
 * 
 * <h3>5.4. Design Patterns and Best Practices</h3>
 * 
 * Controller pattern:
 * The controller call the classes(I.E.) responsible to execute their functions.
 * 
 * 
 * 
 * <h2>6. Implementation</h2>
 * 
 * It was added new compilers for macro, new language for macro, all based in existing classes. New window to run and edit the macro and an extra window,
 * to help the user.
 * 
 * <h2>7. Integration/Demonstration</h2>
 * 
 * -In this section document your contribution and efforts to the integration of your work with the work of the other elements of the team and also your work regarding the demonstration (i.e., tests, updating of scripts, etc.)-
 * 
 * <h2>8. Final Remarks</h2>
 * 
 * <p>
 * I did not limit myself just to do what I had to do, I did done some extra work in order to facilitate the work yet to come.
 * 
 * 
 * <h2>9. Work Log</h2> 
 * 
 * -Insert here a log of you daily work. This is in essence the log of your daily standup meetings.-
 * <p>
 * Example
 * <p>
 * <b>Monday</b>
 * <p>
 * Yesterday I worked on:
 * <p>
 * 1. -nothing-
 * <p>
 * Today
 * <p>
 * 1. Analysis of the...
 * <p>
 * Blocking:
 * <p>
 * 1. -nothing-
 * <p>
 * <b>Tuesday</b>
 * <p>
 * Yesterday I worked on: 
 * <p>
 * 1. ...
 * <p>
 * Today
 * <p>
 * 1. ...
 * <p>
 * Blocking:
 * <p>
 * 1. ...
 * 
 * <h2>10. Self Assessment</h2> 
 * 
 * Great work
 * 
 * <h3>10.1. Design and Implementation:3</h3>
 * 
 * 3- bom: os testes cobrem uma parte significativa das funcionalidades (ex: mais de 50%) e apresentam código que para além de não ir contra a arquitetura do cleansheets segue ainda as boas práticas da área técnica (ex: sincronização, padrões de eapli, etc.)
 * <p>
 * <b>Evidences:</b>
 * <p>
 * - url of commit: ... - description: this commit is related to the implementation of the design pattern ...-
 * 
 * <h3>10.2. Teamwork: ...</h3>
 * 
 * <h3>10.3. Technical Documentation: ...</h3>
 * 
* @author 1140234
 */

package csheets.worklog.n1140234.sprint2;

/**
 * This class is only here so that javadoc includes the documentation about this EMPTY package! Do not remove this class!
 * 
 * @author 1140234
 */
class _Dummy_ {}